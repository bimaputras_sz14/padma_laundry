<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "otorisasi";
$route['404_override'] = '';
$route['master_data/produk'] = "produk";
$route['master_data/produk/tambah'] = "produk/tambah";
$route['master_data/produk/ubah'] = "produk/ubah";
$route['master_data/produk/ubah/(:num)'] = "produk/ubah/$1";
$route['master_data/produk/hapus'] = "produk/hapus";
$route['master_data/produk/hapus/(:num)'] = "produk/hapus/$1";
$route['master_data/layanan'] = "layanan";
$route['master_data/layanan/tambah'] = "layanan/tambah";
$route['master_data/layanan/ubah'] = "layanan/ubah";
$route['master_data/layanan/ubah/(:num)'] = "layanan/ubah/$1";
$route['master_data/layanan/hapus'] = "layanan/hapus";
$route['master_data/layanan/hapus/$1'] = "layanan/hapus/$1";
$route['master_data/pegawai'] = "pegawai";
$route['master_data/pegawai/tambah'] = "pegawai/tambah";
$route['master_data/pegawai/ubah'] = "pegawai/ubah";
$route['master_data/pegawai/ubah/(:num)'] = "pegawai/ubah/$1";
$route['master_data/pegawai/hapus'] = "pegawai/hapus";
$route['master_data/pegawai/hapus/(:num)'] = "pegawai/hapus/$1";
$route['master_data/membership'] = "membership";
$route['master_data/membership/tambah'] = "membership/tambah";
$route['master_data/membership/ubah'] = "membership/ubah";
$route['master_data/membership/ubah/(:num)'] = "membership/ubah/$1";
$route['master_data/membership/hapus'] = "membership/hapus";
$route['master_data/membership/hapus/(:num)'] = "membership/hapus/$1";
$route['master_data/pelanggan'] = "pelanggan";
$route['master_data/pelanggan/tambah'] = "pelanggan/tambah";
$route['master_data/pelanggan/ubah'] = "pelanggan/ubah";
$route['master_data/pelanggan/ubah/(:num)'] = "pelanggan/ubah/$1";
$route['master_data/pelanggan/hapus'] = "pelanggan/hapus";
$route['master_data/pelanggan/hapus/(:num)'] = "pelanggan/hapus/$1";
$route['data/order'] = "order_admin";
$route['data/order/lihat'] = "order_admin/lihat";
$route['data/order/lihat/(:num)'] = "order_admin/lihat/$1";
$route['data/order/ubah'] = "order_admin/ubah";
$route['data/order/ubah/(:num)'] = "order_admin/ubah/$1";
$route['data/order/hapus'] = "order_admin/hapus";
$route['data/order/hapus/(:num)'] = "order_admin/hapus/$1";
$route['data/order/updatestats'] = "order_admin/updatestats";
$route['data/order/updatestats/(:num)'] = "order_admin/updatestats/$1";
$route['master_data/universitas'] = "universitas";
$route['master_data/universitas/tambah'] = "universitas/tambah";
$route['master_data/universitas/ubah'] = "universitas/ubah";
$route['master_data/universitas/ubah/(:num)'] = "universitas/ubah/$1";
$route['master_data/universitas/hapus'] = "universitas/hapus";
$route['master_data/universitas/hapus/(:num)'] = "universitas/hapus/$1";
$route['master_data/gedung'] = "gedung";
$route['master_data/gedung/tambah'] = "gedung/tambah";
$route['master_data/gedung/ubah'] = "gedung/ubah";
$route['master_data/gedung/ubah/(:num)'] = "gedung/ubah/$1";
$route['master_data/gedung/hapus'] = "gedung/hapus";
$route['master_data/gedung/hapus/(:num)'] = "gedung/hapus/$1";
$route['master_data/teknik'] = "teknik";
$route['master_data/teknik/tambah'] = "teknik/tambah";
$route['master_data/teknik/ubah'] = "teknik/ubah";
$route['master_data/teknik/ubah/(:num)'] = "teknik/ubah/$1";
$route['master_data/teknik/hapus'] = "teknik/hapus";
$route['master_data/teknik/hapus/(:num)'] = "teknik/hapus/$1";
/* End of file routes.php */
/* Location: ./application/config/routes.php */