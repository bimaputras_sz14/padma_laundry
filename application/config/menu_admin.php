<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Menu
| -------------------------------------------------------------------------
| This file lets you define navigation menu items on sidebar.
|
*/

$config['menu_utama'] = array( 
	'dashboard' => array(
		'name'      => 'Dashboard',
		'url'       => site_url('home/dashboard'),		
		'icon'      => 'fa fa-line-chart'
	) 
);

$config['menu_superadmin'] = array( 
	'dashboard' => array(
		'name'      => 'Dashboard',
		'url'       => site_url('home/dashboard'),		
		'icon'      => 'fa fa-line-chart'
	) ,
	'buletin_board' => array(
		'name'      => 'News',
		'url'       => '#',
		'icon'      => 'fa fa-tasks'
	),  
	'data' => array(
		'name'      => 'Order',
		'url'       => site_url('data'),
		'icon'      => 'fa fa-calendar',
		'children'  => array(
			'order' => array(
				'child_name' => 'Data Order',
				'icon'      => 'fa fa-calendar',
				'child_url' => site_url('data/order')
			)
		)
	), 
	'master_data' => array(
		'name'      => 'Master',
		'url'       => site_url('master'),
		'icon'      => 'fa fa-cog',
		'children'  => array(
			'produk' => array(
				'child_name' => 'Produk',
				'child_url' => site_url('master_data/produk')
			), 
			'pegawai' => array(
				'child_name' => 'Pegawai',
				'child_url' => site_url('master_data/pegawai')
			), 
			'pelanggan' => array(
				'child_name' => 'Pelanggan',
				'child_url' => site_url('master_data/pelanggan')
			),
			'layanan' => array(
				'child_name' => 'Layanan',
				'child_url' => site_url('master_data/layanan')
			),
			'membership' => array(
				'child_name' => 'Membership',
				'child_url' => site_url('master_data/membership')
			),
			'universitas' => array(
				'child_name' => 'Universitas',
				'child_url' => site_url('master_data/universitas')
			),
			'gedung' => array(
				'child_name' => 'Gedung',
				'child_url' => site_url('master_data/gedung')
			),
			'teknik' => array(
				'child_name' => 'Teknik Cucian',
				'child_url' => site_url('master_data/teknik')
			),
		)
	)
);

$config['menu_kasir'] = array( 
	'dashboard' => array(
		'name'      => 'Dashboard',
		'url'       => site_url('home/dashboard'),		
		'icon'      => 'fa fa-line-chart'
	), 
	'member' => array(
		'name'      => 'Member',
		'url'       => site_url('member'),
		'icon'      => 'fa fa-users',
		'children'  => array(
			'member' => array(
				'child_name' => 'Data Member',
				'icon'      => 'fa fa-book',
				'child_url' => site_url('member')
			),  
			'member_prospek' => array(
				'child_name' => 'Data Member Prospek',
				'icon'      => 'fa fa-book',
				'child_url' => site_url('member_prospek')
			),   
		)
	),  
);

$config['menu_admin'] = array( 
	'dashboard' => array(
		'name'      => 'Dashboard',
		'url'       => site_url('home/dashboard'),		
		'icon'      => 'fa fa-line-chart'
	), 
	   
);


$config['menu_pemilik'] = array( 
	'dashboard' => array(
		'name'      => 'Dashboard',
		'url'       => site_url('home/dashboard'),		
		'icon'      => 'fa fa-line-chart'
	),  

);

$config['menu_finance'] = array( 
	'dashboard' => array(
		'name'      => 'Dashboard',
		'url'       => site_url('home/dashboard'),		
		'icon'      => 'fa fa-line-chart'
	),  

); 

$config['menu_sales'] = array( 
	'dashboard' => array(
		'name'      => 'Dashboard',
		'url'       => site_url('home/dashboard'),		
		'icon'      => 'fa fa-line-chart'
	),  

);
