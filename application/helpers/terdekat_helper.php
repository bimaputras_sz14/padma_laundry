<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

///////////////////////

function getMyIDByNRP($nrp=null)
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	$CI->db->select('id');
	$CI->db->where('nrp', $nrp);
	$q = $CI->db->get('anggota');
	if($q->num_rows() > 0)
	{
		$r = $q->row();
		return $r->id;
	}
	else
	{
		return false;
	}
}

////////////////////

function getMyKR($nrp = null)
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	$CI->db->select('kr_patroli');
	$CI->db->where('nrp', $nrp);
	$CI->db->where('kr_patroli IS NOT NULL');	
	$CI->db->limit(1);
	$q = $CI->db->get('anggota');
	if($q->num_rows() > 0)
	{
		$r = $q->row();
		return $r->kr_patroli;
	}
	else
	{
		return false;
	}
}

///////////////////////

function getMyData($id = null)
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	$CI->db->where('id', $id);
	$CI->db->limit(1);
	$q = $CI->db->get('anggota');
	if($q->num_rows() > 0)
	{
		return $q->row();
	}
	else
	{
		return false;
	}
}

///////////////////////

function getMyMobil($id = null)
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	$CI->db->where('kr', $id);
	$CI->db->limit(1);
	$q = $CI->db->get('device_list');
	if($q->num_rows() > 0)
	{
		return $q->row();
	}
	else
	{
		return false;
	}
}

//////////////

function getMyTime($id=null,$nrp=null)
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	// TRY BHABIN
	$CI->db->select('waktu,lat,lng');
	$CI->db->where('id_anggota', $id);
	$CI->db->order_by('waktu', 'desc');
	$CI->db->limit(1);
	$q = $CI->db->get('posisi_anggota');
	if($q->num_rows() > 0)
	{
		return $q->row();
	}
	else
	{
		// TRY SABHARA
		$CI->db->select('waktu,lat,lng');
		$CI->db->where('nrp', $nrp);
		$CI->db->order_by('waktu', 'desc');
		$CI->db->limit(1);
		$z = $CI->db->get('posisi_patko');
		if($z->num_rows() > 0)
		{
			return $z->row();
		}
		else
		{
			return false;
		}
	}
}

//////////////

function getMobilTime($kr=null)
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	// TRY BHABIN
	$CI->db->select('waktu,lat,lng');
	$CI->db->where('kr', $kr);
	$CI->db->order_by('waktu', 'desc');
	$CI->db->limit(1);
	$q = $CI->db->get('posisi_lantas');
	if($q->num_rows() > 0)
	{
		return $q->row();
	}
	else
	{
		return false;
	}
}

////////////////////

function getMyKelurahan($id_kel = null)
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	$CI->db->select('nama_desa');
	$CI->db->where('id_desa', $id_kel);
	$CI->db->limit(1);
	$q = $CI->db->get('wil_desa');
	if($q->num_rows() > 0)
	{
		$r = $q->row();
		return $r->nama_desa;
	}
	else
	{
		return false;
	}
}

////////////////////

function getKatIcon($kat_id = null)
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	$CI->db->select('icon');
	$CI->db->where('id', $kat_id);
	$CI->db->limit(1);
	$q = $CI->db->get('kat_potensi');
	if($q->num_rows() > 0)
	{
		$r = $q->row();
		return $r->icon;
	}
	else
	{
		return false;
	}
}

////////////////