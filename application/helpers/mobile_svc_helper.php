<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function getDistance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}

/* ================ */

function aasort (&$array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    asort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii]=$array[$ii];
    }
    $array=$ret;
	return $array;
}

/* ================ */

function humanTiming ($time)
{

    $time = time() - $time; // to get the time since that moment
    $time = ($time<1)? 1 : $time;
    $tokens = array (
        31536000 => 'tahun',
        2592000 => 'bulan',
        604800 => 'minggu',
        86400 => 'hari',
        3600 => 'jam',
        60 => 'menit',
        1 => 'detik'
    );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'':'');
    }

}

/* ================ */

/* =========================================================== GCM == */
/* =========================================================== GCM == */

function getAllRegIDs()
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	$CI->db->select('gcm');
	$CI->db->where('gcm IS NOT NULL');
	$CI->db->where('status', 1);
	$q = $CI->db->get('apk_users');
	if($q->num_rows() > 0)
	{
		$res = array();
		foreach($q->result() as $r)
		{
			$res[] = $r->gcm;
		}
		return $res;
	}
	else
	{
		return false;
	}
}

///////////////

function getRegIDs($nrp=null)
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	$CI->db->select('gcm');
	$CI->db->where('gcm IS NOT NULL');
	$CI->db->where('status', 1);
	$CI->db->where_in('nrp', $nrp);
	$q = $CI->db->get('apk_users');
	if($q->num_rows() > 0)
	{
		$res = array();
		foreach($q->result() as $row)
		{
			$res[] = $row->gcm;
		}
		return $res;
	}
	else
	{
		return false;
	}
}

////

function getRegIDsInWilayah($wilayah=null) // returns gcm_regid
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	$CI->db->select('gcm');
	$CI->db->where('gcm IS NOT NULL');
	$CI->db->where('status', 1);
	$CI->db->where('kode_wilayah', $kel);
	$q = $CI->db->get('apk_users');
	if($q->num_rows() > 0)
	{
		$res = array();
		foreach($q->result() as $r)
		{
			$res[] = $r->gcm;
		}
		return $res;
	}
	else
	{
		return false;
	}
}

/* ========================= */

function getNRPByRegID($myregid=null)
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	$CI->db->select('nrp');
	$CI->db->where('gcm', $myregid);
	$CI->db->where('status', 1);
	$CI->db->limit(1);
	$q = $CI->db->get('apk_users');
	if($q->num_rows() > 0)
	{
		$r = $q->row();
		return $r->nrp;
	}
	else
	{
		return null;
	}
}

/* ========================= */

function sendNotification($regid=null,$msg=null)
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	
}

/* ======================== */

function recordNotification($dt=null)
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
}

/* =========================== */

function sendGCM($message=null,$rcpt=null,$dt=null,$ttl=null,$group=null)
{
	$CI =& get_instance();
	//
	$CI->gcm->setMessage($message);
	foreach($rcpt as $rcv)
	{
		$CI->gcm->addRecepient($rcv);
	}	
	$CI->gcm->setData($dt);
	$CI->gcm->setTtl($ttl);
	$CI->gcm->setGroup($group);
	if($CI->gcm->send())
	{
		return true;
	}
	else
	{
		return false;
	}
}

/* ============================== */

function sendGCMsingle($message=null,$rcpt=null,$dt=null,$ttl=null,$group=null)
{
	$CI =& get_instance();
	//
	$CI->gcm->setMessage($message);
	$CI->gcm->addRecepient($rcpt);
	$CI->gcm->setData($dt);
	$CI->gcm->setTtl($ttl);
	$CI->gcm->setGroup($group);
	if($CI->gcm->send())
	{
		return true;
	}
	else
	{
		return false;
	}
}


/* =================================================================== */
/* =================================================================== */