<?php
	// Delete gambar sekolah
	function delete_image($id){
		$CI =& get_instance();
		$result = $CI->db->query("SELECT * FROM produk WHERE id = '$id'")->row_array();
		unlink('uploads/'.$result['foto']);
	}
	// Delete gambar sekolah
	function delete_pegawai($id){
		$CI =& get_instance();
		$result = $CI->db->query("SELECT * FROM personil WHERE id = '$id'")->row_array();
		unlink('uploads/'.$result['foto']);
	}
	// Helper data order gedung
	function get_data_gedung($status,$id){
		$CI =& get_instance();
		if ($status == 'gedung') {
			$data = $CI->db->query("SELECT name FROM gedung WHERE id = '$id'")->row_array();
			$record = $data['name'];
		}
		elseif ($status == 'member') {
			$data = $CI->db->query("SELECT alamat FROM pelanggan_address WHERE pelanggan_id = '$id'")->row_array();
			if (!isset($data['alamat'])) {
				$record = '-';
			}
			else {
				$record = $data['alamat'];
			}
		}
		return $record;
	}
	function auto_get_options($table,$column,$queryplus){
		$CI =& get_instance();
		$record = $CI->db->query("SELECT $column FROM $table $queryplus");
		return $record->result_array();
	}
	function tanggal($tanggal){
		$bulan = array (1 =>   'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
		$split = explode('-', $tanggal);
		return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	}
	function header_order_view($id){
		$CI =& get_instance();
		$record = $CI->db->query("
			SELECT nama_pelanggan, c.name as layanan, d.keterangan as jam_pickup, gedung_id, b.id as id, e.name as jenis, c.price as harga
			FROM penjualan a 
			INNER JOIN pelanggan b ON a.pelanggan_id = b.id
			INNER JOIN layanan c ON a.jenis_layanan = c.id
			INNER JOIN jam_pickup d ON a.time_pickup = d.id
			INNER JOIN teknik_cucian e on a.jenis_cucian = e.id
			WHERE a.id = '$id'")->row_array();
		return $record;
	}
	function penjualan_item($id){
		$CI =& get_instance();
		$record = $CI->db->query("SELECT a.id as id,nama_produk,qty,total,c.note FROM penjualan_item a INNER JOIN produk b ON a.produk_id = b.id 
			left join penjualan_note c ON a.penjualan_id = c.penjualan_id and c.produk_id = a.produk_id

			WHERE a.penjualan_id = '$id'");
		return $record;
	}

	function penjualan_note($id,$produk_id){
		$CI =& get_instance();
		$record = $CI->db->query("SELECT `produk_id`,`note` FROM penjualan_note a   WHERE a.penjualan_id = '$id' and a.produk_id = '$produk_id' ");
		return $record;
	}


	function penjualan_photo($id ){
		$CI =& get_instance();
		$record = $CI->db->query("SELECT `produk_id`,`path`,nama_produk FROM penjualan_photo a   INNER JOIN produk b ON a.produk_id = b.id  WHERE a.penjualan_id = '$id' order by produk_id ");
		return $record;
	}
	function history_penjualan($id){
		$CI =& get_instance();
		$record = $CI->db->query("
			SELECT b.nama as status,c.nama as nama
			FROM history_penjualan a 
			INNER JOIN status_order b ON a.status = b.id
			INNER JOIN personil c ON a.personil_id = c.id 
			WHERE a.penjualan_id = '$id'");
		return $record;
	}
/* End of file my_helper.php */
/* Location: ./application/helper/my_helper.php */
?>