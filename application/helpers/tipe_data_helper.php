<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

///////////////////////

function check_integer($data=null,$jumlah=null)
{
	$result=array();
	$result['status']	= 'error';
	$result['message']	= 'Jumlah Karakter Tidak Sesuai';
	$result['data']		= $data;
	if(strlen($data)<=$jumlah){
		$result['message']	= 'Karakter Harus Berupa Angka';
		if(is_numeric($data)){
			$result['data']	= $data;
			$result['status']	= 'sukses';
			$result['message']	= '';
		}
	}
	return $result;
}

function replace_special_char($data=null,$jumlah=null){
	// $sourcestring="zREBsZt-%@yvw";
	$result=array();
	$result['status']	= 'error';
	$result['message']	= 'Jumlah Karakter Tidak Sesuai';
	$result['data']		= $data;
	if(strlen($data)<=$jumlah){
		// $data	= str_replace('-','',$data);
		$result['data']	= preg_replace('/[^A-Za-z0-9\-]/', '', $data);
		$result['status']	= 'sukses';
		$result['message']	= '';
	}
	return $result;
}

function check_telepon(){
	$result=array();
	$result['status']	= 'error';
	$result['message']	= 'Jumlah Karakter Tidak Sesuai';
	$result['data']		= $data;
	if(strlen($data)<=$jumlah){
		$result['message']	= 'Karakter Harus Berupa Angka';
		if(is_numeric($data)){
			$result['data']	= str_replace('-','',$data);
			$result['status']	= 'sukses';
			$result['message']	= '';
		}
	}
	return $result;
}

function check_nopol_exist($data=null,$jumlah=null){
	$result=array();
	$result['status']	= 'error';
	$result['data']		= $data;
	$result['message']	= 'Jumlah Karakter Tidak Sesuai';
	if(strlen($data)<=$jumlah){
		$result['message']	= 'Nopol Sudah Terpakai';
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->select('id');
		$CI->db->where('no_polisi', $data);
		$q = $CI->db->get('ranmor_mstbm');
			if($q->num_rows() == 0)
			{
				$result['status']	= 'sukses';
				$result['message']	= '';
			}else{
				$result['status']	= 'tersedia';
				$result['message']	= '';
			}
	}
	// var_dump($result);die();
	return $result;
}

function check_format_date1($date=null,$jumlah=null){
	$result=array();
	$result['status']	= 'error';
	$result['message']	= 'Format Tanggal Tidak Sesuai';
	$result['data']		= $date;
	
		if (strstr($date, '/')) {
			if(validateDate($date, 'd/m/Y')){
				$date = str_replace('/', '-', $date);
				$date = date('Y-m-d', strtotime($date));
				$result['status'] = 'sukses';
				$result['message']	= '';
				$result['data']	= $date;
			}
		}else{
			
			if(validateDate($date, 'Y-m-d')){
				$result['status'] = 'sukses';
				$result['message']	= '';
				$result['data']	= $date;
			}elseif(excelDateToDate($date)!=false){
				$result['status'] = 'sukses';
				$result['message']	= '';
				$result['data']	= excelDateToDate($date);
			}
		}
	
	return $result;
}
function check_format_date2($date=null,$jumlah=null){
	$result=array();
	$result['status']	= 'error';
	$result['message']	= 'Format Tanggal Tidak Sesuai';
	$result['data']		= $date;
	
		if (strstr($date, '/')) {
			if(validateDate($date, 'd/m/Y')){
				$date = str_replace('/', '-', $date);
				$date = date('Y-m-d', strtotime($date));
				$result['status'] = 'sukses';
				$result['message']	= '';
				$result['data']	= $date;
			}
		}else{
			
			if(validateDate($date, 'Y-m-d')){
				$result['status'] = 'sukses';
				$result['message']	= '';
				$result['data']	= $date;
			}elseif(excelDateToDate($date)!=false){
				$result['status'] = 'sukses';
				$result['message']	= '';
				$result['data']	= excelDateToDate($date);
			}
		}
	
	return $result;
}
function validateDate($date, $format = 'Y-m-d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}
function excelDateToDate($readDate){
		// $readDate=42124;
		if(is_numeric($readDate)){
			$phpexcepDate = $readDate-25569; //to offset to Unix epoch
			$date= strtotime("+$phpexcepDate days", mktime(0,0,0,1,1,1970));
			return date('Y-m-d', $date);
		}else{
			return false;
		}
	}

?>