<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* GCM Helpers */

/////////////// GCM

function getGCMSender($nrp=null)
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	$res = '';
	$CI->db->select('ems_personil.nama,ems_profesi.profesi');
	$CI->db->from('ems_personil');
	$CI->db->join('ems_profesi', 'ems_personil.id_profesi=ems_profesi.id_profesi', 'left');
	$CI->db->where('ems_personil.nrp', $nrp);
	$CI->db->limit(1);
	$q = $CI->db->get();
	if($q->num_rows() > 0)
	{
		$r = $q->row();
		$res = ucwords(strtolower($r->nama).', '.strtolower($r->profesi));
	}
	return $res;
}

/* */

function getRegID($nrp=null)
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	$CI->db->select('gcm_reg_id');
	$CI->db->where('gcm_reg_id IS NOT NULL');
	$CI->db->where('nrp', $nrp);
	$CI->db->where('active', 1);
	$q = $CI->db->get('ems_user_apk');
	if($q->num_rows() > 0)
	{
		$r = $q->row();
		return $r->gcm_reg_id;
	}
	else
	{
		return false;
	}
}

//////////////////////////////////////

function getRegIDs($nrps=null)
{
	$CI =& get_instance();
	$CI->db = $CI->load->database('default', TRUE);
	//
	$CI->db->select('gcm');
	$CI->db->where('gcm IS NOT NULL');
	$CI->db->where_in('nrp', $nrps);
	$CI->db->where('active', 1);
	$q = $CI->db->get('apk_users');
	if($q->num_rows() > 0)
	{
		$res = array();
		foreach($q->result() as $r)
		{
			$res[] = $r->gcm;
		}
		return $res;
	}
	else
	{
		return false;
	}
}

/* */

function sendGCM($message=null,$rcpt=null,$dt=null,$ttl=null,$group=null)
{
	$CI =& get_instance();
	//
	$CI->gcm->setMessage($message);
	foreach($rcpt as $rcv)
	{
		$CI->gcm->addRecepient($rcv);
	}	
	$CI->gcm->setData($dt);
	$CI->gcm->setTtl($ttl);
	$CI->gcm->setGroup($group);
	if($CI->gcm->send())
	{
		return 'GCM sent';
	}
	else
	{
		return 'GCM failed';
	}
}
function sendGCM2($message=null,$rcpt=null,$dt=null,$ttl=null,$group=null)
{
	$CI =& get_instance();
	//
	$CI->gcm->setMessage($message);
	foreach($rcpt as $rcv)
	{
		$CI->gcm->addRecepient($rcv);
	}	
	$CI->gcm->setData($dt);
	$CI->gcm->setTtl($ttl);
	$CI->gcm->setGroup($group);
	if($CI->gcm->send())
	{
		return 'GCM sent';
	}
	else
	{
		return 'GCM failed';
	}
}

//////////////////////////////////////

function sendGCMsingle($message=null,$rcpt=null,$dt=null,$ttl=null,$group=null)
{
	$CI =& get_instance();
	//
	$CI->gcm->setMessage($message);
	$CI->gcm->addRecepient($rcpt);
	$CI->gcm->setData($dt);
	$CI->gcm->setTtl($ttl);
	$CI->gcm->setGroup($group);
	if($CI->gcm->send())
	{
		return 'GCM sent';
	}
	else
	{
		return 'GCM failed';
	}
}
