<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function screenshoot_map($folder=null,$file_name=null,$lat=null,$lon=null,$size='500x300')
	{
		
		if($lat!=null && $lon!=null && $folder!=null && $file_name!=null){
			$api_snapshoot="https://api.mapbox.com/v4/mapbox.streets/pin-l-circle-stroked+3bb2d0%28".$lon.",".$lat.",13%29/".$lon.",".$lat.",13/".$size.".png?access_token=".$this->config->item('access_token')."";
			file_put_contents($folder.'/'.$file_name.'.png',file_get_contents($api_snapshoot));
			$image=$file_name.".png";
			return $image
		}
		
		return false;
	}
?>