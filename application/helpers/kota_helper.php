<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/* =========================================================================================================================================== KEWILAYAHAN ====== */
	
	function getProvinsi($id = null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->where('id_prov', $id);
		$q = $CI->db->get('all_provinsi');
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			return $r->nama_prov;
		}
		else
		{
			return '';
		}
	}
	
	function getKabupaten($id = null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->where('id_kabkot', $id);
		$q = $CI->db->get('all_kabkot A');
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			return $r->nama_kabkot;
		}
		else
		{
			return '';
		}
	}
	function getKecamatan($id = null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		$CI->db->where('A.id_kec', $id);
		$q = $CI->db->get('all_kecamatan A');
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			return $r->nama_kec;
		}
		else
		{
			return '';
		}
	}
	
	
	function getAllProvinsi(){
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$q = $CI->db->get('all_provinsi');
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
		else
		{
			return '';
		}
	}
	
	function getKab($id = null){
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->where('id_prov', $id);
		$q = $CI->db->get('all_kabkot');
		if($q->num_rows() > 0)
		{
			return $q->result_array();
		}
		else
		{
			return '';
		}
	}
	
	function getKec($id = null){
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->where('id_kabkot', $id);
		$q = $CI->db->get('all_kecamatan');
		if($q->num_rows() > 0)
		{
			return $q->result_array();
		}
		else
		{
			return '';
		}
	}
	
	function getKelurahan($id = null){
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->where('id_kec', $id);
		$q = $CI->db->get('all_kelurahan');
		if($q->num_rows() > 0)
		{
			return $q->result_array();
		}
		else
		{
			return '';
		}
	}
	// function getProvJawabarat(){
		
	// }
	
	
?>
