<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function getAllPangkat()
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		$CI->db->order_by('urut', 'asc');
		$q = $CI->db->get('pangkat');
		return $q->result_array();
	}
	//
	
	function getAllFungsi()
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		// $CI->db->order_by('urut', 'asc');
		$q = $CI->db->get('fungsi');
		return $q->result_array();
	}
	
	//
	
	function isDataExists($id=null,$column=null,$ref=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		if($id !== null)
		{
			$CI->db->where('id <>', $id);
		}
		$CI->db->where($column,$ref);
		$q = $CI->db->get('personil');
		if($q->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//
	
	function isAnggotaExists($id=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		$CI->db->where('id', $id);
		$q = $CI->db->get('apk_users');
		if($q->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function getGrade($pangkat=null){
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		$CI->db->select('grade');
		$CI->db->where('kode', $pangkat);
		$q = $CI->db->get('pangkat');
		if($q->num_rows() > 0)
		{
			$data=$q->row();
			return $data->grade;
		}
		else
		{
			return null;
		}
	}
	function getNIK($nik=null){
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		$CI->db->where('nik', $nik);
		$q = $CI->db->get('personil');
		if($q->num_rows() > 0)
		{
			$data=$q->row();
			return $data->nama;
		}
		else
		{
			return null;
		}
	}
	function getPublicUser($username=null){
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		$CI->db->where('username', $username);
		$q = $CI->db->get('public_users');
		if($q->num_rows() > 0)
		{
			$data=$q->row();
			return $data->nama;
		}
		else
		{
			return null;
		}
	}
	
	
	
	//
	
	
	