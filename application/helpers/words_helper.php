<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	//
	
	function convertTanggal($tgl=null,$short_month=false,$with_day=false)
	{
		if($tgl != null && $tgl != '' && $tgl != '0000-00-00'){
			if($short_month == false){
				$bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
			}else{
				$bulan = array('Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sep', 'Okt', 'Nov', 'Des');
			}
			
			if($with_day == true){
				$hari = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu');
				$date = new DateTime($tgl);
				$dw = $hari[intval($date->format('w'))].", ";
			}else{
				$dw = "";
			}
			
			$tgl = substr($tgl,0,10);
			$arr_tgl = explode("-", $tgl);
			
			if(count($arr_tgl) == 3){
				return $dw.intval($arr_tgl[2])." ".$bulan[($arr_tgl[1] - 1)]." ".$arr_tgl[0];
			}else{
				return $tgl;
			}
		}else{
			return '';
		}
	}
	
	//
	
	function convertBulan($bln,$short_month=false)
	{
		if($short_month == false){
			$bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
		}else{
			$bulan = array('Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sep', 'Okt', 'Nov', 'Des');
		}
		
		return $bulan[($bln - 1)];
	}
	
	//
	
	function convertTanggalShort($tgl=null)
	{
		$bulan = array('Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agt', 'Sep', 'Okt', 'Nov', 'Des');
		
		$arr_tgl = explode("-", $tgl);
		
		if(count($arr_tgl) == 3){
			return intval($arr_tgl[2])." ".$bulan[($arr_tgl[1] - 1)]." ".$arr_tgl[0];
		}else{
			return $tgl;
		}
	}
	
	//
	
	// function convertBulan($bln=null)
	// {
		// $bulan = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
		// return $bulan[(intval($bln) - 1)];
	// }
	
	//
	
	function replaceKalimat($target=null, $pengganti=null, $kalimat=null, $alternatif=null)
	{
		if(($pengganti == null) || ($pengganti == "")){
			$pengganti = $alternatif;
		}
		
		if(($kalimat != null) && ($kalimat != "")){
			return str_replace($target, $pengganti, $kalimat);
		}else{
			return "";
		}
	}
	
	function text2html($txt){
		$uraian = str_replace('\r\n', "<br>", $txt);
		$uraian = str_replace("\n", "<br>", $uraian);
		$uraian = str_replace('\n', "<br>", $uraian);
		$uraian = str_replace('\\\"', '"', $uraian);
		$uraian = str_replace("\\\'", "'", $uraian);
		$uraian = str_replace('\"', '"', $uraian);
		$uraian = str_replace("\'", "'", $uraian);
		
		return $uraian;
	}
	
	function textHtml5($txt){
		$uraian = str_replace("'", "&lsquo;", $txt);
		$uraian = str_replace('"', "&quot;", $uraian);
		
		
		return $uraian;
	}
	
	/**
	 * Returns an encrypted & utf8-encoded
	 */
	function myEncrypt($pure_string, $encryption_key) {
		$iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
		$res = urlencode($encrypted_string);
		$res = str_replace("%", "sTRpRsN", $res);
		return $res;
	}

	/**
	 * Returns decrypted original string
	 */
	function myDecrypt($encrypted_string, $encryption_key) {
		$encrypted_string = str_replace("sTRpRsN", "%", $encrypted_string);
		$encrypted_string = urldecode($encrypted_string);
		$iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
		return $decrypted_string;
	}
	
	
	function setNmrTlp($tlp1, $tlp2){
		$mdn = trim($tlp1);
		
		if($mdn != '' && $mdn != null){
			if(is_numeric($mdn)){
				$pref = substr($mdn, 0, 2);
				
				if($pref == '62'){
					$tlp = '0'.substr($mdn, 2);
				}else if($pref == '00'){
					$tlp = $tlp2;
				}else{
					$tlp = $mdn;
				}
			}else{
				$tlp = $tlp2;
			}
		}else{
			$tlp = $tlp2;
		}
		
		$pref = substr($tlp, 0, 2);
		
		if($pref == '62'){
			$tlp = '0'.substr($tlp, 2);
		}
		
		return $tlp;
	}

	function cekUmur($tgl_lahir)
	{
		if($tgl_lahir){
			$rubah = date_format(date_create($tgl_lahir), 'Y');
			$thn_skrg = date('Y');
			$usia = $thn_skrg - $rubah;
			return $usia;
		}else{
			return "";
		}
	}
	
	
	function generateRandomString($length = 5) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	
	function generateRandomNumber($length = 4) {
		$characters = '0123456789';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	
	
	
	
	//
	
	
	