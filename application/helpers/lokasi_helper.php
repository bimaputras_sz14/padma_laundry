<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function getAllLokasi()
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		$CI->db->order_by('urut', 'asc');
		$q = $CI->db->get('places');
		return $q->result_array();
	}
	
	//
	
	function isDataExists($id=null,$column=null,$ref=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		if($id !== null)
		{
			$CI->db->where('id <>', $id);
		}
		$CI->db->where($column,$ref);
		$q = $CI->db->get('places');
		if($q->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//
	
	function isLokasiExists($id=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		$CI->db->where('id', $id);
		$q = $CI->db->get('places');
		if($q->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//
	
	