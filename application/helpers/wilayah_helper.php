<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/* =========================================================================================================================================== KEWILAYAHAN ====== */
	
	function getNamaKewilayahan($id = null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->select('nama');
		$CI->db->limit(1);
		$CI->db->where('id', $id);
		$q = $CI->db->get('kewilayahan');
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			return $r->nama;
		}
		else
		{
			return '';
		}
	}
	
	function getNamaKewilayahan2($id = null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->select('nama');
		$CI->db->limit(1);
		$CI->db->where('kode', $id);
		$q = $CI->db->get('kewilayahan');
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			return $r->nama;
		}
		else
		{
			return '';
		}
	}	
	
	//
	
	function getKodeKewilayahan($nama = null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->select('kode');
		$CI->db->limit(1);
		$CI->db->where('nama', $nama);
		$q = $CI->db->get('kewilayahan');
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			return $r->kode;
		}
		else
		{
			return '';
		}
	}	
	
	//
	
	function getWilayahLevel($tipeid = null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->select('level');
		$CI->db->limit(1);
		$CI->db->where('id', $tipeid);
		$q = $CI->db->get('tipe_wilayah');
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			return $r->level;
		}
		else
		{
			return false;
		}
	}
	
	//
	
	function getWilayahparent($id = null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->select('kode');
		$CI->db->limit(1);
		$CI->db->where('id', $id);
		$q = $CI->db->get('kewilayahan');
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			return $r->kode;
		}
		else
		{
			return false;
		}
	}
	
	//
	
	function isWilayahkode($id=null,$parent=null,$kode=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$kode_get=getWilayahparent($parent)."-".$kode;
		if($id !== null)
		{
			$CI->db->where('id <>', $id);
		}
		$CI->db->select('nama');
		$CI->db->where('kode', $kode_get);
		$q = $CI->db->get('kewilayahan');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}
	
	//
	
	function isWilayahExists($id=null,$tipe=null,$nama=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		if($id !== null)
		{
			$CI->db->where('id <>', $id);
		}
		$CI->db->where('tipe', $tipe);
		$CI->db->where('nama', $nama);
		$q = $CI->db->get('kewilayahan');
		if($q->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/* */
	
	function getAllWilayah($wilayah=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		if($wilayah !== null)
		{
			$CI->db->where('nama', $wilayah);
		}
		$CI->db->where('status', 1);
		$CI->db->order_by('level', 'asc');
		$CI->db->order_by('nama', 'asc');
		$q = $CI->db->get('kewilayahan');
		return $q->result_array();
	}
	
	//
	function getSatwil($kode=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		if($kode !== null)
		{
			$CI->db->like('kode', $kode);
		}
		$CI->db->where('status', 1);
		$CI->db->order_by('level', 'asc');
		$CI->db->order_by('nama', 'asc');
		$q = $CI->db->get('kewilayahan');
		return $q->result_array();
	}
	
	//
	
	function isWilayahGotChild($id=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->where('parent_id', $id);
		$q = $CI->db->get('kewilayahan');
		if($q->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//
	
	function getWilayahScope($id=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		$CI->db->select('tipe');
		$CI->db->where('id', $id);
		$q = $CI->db->get('kewilayahan');
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			return $r->tipe;
		}
		else
		{
			return null;
		}
	}
	
	function getWilayahScopebyKode($kode=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		$CI->db->select('tipe');
		$CI->db->where('kode', $kode);
		$q = $CI->db->get('kewilayahan');
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			return $r->tipe;
		}
		else
		{
			return null;
		}
	}
	
	function getWilayahAndScope($id=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		$CI->db->select('tipe,wilayah_teritorial,parent_id');
		$CI->db->where('kode', $id);
		$CI->db->or_where('kode', substr($id,0,-3));
		$CI->db->order_by('id', 'DESC');
		$q = $CI->db->get('kewilayahan');
		if($q->num_rows() > 0)
		{
			$data = $q->result_array();
			// var_dump($data);die();
			$r['wilayah_teritorial'] = $data[0]['wilayah_teritorial'];
			$r['parent_id'] = false;
			if($data[0]['wilayah_teritorial'] ==null && $data[0]['parent_id']!=null){
				$r['parent_id'] = true;
				$wtParents = getKecamatanChildfromParent($data[1]['wilayah_teritorial']);
				$r['wilayah_teritorial'] = $wtParents;
			}
			$r['tipe'] = $data[0]['tipe'];
			// var_dump($r);die();
			return $r;
		}
		else
		{
			return null;
		}
	}
	
	
	function getKecamatanChildfromParent($kabkot=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		$CI->db->select('id_kec');
		$CI->db->where_in('id_kabkot', $kabkot);
		$q = $CI->db->get('all_kecamatan');
		if($q->num_rows() > 0)
		{
			// $r = $q->row();
			$wilayah=array();
			foreach($q->result() as $row){
				array_push($wilayah,$row->id_kec);
			}
			return $wilayah;
		}
		else
		{
			return null;
		}
	}
	
	function getWilayahTeritorial($id=array(),$tipe=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		if($tipe=="polsek"){
			$CI->db->select('id_kec,nama_kec');
			$CI->db->where_in('id_kec', $id);
			$q = $CI->db->get('all_kecamatan');
		}else{
			$CI->db->select('id_kabkot,nama_kabkot');
			$CI->db->where_in('id_kabkot', $id);
			$q = $CI->db->get('all_kabkot');
		}
		if($q->num_rows() > 0)
		{
			// $r = $q->row();
			return $q->result();
		}
		else
		{
			return null;
		}
	}
	
	function getWilayahBinaan($id=array(),$tipe=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		if($tipe=="polsek"){
			$CI->db->select('id_kel,nama_kel,id_kec');
			$CI->db->where_in('id_kec', $id);
			$q = $CI->db->get('all_kelurahan');
		}else{
			$CI->db->select('id_kec,nama_kec,id_kabkot');
			$CI->db->where_in('id_kabkot', $id);
			$q = $CI->db->get('all_kecamatan');
		}
		if($q->num_rows() > 0)
		{
			// $r = $q->row();
			return $q->result();
		}
		else
		{
			return null;
		}
	}
	
	//
	
	
	
	/* ============================================================================================================================================= KESATUAN ====== */
	
	
	function isKesatuanExists($id=null,$scope=null,$kode=null,$nama=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		if($id !== null)
		{
			$CI->db->where('id <>', $id);
		}
		$CI->db->where('scope', $scope);
		$CI->db->where('kode', $kode);
		$CI->db->where('nama', $nama);
		$q = $CI->db->get('kesatuan');
		if($q->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//
	
	function isKesatuanGotChild($id=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->where('kesatuan_id', $id);
		$q = $CI->db->get('unitkerja');
		if($q->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//
	
	function getKodeKesatuan($id=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->select('kode');
		$CI->db->limit(1);
		$CI->db->where('id', $id);
		$q = $CI->db->get('kesatuan');
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			return $r->kode;
		}
		else
		{
			return '';
		}
	}
	
	//
	
	function getAllKesatuan($scope=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		if($scope !== null)
		{
			$CI->db->where('scope', $scope);
		}
		$CI->db->order_by('kode', 'asc');
		$q = $CI->db->get('kesatuan');
		return $q->result_array();
	}
	
	//
	
	/* ========================================================================================= UNIT KERJA ============================== */
	
	
	function isUnitExists($id=null,$kesatuan=null,$kode=null,$nama=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		if($id !== null)
		{
			$CI->db->where('id <>', $id);
		}
		$CI->db->where('kesatuan_id', $kesatuan);
		$CI->db->where('kode', $kode);
		$CI->db->where('nama', $nama);
		$q = $CI->db->get('unitkerja');
		if($q->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/* */
	
	function getKodeUnit($id=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->select('kode');
		$CI->db->limit(1);
		$CI->db->where('id', $id);
		$q = $CI->db->get('unitkerja');
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			return $r->kode;
		}
		else
		{
			return '';
		}
	}
	
	/* */
	
	function getAllUnit($id=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		if($id !== null)
		{
			$CI->db->where('kesatuan_id', $id);
		}
		$CI->db->order_by('kode', 'asc');
		$q = $CI->db->get('unitkerja');
		return $q->result_array();
	}
	
	
	/* ========================================================================================= JABATAN ============================== */
	
	function isJabatanExists($id=null,$group=NULL,$divisi=null,$jabatan=null,$level=NULL)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		if($id !== null)
		{
			$CI->db->where('id <>', $id);
		}
		$CI->db->where('group', $group);
		$CI->db->where('divisi', $divisi);
		$CI->db->where('jabatan', $jabatan);
		$CI->db->where('level', $level);
		$q = $CI->db->get('jabatan');
		if($q->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/* */
	
	function getAllJabatan($id=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		if($id !== null)
		{
			$CI->db->where('kesatuan_id', $id);
		}
		$CI->db->order_by('jabatan', 'asc');
		$q = $CI->db->get('jabatan');
		return $q->result_array();
	}
	
	/* */
	
	function getJabatan($id=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		//
		$CI->db->select('jabatan');
		$CI->db->limit(1);
		$CI->db->where('id', $id);
		$q = $CI->db->get('jabatan');
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			return $r->jabatan;
		}
		else
		{
			return '';
		}
	}
	function getJabatanWilayah($tipe=null)
	{
		$CI =& get_instance();
		$CI->db = $CI->load->database('default', TRUE);
		if($tipe !== null)
		{
			$CI->db->where('tipe', strtolower($tipe));
		}
		$CI->db->order_by('jabatan', 'asc');
		$q = $CI->db->get('jabatan');
		return $q->result_array();
	}
	
	
	//
	
	//
	
	
	