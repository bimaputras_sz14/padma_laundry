<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrator_model extends CI_Model {
	
	// The following method prevents an error occurring when $this->data is modified.
	// Error Message: 'Indirect modification of overloaded property Demo_cart_admin_model::$data has no effect'.
	public function &__get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}
	
	public function get_data_menu(){
		$this->db->order_by('a.sequence', 'ASC');
		$query = $this->db->select('a.id,a.name,a.url,a.icon,a.title,b.title as groupname')
					->join('menu  b', ' b.id = a.parent_id','left')
					->from('menu  a');
		return $query->get();
	}
	public function get_menu(){
		$this->db->order_by('sequence', 'ASC');
		$q = $this->db->get('menu');
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
		else
		{
			return array();
		}
	} 
	public function get_menu_detail($id=null)
	{
		if($id!=null){
			$this->db->where('id', $id);
		}
		$q = $this->db->get('menu');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}
	 
	//   

	public function add_menu($data){
		$list = $this->db->query("SELECT * FROM menu WHERE name='". $data['name'] ."'");
		if($list->num_rows() > 0){
			return false;
		}else{
			$data = $this->db->insert('menu', $data);
			return $data;
		}
	}

	public function update_menu($id, $data){
		$this->db->where('id', $id);
		$data = $this->db->update('menu', $data );
		return $data;
	}

	public function delete_menu($id){
		$this->db->where('id', $id);
		if($this->db->delete('menu'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	 
	
	 
	
	
}