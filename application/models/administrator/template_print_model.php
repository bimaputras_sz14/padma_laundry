<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template_print_model extends CI_Model {
	
	// The following method prevents an error occurring when $this->data is modified.
	// Error Message: 'Indirect modification of overloaded property Demo_cart_admin_model::$data has no effect'.
	public function &__get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}
	
	public function get_data(){
		$this->db->order_by('a.module', 'ASC');
		$query = $this->db->select('a.id,a.module') 
					->from('default_setting  a')
					->where('tipe','print');
		return $query->get();
	}
	public function get_menu(){
		$this->db->order_by('sequence', 'ASC');
		$q = $this->db->get('menu');
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
		else
		{
			return array();
		}
	} 
	public function get_detail($id=null)
	{
		if($id!=null){
			$this->db->where('id', $id);
		}
		$q = $this->db->get('default_setting');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}
	 
	//   

	public function add($data){
		$list = $this->db->query("SELECT * FROM default_setting WHERE tipe = 'print' module='". $data['name'] ."'");
		if($list->num_rows() > 0){
			return false;
		}else{
			$data = $this->db->insert('default_setting', $data);
			return $data;
		}
	}

	public function update($id, $data){
		$this->db->where('id', $id);
		$data = $this->db->update('default_setting', $data );
		return $data;
	}

	public function delete($id){
		$this->db->where('id', $id);
		if($this->db->delete('default_setting'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	 
	
	 
	
	
}