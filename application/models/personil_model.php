<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Personil_model extends CI_Model {
	
	// The following method prevents an error occurring when $this->data is modified.
	// Error Message: 'Indirect modification of overloaded property Demo_cart_admin_model::$data has no effect'.
	var $table = 'personil';
    var $column_order = array(null, 'nik','nama','email','dob'); //set column field database for datatable orderable
    var $column_search = array('nik','nama','email','dob'); //set column field database for datatable searchable 
    var $order = array('id' => 'asc'); // default order 

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	public function &__get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}

	function get_data_personil($id=null){
		if($id != null){
			$this->db->where('a.id', $id);
		}
		$this->db->select("a.*,  d.jabatan AS nm_jabatan");  
		$this->db->join("jabatan d","d.id=a.jabatan_id","left");
		$data = $this->db->get('personil a');
		
		if($id != null)
		{
			if($data->num_rows() > 0)
			{
				return $data->row();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return $data;
		}
	}
	
	function get_jabatan_list($group=null, $divisi=null){
		$this->db->order_by('jabatan','asc'); 
		$q = $this->db->get('jabatan');
		if($q->num_rows() > 0)
		{
			return $q->result_array();
		}
		else
		{
			return '';
		}
	}
	
	function do_add_personil($data=null)
	{
		$data = $this->db->insert('personil', $data);
		return $data;
	}
	
	function do_edit_personil($id=null,$data=null)
	{
		$this->db->where('id', $id);
		$data = $this->db->update('personil', $data);
		return $data;
	}
	
	function do_reset_password($id=null,$nik=null){
		$data=array(
				"password"=>md5('pass'.substr($nik, 4)) 
			);
		// if($this->db->delete('apk_users'))
		$this->db->where('id', $id);
		if($this->db->update('personil',$data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function insert_user($data){
	
		$this->db->insert('user_accounts',$data); 
	}
	
	function do_delete_personil($id=null)
	{
		$data=array("status"=>3,
					"iccid"=>null,
					"imei"=>null,
					"msisdn"=>null,
					"carrier"=>null,
					"model"=>null
					);
		$this->db->where('id', $id);
		if($this->db->update('personil',$data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
}

?>