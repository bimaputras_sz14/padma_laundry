<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model {
	
	function get_user_web($id=null){
		$select = "user_accounts.*, personil.nama AS anggota";
		$this->db->select($select);
		$this->db->from('user_accounts');
		$this->db->join('personil', 'personil.nik=user_accounts.nik', 'left');
		$this->db->order_by('apk_users.nama');
		
		if(($id != null) && ($id != "")){
			$this->db->where('user_accounts.id', $id);
		}
		
		$query = $this->db->get();
		return $query;
	}
	
	function update_user_web($data, $id){
		$this->db->where('id', $id);
		if($this->db->update('user_accounts', $data)){
			return true;
		}else{
			return false;
		}
	}
	
	
}