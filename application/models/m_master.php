<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_master extends CI_Model {
	
	// The following method prevents an error occurring when $this->data is modified.
	// Error Message: 'Indirect modification of overloaded property Demo_cart_admin_model::$data has no effect'.
	public function &__get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}
  
	public function get_table($table){
		$query = $this->db->select('*')->from($table);
		return $query->get()->result();
	}

	public function get_table_noresult($table){
		$query = $this->db->select('*')->from($table);
		return $query->get();
	}

	function get_table_k($select,$table,$where){
		$select=$select!=''? $select:'*';
		$query = $this->db->select($select)->from($table)->where($where);
		return $query->get()->result();
	}

	function get_query($select){
		$query=$this->db->query($select);
		return $query->result();
	}
	
	function get_query_master($select){
		$query=$this->db->query($select);
		return $query;
	}
	function get_my_satwil($kode=null)
	{
		$this->db->select('nama');
		$this->db->where('kode', $kode);
		$this->db->limit(1);
		$q = $this->db->get('kewilayahan');
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			return ucwords(strtolower($r->nama));
		}
		else
		{
			return '';
		}
	}
	function do_login($nrp = null, $password = null,$kd_wil =null)
	{
		$this->db->where('nrp', $nrp);
		$this->db->where('password', $password);
		$this->db->like('kode_wilayah', $kd_wil,'after');
		$this->db->limit(1);
		$q = $this->db->get('apk_users');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}

	function get_jml_lap($tgl=null, $kd_satwil=null,$format=null,$thn=null){
		if(($tgl != null) && ($tgl != '')){
			if($format=='day'){
				$whereDate = "tgl_kejadian LIKE '". $tgl. "%'";	
			}elseif($format=='month'){
				$whereDate = "MONTH(tgl_kejadian)='". $tgl ."' AND YEAR(tgl_kejadian)='". $thn ."'";
			}else{
				$whereDate = "YEAR(tgl_kejadian)='". $tgl ."'";
			}
		}
		
		$sql = "SELECT id FROM  bagops_jurnal WHERE kode_satwil LIKE '". $kd_satwil ."%' AND ". $whereDate;
		// die($sql);
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
 
	function get_jml_info_kriminal($tgl=null, $kd_satwil=null,$format=null,$thn=null){
		if(($tgl != null) && ($tgl != '')){
			if($format=='day'){
				$whereDate = "tanggal_laporan LIKE '". $tgl. "%'";	
			}elseif($format=='month'){
				$whereDate = "MONTH(tanggal_laporan)='". $tgl ."' AND YEAR(tanggal_laporan)='". $thn ."'";
			}else{
				$whereDate = "YEAR(tanggal_laporan)='". $tgl ."'";
			}
		}
		
		$sql = "SELECT id FROM info_kriminal WHERE kode_satwil LIKE '". $kd_satwil ."%' AND ". $whereDate;
		// die($sql);
		$query = $this->db->query($sql);
		return $query->num_rows();
	}


	function get_jml_info_tahanan($tgl=null, $kd_satwil=null,$format=null,$thn=null){
		if(($tgl != null) && ($tgl != '')){
			if($format=='day'){
				$whereDate = "masuk LIKE '". $tgl. "%'";	
			}elseif($format=='month'){
				$whereDate = "MONTH(masuk)='". $tgl ."' AND YEAR(masuk)='". $thn ."'";
			}else{
				$whereDate = "YEAR(masuk)='". $tgl ."'";
			}
		}
		
		$sql = "SELECT id FROM bagops_tahanan WHERE kode_satwil LIKE '". $kd_satwil ."%' AND ". $whereDate;
		// die($sql);
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	function get_jml_info_agenda($tgl=null, $kd_satwil=null,$format=null,$thn=null){
		if(($tgl != null) && ($tgl != '')){
			if($format=='day'){
				$whereDate = "tanggal_mulai LIKE '". $tgl. "%'";	
			}elseif($format=='month'){
				$whereDate = "MONTH(tanggal_mulai)='". $tgl ."' AND YEAR(tanggal_mulai)='". $thn ."'";
			}else{
				$whereDate = "YEAR(tanggal_mulai)='". $tgl ."'";
			}
		}
		
		$sql = "SELECT id FROM ijin_keramaian WHERE kode_satwil LIKE '". $kd_satwil ."%' AND ". $whereDate;
		// die($sql);
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	function get_jml_info_orang_asing($tgl=null, $kd_satwil=null,$format=null,$thn=null){
		if(($tgl != null) && ($tgl != '')){
			if($format=='day'){
				$whereDate = "tanggal_laporan LIKE '". $tgl. "%'";	
			}elseif($format=='month'){
				$whereDate = "MONTH(tanggal_laporan)='". $tgl ."' AND YEAR(tanggal_laporan)='". $thn ."'";
			}else{
				$whereDate = "YEAR(tanggal_laporan)='". $tgl ."'";
			}
		}
		
		$sql = "SELECT id FROM orang_asing WHERE kode_satwil LIKE '". $kd_satwil ."%' AND ". $whereDate;
		// die($sql);
		$query = $this->db->query($sql);
		return $query->num_rows();
	}


	function get_query_katKejadian(){
		$query=$this->db->get('kategori_kriminal');
		return $query->result();
	}

	function get_query_ppgk(){
		$query=$this->db->get('kategori_ppgk');
		return $query->result();
	}

	function get_query_ppgkKhusus(){
		$query=$this->db->get('kategori_ppgk_khusus');
		return $query->result();
	}

	function get_data_user(){ 
		$this->db->select('a.id, a.lvl_id as id_profesi,a.email, a.nrp, b.profesi, c.nama');
		$this->db->from('ems_user_accounts a');
		$this->db->join('ems_profesi b', 'a.lvl_id=b.id_profesi');
		$this->db->join('ems_personil c', 'a.nrp=c.nrp'); 
		return $this->db->get();
	}
	
	function insert_user(){
		$email = $this->input->post('email');
		$username = '';
        $nrp = $this->input->post('nrp');
        $password = $this->input->post('password');
        $level = $this->input->post('level');
        $data_added = date('Y-m-d H:i:s');
        $activate = 1;
		$store_database_salt = $this->auth->auth_security['store_database_salt'];
	    $database_salt = $store_database_salt ? $this->flexi_auth_model->generate_token($this->auth->auth_security['database_salt_length']): FALSE;
		$hash_password = $this->flexi_auth_model->generate_hash_token($password, $database_salt, TRUE);
		$user_data=array(
			'nrp'=>$nrp,
			'password'=>$hash_password,
			'lvl_id'=>$level,
			'date_added'=>$data_added,
			'email'=>$email,
			'salt'=>$database_salt,
			'active'=>1
		);
		$this->db->insert('ems_user_accounts',$user_data);
		redirect('user/management_user');
	}
	function get_data_direktori($data_login=null,$group=null){
		// var_dump(($data_login['kode_satwil']!='' && $data_login['lvl_id']!=1));die();
		$post=$this->input->post('data');
		// var_dump(count($post));die();
		if(count($post) > 1){
			$this->db->where_in('a.id_kategori',$post);
		}else{
			
		}
		if($data_login['kode_satwil']!='' && $data_login['lvl_id']!=1){
			$this->db->like('a.kode_satwil',$data_login['kode_satwil'],'after');
		}
		// var_dump($group->group);die();
		// if($group->group =="DISHUB" || $group->group=='TEAM KETUPAT'){
			// $this->db->where('createdby',$data_login['nrp']);
		// }
		$this->db->select('a.*,b.nama as nama_kategori,b.icon');
		$this->db->join('direktori_kategori b','a.id_kategori=b.id');
		$this->db->where('a.aktif',1);
		$data = $this->db->get('direktori_kota a');
		return $data;
	}
	
	function get_data_direktori2($data_login=null,$group=null){
		// var_dump(($data_login['kode_satwil']!='' && $data_login['lvl_id']!=1));die();
		$post=$this->input->post('data');
		// var_dump(count($post));die();
		if(count($post) > 0){
			$this->db->where_in('a.id_kategori',$post);
			if($data_login['kode_satwil']!='' && $data_login['lvl_id']!=1){
			$this->db->like('a.kode_satwil',$data_login['kode_satwil'],'after');
			}
			// var_dump($group->group);die();
			// if($group->group =="DISHUB" || $group->group=='TEAM KETUPAT'){
				// $this->db->where('createdby',$data_login['nrp']);
			// }
			$this->db->select('a.*,b.nama as nama_kategori,b.icon');
			$this->db->join('direktori_kategori b','a.id_kategori=b.id');
			$this->db->where('a.aktif',1);
			$data = $this->db->get('direktori_kota a')->result();
		}else{
			$data = array();
		}
		
		return $data;
	}
	function insert_data_map($data=null){
		$data = $this->db->insert('data_peta', $data);
		return $data;
	}
	function update_kelurahan($data,$nama){
		$this->db->where('upper(nama_kel)', $nama);
		$data = $this->db->update('all_kelurahan', $data);
		return $data;
	}
	function insert_kota($data=null){
		$data = $this->db->insert('direktori_kota', $data);
		return $data;
	}
	function edit_direktori($id=null,$data=null)
	{
		$this->db->where('id', $id);
		$data = $this->db->update('direktori_kota', $data);
		return $data;
	}
	function get_direktori_detail($id=null)
	{
		$hasil = array();
		$this->db->select("a.*,c.id_kabkot,c.id_prov");
		$this->db->join('all_kecamatan b', 'b.id_kec=a.id_kec','left');
		$this->db->join('all_kabkot c', 'b.id_kabkot=c.id_kabkot');
		$this->db->where('id', $id);
		$q = $this->db->get('direktori_kota a');
		if($q->num_rows() > 0)
		{
			$hasil['gerbang_tol']=null;
			if($q->row()->id_kategori==7){
				$this->db->where('id_direktori',$id);
				$hasil['gerbang_tol'] = $this->db->get("status_gerbangtol")->result();
			}
			$hasil['direktori']=$q->row();
			return $hasil;
		}
		else
		{
			return false;
		}
	}
	
	function insert_status_direktori($data,$kategori){
		$hasil =false;
		if($kategori=='SPBU'){
			$hasil = $this->db->insert('status_spbu',$data);
		}else if($kategori=='Gerbang Tol'){
			$hasil = $this->db->insert_batch('status_gerbangtol',$data);
		}
		return $hasil;
	}
	
	function update_status_direktori($data,$kategori){
		$hasil =false;
		if($kategori=='SPBU'){
			$this->db->where('id',$id);
			$hasil = $this->db->update('status_spbu',$data);
		}else if($kategori=='Gerbang Tol'){
			$hasil = $this->db->update_batch('status_gerbangtol',$data,'id');
		}
		return $hasil;
	}
	
	function delete_status_direktori($data,$kategori){
		$hasil =false;
		if($kategori=='SPBU'){
			$this->db->where('id',$data);
			$hasil = $this->db->delete('status_spbu');
		}else if($kategori=='Gerbang Tol'){
			$this->db->where_in('id',$data);
			$hasil = $this->db->delete('status_gerbangtol');
		}
		return $hasil;
	}
	
	function do_delete_status_direktori($table=null,$id=null)
	{
		$this->db->where('id_direktori', $id);
		if($this->db->delete($table))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function get_satwilDirektori($kabkot=null)
	{
		$this->db->select("kode");
		$this->db->limit(1);
		$this->db->where("kode !='00-08'",null);
		$this->db->like('wilayah_teritorial', $kabkot);
		$q = $this->db->get('kewilayahan');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			$satwil = '00-08';
			return $satwil;
		}
	}
	function do_delete_direktori($id=null)
	{
		$data=array("aktif"=>0);
		$this->db->where('id', $id);
		if($this->db->update('direktori_kota',$data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function update_direktori($data){
		$hasil = $this->db->update_batch('direktori_kota',$data,'id');
		return $hasil;
	}
	function get_kewilayahan($jenis){
		if($jenis==1){
			$this->db->select('kode'); 
			$data = $this->db->get('kewilayahan')->result_array();
		}else{
			$this->db->select('kode,wilayah_teritorial'); 
			$data = $this->db->get('kewilayahan_sispi')->result_array();
		}
		return $data;
	}
	
	function update_kewilayahan($data){
		$hasil = $this->db->update_batch('kewilayahan',$data,'kode');
		return $hasil;
	}

	// parameter pelanggaran

	public function get_sim(){
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('jenis_sim');
		return $query->result(); 
	}

	public function get_job(){
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('kategori_pekerjaan');
		return $query->result(); 
	}

	public function get_pel(){
		$this->db->order_by('id', 'asc');
		$query = $this->db->get('kategori_pelanggaran');
		return $query->result(); 
	}

	public function get_pel_det($id){
		$this->db->order_by('id', 'asc');
		$this->db->where('id_kategori',$id);
		$query = $this->db->get('kategori_pelanggaran_sub');
		return $query->result(); 
	}

	public function get_penindakan(){
		$this->db->order_by('id', 'asc'); 
		$query = $this->db->get('kategori_penindakan');
		return $query->result(); 
	}

	public function get_barbuk(){
		$this->db->order_by('id', 'asc'); 
		$query = $this->db->get('barang_bukti');
		return $query->result(); 
	}

	public function get_lokasi($id){
		$this->db->order_by('id', 'asc'); 
		$this->db->where('parent_id',$id);
		$query = $this->db->get('kategori_lokasi_sub');
		return $query->result(); 
	}

	public function get_kategori_pendidikan(){
		$this->db->order_by('id', 'asc');  
		$query = $this->db->get('kategori_pendidikan');
		return $query->result(); 
	}

	public function get_jenis_kendaraan(){
		$this->db->order_by('id', 'asc');  
		$query = $this->db->get('kategori_kendaraan');
		return $query->result(); 
	}

	public function get_waktu_kejadian(){
		$this->db->order_by('id', 'asc');  
		$query = $this->db->get('kategori_waktu');
		return $query->result(); 
	}

	public function get_jenis_det_kendaraan($id=null){
		$this->db->order_by('id', 'asc');  
		$this->db->where('id_kategori_kendaraan',$id);
		$query = $this->db->get('kategori_det_kendaraan');
		return $query->result(); 
	}

	public function get_merek($id=null){
		$this->db->order_by('id', 'asc');  
		$this->db->where('kategori',$id);
		$query = $this->db->get('merek_kendaraan');
		return $query->result(); 
	}

	public function get_kota($id){
		$this->db->order_by('id', 'asc');  
		$this->db->where('id_prov',$id);
		$query = $this->db->get('all_kabkot');
		return $query->result(); 
	}

	public function get_kec($id){
		$this->db->order_by('nama_kec', 'asc'); 
		$this->db->where('id_kabkot',$id); 
		$query = $this->db->get('all_kecamatan');
		return $query->result(); 
	}

	public function get_kec_parent($id){
		$this->db->select("all_kabkot.id_kabkot,all_kecamatan.id_kec,all_kabkot.id_prov"); 
		$this->db->where("id_kec",$id); 
		$this->db->join("all_kabkot"," all_kabkot.id_kabkot = all_kecamatan.id_kabkot"); 
		$q = $this->db->get('all_kecamatan');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			 
			return 0;
		}
	}

	public function get_all_uu($id_uu=null){
		if($id_uu != null){
			$this->db->where('id_uu', $id_uu);
		}
		$this->db->order_by('id_uu', 'asc');
		$query = $this->db->get('uu');
		return $query->result();   
	}

	public function get_status_lp(){
		$this->db->order_by('urutan', 'asc');
		$query = $this->db->get('status_lp');
		return $query->result();   
	}

	public function get_all_pasal($id_uu=null,$pasal=null,$ayat=null){
		if($id_uu != null)
		{
			$this->db->where('id_uu', $id_uu);
			$this->db->group_by('pasal');
		}

		if($id_uu != null && $pasal != null && $ayat != null)
		{
			$this->db->where('id_uu', $id_uu);
			$this->db->where('pasal', $pasal);
			$this->db->where('ayat', $ayat);
		}

		$this->db->order_by('CAST(pasal AS UNSIGNED)');
		$query = $this->db->get('pasal');
		return $query->result();   
	}

	public function get_all_ayat($pasal){
		$this->db->order_by('CAST(ayat AS UNSIGNED)');
		$this->db->where('pasal', $pasal);
		$this->db->where('ayat !=', 'NULL');
		$this->db->group_by('pasal');
		$query = $this->db->get('pasal');
		return $query->result();   
	}

	public function get_all_huruf($pasal, $ayat){
		$this->db->order_by('CAST(huruf AS UNSIGNED)');
		$this->db->where('pasal', $pasal);
		$this->db->where('ayat', $ayat);
		$this->db->where('huruf !=', 'NULL');
		$this->db->group_by('pasal');
		$query = $this->db->get('pasal');
		return $query->result();   
	}

	public function get_kel($id){
		$this->db->order_by('nama_kel', 'asc');  
		$this->db->where('id_kec',$id); 
		$query = $this->db->get('all_kelurahan');
		return $query->result();   
	}

	public function get_all_kel(){ 
		$this->db->where('boundary <>', ''); 
		// $this->db->like('id_kel','3578100001', 'after'); 
		$this->db->like('id_kel','3578', 'after'); 
		$query = $this->db->get('all_kelurahan');
		return $query ;   
	}

	public function get_usia(){
		$this->db->order_by('id', 'asc');   
		$query = $this->db->get('kategori_usia');
		return $query->result(); 
	}

	function get_data_kriminal($tipe_time=null,$cat=null){
		$cat_all = str_replace('all,', '',$cat); 
		$select = "count(kelurahan_id) as total_kriminal, all_kelurahan.nama_kel , all_kelurahan.boundary "; 
		$this->db->select($select);
		$this->db->from('info_kriminal');
		$this->db->join('all_kelurahan', 'all_kelurahan.id_kel=info_kriminal.kelurahan_id', 'left'); 


		if($tipe_time==2){
			$this->db->where("MONTH(tanggal_kejadian) = MONTH(CURDATE())  AND YEAR(tanggal_kejadian) = YEAR(CURDATE())");
		}elseif($tipe_time==1){
			$this->db->where("EXTRACT(YEAR_MONTH FROM tanggal_kejadian) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 1 MONTH)");
		}else{
			$this->db->where("YEAR(tanggal_kejadian) = YEAR(CURDATE())");
		}

		if($cat_all!='all'){
			$this->db->join('uu', 'uu.nama=info_kriminal.uu', 'left');  
			$this->db->where_in("kategori_kriminal",explode(',', $cat_all));
		}else{

		}

		$this->db->group_by('all_kelurahan.nama_kel' , 'all_kelurahan.boundary'); 
		$query = $this->db->get();
		return $query;
	}

	function get_data_kriminal_time($tipe_time=null,$month=null){
		$select = "count(kelurahan_id) as total_kriminal, all_kelurahan.nama_kel , all_kelurahan.boundary "; 
		$this->db->select($select);
		$this->db->from('info_kriminal');
		$this->db->join('all_kelurahan', 'all_kelurahan.id_kel=info_kriminal.kelurahan_id', 'left');  

		if($tipe_time==1){
			$this->db->where("TIME(tanggal_kejadian) between '00:00' and '02:59' ");
		}elseif($tipe_time==2){
			$this->db->where("TIME(tanggal_kejadian) between '03:00' and '05:59' ");
		}elseif($tipe_time==3){
			$this->db->where("TIME(tanggal_kejadian) between '06:00' and '08:59' ");
		}
		elseif($tipe_time==4){
			$this->db->where("TIME(tanggal_kejadian) between '09:00' and '11:59' ");
		}
		elseif($tipe_time==5){
			$this->db->where("TIME(tanggal_kejadian) between '12:00' and '14:59' ");
		}
		elseif($tipe_time==6){
			$this->db->where("TIME(tanggal_kejadian) between '15:00' and '17:59' ");
		}
		elseif($tipe_time==7){
			$this->db->where("TIME(tanggal_kejadian) between '18:00' and '20:59' ");
		} 
		else{
			$this->db->where("TIME(tanggal_kejadian) between '21:00' and '23:59' ");
		} 

		if($month==2){
			$this->db->where("MONTH(tanggal_kejadian) = MONTH(CURDATE())  AND YEAR(tanggal_kejadian) = YEAR(CURDATE())");
		}elseif($month==1){
			$this->db->where("EXTRACT(YEAR_MONTH FROM tanggal_kejadian) = EXTRACT(YEAR_MONTH FROM CURDATE() - INTERVAL 1 MONTH)");
		}else{
			$this->db->where("YEAR(tanggal_kejadian) = YEAR(CURDATE())");
		}

		$this->db->group_by('all_kelurahan.nama_kel' , 'all_kelurahan.boundary'); 
		$query = $this->db->get();
		return $query;
	}

	public function get_kat_kriminal(){
		$this->db->order_by('id', 'asc');   
		$query = $this->db->get('kategori_kriminal');
		return $query->result(); 
	}

	public function get_kat_potensi(){
		$this->db->order_by('id', 'asc');   
		$query = $this->db->get('kat_potensi');
		return $query->result(); 
	}

	function get_jadwal_detail($id=null){
		$select = "ijin_keramaian.*, kewilayahan.nama AS satwil, apk_users.nama AS nama_agt, apk_users.pangkat"; 
		$this->db->select($select);
		$this->db->from('ijin_keramaian');
		$this->db->join('kewilayahan', 'kewilayahan.kode=ijin_keramaian.kode_satwil', 'left');
		$this->db->join('apk_users', 'apk_users.nrp=ijin_keramaian.nrp_petugas', 'left');
		$this->db->where('ijin_keramaian.id', $id); 
		$query = $this->db->get();
		return $query;
	}

	function get_jadwal($limit=null, $kd_satwil=null){
		$this->db->select('ijin_keramaian.id, tanggal_mulai, tanggal_akhir, nama_kegiatan, kewilayahan.nama AS satwil');
		$this->db->join('kewilayahan', 'kewilayahan.kode=ijin_keramaian.kode_satwil', 'left');
		$this->db->order_by('tanggal_mulai', 'desc'); 
		
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('kode_satwil', $kd_satwil, 'after');
		}
		if(($limit != null) && ($limit != "")){
			$this->db->limit($limit);
		}

		$query = $this->db->get('ijin_keramaian');
		return $query;
	}



}