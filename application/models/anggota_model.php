<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anggota_model extends CI_Model {
	
	// The following method prevents an error occurring when $this->data is modified.
	// Error Message: 'Indirect modification of overloaded property Demo_cart_admin_model::$data has no effect'.
	public function &__get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}
	
	function do_add_anggota($data=null)
	{
		$data = $this->db->insert('anggota', $data);
		return $data;
	}
	
	/* */
	
	function get_anggota_detail($id=null)
	{
		$this->db->where('id', $id);
		$q = $this->db->get('anggota');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}
	
	/* */
	
	function do_edit_anggota($id=null,$dt=null)
	{
		$this->db->where('id', $id);
		$data = $this->db->update('anggota', $data);
		return $data;
	}
	
	/* */
	
	function do_delete_anggota($id=null)
	{
		$this->db->where('id', $id);
		if($this->db->delete('anggota'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
?>