<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_model extends CI_Model {
	
	// The following method prevents an error occurring when $this->data is modified.
	// Error Message: 'Indirect modification of overloaded property Demo_cart_admin_model::$data has no effect'.
	public function &__get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}
	
	public function tipe_token(){
		$q = $this->db->get('token_type');
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
		else
		{
			return array();
		}
	}     

	function get_total_member(){ 
		$q = $this->db->get('pelanggan');
		if($q->num_rows() > 0)
		{
			return $q->num_rows();
		}
		else
		{
			return 0;
		}
	}


	function get_sales(){
		$this->db->like('tanggal_penjualan',date('Y-m-d'),'after');
		$this->db->select('sum(total_penjualan) as total_penjualan');
		$q = $this->db->get('penjualan');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return 0;
		}

	}
	function get_visit(){
		$this->db->like('date_start',date('Y-m-d'),'after');
		$this->db->select('count(pelanggan_id) as total_visit');
		$q = $this->db->get('pelanggan_attendence');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return 0;
		}
	}

	function get_visit_graph(){ 
		$this->db->select('count(pelanggan_id) as total_visit,date_start');
		$q = $this->db->get('pelanggan_attendence');
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
		else
		{
			return 0;
		}
	}

	function get_sales_graph(){
 		$this->db->select('sum(total_penjualan) as total_penjualan,tanggal_penjualan');
		$q = $this->db->get('penjualan');
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
		else
		{
			return 0;
		}

	}

	public function membership_type(){
		$q = $this->db->get('membership_type');
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
		else
		{
			return array();
		}
	}

	public function agama(){
		$q = $this->db->get('agama');
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
		else
		{
			return array();
		}
	}

	public function kategori_produk(){
		$this->db->where('fl_show',1);
		$q = $this->db->get('kategori_produk');
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
		else
		{
			return array();
		}
	}

	public function kelas(){
		$this->db->where('fl_show',1);
		$q = $this->db->get('kelas');
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
		else
		{
			return array();
		}
	}

	public function jadwal_kelas($day=null){ 
		if($day){
			$this->db->where('day',$day);
		}
		
		$q = $this->db->get('kelas_sub');
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
		else
		{
			return array();
		}
	}

	public function get_jadwal_kelas(){
		$query = $this->db->select('*')->from('kelas_sub') ;
		return $query->get();
	}

	public function add_jadwal_kelas($data){  
		$this->db->insert('kelas_sub', $data);
		$id   =  $this->db->insert_id();
		return $id; 
	}

	public function edit_jadwal_kelas($id, $data){
		$data = $this->db->update('kelas_sub', $data, array('id' => $id));
		return $data;
	}

	public function get_jadwal_kelas_detail($id=null)
	{	
		if($id!=null){
				$this->db->where('id', $id);
		}
		$q = $this->db->get('kelas_sub');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}

	public function get_kelas_detail($id=null)
	{	
		if($id!=null){
				$this->db->where('id', $id);
		}
		$q = $this->db->get('kelas');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}

	public function kelas_trans($data){ 
		$this->db->where_in('id', $data);
		$q = $this->db->get('kelas');
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
		else
		{
			return array();
		}
	}


}
