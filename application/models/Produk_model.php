<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk_model extends CI_Model {
	 
	public function &__get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}
	     
	//module barang
	 
	public function get_produk(){ 
		$this->db->join('kategori_produk b','b.id=a.kategori_produk_id','Left');
		$query = $this->db->select('a.*,b.nama as nama_kategori')->from('produk a');
		return $query->get();  
	}

	// public function add_produk($data){
	// 	$list = $this->db->query("SELECT * FROM produk WHERE kode='". $data['kode'] ."'");
	// 	if($list->num_rows() > 0){
	// 		return false;
	// 	}else{
	// 		$data = $this->db->insert('produk', $data);
	// 		return $data;
	// 	}
	// }
	 
	
	public function add_produk($data){  
		$this->db->insert('produk', $data);
		$id   =  $this->db->insert_id();
		return $id; 
	}

	public function edit_produk($id, $data){
		$data = $this->db->update('produk', $data, array('id' => $id));
		return $data;
	}

	public function get_produk_detail($id=null)
	{	
		if($id!=null){
				$this->db->where('id', $id);
		}
		$q = $this->db->get('produk');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}
	
	public function delete_produk($id){
		$this->db->where('id', $id);
		if($this->db->delete('produk'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	 
	
}