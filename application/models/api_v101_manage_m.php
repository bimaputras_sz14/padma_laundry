<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_v101_manage_m extends CI_Model {
	
	// The following method prevents an error occurring when $this->data is modified.
	// Error Message: 'Indirect modification of overloaded property Demo_cart_admin_model::$data has no effect'.
	public function &__get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}

	function do_add_pemohon($data=null){
		$data = $this->db->insert('rikkes_pemohon', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}

	function do_add_pemohon_trx($data=null){
		$data = $this->db->insert('rikkes_pemohon_trx', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
}