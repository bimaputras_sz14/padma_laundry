<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Penjualan_model extends CI_Model {
	
	// The following method prevents an error occurring when $this->data is modified.
	// Error Message: 'Indirect modification of overloaded property Demo_cart_admin_model::$data has no effect'.
	public function &__get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}

	public function get_penjualan_detail($id=null)
	{	
		if($id!=null){
				$this->db->where('id', $id);
		}
		$q = $this->db->get('penjualan');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}

	public function get_penjualan_item_detail($id=null)
	{	
		if($id!=null){
				$this->db->where('penjualan_id', $id);
		}
		$q = $this->db->get('penjualan_item');
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
		else
		{
			return false;
		}
	}
	  
	//module sales begin
	public function get_penjualan($id=null,$divisi=null){

		$this->db->select('a.*,b.nama_pelanggan', FALSE); 
		$this->db->join('pelanggan b','b.id = a.pelanggan_id') ; 
		if($id){ 
			$this->db->where('a.id', $id); 	
		} 
		$query = $this->db->get('penjualan a'); 
		if($id==null || $divisi!=null){
			return $query;
		}else{
			return $query->row();
		}
	}
	public function get_det_penjualan($id){
		$this->db->select(' a.id,a.id_pembelian,b.kategori,a.id_barang,a.jml,a.harga_satuan,a.spesifikasi,a.keterangan', FALSE); 
		$this->db->join('barang b','b.id=a.id_barang');	
		$this->db->where('a.id_pembelian', $id); 	
		$query = $this->db->get('detail_pembelian a');
		return $query->result();
	}
	 
	public function get_total_det_penjualan($id){
		$this->db->select('sum(harga_satuan*jml) as total'); 
		$this->db->where('id_pembelian',$id);
		$q = $this->db->from('detail_pembelian')->get();

		return $q->row();
	}
	public function add_penjualan_barang($data){
		$this->db->insert('penjualan', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function add_penjualan_barang_det($data){
		$this->db->insert('penjualan_item', $data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}

	public function update_penjualan($id, $data){
		$this->db->where('id', $id);
		$data = $this->db->update('penjualan', $data );
		return $data;
	}
 
	public function delete_penjualan($id){
		$this->db->where('id', $id);
		if($this->db->delete('penjualan'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function get_tipe_sales(){
		$q = $this->db->get('tipe_penjualan');
		if($q->num_rows() > 0)
		{
			return $q->result_array();
		}
		else
		{
			return array();
		}
	}
	//end sales module 
}