<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_crud extends CI_Model {

	// bisa dipake dimana aja
	function get_record($select,$tableName,$order,$join,$relation,$where,$limit)
	{
		$this->db->select($select);
		$this->db->from($tableName);
		if($join != "")
		{
			$this->db->join($join,$relation);
		}
		if($where != "")
		{
			$this->db->where($where);
		}
		if($order != "")
		{
			$this->db->order_by($order,'desc');
		}
		if($limit != "")
		{
			$this->db->limit($limit);
		}

		$query = $this->db->get(); 

		return $query->result_array();
	}

	function create_data($table,$data){
		$this->db->insert($table,$data);
	}

	function update_data($table,$data,$id_name,$id){
		$this->db->where($id_name,$id);
		$this->db->update($table,$data);
	}

	function delete_data($table,$id_name,$id){
		$this->db->where($id_name,$id);
		$this->db->delete($table);
	}

	function get_one($table,$id_name,$id){
		$data = array($id_name=>$id);
		return $this->db->get_where($table,$data);
	}

	function get_where($table,$where){
		return $this->db
			->select('*')
			->from($table)
			->where($where)
			->get();
	}

	function count_data($table,$where=NULL,$field=NULL){
		if(empty($where)) {
			return $this->db->get($table)->num_rows();
		}
		else {
			return $this->db->get_where($table,array($where=>$field))->num_rows();
		}
	}

}

/* End of file model_crud.php */
/* Location: ./application/models/model_crud.php */