<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_v101_pemohon_m extends CI_Model {
	
	
	function __construct()
    {
        parent::__construct();
		//Load them in the constructor
    }
	
	/* ============== */
	
	function get_pemohon($ktp = null)
	{
		$this->db->where('no_ktp', $ktp);
		$this->db->limit(1);
		$q = $this->db->get('rikkes_pemohon');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}
	
	/* =================== */
	
	function get_pemohon_foto($id_pemohon=null)
	{
		$dir = 'rikkes/foto/';
		$dir_ktp = 'rikkes/ktp/';
		$query = "SELECT foto_ktp, foto_pemohon FROM rikkes_pemohon_trx WHERE ref_id='".$id_pemohon."' AND foto_pemohon IS NOT NULL ORDER BY created_time DESC LIMIT 1";
		
		$q = $this->db->query($query);
		if($q->num_rows() > 0)
		{
			$r = $q->row();
			if($r->foto_ktp !== '')
			{
				$ktp = get_foto_source($dir_ktp, $r->foto_ktp);
			}
			else
			{
				$ktp = 'img/idcard.png';
			}
			
			if($r->foto_pemohon !== '')
			{
				$foto = get_foto_source($dir, $r->foto_pemohon);
			}
			else
			{
				$foto = 'img/ava.jpg';
			}
		}
		else
		{
			$ktp = 'img/idcard.png';
			$foto = 'img/ava.jpg';
		}
		
		return array('ktp' => $ktp, 'foto' => $foto);
	}
	
	/* ================= */	
	
	function save_pemohon($dt=null)
	{
		if(!$data = $this->get_pemohon($dt['no_ktp'])){
			if($this->db->insert('rikkes_pemohon',$dt))
			{
				$id = $this->db->insert_id();
				return $id;
			}
			else
			{
				return false;
			}
		}else{
			$this->db->where('no_ktp', $dt['no_ktp']);
			$this->db->update('rikkes_pemohon', $dt);
			return $data->id;
		}
	}
	
	/* ================= */
	
	function get_pemohon_list($id_dokter=null,$offset=null,$direction=null)
	{
		$this->db->select('rikkes_pemohon.*');
		$this->db->join('rikkes_pemohon_trx', 'rikkes_pemohon.id=rikkes_pemohon_trx.ref_id');
		$this->db->where('rikkes_pemohon_trx.dokter_id', $id_dokter);
		$this->db->group_by('rikkes_pemohon.id', 'asc');
		$this->db->order_by('rikkes_pemohon.nama', 'asc');
		
		switch($direction)
		{
			case 'older':
				if($offset == null || $offset == ''){
					$offset = 10;
				}
			break;
			default: // newer //
				$offset = 0;
			break;			
		}
		
		$this->db->limit(10, $offset);
		$q = $this->db->get('rikkes_pemohon');
		if($q->num_rows() > 0)
		{
			$res = array();
			$pg = $offset + 10;
			
			foreach($q->result() as $row)
			{
				$arr_foto = $this->get_pemohon_foto($row->id);
				$photo_foto = '{url: \''.$arr_foto["foto"].'\', caption: \''.$row->nama.'\'}';
				$photo_ktp = '{url: \''.$arr_foto["ktp"].'\', caption: \'No.KTP: '.$row->no_ktp.'\'}';
				$photo_pop = '['.$photo_foto.','.$photo_ktp.']';
				
				if($row->tgl_lahir != null && $row->tgl_lahir != '' && $row->tgl_lahir != '0000-00-00'){
					$arr_tgl = explode('-', $row->tgl_lahir);
					$th = $arr_tgl[0];
					$this_year = date('Y');
					$umur = intval($this_year) - intval($th);
					$umur = $umur.' th';
				}else{
					$umur = '0 th';
				}
				
				$card = '<div class="card" onClick="showDetailPemohon('.$row->id.')">
							  <div class="card-content"> 
								<div class="list-block media-list">
								  <ul>
									<li class="item-content bg-green">
									  <div class="item-media" onClick="showMyPhotoBrowserArr('.$photo_pop.')">
										<div>
											<img src="'.$arr_foto["foto"].'" style="height:44px; width:44px; border: 4px solid white;"/>
											<div style="height:20px; width:100%; font-size:10pt; text-align:center; color:#fff; margin-top: 10px;"><b>'.$umur.'</b></div>
										</div>
									  </div>
									  <div class="item-inner" style="background: #fff; padding-left:15px;">
										<div class="item-title-row" style="color:#000">
										  <div class="item-title"><b>'.$row->nama.'</b></div>
										</div>
										<hr size="5" style="border-color:#fff; background:#00B086;">
										<div class="item-subtitle" style="color:#7D7975; font-size:10pt;">
											'.$row->no_ktp.'<br>
											'.$row->pekerjaan.'
										</div>
									  </div>
									</li>
								  </ul>
								  </ul>
								</div>
							  </div>
							</div>';
							
				$res[] = array(
					'id' => $row->id,
					'excerpt' => $card,
					'page' => $pg
				);
			}
			//
			return $res;
		}
		else
		{
			return false;
		}
	}
	
	/* ============== */
	
	function get_pemohon_detail($id = null)
	{
		$this->db->select('rikkes_pemohon.*, all_kabkot.nama_kabkot AS tpt_lahir');
		$this->db->join('all_kabkot', 'rikkes_pemohon.tempat_lahir=all_kabkot.id_kabkot');
		$this->db->where('rikkes_pemohon.id', $id);
		$this->db->limit(1);
		$q = $this->db->get('rikkes_pemohon');
		if($q->num_rows() > 0)
		{
			$row = $q->row();
			
			$arr_foto = $this->get_pemohon_foto($row->id);
			$photo_foto = '{url: \''.$arr_foto["foto"].'\', caption: \''.$row->nama.'\'}';
			$photo_ktp = '{url: \''.$arr_foto["ktp"].'\', caption: \'No.KTP: '.$row->no_ktp.'\'}';
			$photo_pop = '['.$photo_foto.','.$photo_ktp.']';
			
			if($row->tgl_lahir != null && $row->tgl_lahir != '' && $row->tgl_lahir != '0000-00-00'){
				$arr_tgl = explode('-', $row->tgl_lahir);
				$th = $arr_tgl[0];
				$this_year = date('Y');
				$umur = intval($this_year) - intval($th);
				$umur = $umur.' th';
			}else{
				$umur = '0 th';
			}
			
			$kelamin = $row->kelamin == 'L' ? 'Laki-laki' : 'Perempuan';
			
			$konten = '<div class="header-profile">
								<div class="row wrap">
									<div class="col-100">
										<div class="ava-profil rounded" onClick="showMyPhotoBrowserArr('.$photo_pop.')">
											<img class="img-responsive rounded" src="'.$arr_foto["foto"].'" style="height:75px; width:75px;">
										</div>
										<div class="profil-feature">
											<h3>'.$row->nama.'</h3>
											<p class="side-desc">'.$umur.'</p>
											<p class="side-desc">'.$row->pekerjaan.'</p>
										</div>
									</div>
								</div>
							</div>
							
							<div class="card bg-white">
								<div class="card-content"> 
									<div class="list-block accordion-list">
										<ul>
											<li class="accordion-item accordion-item-expanded">
												<a href="#" class="item-link item-content">
													<div class="item-inner">
														<div class="item-title"><b>Info Personal</b></div>
													</div>
												</a>
												<div class="accordion-item-content">
													<div class="content-block">
														<div style="padding: 0 4px 15px 15px">
															<h4 style="margin-top:0; margin-bottom:5px;">Tempat Lahir</h4>
															<span>'.$row->tpt_lahir.'</span>
															<h4 style="margin-bottom:5px;">Tgl. Lahir</h4>
															<span>'.convertTanggal($row->tgl_lahir).'</span>
															<h4 style="margin-bottom:5px;">Pendidikan</h4>
															'.$row->pendidikan.'
															<h4 style="margin-bottom:5px;">Jenis Kelamin</h4>
															'.$kelamin.'
															<h4 style="margin-bottom:5px;">Agama</h4>
															'.$row->agama.'
														</div>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>';
			
			$card_rikkes = $this->get_rikkes_card($row->id);
			$konten .= $card_rikkes;
			
			$res = array(
				'id' => $row->id,
				'judul' => 'KTP: '.$row->no_ktp,
				'konten' => $konten
			);
			
			return $res;
		}
		else
		{
			return false;
		}
	}
	
	function get_rikkes_card($id_pemohon){
		$res = '';
		
		$this->db->select('rikkes_pemohon_trx.*, jenis_sim.jenis AS jns_sim');
		$this->db->join('jenis_sim', 'jenis_sim.id=rikkes_pemohon_trx.sim_id');
		$this->db->where('ref_id', $id_pemohon);
		$this->db->order_by('created_time', 'asc');
		$q = $this->db->get('rikkes_pemohon_trx');
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $row)
			{
				$catatan = '';
				if($row->hasil_analisa != '' && $row->hasil_analisa != null){
					$catatan = '<h4 style="margin-bottom:5px;">Catatan</h4>'.nl2br($row->hasil_analisa);
				}
				
				if($row->lulus == 'lulus'){
					$badge = '<span class="badge" style="background:#46BFBD;"><b>Lulus</b></span>';
				}else{
					$badge = '<span class="badge" style="background:#F7464A;"><b>Tidak Lulus</b></span>';
				}
				
				if($row->cacat == '1'){
					$cacat = 'cacat';
				}else{
					$cacat = 'tidak';
				}
				
				$konten = '<div class="card bg-white">
									<div class="card-content"> 
										<div class="list-block accordion-list">
											<ul>
												<li class="accordion-item">
													<a href="#" class="item-link item-content">
														<div class="item-inner">
															<div class="item-title"><b>'.convertTanggal($row->created_time, true).'</b> &nbsp;&nbsp;&nbsp;&nbsp;'.$badge.'</div>
														</div>
													</a>
													<div class="accordion-item-content">
														<div class="content-block">
															<div style="padding: 0 4px 15px 15px">
																<h4 style="margin-top:0; margin-bottom:5px;">Nomor Rikkes</h4>
																'.$row->norikkes.'
																<h4 style="margin-bottom:5px;">Berlaku Sampai</h4>
																'.convertTanggal($row->expired_date, true).'
																<h4 style="margin-bottom:5px;">Untuk Pengajuan SIM</h4>
																'.$row->jns_sim.'
																<h4 style="margin-bottom:5px;">Tinggi</h4>
																'.$row->tinggi.' cm
																<h4 style="margin-bottom:5px;">Berat Badan</h4>
																'.$row->berat.' Kg
																<h4 style="margin-bottom:5px;">Tekanan Darah</h4>
																'.$row->tekanan_darah.' mm Hg
																<h4 style="margin-bottom:5px;">Buta Warna</h4>
																'.$row->buta_warna.'
																<h4 style="margin-bottom:5px;">Cacat</h4>
																'.$cacat.'
																'.$catatan.'
															</div>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>';
								
				$res .= $konten;
			}
			
		}
		return $res;
	}
	
}	