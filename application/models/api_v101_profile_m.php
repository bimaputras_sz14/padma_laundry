<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_v101_profile_m extends CI_Model {
	
	
	function __construct()
    {
        parent::__construct();
		//Load them in the constructor
    }
	
	/* ============== */
	
	function get_profile($email=null,$password=null)
	{
		$this->db->select('pelanggan.*'); 
		$this->db->where('pelanggan.email', $email);
		$this->db->where('pelanggan.password', md5($password)) ;
		$this->db->limit(1);
		$q = $this->db->get('pelanggan');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}
	function get_profile2($id=null )
	{
		$this->db->select('pelanggan.*'); 
		$this->db->where('pelanggan.id', $id); 
		$this->db->limit(1);
		$q = $this->db->get('pelanggan');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}
	
	/* ============== */
	
	function update_profile($email=null,$upd=null)
	{
		$this->db->where('email', $email);
		if($this->db->update('pelanggan', $upd))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/* ============== */
	
	function update_dokter($email=null,$upd=null)
	{
		$this->db->where('email', $email);
		if($this->db->update('rikkes_dokter', $upd))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/* ============== */
	
	function get_myttd($email=null,$kode_akses=null)
	{
		$this->db->select('rikkes_dokter.*, rikkes_apk_users.kode_akses, rikkes_apk_users.password, rikkes_apk_users.status, rikkes_apk_users.foto, rikkes_apk_users.avatar');
		$this->db->join('rikkes_apk_users', 'rikkes_apk_users.ref_id=rikkes_dokter.id');
		$this->db->where('rikkes_apk_users.email', $email);
		$this->db->where('rikkes_apk_users.kode_akses', $kode_akses);
		$this->db->limit(1);
		$q = $this->db->get('rikkes_dokter');
		if($q->num_rows() > 0)
		{
			$row = $q->row();
			
			$dir = 'profile/dokter/ttd/';
			if($row->ttd !== '' && $row->ttd != null){
				$ttd = get_foto_source($dir, $row->ttd);
			
				$konten = '<div class="card bg-white">
									<div class="card-content">
										<img src="'.$ttd.'" class="lazy lazy-fadeIn ks-demo-lazy portrait" alt="image">
									</div>
								</div>';
			}else{
				$konten = '';
			}
			
			$res = array(
				'id' => $row->id,
				'judul' => $row->nama,
				'konten' => $konten
			);
			
			return $res;
		}
		else
		{
			return false;
		}
	}
	
	
	
}	