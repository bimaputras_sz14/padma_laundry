<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_v101_svc_m extends CI_Model {
	
	
	function __construct()
    {
        parent::__construct();
		//Load them in the constructor
    }
	
	/* ============== */
	
	function do_login($email = null)
	{
		$this->db->select('pelanggan.*, pelanggan.kode_akses'); 
		$this->db->where('pelanggan.email', $email);
		$this->db->limit(1);
		$q = $this->db->get('pelanggan');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}


	public function insert_pelanggan($data){  
		$this->db->insert('pelanggan', $data);
		$id   =  $this->db->insert_id();
		return $id; 
	}

	public function insert_penjualan($data){  
		$this->db->insert('penjualan', $data);
		$id   =  $this->db->insert_id();
		return $id; 
	}

	public function insert_penjualan_item($data){  
		$this->db->insert('penjualan_item', $data);
		$id   =  $this->db->insert_id();
		return $id; 
	}

	public function insert_penjualan_photo($data){  
		$this->db->insert('penjualan_photo', $data);
		$id   =  $this->db->insert_id();
		return $id; 
	}

	public function insert_penjualan_note($data){  
		$this->db->insert('penjualan_note', $data);
		$id   =  $this->db->insert_id();
		return $id; 
	}
	
	public function update_penjualan($id, $data){
		$data = $this->db->update('penjualan', $data, array('id' => $id));
		return $data;
	}

	/* ================= */
	
	function get_arr_kabkot()
	{
		$this->db->select('all_kabkot.*, all_provinsi.nama_prov');
		$this->db->join('all_provinsi', 'all_provinsi.id_prov=all_kabkot.id_prov');
		$this->db->order_by('all_kabkot.nama_kabkot', 'asc');
		$q = $this->db->get('all_kabkot');
		$res['id'] = array();
		$res['nama'] = array();
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $r)
			{
				$nama = str_replace('KABUPATEN', 'KAB.', $r->nama_kabkot);
				$res['id'][] = $r->id_kabkot;
				$res['nama'][] = ucwords(strtolower($nama)).' <small>('.ucwords(strtolower($r->nama_prov)).')</small>';
			}
		}
		return $res;
	}
	
	/* ================= */
	
	function get_arr_kelurahan($id_kabkot=null)
	{
		if($id_kabkot != '' && $id_kabkot != null){
			$this->db->select('all_kelurahan.*, all_kecamatan.nama_kec');
			$this->db->join('all_kecamatan', 'all_kecamatan.id_kec=all_kelurahan.id_kec');
			$this->db->where('all_kecamatan.id_kabkot', $id_kabkot);
			$this->db->order_by('all_kelurahan.nama_kel', 'asc');
			$q = $this->db->get('all_kelurahan');
			$res['id'] = array();
			$res['nama'] = array();
			if($q->num_rows() > 0)
			{
				foreach($q->result() as $r)
				{
					$nama = $r->nama_kel;
					$res['id'][] = $r->id_kel;
					$res['nama'][] = ucwords(strtolower($nama)).' <small>(Kec. '.ucwords(strtolower($r->nama_kec)).')</small>';
				}
			}
		}else{
			$res['id'] = array();
			$res['nama'] = array();
		}
		return $res;
	}
	
	 
	/* ================= */	

	function queue_exec(){
		$char = 'ABCDEFGHJKLMNPQRSTUVWXY2345678923456789';
		$queue_no = '';
		
		for($i=0; $i < 6; $i++){
			$pos = rand(0, strlen($char)-1);
			$queue_no .= $char{$pos};
		}
		
		$q = $this->db->query("SELECT * FROM penjualan WHERE no_penjualan='". $queue_no ."'");
		if($q->num_rows() > 0){
			$this->queue_exec();
		}else{
			return $queue_no;
		}
	}
	
	/* ================= */
	
	function get_seq_numer()
	{
		$this->db->order_by('no_penjualan', 'desc'); 
		$this->db->limit(1);
		$q = $this->db->get('penjualan');
		if($q->num_rows() > 0){
			$arr = $q->row();
			$res = intval($arr->sequence_no) + 1;
		}else{
			$res = '100000';
		}
		return $res;
	}
	
	 
	
	function get_hasil_rikkes_detail($id = null)
	{
		$sql = 'rikkes_pemohon_trx.*, rikkes_pemohon.no_ktp, rikkes_pemohon.nama, rikkes_pemohon.kelamin, rikkes_pemohon.tgl_lahir, rikkes_pemohon.pekerjaan, rikkes_pemohon.pendidikan, rikkes_pemohon.agama, all_kabkot.nama_kabkot AS tpt_lahir, jenis_sim.jenis AS jns_sim';
		$this->db->select($sql);
		$this->db->join('rikkes_pemohon', 'rikkes_pemohon.id=rikkes_pemohon_trx.ref_id');
		$this->db->join('all_kabkot', 'rikkes_pemohon.tempat_lahir=all_kabkot.id_kabkot');
		$this->db->join('jenis_sim', 'jenis_sim.id=rikkes_pemohon_trx.sim_id');
		$this->db->where('rikkes_pemohon_trx.id', $id);
		$this->db->limit(1);
		$q = $this->db->get('rikkes_pemohon_trx');
		if($q->num_rows() > 0)
		{
			$row = $q->row();
			
			$dir = 'rikkes/foto/';
			$dir_ktp = 'rikkes/ktp/';
			if($row->foto_ktp !== '' && $row->foto_ktp != null){
				$ktp = get_foto_source($dir_ktp, $row->foto_ktp);
			}else{
				$ktp = 'img/idcard.png';
			}
			if($row->foto_pemohon !== '' && $row->foto_pemohon != null){
				$foto = get_foto_source($dir, $row->foto_pemohon);
			}else{
				$foto = 'img/ava.jpg';
			}
			$photo_foto = '{url: \''.$foto.'\', caption: \''.$row->nama.'\'}';
			$photo_ktp = '{url: \''.$ktp.'\', caption: \'No.KTP: '.$row->no_ktp.'\'}';
			$photo_pop = '['.$photo_foto.','.$photo_ktp.']';
			
			if($row->tgl_lahir != null && $row->tgl_lahir != '' && $row->tgl_lahir != '0000-00-00'){
				$arr_tgl = explode('-', $row->tgl_lahir);
				$th = $arr_tgl[0];
				$this_year = date('Y');
				$umur = intval($this_year) - intval($th);
				$umur = $umur.' th';
			}else{
				$umur = '0 th';
			}
			
			$kelamin = $row->kelamin == 'L' ? 'Laki-laki' : 'Perempuan';
			
			$konten = '<div class="header-profile">
								<div class="row wrap">
									<div class="col-100">
										<div class="ava-profil rounded" onClick="showMyPhotoBrowserArr('.$photo_pop.')">
											<img class="img-responsive rounded" src="'.$foto.'" style="height:75px; width:75px;">
										</div>
										<div class="profil-feature">
											<h3>'.$row->nama.'</h3>
											<p class="side-desc">'.$umur.'</p>
											<p class="side-desc">'.$row->pekerjaan.'</p>
										</div>
									</div>
								</div>
							</div>
							
							<div class="card bg-white">
								<div class="card-content"> 
									<div class="list-block accordion-list">
										<ul>
											<li class="accordion-item">
												<a href="#" class="item-link item-content">
													<div class="item-inner">
														<div class="item-title"><b>Info Personal</b></div>
													</div>
												</a>
												<div class="accordion-item-content">
													<div class="content-block">
														<div style="padding: 0 4px 15px 15px">
															<h4 style="margin-top:0; margin-bottom:5px;">Tempat Lahir</h4>
															<span>'.$row->tpt_lahir.'</span>
															<h4 style="margin-bottom:5px;">Tgl. Lahir</h4>
															<span>'.convertTanggal($row->tgl_lahir).'</span>
															<h4 style="margin-bottom:5px;">Pendidikan</h4>
															'.$row->pendidikan.'
															<h4 style="margin-bottom:5px;">Jenis Kelamin</h4>
															'.$kelamin.'
															<h4 style="margin-bottom:5px;">Agama</h4>
															'.$row->agama.'
														</div>
													</div>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>';
			
				$catatan = '';
				if($row->hasil_analisa != '' && $row->hasil_analisa != null){
					$catatan = '<h4 style="margin-bottom:5px;">Catatan</h4>'.nl2br($row->hasil_analisa);
				}
				
				if($row->lulus == 'lulus'){
					$badge = '<span class="badge" style="background:#46BFBD;"><b>Lulus</b></span>';
				}else{
					$badge = '<span class="badge" style="background:#F7464A;"><b>Tidak Lulus</b></span>';
				}
				
				if($row->cacat == '1'){
					$cacat = 'cacat';
				}else{
					$cacat = 'tidak';
				}
				
				$konten .= '<div class="card bg-white">
									<div class="card-content"> 
										<div class="list-block accordion-list">
											<ul>
												<li class="accordion-item accordion-item-expanded">
													<a href="#" class="item-link item-content">
														<div class="item-inner">
															<div class="item-title"><b>'.convertTanggal($row->created_time, true).'</b> &nbsp;&nbsp;&nbsp;&nbsp;'.$badge.'</div>
														</div>
													</a>
													<div class="accordion-item-content">
														<div class="content-block">
															<div style="padding: 0 4px 15px 15px">
																<h4 style="margin-top:0; margin-bottom:5px;">Nomor Rikkes</h4>
																'.$row->norikkes.'
																<h4 style="margin-bottom:5px;">Berlaku Sampai</h4>
																'.convertTanggal($row->expired_date, true).'
																<h4 style="margin-bottom:5px;">Untuk Pengajuan SIM</h4>
																'.$row->jns_sim.'
																<h4 style="margin-bottom:5px;">Tinggi</h4>
																'.$row->tinggi.' cm
																<h4 style="margin-bottom:5px;">Berat Badan</h4>
																'.$row->berat.' Kg
																<h4 style="margin-bottom:5px;">Tekanan Darah</h4>
																'.$row->tekanan_darah.' mm Hg
																<h4 style="margin-bottom:5px;">Buta Warna</h4>
																'.$row->buta_warna.'
																<h4 style="margin-bottom:5px;">Cacat</h4>
																'.$cacat.'
																'.$catatan.'
															</div>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>';
			
			$res = array(
				'id' => $row->id,
				'judul' => 'KTP: '.$row->no_ktp,
				'konten' => $konten
			);
			
			return $res;
		}
		else
		{
			return false;
		}
	}
	
	/* ================= */
	
	function get_transaksi_list($id_dokter=null,$offset=null,$direction=null,$tgl1=null,$tgl2=null)
	{
		if($tgl1 == null){
			$tgl1 = date('Y-m-d');
		}
		if($tgl2 == null){
			$tgl2 = date('Y-m-d');
		}
		
		$this->db->select('rikkes_pemohon_trx.*, rikkes_pemohon.nama, rikkes_pemohon.tgl_lahir, rikkes_pemohon.pekerjaan');
		$this->db->join('rikkes_pemohon', 'rikkes_pemohon.id=rikkes_pemohon_trx.ref_id');
		$this->db->where('rikkes_pemohon_trx.dokter_id', $id_dokter);
		$this->db->where('rikkes_pemohon_trx.created_time >=', $tgl1.' 00:00:00');
		$this->db->where('rikkes_pemohon_trx.created_time <=', $tgl2.' 23:59:59');
		$this->db->order_by('rikkes_pemohon_trx.created_time', 'desc');
		
		switch($direction)
		{
			case 'older':
				if($offset == null || $offset == ''){
					$offset = 10;
				}
			break;
			default: // newer //
				$offset = 0;
			break;			
		}
		
		$this->db->limit(10, $offset);
		$q = $this->db->get('rikkes_pemohon_trx');
		if($q->num_rows() > 0)
		{
			$res = array();
			$pg = $offset + 10;
			
			foreach($q->result() as $row)
			{
				if($row->amount != null && $row->amount != '' && $row->amount != '0'){
					$amount = 'Rp. '.number_format($row->amount).',-';
				}else{
					$amount = 'Rp. 0,-';
				}
				
				if($row->pembayaran == '1'){
					$badge = '<span class="badge" style="background:#46BFBD;"><b>success</b></span>';
				}else{
					$badge = '<span class="badge" style="background:#F7464A;"><b>pending</b></span>';
				}
				
				$card = '<div class="list-block media-list bg-white" style="margin-top:0;margin-bottom:0;">
								<ul><li><a href="javascript: showDetailTransaksi('.$row->id.')" class="item-link item-content">
									<div class="item-inner">
									  <div class="item-title-row">
										<div class="item-title">'.$row->norikkes.' / '.$row->nama.'</div>
										<div class="item-after">'.$badge.'</div>
									  </div>
									  <div class="item-text">'.convertTanggal($row->created_time, true).' '.date('H:i',strtotime($row->created_time)).' &nbsp;&nbsp;&nbsp; <i>'.$amount.'</i></div>
									</div>
								</a></li></ul>
							</div>';
							
				$res[] = array(
					'id' => $row->id,
					'excerpt' => $card,
					'page' => $pg
				);
			}
			//
			return $res;
		}
		else
		{
			return false;
		}
	}
	
	 
	/* ================= */
	
	function get_pending_list($id_dokter=null,$offset=null,$direction=null)
	{
		$this->db->select('rikkes_pemohon_trx.*, rikkes_pemohon.nama, rikkes_pemohon.tgl_lahir, rikkes_pemohon.pekerjaan');
		$this->db->join('rikkes_pemohon', 'rikkes_pemohon.id=rikkes_pemohon_trx.ref_id');
		$this->db->where('rikkes_pemohon_trx.dokter_id', $id_dokter);
		$this->db->where('rikkes_pemohon_trx.pembayaran', '0');
		$this->db->order_by('rikkes_pemohon_trx.created_time', 'desc');
		
		switch($direction)
		{
			case 'older':
				if($offset == null || $offset == ''){
					$offset = 10;
				}
			break;
			default: // newer //
				$offset = 0;
			break;			
		}
		
		$this->db->limit(10, $offset);
		$q = $this->db->get('rikkes_pemohon_trx');
		if($q->num_rows() > 0)
		{
			$res = array();
			$pg = $offset + 10;
			
			foreach($q->result() as $row)
			{
				if($row->amount != null && $row->amount != '' && $row->amount != '0'){
					$amount = 'Rp. '.number_format($row->amount).',-';
				}else{
					$amount = 'Rp. 0,-';
				}
				
				if($row->pembayaran == '1'){
					$badge = '<span class="badge" style="background:#46BFBD;"><b>success</b></span>';
				}else{
					$badge = '<span class="badge" style="background:#F7464A;"><b>pending</b></span>';
				}
				
				$card = '<div class="list-block media-list bg-white" style="margin-top:0;margin-bottom:0;">
								<ul><li><a href="javascript: showDetailTransaksiPending('.$row->id.')" class="item-link item-content">
									<div class="item-inner">
									  <div class="item-title-row">
										<div class="item-title">'.$row->norikkes.' / '.$row->nama.'</div>
										<div class="item-after">'.$badge.'</div>
									  </div>
									  <div class="item-text">'.convertTanggal($row->created_time, true).' '.date('H:i',strtotime($row->created_time)).' &nbsp;&nbsp;&nbsp; <i>'.$amount.'</i></div>
									</div>
								</a></li></ul>
							</div>';
							
				$res[] = array(
					'id' => $row->id,
					'excerpt' => $card,
					'page' => $pg
				);
			}
			//
			return $res;
		}
		else
		{
			return false;
		}
	}
	 
	/* =================== */
	
	 

	function get_opt_layanan()
	{
		$this->db->order_by('name', 'asc');
		$q = $this->db->get('layanan');
		$res = array();
		if($q->num_rows() > 0)
		{
			$res = '<select name="jenis_layanan" id="opt_jenis_layanan"><option value="">-Pilih Layanan-</option>';
			foreach($q->result() as $r)
			{
				$res .= '<option value="'.$r->id.'">'.$r->name.'</option>';
			}
		}
		$res.='</select>';
		return $res;
	}

	function get_opt_jam_pickup()
	{
		$this->db->order_by('id','asc');
		$q = $this->db->get('jam_pickup');
		$res = array();
		if($q->num_rows() > 0)
		{
			$res = '<select name="time_pickup" id="opt_time_pickup"><option value="">-Pilih Jam-</option>';
			foreach($q->result() as $r)
			{
				$res .= '<option value="'.$r->id.'">'.$r->keterangan.'</option>';
			}
		}
		$res.='</select>';
		return $res;
	}
	

	function get_opt_teknik_cucian()
	{
		$this->db->order_by('id', 'asc');
		$q = $this->db->get('teknik_cucian');
		$res = array();
		if($q->num_rows() > 0)
		{
			$res = '<select name="jenis_cucian" id="opt_jenis_cucian"><option value="">-Pilih Teknik-</option>';
			foreach($q->result() as $r)
			{
				$res .= '<option value="'.$r->id.'">'.$r->name.'</option>';
			}
		}
		$res.='</select>';
		return $res;
	}
	/* ================= */

	function get_product()
	{
		$this->db->order_by('nama_produk', 'asc');
		$q = $this->db->get('produk');
		$res = array();
		if($q->num_rows() > 0)
		{
			$res = '';
			foreach($q->result() as $r)
			{
				$name = $r->nama_produk;
				$short_name = strtolower(str_replace(' ','', $r->nama_produk));
				$res .= '<div class="row margin-top-20">
	                       <div class="col-50">
	                          <div class="cartpage-image">
	                            <div class="product-quantity"><h5>'.$r->nama_produk.'</h5></div>
	                          </div>
	                       </div>
	                       <div class="col-50"> 
	                          <div class="row">
	                             <div class="col-45"><a href="javascript: minus(\'qty_'.$r->id.'\',\'show_note_'.$r->id.'\',\'show_camera_'.$r->id.'\',\'qty_prod_'.$r->id.'\')"> <button class="button"><i class="fa fa-minus-circle"></i></button> </a></div>
	                             <div class="col-10" style=" padding-top: 0; font-size: 21px; font-weight: bold; "><span id="qty_'.$r->id.'">0</span></div>
	                             <div class="col-45"><a href="javascript: plus(\'qty_'.$r->id.'\',\'show_note_'.$r->id.'\',\'show_camera_'.$r->id.'\',\'qty_prod_'.$r->id.'\')"> <button class="button"><i class="fa fa-plus-circle"></i></button> </a></div> 
	                          </div>
	                       </div>
	                       <div class="col-80"><textarea class="inputtype note" name="'.$r->id.'" id="show_note_'.$r->id.'" style="display: none;"></textarea> </div> 
	                       <div class="col-20" style="width: 20%;"><a href="javascript: GoCamera(\'qty_'.$r->id.'\','.$r->id.')" style="display: none;" id="show_camera_'.$r->id.'"> <button class="button"><i class="fa fa-camera"></i></button> </a></div>
	                    </div> <input type="hidden" value="0" name="qty_prod[]['.$r->id.']" id="qty_prod_'.$r->id.'"> ';
			}
		}
		return $res;
	}
	/* ================= */

	function get_hist_order($status=null,$id_pel)
	{
		$this->db->order_by('a.id', 'desc');
		$this->db->join('pelanggan b','b.id=a.pelanggan_id');
		$this->db->join('layanan c','c.id=a.jenis_layanan');
		$this->db->join('teknik_cucian d','d.id=a.jenis_cucian');	
		$this->db->join('jam_pickup e','e.id=a.time_pickup');	
		$this->db->join('status_order f','f.id=a.status');	
		$this->db->select('a.*,b.nama_pelanggan,c.name as nama_layanan,e.keterangan as jam_pickup ,d.name as teknik_cucian,f.nama as status');
		$this->db->where('a.pelanggan_id',$id_pel);
		if($status==0){
			$this->db->where('a.status !=',10);
		}else{
			$this->db->where('a.status',10);
		}
		
		$q = $this->db->get('penjualan a');
		$res = array();
		if($q->num_rows() > 0)
		{
			$res = '';
			foreach($q->result() as $r)
			{
				 
				$res .= ' <div class="card facebook-card">
		                     <div class="card-content">
		                        <div class="card-content-inner">
		                           <p>NO WO : '.$r->no_penjualan.' </p>
		                           <hr>
		                           <a href="#" class="open-slider-modal">  </a>
		                            
		                           <p>Tanggal Masuk : '.$r->tanggal_penjualan.'</p> 
		                           <p>Jenis Layanan : '.$r->nama_layanan.'</p>
		                           <p>Teknik Cucian : '.$r->teknik_cucian.'</p>
		                           <p>Berat Timbangan : 10 Kg</p>
		                           <hr>
		                           <p><h4>Status : '.$r->status.'</h4></p>
		                           
		                        </div>
		                     </div> 
		                  </div> ';
			}
		}
		return $res;
	}
	/* ================= */
	
}	