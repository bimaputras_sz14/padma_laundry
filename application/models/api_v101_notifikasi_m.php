<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_v101_notifikasi_m extends CI_Model {
	
	
	function __construct()
    {
        parent::__construct();
		//Load them in the constructor
    }
	
	/* ================= */
	
	function get_list_notifikasi($email=null,$timestamp=null,$limit=5,$direction=null)
	{
		if($email != null && $email != ''){
			$this->db->where('email', $email);
		}
		
		$this->db->order_by('timestamp', 'desc');
		//
		switch($direction)
		{
			case 'older':
				if($timestamp != null)
				{
					$this->db->where('timestamp <', $timestamp); 
				}
				
			break;
			default: // newer //
			
			break;			
		}		
		$this->db->limit($limit);
		$q = $this->db->get('notifikasi');
		if($q->num_rows() > 0)
		{
			$res = array();
			//
			foreach($q->result() as $row)
			{
				$waktu = humanTiming(strtotime($row->timestamp));
				$arr_waktu = explode(' ', $waktu);
				
				if($row->read == '0'){
					$bg = 'bg-white';
				}else{
					$bg = '';
				}
				
				if(strlen($row->pesan) > 40){
					$pesan = substr($row->pesan, 0, 40).'...';
				}else{
					$pesan = $row->pesan;
				}
				
				$text = '<div class="list-block media-list '.$bg.'" style="margin-top:0;margin-bottom:0;">
								<ul><li><a href="javascript: showDetailNotifikasi('.$row->id.')" class="item-link item-content">
									<!--<div class="item-media"><img src="http://lorempixel.com/160/160/people/1" width="80"/></div>-->
									<div class="item-inner">
									  <div class="item-title-row">
										<div class="item-title">'.$row->judul.'</div>
										<div class="item-after">'.$arr_waktu[0].'&nbsp; <small>'.$arr_waktu[1].'</small></div>
									  </div>
									  <!--<div class="item-subtitle">Beatles</div>-->
									  <div class="item-text">'.$pesan.'</div>
									</div>
								</a></li></ul>
							</div>';
				
				$res[] = array(
					'id' => $row->id,
					'text' => $text,
					'timestamp' => $row->timestamp
				);
			}
			//
			return $res;
		}
		else
		{
			return false;
		}
	}
	
	function get_jml_unread_notif($email){
		$this->db->where('email', $email);
		$this->db->where('read', '0');
		$q = $this->db->get('notifikasi');
		if($q->num_rows() > 0){
			return $q->num_rows();
		}else{
			return false;
		}
	}
	
	/* ==================== */
	
	function get_notifikasi_detail($id=null)
	{
		$this->db->where('id', $id);
		$q = $this->db->get('notifikasi');
		if($q->num_rows() > 0)
		{
			$row = $q->row();
			
			if($row->read == '0'){
				$updated = true;
				$upd = array(
					'read' => 1
				);
				$this->update_data_notifikasi($row->id, $upd);
			}else{
				$updated = '';
			}
			
			$judul = humanTiming(strtotime($row->timestamp)).' yang lalu';
			
			$card = '<div class="card">
							<div class="card-content"> 
								<div class="card-content-inner">'.$row->pesan.'</div>
							</div>
						</div>';
			
			if($id != '')
			{
				$konten = $card;
				
				$res = array(
					'id' => $id,
					'judul' => $judul,
					'konten' => $konten,
					'updated' => $updated
				);
			
				return $res;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
	/* ============================================== */
	
	function update_data_notifikasi($id,$upd)
	{
		$this->db->where('id', $id);
		$this->db->update('notifikasi', $upd);
		return true;
	}
	
	
	
}	