<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pelanggan_model extends CI_Model {
	
	// The following method prevents an error occurring when $this->data is modified.
	// Error Message: 'Indirect modification of overloaded property Demo_cart_admin_model::$data has no effect'.
	public function &__get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}
	
	public function get_pelanggan(){
		$query = $this->db->select('*')->from('pelanggan')->where('fl_prospek',0);
		return $query->get();
	}

	public function get_pelanggan_prospek(){
		$query = $this->db->select('*')->from('pelanggan')->where('fl_prospek',1);
		return $query->get();
	}

	public function get_pelanggan_all(){
		$query = $this->db->select('*')->from('pelanggan');
		return $query->result();
	}
	
	public function add_pelanggan($data){  
		$this->db->insert('pelanggan', $data);
		$id   =  $this->db->insert_id();
		return $id; 
	}

	public function add_pelanggan_transaksi_token($data){  
		$this->db->insert('pelanggan_token_transaksi', $data);
		$id   =  $this->db->insert_id();
		return $id; 
	}

	public function get_pelanggan_token_transaksi($pelanggan_id){ 
		$query	= $this->db->select('sum(a.total) as total,a.produk_id, a.kelas_id,b.name ')
					->from('pelanggan_token_transaksi a')
					->join('kelas b','b.id = a.kelas_id') 
					->where('pelanggan_id',$pelanggan_id) ;
		$q = $query->get();
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}

	public function get_pelanggan_attendence($pelanggan_id){ 
		$query	= $this->db->select('a.*,b.* ')
					->from('pelanggan_attendence a')
					->join('pelanggan b','b.id = a.pelanggan_id') 
					->where('pelanggan_id',$pelanggan_id) ;
		$q = $query->get();
		if($q->num_rows() > 0)
		{
			return $q->result();
		}
		else
		{
			return false;
		}
	}
 
	public function add_attendence($data){  
		$this->db->insert('pelanggan_attendence', $data);
		$id   =  $this->db->insert_id();
		return $id; 
	}

	public function edit_attendence($id, $data){
		$data = $this->db->update('pelanggan_attendence', $data, array('id' => $id));
		return $data;
	}
	
	public function edit_pelanggan($id, $data){
		$data = $this->db->update('pelanggan', $data, array('id' => $id));
		return $data;
	}
	
	public function find_pelanggan($keyword=null)
	{	
		$q = "select * from pelanggan where nama_pelanggan like '%".$keyword."%' or no_kartu like '%".$keyword."%' "; 
		$query = $this->db->query($q);
		return $query ;
	}

	public function get_pelanggan_detail($id=null)
	{	
		if($id!=null){
				$this->db->where('id', $id);
		}
		$q = $this->db->get('pelanggan');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}
	
	public function delete_pelanggan($id){
		$this->db->where('id', $id);
		if($this->db->delete('pelanggan'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
}
