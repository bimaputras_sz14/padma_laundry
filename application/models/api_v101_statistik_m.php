<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_v101_statistik_m extends CI_Model {
	
	
	function __construct()
    {
        parent::__construct();
		//Load them in the constructor
    }
	
	/* ============== */
	
	function get_jml_rikkes($id_dokter=null,$tgl=null)
	{
		if($id_dokter == null || $id_dokter == '' || $tgl == null || $tgl == ''){
			return 0;
		}else{
			$this->db->where('rikkes_pemohon_trx.dokter_id', $id_dokter); 
			$this->db->like('rikkes_pemohon_trx.created_time', $tgl, 'after');
			$q = $this->db->get('rikkes_pemohon_trx');
			
			$jml = $q->num_rows();
			
			return $jml;
		}
	}
	
	/* ============== */
	
	function get_jml_transaksi($id_dokter=null,$tgl=null)
	{
		if($id_dokter == null || $id_dokter == '' || $tgl == null || $tgl == ''){
			return 0;
		}else{
			$this->db->select('SUM(amount) AS jml'); 
			$this->db->where('rikkes_pemohon_trx.dokter_id', $id_dokter); 
			$this->db->like('rikkes_pemohon_trx.created_time', $tgl, 'after');
			$q = $this->db->get('rikkes_pemohon_trx');
			
			$row = $q->row();
			$jml = $row->jml;
			
			return $jml;
		}
	}
	
	/* ============== */
	
	function get_dashboard_top($id_dokter)
	{
		$bln = date('Y-m');
		$tgl = date('Y-m-d');
		
		$bulan_ini = $this->get_jml_rikkes($id_dokter,$bln);
		$bulan_ini = number_format($bulan_ini,0,",",".");
		$legend_bulan_ini = $this->get_jml_rikkes_legend($id_dokter, $bln);
		
		$hari_ini = $this->get_jml_rikkes($id_dokter,$tgl);
		$hari_ini = number_format($hari_ini,0,",",".");
		$legend_hari_ini = $this->get_jml_rikkes_legend($id_dokter, $tgl);
		
		$res = '<div class="card">
					  <div class="card-content"> 
						<div class="list-block accordion-list">
						  <ul>
							<li class="accordion-item"><a href="#" class="item-link item-content">
								<div class="item-inner"> 
								  <div class="item-title"><b>Rikkes Bulan Ini</b></div><span class="badge" style="background:#46BFBD;"><b>'.$bulan_ini.'</b></span>
								</div></a>
								<div class="accordion-item-content">
									<div class="content-block">'.$legend_bulan_ini.'<br></div>
								</div>
							</li>
						  </ul>
						  </ul>
						</div>
					  </div>
					</div>
					<div class="card">
					  <div class="card-content"> 
						<div class="list-block accordion-list">
						  <ul>
							<li class="accordion-item"><a href="#" class="item-link item-content">
								<div class="item-inner"> 
								  <div class="item-title"><b>Rikkes Hari Ini</b></div><span class="badge" style="background:#46BFBD;"><b>'.$hari_ini.'</b></span>
								</div></a>
								<div class="accordion-item-content">
									<div class="content-block">'.$legend_hari_ini.'<br></div>
								</div>
							</li>
						  </ul>
						  </ul>
						</div>
					  </div>
					</div>';
		
		return $res;
	}
	
	/* ============== */
	
	function get_dashboard_transaksi_top($id_dokter)
	{
		$bln = date('Y-m');
		$tgl = date('Y-m-d');
		
		$bulan_ini = $this->get_jml_transaksi($id_dokter,$bln);
		$bulan_ini = number_format($bulan_ini,0,",",".");
		$legend_bulan_ini = $this->get_jml_transaksi_legend($id_dokter, $bln);
		
		$hari_ini = $this->get_jml_transaksi($id_dokter,$tgl);
		$hari_ini = number_format($hari_ini,0,",",".");
		$legend_hari_ini = $this->get_jml_transaksi_legend($id_dokter, $tgl);
		
		$res = '<div class="card">
					  <div class="card-content"> 
						<div class="list-block accordion-list">
						  <ul>
							<li class="accordion-item"><a href="#" class="item-link item-content">
								<div class="item-inner"> 
								  <div class="item-title"><b>Transaksi Bulan Ini</b></div><span class="badge" style="background:#46BFBD;"><b>Rp. '.$bulan_ini.'</b></span>
								</div></a>
								<div class="accordion-item-content">
									<div class="content-block">'.$legend_bulan_ini.'<br></div>
								</div>
							</li>
						  </ul>
						  </ul>
						</div>
					  </div>
					</div>
					<div class="card">
					  <div class="card-content"> 
						<div class="list-block accordion-list">
						  <ul>
							<li class="accordion-item"><a href="#" class="item-link item-content">
								<div class="item-inner"> 
								  <div class="item-title"><b>Transaksi Hari Ini</b></div><span class="badge" style="background:#46BFBD;"><b>Rp. '.$hari_ini.'</b></span>
								</div></a>
								<div class="accordion-item-content">
									<div class="content-block">'.$legend_hari_ini.'<br></div>
								</div>
							</li>
						  </ul>
						  </ul>
						</div>
					  </div>
					</div>';
		
		return $res;
	}
	
	/* ============== */
	
	function get_dashboard_chart1($id_team)
	{
		$tgl1 = date('Y-m-d', strtotime( ' -6 day' ) );
		$tgl2 = date('Y-m-d');
		
		$qry = 'SELECT SUBSTR(created_time,1,10) AS tgl,
					SUM(IF(lulus = "lulus", 1,0)) AS lulus,
					SUM(IF(lulus = "tidak", 1,0)) AS tidak
					FROM rikkes_pemohon_trx 
					WHERE DATE(created_time) BETWEEN "'.$tgl1.'" AND "'.$tgl2.'"
					AND dokter_id="'.$id_team.'"
					GROUP BY SUBSTR(created_time,1,10)
					ORDER BY SUBSTR(created_time,1,10)';
		
		$q = $this->db->query($qry);
		
		$lulus = array();
		$tidak = array();
		$arr_tgl = array();
		$arr_lulus = array();
		$arr_tidak = array();
		
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $row)
			{
				$lulus[$row->tgl] = $row->lulus;
				$tidak[$row->tgl] = $row->tidak;
			}
		}
		
		while (strtotime($tgl1) <= strtotime($tgl2)) {
            $tgl = explode("-",$tgl1);
			$arr_tgl[] = $tgl[2];
			
			if(array_key_exists($tgl1,$lulus)){
				$arr_lulus[] = intval($lulus[$tgl1]);
			}else{
				$arr_lulus[] = 0;
			}
			
			if(array_key_exists($tgl1,$tidak)){
				$arr_tidak[] = intval($tidak[$tgl1]);
			}else{
				$arr_tidak[] = 0;
			}
			
            $tgl1 = date ("Y-m-d", strtotime("+1 day", strtotime($tgl1)));
		}
		
		$datasets[] = array(
			'label' => 'Lulus',
			'fillColor' => 'rgba(70,191,189,0)',
			'strokeColor' => 'rgba(70,191,189,1)',
			'pointColor' => 'rgba(70,191,189,1)',
			'pointStrokeColor' => '#fff',
			'pointHighlightFill' => '#fff',
			'pointHighlightStroke' => 'rgba(220,220,220,1)',
			'data' => $arr_lulus
		);
		
		$datasets[] = array(
			'label' => 'Tdk Lulus',
			'fillColor' => 'rgba(247,70,74,0)',
			'strokeColor' => 'rgba(247,70,74,1)',
			'pointColor' => 'rgba(247,70,74,1)',
			'pointStrokeColor' => '#fff',
			'pointHighlightFill' => '#fff',
			'pointHighlightStroke' => 'rgba(151,187,205,1)',
			'data' => $arr_tidak
		);
		
		$res = array(
			'labels' => $arr_tgl,
			'datasets' => $datasets
		);
		
		return $res;
	}
	
	/* ============== */
	
	function get_dashboard_transaksi_chart1($id_team)
	{
		$tgl1 = date('Y-m-d', strtotime( ' -6 day' ) );
		$tgl2 = date('Y-m-d');
		
		$qry = 'SELECT SUBSTR(created_time,1,10) AS tgl,
					SUM(IF(pembayaran = "1", amount,0)) AS success,
					SUM(IF(pembayaran = "0", amount,0)) AS pending
					FROM rikkes_pemohon_trx 
					WHERE DATE(created_time) BETWEEN "'.$tgl1.'" AND "'.$tgl2.'"
					AND dokter_id="'.$id_team.'"
					GROUP BY SUBSTR(created_time,1,10)
					ORDER BY SUBSTR(created_time,1,10)';
		
		$q = $this->db->query($qry);
		
		$success = array();
		$pending = array();
		$arr_tgl = array();
		$arr_success = array();
		$arr_pending = array();
		
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $row)
			{
				$success[$row->tgl] = $row->success;
				$pending[$row->tgl] = $row->pending;
			}
		}
		
		while (strtotime($tgl1) <= strtotime($tgl2)) {
            $tgl = explode("-",$tgl1);
			$arr_tgl[] = $tgl[2];
			
			if(array_key_exists($tgl1,$success)){
				$arr_success[] = intval($success[$tgl1]);
			}else{
				$arr_success[] = 0;
			}
			
			if(array_key_exists($tgl1,$pending)){
				$arr_pending[] = intval($pending[$tgl1]);
			}else{
				$arr_pending[] = 0;
			}
			
            $tgl1 = date ("Y-m-d", strtotime("+1 day", strtotime($tgl1)));
		}
		
		$datasets[] = array(
			'label' => 'Success',
			'fillColor' => 'rgba(70,191,189,0)',
			'strokeColor' => 'rgba(70,191,189,1)',
			'pointColor' => 'rgba(70,191,189,1)',
			'pointStrokeColor' => '#fff',
			'pointHighlightFill' => '#fff',
			'pointHighlightStroke' => 'rgba(220,220,220,1)',
			'data' => $arr_success
		);
		
		$datasets[] = array(
			'label' => 'Pending',
			'fillColor' => 'rgba(247,70,74,0)',
			'strokeColor' => 'rgba(247,70,74,1)',
			'pointColor' => 'rgba(247,70,74,1)',
			'pointStrokeColor' => '#fff',
			'pointHighlightFill' => '#fff',
			'pointHighlightStroke' => 'rgba(151,187,205,1)',
			'data' => $arr_pending
		);
		
		$res = array(
			'labels' => $arr_tgl,
			'datasets' => $datasets
		);
		
		return $res;
	}
	
	/* ============== */
	
	function get_dashboard_chart2($id_dokter)
	{
		$res = array();
		$bln = date('Y-m');
		
		$qry = '(SELECT sim_id, COUNT(sim_id) AS jml
					FROM rikkes_pemohon_trx 
					WHERE created_time LIKE "'.$bln.'%"
					AND dokter_id IN ('.$id_dokter.')
					GROUP BY sim_id) AS trx';
		
		$this->db->join($qry, 'trx.sim_id=jenis_sim.id', 'left');
		$this->db->where_in('jenis_sim.id', array('1','2','3','4','5','6','7','8'));
		$this->db->order_by('jenis_sim.id', 'asc');
		$q = $this->db->get('jenis_sim');
		if($q->num_rows() > 0)
		{
			$legend = '<div class="content-block" style="margin-left:-20px;">';
			$i = 0;
			foreach($q->result() as $row)
			{
				$res['chart'][] = array(
					'value' => $row->jml,
					'color' => $this->getColor($i),
					'highlight' => $this->getHighlight($i),
					'label' => $row->jenis
				);
				
				$jml = number_format($row->jml,0,",",".");
				$legend .= '<div class="chip">
									<div class="chip-media" style="background:'.$this->getColor($i).'; border:#B3B3B3 solid 1px; font-size:8pt;">'.$jml.'</div>
									<div class="chip-label" style="font-size:8pt;">'.$row->jenis.'</div>
								</div><br>';
				
				$i++;
			}
			$legend .= '</div>';
			$res['legend'] = $legend;
		}
		
		return $res;
	}
	
	/* ============== */
	
	function get_jml_anggota_pencapai_target($id_team){
		if($id_team == null || $id_team == ''){
			return 0;
		}else{
			$target = $this->config->item('telusur_target');
			$bln = date('Y-m');
			$arr_team = explode(',',$id_team);
			
			$this->db->select('nrp_petugas, COUNT(id) AS jml'); 
			$this->db->like('tg_sambang', $bln, 'after'); 
			$this->db->where_in('id_team', $arr_team); 
			$this->db->where('status', '1'); 
			$this->db->where('nrp_petugas IS NOT NULL'); 
			$this->db->group_by('nrp_petugas'); 
			$this->db->having('COUNT(id) >= '.$target, false);
			$q = $this->db->get('ranmor_mstbm');
			
			return $q->num_rows();
		}
	}
	
	/* ============== */
	
	function get_legend_penelusuran_per_team($id_team){
		$total_target = 0;
		$total_agt = 0;
		$res = array();
		$res['legend'] = '';
		$res['target'] = $total_target;
		$res['anggota'] = $total_agt;
		
		if($id_team == null || $id_team == ''){
			return $res;
		}else{
			$res_hasil_agt = array();
			$res_hasil_valid = array();
			// $res_hasil_reject = array();
			// $res_hasil_new = array();
			$arr_team = explode(',',$id_team);
			$str_team = '"'.implode( '","', $arr_team).'"';
			
			$bln = date('Y-m');
			
			$qry = '(SELECT id_team, 
							SUM(IF(status = "1", 1,0)) AS valid,
							SUM(IF(status = "2", 1,0)) AS reject,
							SUM(IF(status = "0", 1,0)) AS new
							FROM ranmor_mstbm 
							WHERE nrp_petugas IS NOT NULL 
							AND tg_sambang LIKE "'.$bln.'%" 
							AND id_team IN ('.$str_team.')
							GROUP BY id_team) AS ranmor';
			
			$this->db->select('telusur_akun.id_team, COUNT(telusur_akun.user_id) AS jml_agt, ranmor.valid, ranmor.reject, ranmor.new'); 
			$this->db->join($qry, 'telusur_akun.id_team=ranmor.id_team', 'left');
			$this->db->where_in('telusur_akun.id_team', $arr_team); 
			$this->db->where('telusur_akun.status', '1'); 
			$this->db->where('telusur_akun.flag', 'anggota'); 
			$this->db->where('telusur_akun.kode_wilayah IS NOT NULL'); 
			$this->db->group_by('telusur_akun.id_team'); 
			$this->db->order_by('ranmor.valid', 'desc'); 
			$q = $this->db->get('telusur_akun');
			
			if($q->num_rows() > 0)
			{
				foreach($q->result() as $row)
				{
					$kode = $row->id_team;
					
					if(array_key_exists($kode,$res_hasil_agt)){
						$hasil_agt = $res_hasil_agt[$kode] + $row->jml_agt;
						$hasil_valid = $res_hasil_valid[$kode] + $row->valid;
						// $hasil_reject = $res_hasil_reject[$kode] + $row->reject;
						// $hasil_new = $res_hasil_new[$kode] + $row->new;
					}else{
						$hasil_agt = $row->jml_agt;
						$hasil_valid = $row->valid;
						// $hasil_reject = $row->reject;
						// $hasil_new = $row->new;
					}
					
					$res_hasil_agt[$kode] = $hasil_agt;
					$res_hasil_valid[$kode] = $hasil_valid;
					// $res_hasil_reject[$kode] = $hasil_reject;
					// $res_hasil_new[$kode] = $hasil_new;
				}
			}
			
			
			$this->db->where_in('id', $arr_team);
			$this->db->order_by('nama', 'asc'); 
			$q = $this->db->get('team');
			if($q->num_rows() > 0)
			{
				$legend = '<div class="content-block" style="margin-left:-20px;">';
				$i = 1;
				$arr_hasil = array();
				$arr_persen = array();
				$arr_color = array();
				
				foreach($q->result() as $row)
				{
					if(array_key_exists($row->id,$res_hasil_agt)){
						$hasil_agt = $res_hasil_agt[$row->id];
						$hasil_valid = $res_hasil_valid[$row->id];
					}else{
						$hasil_agt = 0;
						$hasil_valid = 0;
					}
					
					$target = ($hasil_agt * $this->config->item('telusur_target'));
					$total_target += $target;
					$total_agt += $hasil_agt;
					$persen = 0;
					if($hasil_valid > 0){
						$persen = ($hasil_valid / $target) * 100;
					}
					
					if($persen <= 25){
						$color = '#F44336';
					}else if($persen <= 50){
						$color = '#FF9800';
					}else if($persen <= 75){
						$color = '#e0c909';
					}else if($persen <= 90){
						$color = '#8BC34A';
					}else{
						$color = '#4CAF50';
					}
					
					$arr_hasil[$row->nama] = $hasil_valid;
					$arr_persen[$row->nama] = $persen;
					$arr_color[$row->nama] = $color;
				
					$i++;
				}
				
				arsort($arr_persen);
				foreach($arr_persen as $key => $value){
					$value = round($value, 2);
					$value = number_format($value,2,",",".");
					$legend .= '<div class="chip">
											<div class="chip-media" style="background:'.$arr_color[$key].'; border:#B3B3B3 solid 1px; font-size:8pt;">'.$value.'</div>
											<div class="chip-label">'.$key.'</div>
										  </div><br>';
				}
				$legend .= '</div>';
				//
				$res['legend'] = $legend;
				$res['target'] = $total_target;
				$res['anggota'] = $total_agt;
			}
			
			return $res;
		}
	}
	
	/* ============== */
	
	function get_jml_rikkes_legend($id_dokter, $tgl){
		if($id_dokter == null || $id_dokter == ''){
			return '';
		}else{
			$res_hasil_agt = array();
			$res_hasil_valid = array();
			$res = array();
			
			$this->db->select('lulus, COUNT(lulus) AS jml');
			$this->db->like('created_time', $tgl, 'after');
			$this->db->where('dokter_id', $id_dokter); 
			$this->db->group_by('lulus'); 
			$this->db->order_by('lulus', 'desc'); 
			$q = $this->db->get('rikkes_pemohon_trx');
			
			if($q->num_rows() > 0)
			{
				$legend = '<div class="content-block" style="margin-left:-20px;">';
				foreach($q->result() as $row)
				{
					if($row->lulus == 'lulus'){
						$bg = '#8BC34A';
					}else{
						$bg = '#F44336';
					}
					
					$legend .= '<div class="chip">
										<div class="chip-media" style="background:'.$bg.'; border:#B3B3B3 solid 1px; font-size:8pt;">'.$row->jml.'</div>
										<div class="chip-label">'.$row->lulus.'</div>
									</div><br>';
									 
				}
				$legend .= '</div>';
				$res = $legend;
			}
			else
			{
				$res = '';
			}
			return $res;
		}
	}
	
	/* ============== */
	
	function get_jml_transaksi_legend($id_dokter, $tgl){
		if($id_dokter == null || $id_dokter == ''){
			return '';
		}else{
			$res_hasil_agt = array();
			$res_hasil_valid = array();
			$res = array();
			
			$this->db->select('pembayaran, SUM(amount) AS jml');
			$this->db->like('created_time', $tgl, 'after');
			$this->db->where('dokter_id', $id_dokter); 
			$this->db->group_by('pembayaran'); 
			$this->db->order_by('pembayaran', 'desc'); 
			$q = $this->db->get('rikkes_pemohon_trx');
			
			if($q->num_rows() > 0)
			{
				$jml_success = 0;
				$jml_pending = 0;
				foreach($q->result() as $row)
				{
					if($row->pembayaran == '1'){
						$jml_success = $row->jml;
					}else{
						$jml_pending = $row->jml;
					}
				}
				$legend = '<div class="content-block" style="margin-left:-20px;">';
				$legend .= '<div class="chip">
									<div class="chip-media" style="background:#8BC34A; border:#B3B3B3 solid 1px; font-size:8pt;">'.$this->set_short_number($jml_success).'</div>
									<div class="chip-label">success</div>
								</div><br>';
				$legend .= '<div class="chip">
									<div class="chip-media" style="background:#F44336; border:#B3B3B3 solid 1px; font-size:8pt;">'.$this->set_short_number($jml_pending).'</div>
									<div class="chip-label">pending</div>
								</div><br>';
				$legend .= '</div>';
				$res = $legend;
			}
			else
			{
				$res = '';
			}
			return $res;
		}
	}
	
	/* ============== */
	
	function get_dashboard_chart3($id_dokter)
	{
		$tgl1 = date('Y-m', strtotime( ' -3 month' ) );
		$tgl2 = date('Y-m');
		$today = date('Y-m-d');
		
		$qry = 'SELECT SUBSTR(created_time,1,7) AS tgl,
					SUM(IF(lulus = "lulus", 1,0)) AS lulus,
					SUM(IF(lulus = "tidak", 1,0)) AS tidak
					FROM rikkes_pemohon_trx 
					WHERE DATE(created_time) BETWEEN "'.$tgl1.'-01" AND "'.$today.'"
					AND dokter_id="'.$id_dokter.'"
					GROUP BY SUBSTR(created_time,1,7)
					ORDER BY SUBSTR(created_time,1,7)';
			
		$q = $this->db->query($qry);
		
		$lulus = array();
		$tidak = array();
		$arr_tgl = array();
		$arr_lulus = array();
		$arr_tidak = array();
		
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $row)
			{
				$lulus[$row->tgl] = $row->lulus;
				$tidak[$row->tgl] = $row->tidak;
			}
		}
		
		while (strtotime($tgl1) <= strtotime($tgl2)) {
            $tgl = explode("-",$tgl1);
			$arr_tgl[] = convertBulan($tgl[1]);
			$tgl1 = substr($tgl1, 0, 7);
			
			if(array_key_exists($tgl1,$lulus)){
				$arr_lulus[] = intval($lulus[$tgl1]);
			}else{
				$arr_lulus[] = 0;
			}
			
			if(array_key_exists($tgl1,$tidak)){
				$arr_tidak[] = intval($tidak[$tgl1]);
			}else{
				$arr_tidak[] = 0;
			}
			
            $tgl1 = date ("Y-m-d", strtotime("+1 month", strtotime($tgl1)));
		}
		
		$datasets[] = array(
			'label' => 'Lulus',
			'fillColor' => 'rgba(70,191,189,0.4)',
			'strokeColor' => 'rgba(70,191,189,1)',
			'pointColor' => 'rgba(70,191,189,1)',
			'pointStrokeColor' => '#fff',
			'pointHighlightFill' => '#fff',
			'pointHighlightStroke' => 'rgba(220,220,220,1)',
			'data' => $arr_lulus
		);
		
		$datasets[] = array(
			'label' => 'Tdk Lulus',
			'fillColor' => 'rgba(247,70,74,0.4)',
			'strokeColor' => 'rgba(247,70,74,1)',
			'pointColor' => 'rgba(247,70,74,1)',
			'pointStrokeColor' => '#fff',
			'pointHighlightFill' => '#fff',
			'pointHighlightStroke' => 'rgba(151,187,205,1)',
			'data' => $arr_tidak
		);
		
		$res = array(
			'labels' => $arr_tgl,
			'datasets' => $datasets
		);
		
		return $res;
	}
	
	/* ============== */
	
	function get_transaksi_chart3($id_dokter)
	{
		$tgl1 = date('Y-m', strtotime( ' -3 month' ) );
		$tgl2 = date('Y-m');
		$today = date('Y-m-d');
		
		$qry = 'SELECT SUBSTR(created_time,1,7) AS tgl,
					SUM(amount) AS jml
					FROM rikkes_pemohon_trx 
					WHERE DATE(created_time) BETWEEN "'.$tgl1.'-01" AND "'.$today.'"
					AND dokter_id="'.$id_dokter.'"
					GROUP BY SUBSTR(created_time,1,7)
					ORDER BY SUBSTR(created_time,1,7)';
			
		$q = $this->db->query($qry);
		
		$jml = array();
		$arr_tgl = array();
		$arr_jml = array();
		
		if($q->num_rows() > 0)
		{
			foreach($q->result() as $row)
			{
				$jml[$row->tgl] = $row->jml;
			}
		}
		
		while (strtotime($tgl1) <= strtotime($tgl2)) {
            $tgl = explode("-",$tgl1);
			$arr_tgl[] = convertBulan($tgl[1]);
			$tgl1 = substr($tgl1, 0, 7);
			
			if(array_key_exists($tgl1,$jml)){
				$arr_jml[] = intval($jml[$tgl1]);
			}else{
				$arr_jml[] = 0;
			}
			
            $tgl1 = date ("Y-m-d", strtotime("+1 month", strtotime($tgl1)));
		}
		
		$datasets[] = array(
			'label' => 'Jumlah',
			'fillColor' => 'rgba(70,191,189,0.4)',
			'strokeColor' => 'rgba(70,191,189,1)',
			'pointColor' => 'rgba(70,191,189,1)',
			'pointStrokeColor' => '#fff',
			'pointHighlightFill' => '#fff',
			'pointHighlightStroke' => 'rgba(220,220,220,1)',
			'data' => $arr_jml
		);
		
		$res = array(
			'labels' => $arr_tgl,
			'datasets' => $datasets
		);
		
		return $res;
	}
	
	/* ============== */
	
	function getColor($index){
		$col = array("#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360", "#9b59b6", "#4CAF50");
		if($index > 6){
			$idx = $index % 7;
			$res = $col[$idx];
		}else{
			$res = $col[$index];
		}
		return $res;
	}
	
	/* ============== */
	
	function getHighlight($index){
		$col = array("#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774", "#bd8fd0", "#8BC34A");
		if($index > 6){
			$idx = $index % 7;
			$res = $col[$idx];
		}else{
			$res = $col[$index];
		}
		return $res;
	}
	
	/* ============== */
	
	function get_jml_penelusuran_team($id_team=null,$bulan=null,$status='0') //0=belum validasi; 1=valid; 2=tidak valid
	{
		if($id_team == null || $id_team == '' || $bulan == null || $bulan == ''){
			return 0;
		}else{
			$arr_team = explode(',',$id_team);
		
			$this->db->where('ranmor_mstbm.status', $status); 
			$this->db->where_in('ranmor_mstbm.id_team', $arr_team); 
			$this->db->like('ranmor_mstbm.tg_sambang', $bulan, 'after'); 
			$q = $this->db->get('ranmor_mstbm');
			
			$jml = $q->num_rows();
			
			return $jml;
		}
	}
	
	/* ============== */
	
	function get_jml_anggota_team($id_team=null) //0=belum validasi; 1=valid; 2=tidak valid
	{
		if($id_team == null || $id_team == ''){
			return 0;
		}else{
			$arr_team = explode(',',$id_team);
			
			$this->db->where_in('id_team', $arr_team); 
			$this->db->where('status', '1'); 
			$this->db->where('telusur_akun.flag', 'anggota'); 
			$this->db->where('kode_wilayah IS NOT NULL'); 
			$q = $this->db->get('telusur_akun');
			
			$jml = $q->num_rows();
			
			return $jml;
		}
	}
	
	/* ============== */
	
	function set_short_number($num){
		if($num < 1000000){
			$num = round(($num/1000), 1).'rb';
		}else if($num < 1000000000){
			$num = round(($num/1000000), 1).'jt';
		}else if($num < 1000000000000){
			$num = round(($num/1000000000), 1).'M';
		}else if($num < 1000000000000000){
			$num = round(($num/1000000000000), 1).'T';
		}else{
			$num = $num;
		}
		return $num;
	}
	
	
}	