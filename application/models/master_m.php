<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_m extends CI_Model {
	
	
	function show_text($text=null){
		$uraian = str_replace('\r\n', "<br>", $text);
		$uraian = str_replace("\n", "<br>", $uraian);
		$uraian = str_replace('\n', "<br>", $uraian);
		$uraian = str_replace('\\\"', '"', $uraian);
		$uraian = str_replace("\\\'", "'", $uraian);
		$uraian = str_replace('\"', '"', $uraian);
		$uraian = str_replace("\'", "'", $uraian);
		
		return $uraian;
	}
	public function get_table_noresult($table){
		$query = $this->db->select('*')->from($table);
		return $query->get();
	}	
	function get_satwil($kd_satwil=null){
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('kode', $kd_satwil, 'after');
		}
		
		$this->db->where('status', '1');
		$this->db->order_by('nama', 'asc');

		$query = $this->db->get('kewilayahan');
		return $query;
	}
	function get_satwil2($kd_satwil=null){
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('kode', $kd_satwil, 'after');
		}
		
		$this->db->where('status', '1');
		$this->db->where('tipe', 'Polres');
		$this->db->order_by('nama', 'asc');

		$query = $this->db->get('kewilayahan');
		return $query;
	}
	
	    
	
	function get_list_berita($kd_satwil=null){
		$select = "berita.*, apk_users.nama AS nama_agt, apk_users.pangkat "; 
		$this->db->select($select);
		$this->db->from('berita');
		$this->db->join('apk_users', 'apk_users.nrp=berita.upd_nrp', 'left');  
		$this->db->where('berita.status', '1'); 
		$this->db->order_by('berita.upd_time', 'desc'); 
		$query = $this->db->get();
		return $query;
	}
	 
	   
	
	function get_detil_berita($id=null){
		$select = "berita.*, apk_users.nama AS nama_agt, apk_users.pangkat"; 
		$this->db->select($select);
		$this->db->from('berita');
		$this->db->join('apk_users', 'apk_users.nrp=berita.upd_nrp', 'left'); 
		$this->db->where('berita.id', $id); 
		$query = $this->db->get();
		return $query;
	}
	
	   
	function insert_berita($data){
		if($this->db->insert('berita', $data)){
			$id = $this->db->insert_id();
			return $id;
		}
		else
		{
			return false;
		}
	}
	
	  
	function update_berita($data, $id){
		$this->db->where('id', $id);
		if($this->db->update('berita', $data)){
			return true;
		}else{
			return false;
		}
	}
	
	        
	//Assesment

	function get_list_assessment($id=null){
		$select = "assessment.*, personil.nama AS nama_agt, personil.pangkat "; 
		$this->db->select($select);
		$this->db->from('assessment');
		$this->db->join('personil', 'personil.nrp=assessment.upd_by', 'left');  
		
		
		if($id){
			$this->db->where('assessment.id',$id); 
		}else{
			$this->db->where('assessment.publish', '1'); 
			// $this->db->where('assessment.closed',0);
		}
		$this->db->order_by('assessment.closed', 'asc');
		$this->db->order_by('assessment.waktu_end', 'asc'); 

		$query = $this->db->get();
		return $query;
	}
	


	function get_list_assessment_old(){
		$select = "assessment.*, personil.nama AS nama_agt, personil.pangkat "; 
		$this->db->select($select);
		$this->db->from('assessment');
		$this->db->join('personil', 'personil.nrp=assessment.upd_by', 'left');  
		$this->db->where('assessment.closed',1); 
		$this->db->order_by('assessment.waktu_end', 'asc'); 
		$query = $this->db->get();
		return $query;
	}

	function get_list_reg_assessment($id=null){
		$select = "user_assessment.*, apk_users.nama AS nama_agt, apk_users.pangkat, jabatan.jabatan "; 
		$this->db->select($select);
		$this->db->from('user_assessment');
		$this->db->join('apk_users', 'apk_users.nrp=user_assessment.personil_id', 'left'); 
		$this->db->join('jabatan', 'jabatan.id=apk_users.jabatan_id', 'left');  
		if($id){
			$this->db->where('user_assessment.assessment_id',$id); 
		}
		$this->db->order_by('user_assessment.created_at', 'desc'); 
		$query = $this->db->get();
		return $query;
	}

	function get_reg_user($id=null){
		$select = "apk_users.*, jabatan.jabatan "; 
		$this->db->select($select);
		$this->db->from('apk_users'); 
		$this->db->join('jabatan', 'jabatan.id=apk_users.jabatan_id', 'left');  
		if($id){
			$this->db->where('apk_users.nrp',$id); 
		} 
		$query = $this->db->get();
		return $query;
	}

	function check_reg_assessment($id=null,$nrp){
		$select = "user_assessment.*"; 
		$this->db->select($select);
		$this->db->from('user_assessment');  
		if($id){
			$this->db->where('user_assessment.assessment_id',$id); 
			$this->db->where('user_assessment.personil_id',$nrp); 
		} 
		$query = $this->db->get();
		return $query;
	}

	function insert_assessment($data){
		if($this->db->insert('assessment', $data)){
			$id = $this->db->insert_id();
			return $id;
		}
		else
		{
			return false;
		}
	}
	
	  
	 

	function update_assessment($data, $id){
		$this->db->where('id', $id);
		if($this->db->update('assessment', $data)){
			return true;
		}else{
			return false;
		}
	}

	function insert_assessment_stat($data){
		if($this->db->insert('assessment_status', $data)){
			$id = $this->db->insert_id();
			return $id;
		}
		else
		{
			return false;
		}
	}

	function insert_reg_assessment($data){
		if($this->db->insert('user_assessment', $data)){
			$id = $this->db->insert_id();
			return $id;
		}
		else
		{
			return false;
		}
	}

	// get parameter status assessment

	function get_param_status($id=null){
		$select = "status_assessment.*"; 
		$this->db->select($select);
		$this->db->from('status_assessment'); 
		$query = $this->db->get();
		return $query;
	}

	function dashboard_total( $type=null){
		$this->db->select('a.id');

		if($type == 'assessment') {
			$this->db->from('assessment a');
		}elseif($type == 'agenda') {
			$this->db->from('kalender a');
		}else{
			$this->db->from('apk_users a'); 
		} 

		$query = $this->db->get();
		return $query->num_rows();
	}

	function get_total_assessment_reg($id=null){
		$select = "user_assessment.id "; 
		$this->db->select($select);
		$this->db->from('user_assessment');
		$this->db->where('user_assessment.assessment_id',$id);  
		$query = $this->db->get();
		return $query->num_rows(); 
	}
	
	  
	
	
}