<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class console_model extends CI_Model {
	
	
	function show_text($text=null){
		$uraian = str_replace('\r\n', "<br>", $text);
		$uraian = str_replace("\n", "<br>", $uraian);
		$uraian = str_replace('\n', "<br>", $uraian);
		$uraian = str_replace('\\\"', '"', $uraian);
		$uraian = str_replace("\\\'", "'", $uraian);
		$uraian = str_replace('\"', '"', $uraian);
		$uraian = str_replace("\'", "'", $uraian);
		
		return $uraian;
	}

	function get_objek($table, $id){
		$query = "SELECT * FROM ". $table ." WHERE id='$id'";
		$result = $this->db->query($query);
		return $result;
	}

	function get_satwil($kd_satwil=null){
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('kode', $kd_satwil, 'after');
		}
		
		$this->db->where('status', '1');
		$this->db->order_by('nama', 'asc');

		$query = $this->db->get('kewilayahan');
		return $query;
	}

	function get_tipeops(){  
		$this->db->order_by('name', 'asc'); 
		$query = $this->db->get('kategori_operasi');
		return $query;
	}


	function get_satwil2($kd_satwil=null){
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('kode', $kd_satwil, 'after');
		}
		
		$this->db->where('status', '1');
		$this->db->where('tipe', 'Polres');
		$this->db->order_by('nama', 'asc');

		$query = $this->db->get('kewilayahan');
		return $query;
	}
	
	function get_kalender_ops($limit=null, $kd_satwil=null){
		$this->db->select('jadwal_operasi.id, waktu_start, waktu_end, nama_ops,tipe_satwil,publish, kewilayahan.nama AS satwil');
		$this->db->join('kewilayahan', 'kewilayahan.kode=jadwal_operasi.kode_satwil', 'left');
		$this->db->order_by('waktu_start', 'desc');
		$this->db->where('publish',1);
		
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('kode_satwil', $kd_satwil, 'after');
		}
		if(($limit != null) && ($limit != "")){
			$this->db->limit($limit);
		}

		$query = $this->db->get('jadwal_operasi');
		return $query;
	}
	function get_new_jadwal_ops($last_id=null){
		if(($last_id != null) && ($last_id != '')){
			$this->db->where('id >', $last_id);
		}else{
			$this->db->limit(50);
		}
		$this->db->from('jadwal_operasi');
		$query = $this->db->count_all_results();
		return $query;
	}
	
	function get_list_jadwal_operasi($stat=null, $kd_satwil=null,$tipe=null){
		$select = "jadwal_operasi.*, kewilayahan.nama AS satwil";
		
		$this->db->select($select);
		$this->db->from('jadwal_operasi');
		$this->db->join('kewilayahan', 'kewilayahan.kode=jadwal_operasi.kode_satwil', 'left');
		
		if(($kd_satwil!='') && ($kd_satwil!=null)){
			
		}
		
		date_default_timezone_set('Asia/Jakarta');
		$today = date('Y-m-d');
		
		if($stat==0){
			$this->db->where('jadwal_operasi.waktu_end >=', $today.' 00:00:00');
			$this->db->order_by('jadwal_operasi.waktu_start', 'asc');
		}else if($stat==1){
			$this->db->where('jadwal_operasi.waktu_end <=', $today.' 00:00:00');
			$this->db->order_by('jadwal_operasi.waktu_start', 'desc');
		}
		if($tipe=='Polda'){
			// 
			$this->db->where('jadwal_operasi.tipe_satwil =', 'Polda');
			$this->db->where('jadwal_operasi.publish',1);
		}else{
			$this->db->where('jadwal_operasi.upd_satwil', $kd_satwil );	
			$this->db->like('jadwal_operasi.kode_satwil', $kd_satwil,'left' );
		}

		$query = $this->db->get();
		return $query;
	}

	function get_all_jadwal_operasi($stat=null, $kd_satwil=null,$tipe=null){
		$select = "jadwal_operasi.*, kewilayahan.nama AS satwil";
		
		$this->db->select($select);
		$this->db->from('jadwal_operasi');
		$this->db->join('kewilayahan', 'kewilayahan.kode=jadwal_operasi.kode_satwil', 'left');
		
		if(($kd_satwil!='') && ($kd_satwil!=null)){
			
		}
		
		date_default_timezone_set('Asia/Jakarta');
		$today = date('Y-m-d');
		
		 
		if($tipe=='Polda'){
			// 
			$this->db->where('jadwal_operasi.tipe_satwil =', 'Polda');
			$this->db->where('jadwal_operasi.publish',1);
		}else{
			$this->db->where('jadwal_operasi.upd_satwil', $kd_satwil );	
			$this->db->like('jadwal_operasi.kode_satwil', $kd_satwil,'left' );
		}

		$query = $this->db->get();
		return $query;
	}

	function get_total_personil($id){
			$select = "sum(jml_personil) as total_personil";
			$this->db->select($select);
			$this->db->where("ref_jad_ops",$id);
			$this->db->from('jadwal_operasi'); 
			$query = $this->db->get();
		return $query->row()->total_personil;
	}

	function get_list_kewil($id){
			$select = " GROUP_CONCAT( nama ) as nm_wil";
			$this->db->select($select);
			$this->db->where_in("kode",$id);
			$this->db->from('kewilayahan'); 
			$query = $this->db->get();
		return $query->row()->nm_wil;
	}
	function get_list_ops($id){
			$select = " GROUP_CONCAT( id ) as nm_wil";
			$this->db->select($select);
			$this->db->where("ref_jad_ops",$id);
			$this->db->from('jadwal_operasi'); 
			$query = $this->db->get();
		return $query->row()->nm_wil;
	}
	function get_list_operasi_wil($kd_satwil=null,$id=null){
		$select = "jadwal_operasi.*, kewilayahan.nama AS satwil";
		$this->db->join('kewilayahan', 'kewilayahan.kode=jadwal_operasi.kode_satwil', 'left');
		
		$this->db->select($select);
		$this->db->where('jadwal_operasi.publish',1);
		$this->db->from('jadwal_operasi'); 
		
		if(($kd_satwil!='') && ($kd_satwil!=null)){
			$this->db->where('jadwal_operasi.upd_satwil', $kd_satwil ); 
		}

		if(($id!='') && ($id!=null)){
			$this->db->where('jadwal_operasi.id', $id ); 
		}

		
		date_default_timezone_set('Asia/Jakarta');
		$today = date('Y-m-d');
		
		 
		$query = $this->db->get();
		return $query;
	}

	function get_list_operasi_polres($id=null){
		$select = "jadwal_operasi.*, kewilayahan.nama AS satwil";
		$this->db->join('kewilayahan', 'kewilayahan.kode=jadwal_operasi.kode_satwil' );
		
		$this->db->select($select);
		$this->db->where('jadwal_operasi.publish',1);
		$this->db->from('jadwal_operasi'); 
		$this->db->where('jadwal_operasi.ref_jad_ops', $id ); 
		 

		
		date_default_timezone_set('Asia/Jakarta');
		$today = date('Y-m-d');
		
		 
		$query = $this->db->get();
		return $query;
	}
	function get_detil_jadwal_ops($id=null){
		$select = "jadwal_operasi.*, kewilayahan.nama AS satwil, apk_users.nama AS nama_agt, apk_users.pangkat";
		
		$this->db->select($select);
		$this->db->from('jadwal_operasi');
		$this->db->join('kewilayahan', 'kewilayahan.kode=jadwal_operasi.kode_satwil', 'left');
		$this->db->join('apk_users', 'apk_users.nrp=jadwal_operasi.upd_by', 'left');
		$this->db->where('jadwal_operasi.id', $id);
		
		$query = $this->db->get();
		return $query;
	}
	
	function insert_data_agenda($data){
		if($this->db->insert('jadwal_operasi', $data)){
			$id = $this->db->insert_id();
			return $id;
		}
		else
		{
			return false;
		}
	}
	
	function update_jadwal_operasi($data, $id){
		$this->db->where('id', $id);
		if($this->db->update('jadwal_operasi', $data)){
			return true;
		}else{
			return false;
		}
	}
	
	function delete_agenda($id){
		$this->db->where('id', $id);
		if($this->db->delete('jadwal_operasi')){
			return true;
		}else{
			return false;
		}
	}
	
	function get_list_email_receiver(){
		$query = $this->db->get('email_receiver');
		return $query;
	}
	
	function get_list_berita($kd_satwil=null){
		$select = "berita.*, apk_users.nama AS nama_agt, apk_users.pangkat, kewilayahan.nama AS satwil";
		
		$this->db->select($select);
		$this->db->from('berita');
		$this->db->join('apk_users', 'apk_users.nrp=berita.upd_nrp', 'left');
		$this->db->join('kewilayahan', 'kewilayahan.kode=apk_users.kode_wilayah', 'left');
		$this->db->join('kesatuan', 'kesatuan.id=apk_users.id_kesatuan', 'left');
		$this->db->where('berita.status', '1');
		
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('berita.satwil_tujuan', $kd_satwil, 'both');
		}
		
		$this->db->order_by('berita.upd_time', 'desc');

		$query = $this->db->get();
		return $query;
	}
	
	function get_list_himbauan_anggota($kd_satwil=null){
		$select = "info_lalin_anggota.*, apk_users.nama AS nama_agt, apk_users.pangkat, kewilayahan.nama AS satwil";
		
		$this->db->select($select);
		$this->db->from('info_lalin_anggota');
		$this->db->join('apk_users', 'apk_users.nrp=info_lalin_anggota.upd_nrp', 'left');
		$this->db->join('kewilayahan', 'kewilayahan.kode=apk_users.kode_wilayah', 'left');
		$this->db->join('kesatuan', 'kesatuan.id=apk_users.id_kesatuan', 'left');
		$this->db->where('info_lalin_anggota.status', '1');
		
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('info_lalin_anggota.satwil_tujuan', $kd_satwil, 'both');
		}
		
		$this->db->order_by('info_lalin_anggota.upd_time', 'desc');

		$query = $this->db->get();
		return $query;
	}
	
	function get_list_himbauan_publik(){
		$select = "info_lalin_publik.*, apk_users.nama AS nama_agt, apk_users.pangkat, kewilayahan.nama AS satwil";
		
		$this->db->select($select);
		$this->db->from('info_lalin_publik');
		$this->db->join('apk_users', 'apk_users.nrp=info_lalin_publik.upd_nrp', 'left');
		$this->db->join('kewilayahan', 'kewilayahan.kode=apk_users.kode_wilayah', 'left');
		$this->db->join('kesatuan', 'kesatuan.id=apk_users.id_kesatuan', 'left');
		$this->db->where('info_lalin_publik.status', '1');
		
		$this->db->order_by('info_lalin_publik.upd_time', 'desc');

		$query = $this->db->get();
		return $query;
	}
	
	function get_detil_email_receiver($id=null){
		$this->db->where('id', $id);
		$query = $this->db->get('email_receiver');
		return $query;
	}
	
	function cek_email_receiver($email=null){
		$this->db->where('email', $email);
		$q = $this->db->get('email_receiver');
		
		if($q->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function get_detil_berita($id=null){
		$select = "berita.*, kewilayahan.nama AS satwil, apk_users.nama AS nama_agt, apk_users.pangkat";
		
		$this->db->select($select);
		$this->db->from('berita');
		$this->db->join('apk_users', 'apk_users.nrp=berita.upd_nrp', 'left');
		$this->db->join('kewilayahan', 'kewilayahan.kode=apk_users.kode_wilayah', 'left');
		$this->db->where('berita.id', $id);
		
		$query = $this->db->get();
		return $query;
	}
	
	function get_detil_himbauan_anggota($id=null){
		$select = "info_lalin_anggota.*, kewilayahan.nama AS satwil, apk_users.nama AS nama_agt, apk_users.pangkat";
		
		$this->db->select($select);
		$this->db->from('info_lalin_anggota');
		$this->db->join('apk_users', 'apk_users.nrp=info_lalin_anggota.upd_nrp', 'left');
		$this->db->join('kewilayahan', 'kewilayahan.kode=apk_users.kode_wilayah', 'left');
		$this->db->where('info_lalin_anggota.id', $id);
		
		$query = $this->db->get();
		return $query;
	}
	
	function get_detil_himbauan_publik($id=null){
		$select = "info_lalin_publik.*, kewilayahan.nama AS satwil, apk_users.nama AS nama_agt, apk_users.pangkat";
		
		$this->db->select($select);
		$this->db->from('info_lalin_publik');
		$this->db->join('apk_users', 'apk_users.nrp=info_lalin_publik.upd_nrp', 'left');
		$this->db->join('kewilayahan', 'kewilayahan.kode=apk_users.kode_wilayah', 'left');
		$this->db->where('info_lalin_publik.id', $id);
		
		$query = $this->db->get();
		return $query;
	}
	
	function insert_email_receiver($data){
		if($this->db->insert('email_receiver', $data)){
			$id = $this->db->insert_id();
			return $id;
		}
		else
		{
			return false;
		}
	}
	
	function insert_berita($data){
		if($this->db->insert('berita', $data)){
			$id = $this->db->insert_id();
			return $id;
		}
		else
		{
			return false;
		}
	}
	
	function insert_himbauan_anggota($data){
		if($this->db->insert('info_lalin_anggota', $data)){
			$id = $this->db->insert_id();
			return $id;
		}
		else
		{
			return false;
		}
	}
	
	function insert_himbauan_publik($data){
		if($this->db->insert('info_lalin_publik', $data)){
			$id = $this->db->insert_id();
			return $id;
		}
		else
		{
			return false;
		}
	}
	
	function update_email_receiver($data, $id){
		$this->db->where('id', $id);
		if($this->db->update('email_receiver', $data)){
			return true;
		}else{
			return false;
		}
	}
	
	function update_berita($data, $id){
		$this->db->where('id', $id);
		if($this->db->update('berita', $data)){
			return true;
		}else{
			return false;
		}
	}
	
	function update_himbauan_anggota($data, $id){
		$this->db->where('id', $id);
		if($this->db->update('info_lalin_anggota', $data)){
			return true;
		}else{
			return false;
		}
	}
	
	function update_himbauan_publik($data, $id){
		$this->db->where('id', $id);
		if($this->db->update('info_lalin_publik', $data)){
			return true;
		}else{
			return false;
		}
	}
	
	function delete_email_receiver($id){
		$this->db->where('id', $id);
		if($this->db->delete('email_receiver')){
			return true;
		}else{
			return false;
		}
	}
	
	function delete_timeline_publik($id){
		$this->db->where('ref_id', $id);
		if($this->db->delete('info_lalin_publik')){
			return true;
		}else{
			return false;
		}
	}
	
	function insert_timeline_anggota($data){
		if($this->db->insert('timeline_anggota', $data)){
			$id = $this->db->insert_id();
			return $id;
		}
		else
		{
			return false;
		}
	}
	
	function insert_timeline_publik($data){
		if($this->db->insert('timeline_publik', $data)){
			$id = $this->db->insert_id();
			return $id;
		}
		else
		{
			return false;
		}
	}
	
	function get_lap_checkin($tgl=null, $kd_satwil=null, $lvl_id=null){
		$select = "apk_checkin.*, 
						pos.nama AS nama_pos, pos.lokasi AS alamat_pos, pos.lat, pos.lon, pos.foto_map,
						apk_users.nama AS nama_agt, apk_users.pangkat, apk_users.telepon AS tlp_agt, apk_users.email AS email_agt, apk_users.foto AS foto_agt, apk_users.avatar AS avatar_agt, 
						kewilayahan.nama AS satwil, 
						kesatuan.kode AS satuan, kesatuan.marker_icon";
		
		$this->db->select($select);
		$this->db->from('apk_checkin');
		$this->db->join('pos', 'pos.kode_pos=apk_checkin.kode_pos');
		$this->db->join('apk_users', 'apk_users.nrp=apk_checkin.nrp');
		$this->db->join('kewilayahan', 'kewilayahan.kode=apk_users.kode_wilayah');
		$this->db->join('kesatuan', 'kesatuan.id=apk_users.id_kesatuan');
		if($lvl_id!=1){
			$this->db->where('apk_users.group','USER');
		}
		$this->db->where('apk_checkin.checkin_lat IS NOT NULL');
		$this->db->where('apk_checkin.checkin_lon IS NOT NULL');
		$this->db->where('apk_checkin.kode_pos IS NOT NULL');
		
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('kewilayahan.kode', $kd_satwil, 'after');
		}
		
		if(($tgl != null) && ($tgl != '')){
			$this->db->like('apk_checkin.checkin', $tgl, 'after');
		}
		
		$this->db->order_by('apk_checkin.checkin', 'desc');

		$query = $this->db->get();
		return $query;
	}
	
	function data_lap_checkin_pos($awal,$akhir,$kat_pos,$kd_satwil=null){
		  $arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
		// $kd_satwil = $arr_usr['kode_satwil'];
		$lvl_id = $arr_usr['lvl_id']; 
		
		$select = "apk_checkin.*, 
						pos.jenis, pos.nama AS nama_pos, pos.lokasi AS alamat_pos, pos.lat, pos.lon, pos.foto_map,
						apk_users.nama AS nama_agt, apk_users.pangkat, apk_users.telepon AS tlp_agt, apk_users.email AS email_agt, apk_users.foto AS foto_agt, apk_users.avatar AS avatar_agt, 
						kewilayahan.nama AS satwil, 
						kesatuan.kode AS satuan, kesatuan.marker_icon";
		
		$this->db->select($select);
		$this->db->from('apk_checkin');
		$this->db->join('pos', 'pos.kode_pos=apk_checkin.kode_pos');
		$this->db->join('apk_users', 'apk_users.nrp=apk_checkin.nrp');
		$this->db->join('kewilayahan', 'kewilayahan.kode=apk_users.kode_wilayah');
		$this->db->join('kesatuan', 'kesatuan.id=apk_users.id_kesatuan');
		if($lvl_id!=1){
			$this->db->where('apk_users.group','USER');
		}
		$this->db->where('apk_checkin.checkin_lat IS NOT NULL');
		$this->db->where('apk_checkin.checkin_lon IS NOT NULL');
		// $this->db->where('apk_checkin.kode_pos IS NOT NULL');
		
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('kewilayahan.kode', $kd_satwil, 'after');
		}
		
		if($kat_pos != 'all' && $kat_pos != ''){
			$this->db->where("pos.id",$kat_pos);
		}
		
		$this->db->where("apk_checkin.checkin >",$awal." 00:00:00");
		$this->db->where("apk_checkin.checkin <",$akhir." 23:59:00");

		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('apk_checkin.kode_pos', $kd_satwil, 'after');
		}
		
		$this->db->order_by('apk_checkin.checkin', 'desc');

		$query = $this->db->get();
		return $query;
	}
	
	function get_lap_lantas($tgl=null, $kd_satwil=null, $lvl_id=null){
		// $select = "apk_lapinfo.*, apk_users.nama AS nama_agt, apk_users.pangkat, apk_users.telepon AS tlp_agt, apk_users.email AS email_agt, apk_users.foto AS foto_agt, apk_users.avatar AS avatar_agt, kewilayahan.nama AS satwil, kesatuan.kode AS satuan, kesatuan.marker_icon, public_users.nama AS nama_usr, public_users.email AS email_usr, public_users.no_telp AS tlp_usr, public_users.foto AS foto_usr, public_users.avatar AS avatar_usr";
		$select = "apk_lapinfo.*, apk_users.nama AS nama_agt, apk_users.pangkat, apk_users.telepon AS tlp_agt, apk_users.email AS email_agt, apk_users.foto AS foto_agt, apk_users.avatar AS avatar_agt, kewilayahan.nama AS satwil, kesatuan.kode AS satuan, kesatuan.marker_icon";
		
		$this->db->select($select);
		$this->db->from('apk_lapinfo');
		$this->db->join('apk_users', 'apk_users.nrp=apk_lapinfo.reported_by');
		$this->db->join('kewilayahan', 'kewilayahan.kode=apk_users.kode_wilayah');
		$this->db->join('kesatuan', 'kesatuan.id=apk_users.id_kesatuan');
		// $this->db->join('public_users', 'public_users.username=apk_lapinfo.reported_by', 'left');
		if($lvl_id!=1){
			$this->db->where('apk_users.group','USER');
		}
		$this->db->where('apk_lapinfo.lat IS NOT NULL');
		$this->db->where('apk_lapinfo.lon IS NOT NULL');
		$this->db->where('apk_lapinfo.report_type', 'anggota');
		
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('apk_lapinfo.kode_satwil', $kd_satwil, 'after');
		}
		
		if(($tgl != null) && ($tgl != '')){
			$this->db->like('apk_lapinfo.timestamp', $tgl, 'after');
		}
		
		$this->db->order_by('apk_lapinfo.timestamp', 'desc');

		$query = $this->db->get();
		return $query;
	}

	function get_lap_preemtif($tgl=null, $kd_satwil=null, $lvl_id=null){
		// $select = "apk_lapinfo.*, apk_users.nama AS nama_agt, apk_users.pangkat, apk_users.telepon AS tlp_agt, apk_users.email AS email_agt, apk_users.foto AS foto_agt, apk_users.avatar AS avatar_agt, kewilayahan.nama AS satwil, kesatuan.kode AS satuan, kesatuan.marker_icon, public_users.nama AS nama_usr, public_users.email AS email_usr, public_users.no_telp AS tlp_usr, public_users.foto AS foto_usr, public_users.avatar AS avatar_usr";
		$select = "apk_lapinfo.*,kategori_lapinfo.kategori as kategori_lapinfo, apk_users.nama AS nama_agt, apk_users.pangkat, apk_users.telepon AS tlp_agt, apk_users.email AS email_agt, apk_users.foto AS foto_agt, apk_users.avatar AS avatar_agt, kewilayahan.nama AS satwil, kesatuan.kode AS satuan, kesatuan.marker_icon"; 
		$this->db->select($select);
		$this->db->from('apk_lapinfo');
		$this->db->join('kategori_lapinfo', 'kategori_lapinfo.id=apk_lapinfo.kategori');
		$this->db->join('apk_users', 'apk_users.nrp=apk_lapinfo.reported_by');
		$this->db->join('kewilayahan', 'kewilayahan.kode=apk_users.kode_wilayah');
		$this->db->join('kesatuan', 'kesatuan.id=apk_users.id_kesatuan');


		// $this->db->join('public_users', 'public_users.username=apk_lapinfo.reported_by', 'left');
		if($lvl_id!=1){
			$this->db->where('apk_users.group','USER');
		}
		$this->db->where('apk_lapinfo.lat IS NOT NULL');
		$this->db->where('apk_lapinfo.lon IS NOT NULL');
		$this->db->where('apk_lapinfo.report_type', 'anggota');
		
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('apk_lapinfo.kode_satwil', $kd_satwil, 'after');
		}
		
		if(($tgl != null) && ($tgl != '')){
			$this->db->like('apk_lapinfo.timestamp', $tgl, 'after');
		}
		
		$this->db->order_by('apk_lapinfo.timestamp', 'desc');
		$this->db->where("apk_lapinfo.kategori not in ('giat','lalin') ");

		$query = $this->db->get();
		return $query;
	}
	
	function get_timeline($limit=null,$last_id=null){
		$select = "apk_lapinfo.*, apk_users.nama AS nama_agt, apk_users.pangkat, apk_users.telepon AS tlp_agt, apk_users.email AS email_agt, apk_users.foto AS foto_agt, apk_users.avatar AS avatar_agt, kewilayahan.nama AS satwil, kesatuan.kode AS satuan, kesatuan.marker_icon, public_users.nama AS nama_usr, public_users.email AS email_usr, public_users.no_telp AS tlp_usr, public_users.foto AS foto_usr, public_users.avatar AS avatar_usr";
		
		$this->db->select($select);
		$this->db->from('apk_lapinfo');
		$this->db->join('apk_users', 'apk_users.nrp=apk_lapinfo.reported_by', 'left');
		$this->db->join('kewilayahan', 'kewilayahan.kode=apk_users.kode_wilayah', 'left');
		$this->db->join('kesatuan', 'kesatuan.id=apk_users.id_kesatuan', 'left');
		$this->db->join('public_users', 'public_users.username=apk_lapinfo.reported_by', 'left');
		$this->db->where('apk_lapinfo.lat IS NOT NULL');
		$this->db->where('apk_lapinfo.lon IS NOT NULL');
		$this->db->where('apk_lapinfo.report_type', 'anggota');
		
		if(($limit != null) && ($limit != '')){
			$this->db->limit($limit);
		}
		
		if(($last_id != null) && ($last_id != '')){
			$this->db->where('apk_lapinfo.id >', $last_id);
		}
		
		$this->db->order_by('apk_lapinfo.timestamp', 'desc');

		$query = $this->db->get();
		return $query;
	}
	
	function get_new_timeline($last_id=null){
		if(($last_id != null) && ($last_id != '')){
			$this->db->where('id >', $last_id);
		}else{
			$this->db->limit(50);
		}
		$this->db->where('report_type', 'anggota');
		$this->db->from('apk_lapinfo');
		$query = $this->db->count_all_results();
		return $query;
	}
	
	function get_lap_lakalantas($tgl=null, $kd_satwil=null, $lvl_id=null){
		$select = "apk_laka.*, apk_users.nama AS nama_agt, apk_users.pangkat, apk_users.telepon AS tlp_agt, apk_users.email AS email_agt, apk_users.foto AS foto_agt, apk_users.avatar AS avatar_agt, kewilayahan.nama AS satwil, kesatuan.kode AS satuan, kesatuan.marker_icon";
		$this->db->select($select);
		$this->db->from('apk_laka');
		$this->db->join('apk_users', 'apk_users.nrp=apk_laka.reported_by', 'left');
		$this->db->join('kewilayahan', 'kewilayahan.kode=apk_users.kode_wilayah', 'left');
		$this->db->join('kesatuan', 'kesatuan.id=apk_users.id_kesatuan', 'left');
		$this->db->where('apk_laka.lat IS NOT NULL');
		$this->db->where('apk_laka.lon IS NOT NULL');
		if($lvl_id!=1){
			$this->db->where('apk_users.group','USER');
		}
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('apk_laka.kode_satwil', $kd_satwil, 'after');
		}
		if(($tgl != null) && ($tgl != '')){
			$this->db->like('apk_laka.timestamp', $tgl, 'after');
		}
		$this->db->order_by('apk_laka.timestamp', 'desc');
		$query = $this->db->get();
		return $query;
	}
	
	function get_lap_penindakan($tgl=null, $kd_satwil=null, $lvl_id=null){
		$select = "apk_penindakan.*, apk_users.nama AS nama_agt, apk_users.pangkat, apk_users.telepon AS tlp_agt, apk_users.email AS email_agt, apk_users.foto AS foto_agt, apk_users.avatar AS avatar_agt, kewilayahan.nama AS satwil, kesatuan.kode AS satuan, kesatuan.marker_icon, kategori_penindakan.kategori as tindakan";
		
		$this->db->select($select);
		$this->db->from('apk_penindakan');
		$this->db->join('apk_users', 'apk_users.nrp=apk_penindakan.petugas', 'left');
		$this->db->join('kewilayahan', 'kewilayahan.kode=apk_users.kode_wilayah', 'left');
		$this->db->join('kesatuan', 'kesatuan.id=apk_users.id_kesatuan', 'left');
		$this->db->join('kategori_penindakan', 'apk_penindakan.id_kategori=kategori_penindakan.id', 'left');
		$this->db->where('apk_penindakan.lat IS NOT NULL');
		$this->db->where('apk_penindakan.lon IS NOT NULL');
		if($lvl_id!=1){
			$this->db->where('apk_users.group','USER');
		}
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('apk_penindakan.kode_satwil', $kd_satwil, 'after');
		}
		
		if(($tgl != null) && ($tgl != '')){
			$this->db->like('apk_penindakan.timestamp', $tgl, 'after');
		}
		
		$this->db->order_by('apk_penindakan.timestamp', 'desc');

		$query = $this->db->get();
		return $query;
	}
	
	function get_lokasi_anggota($tgl=null, $kd_satwil=null){
		$select = "apk_users.*, kewilayahan.nama AS satwil, kesatuan.kode AS satuan, kesatuan.marker_icon";
		
		$this->db->select($select);
		$this->db->from('apk_users');
		$this->db->join('kewilayahan', 'kewilayahan.kode=apk_users.kode_wilayah', 'left');
		$this->db->join('kesatuan', 'kesatuan.id=apk_users.id_kesatuan', 'left');
		$this->db->where('apk_users.lat IS NOT NULL');
		$this->db->where('apk_users.lon IS NOT NULL');
		$this->db->where('apk_users.group !=', 'NBADMIN');
		
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('apk_users.kode_wilayah', $kd_satwil, 'after');
		}
		
		if(($tgl != null) && ($tgl != '')){
			$this->db->like('apk_users.last_activity', $tgl, 'after');
		}
		
		$this->db->order_by('apk_users.nama', 'desc');

		$query = $this->db->get();
		return $query;
	}
	
	function get_jml_tilang($tgl=null, $kd_satwil=null,$format=null){
		$this->db->select('id');
		$this->db->from('apk_penindakan');
		// $this->db->where('tindakan', 'tilang');
		
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('kode_satwil', $kd_satwil, 'after');
		}
		
		if(($tgl != null) && ($tgl != '')){
			if($format=='day'){
				$this->db->like('timestamp', $tgl, 'after');	
			}elseif($format=='month'){
				$this->db->where('MONTH(timestamp)',$tgl);
			}else{
				$this->db->where('YEAR(timestamp)',$tgl);
				// $this->db->like('a.timestamp', $tgl, 'after');
					
			}
		}

		$query = $this->db->get();
		return $query->num_rows();
	}
	
	function jml_lakalantas($tgl=null, $kd_satwil=null,$format=null){
		$this->db->select('id');
		$this->db->from('apk_laka');
		
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('kode_satwil', $kd_satwil, 'after');
		}
		
		if(($tgl != null) && ($tgl != '')){
			if($format=='day'){
				$this->db->like('timestamp', $tgl, 'after');	
			}elseif($format=='month'){
				$this->db->where('MONTH(timestamp)',$tgl);
			}else{
				$this->db->where('YEAR(timestamp)',$tgl);
				// $this->db->like('a.timestamp', $tgl, 'after');
					
			}
		}

		$query = $this->db->get();
		return $query->num_rows();
	}
	function get_jml_checkin($tgl=null, $kd_satwil=null,$format=null){
		$this->db->select('id');
		$this->db->from('apk_checkin');
		// $this->db->where('tindakan', 'tilang');
		
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('kode_pos', $kd_satwil, 'after');
		}
		
		if(($tgl != null) && ($tgl != '')){
			if($format=='day'){
				$this->db->like('checkin', $tgl, 'after');	
			}elseif($format=='month'){
				$this->db->where('MONTH(checkin)',$tgl);
			}else{
				$this->db->where('YEAR(checkin)',$tgl);
				// $this->db->like('a.timestamp', $tgl, 'after');
					
			}
		}

		$query = $this->db->get();
		return $query->num_rows();
	}
	
	function jml_lalin_ty($tgl=null, $kd_satwil=null,$format=null){
		$this->db->select('a.id');
		$this->db->where("a.report_type",'anggota');
		$this->db->where('b.group', 'USER');
		$this->db->join("apk_users b","b.nrp=a.reported_by");
		$this->db->from('apk_lapinfo a');
		
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('a.kode_satwil', $kd_satwil, 'after');
		}
		
		if(($tgl != null) && ($tgl != '')){
			if($format=='day'){
				$this->db->like('a.timestamp', $tgl, 'after');	
			}elseif($format=='month'){
				$this->db->where('MONTH(a.timestamp)',$tgl);
			}else{
				$this->db->where('YEAR(a.timestamp)',$tgl);
				// $this->db->like('a.timestamp', $tgl, 'after');
					
			}
		}

		$query = $this->db->get();
		return $query->num_rows();
	}

	function jml_Tindakan($jenis=null,$satwil=null){ 
		$month = date('m');
		if($jenis=='jen_tindakan'){
			$select = "select a.kategori ,
			             ( select count(b.id_kategori) from apk_penindakan b where b.kode_satwil like'$satwil%' and month(`timestamp`) = '$month' and a.id = b.id_kategori)as jumlah 
						 from kategori_penindakan a ";
		}elseif($jenis=='jen_kendaraan'){ 
			$select = "select b.name ,count(a.jenis_kendaraan) as jumlah
					 from apk_penindakan a   
					 join  kategori_kendaraan b on b.id = a.jenis_kendaraan 
					 where a.kode_satwil like'$satwil%'  and month(`timestamp`) = '$month'
					 GROUP BY a.jenis_kendaraan;"; 
		}elseif($jenis=='jen_lokasi'){
			$select = "select b.name ,count(a.jenis_lokasi2) as jumlah
						 from apk_penindakan a   
						 join kategori_lokasi_sub b on b.id = a.jenis_lokasi2
						 where a.kode_satwil like'$satwil%'  and month(`timestamp`) = '$month' 
						 GROUP BY a.jenis_lokasi2;"; 
		}elseif($jenis=='jen_kelamin'){
			$select = "select a.jk ,count(a.jk) as jumlah
					 from apk_penindakan a   
					 where a.kode_satwil like'$satwil%'  and month(`timestamp`) = '$month'
					 GROUP BY a.jk;"; 
		}elseif($jenis=='jen_usia'){
			$select = "select b.name ,count(a.kategori_umur) as jumlah
						 from apk_penindakan a   
						 join  kategori_usia b on b.id = a.kategori_umur 
						 where a.kode_satwil like'$satwil%' and month(`timestamp`) = '$month'
						 GROUP BY a.kategori_umur;"; 
		}elseif($jenis=='pekerjaan'){
			$select = "select b.name ,count(a.pekerjaan) as jumlah
						 from apk_penindakan a   
						 join  kategori_pekerjaan b on b.id = a.pekerjaan 
						 where a.kode_satwil like'$satwil%' and month(`timestamp`) = '$month'
						 GROUP BY a.pekerjaan;"; 
		}
		$result = $this->db->query($select);
		return $result;
	}
	
	function get_jml_perpanjangan_sim($tgl=null, $kd_satwil=null){
		$this->db->select('id');
		$this->db->from('perpanjangan_sim');
		$this->db->where_in('approved', array('0','1'));
		
		if(($kd_satwil != null) && ($kd_satwil != '')){
			$this->db->like('kode_satwil', $kd_satwil, 'after');
		}
		
		if(($tgl != null) && ($tgl != '')){
			$this->db->like('tgl_jadwal', $tgl, 'after');
		}

		$query = $this->db->get();
		return $query->num_rows();
	}
	
	function get_tilang_polres_bln($bln=null){
		$sql = "SELECT polres.kode, polres.nama, COUNT(tilang.kode_satwil) AS jml
					FROM (SELECT kode, nama 
					FROM kewilayahan
					WHERE tipe='Polres'
					AND parent_id='".$this->config->item('kode_polda')."'
					AND `status`=1) AS polres
					LEFT JOIN (SELECT apk_penindakan.id, apk_penindakan.kode_satwil, polres.kode AS kode_polres
					FROM apk_penindakan, kewilayahan,
					(SELECT kode, nama FROM kewilayahan WHERE tipe='Polres' AND parent_id='".$this->config->item('kode_polda')."' AND `status`=1) AS polres
					WHERE apk_penindakan.`timestamp` LIKE '".$bln."%'
					AND apk_penindakan.tindakan='tilang'
					AND apk_penindakan.kode_satwil=kewilayahan.kode
					AND (polres.kode=kewilayahan.kode OR polres.kode=kewilayahan.parent_id)) AS tilang ON tilang.kode_polres=polres.kode
					GROUP BY polres.kode";
					
		$query = $this->db->query($sql);
		return $query;
	}
	
	function get_chart_tilang_periode($awal=null, $akhir=null){
		$json = array();
		$data = array();
		$num_rows = 0;
		$json['data_chart'] = array();
		$json['chart_graphs'] = array();
		$jml_total = 0;
		
		$this->db->where('tipe', 'Polres');
		$this->db->where('parent_id', $this->config->item('kode_polda'));
		$this->db->where('status', '1');
		$query = $this->db->get('kewilayahan');
		
		if ($query->num_rows() > 0){
			foreach ($query->result() as $row){
				
				$json['chart_graphs'][] = array(
										"bullet"=> "round",
										"bulletBorderAlpha"=> 1,
										"useLineColorForBulletBorder"=> true,
										"bulletColor"=> "#FFFFFF",
										"legendValueText"=> "[[value]]",
										"balloonText"=> "[[title]]: <b>[[value]]</b>",
										"title"=> $row->nama,
										"fillAlphas"=> 0,
										"valueField"=> $row->kode
									);
									
				$arr_polres[] = array(
										"kode"=>$row->kode,
									);
			}
			
			$sql = "SELECT polres.kode, polres.nama, DATE_FORMAT(tilang.timestamp, '%Y-%m-%d') AS tgl, COUNT(tilang.kode_satwil) AS jml
						FROM (SELECT kode, nama 
						FROM kewilayahan
						WHERE tipe='Polres'
						AND parent_id='".$this->config->item('kode_polda')."'
						AND `status`=1) AS polres
						LEFT JOIN (SELECT apk_penindakan.id, apk_penindakan.kode_satwil, polres.kode AS kode_polres, timestamp
						FROM apk_penindakan, kewilayahan,
						(SELECT kode, nama FROM kewilayahan WHERE tipe='Polres' AND parent_id='".$this->config->item('kode_polda')."' AND `status`=1) AS polres
						WHERE apk_penindakan.`timestamp` BETWEEN '$awal 00:00:00' AND '$akhir 23:59:59'
						AND apk_penindakan.tindakan='tilang'
						AND apk_penindakan.kode_satwil=kewilayahan.kode
						AND (polres.kode=kewilayahan.kode OR polres.kode=kewilayahan.parent_id)) AS tilang ON polres.kode=tilang.kode_polres
						GROUP BY tgl, polres.kode
						ORDER BY polres.kode, tgl;";
						
			$query = $this->db->query($sql);
			
			foreach ($query->result() as $row){
				$jml_total += $row->jml;
				$tgl = substr($row->tgl, 0, 10);
				$data[$tgl][] = array(
										"kode"=>$row->kode,
										"nama"=>$row->nama,
										"jml"=>$row->jml
									);
			}
			
			$date = $awal;
			while (strtotime($date) <= strtotime($akhir)) {
				$data_chart = array();
				$data_chart['date'] = $date;
				
				foreach ($arr_polres as $row){
					$data_chart[$row['kode']] = 0;
				}
				
				if (array_key_exists($date, $data)) {
					foreach ($data[$date] as $row){
						$data_chart[$row['kode']] = $row['jml'];
					}
				}
				
				$json['data_chart'][] = $data_chart;
				$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
			}
			
		}
		
		$json['chart_title'] = " Periode ".date('j M', strtotime($awal))." - ".date('j M', strtotime($akhir)).", dengan total ".$jml_total." laporan";
		
		// print(json_encode($json));
		return $json;
	}
	
	function get_perpanjangan_sim_polres_bln($bln=null){
		$sql = "SELECT polres.kode, polres.nama, COUNT(sim.kode_satwil) AS jml
					FROM (SELECT kode, nama 
					FROM kewilayahan
					WHERE tipe='Polres'
					AND parent_id='".$this->config->item('kode_polda')."'
					AND `status`=1) AS polres
					LEFT JOIN (SELECT id, kode_satwil
					FROM perpanjangan_sim
					WHERE tgl_jadwal LIKE '".$bln."%'
					AND approved IN ('0', '1') ) AS sim ON polres.kode=sim.kode_satwil
					GROUP BY polres.kode";
					
		$query = $this->db->query($sql);
		return $query;
	}
	
	function get_chart_perpanjangan_sim_periode($awal=null, $akhir=null){
		$json = array();
		$data = array();
		$num_rows = 0;
		$json['data_chart'] = array();
		$json['chart_graphs'] = array();
		$jml_total = 0;
		
		$this->db->where('tipe', 'Polres');
		$this->db->where('parent_id', $this->config->item('kode_polda'));
		$this->db->where('status', '1');
		$query = $this->db->get('kewilayahan');
		
		if ($query->num_rows() > 0){
			foreach ($query->result() as $row){
				
				$json['chart_graphs'][] = array(
										"bullet"=> "round",
										"bulletBorderAlpha"=> 1,
										"useLineColorForBulletBorder"=> true,
										"bulletColor"=> "#FFFFFF",
										"legendValueText"=> "[[value]]",
										"balloonText"=> "[[title]]: <b>[[value]]</b>",
										"title"=> $row->nama,
										"fillAlphas"=> 0,
										"valueField"=> $row->kode
									);
									
				$arr_polres[] = array(
										"kode"=>$row->kode,
									);
			}
			
			$sql = "SELECT polres.kode, polres.nama, DATE_FORMAT(sim.tgl_jadwal, '%Y-%m-%d') AS tgl, COUNT(sim.kode_satwil) AS jml
						FROM (SELECT kode, nama 
						FROM kewilayahan
						WHERE tipe='Polres'
						AND parent_id='".$this->config->item('kode_polda')."'
						AND `status`=1) AS polres
						LEFT JOIN (SELECT id, kode_satwil, tgl_jadwal
						FROM perpanjangan_sim
						WHERE tgl_jadwal BETWEEN '$awal 00:00:00' AND '$akhir 23:59:59'
						AND approved IN ('0', '1') ) AS sim ON polres.kode=sim.kode_satwil
						GROUP BY tgl, polres.kode
						ORDER BY polres.kode, tgl;";
						
			$query = $this->db->query($sql);
			
			foreach ($query->result() as $row){
				$jml_total += $row->jml;
				$tgl = substr($row->tgl, 0, 10);
				$data[$tgl][] = array(
										"kode"=>$row->kode,
										"nama"=>$row->nama,
										"jml"=>$row->jml
									);
			}
			
			$date = $awal;
			while (strtotime($date) <= strtotime($akhir)) {
				$data_chart = array();
				$data_chart['date'] = $date;
				
				foreach ($arr_polres as $row){
					$data_chart[$row['kode']] = 0;
				}
				
				if (array_key_exists($date, $data)) {
					foreach ($data[$date] as $row){
						$data_chart[$row['kode']] = $row['jml'];
					}
				}
				
				$json['data_chart'][] = $data_chart;
				$date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));
			}
			
		}
		
		$json['chart_title'] = " Periode ".date('j M', strtotime($awal))." - ".date('j M', strtotime($akhir)).", dengan total ".$jml_total." laporan";
		
		// print(json_encode($json));
		return $json;
	}
	
	function get_kategori_pos($satwil){
		if($satwil!=''){
			$this->db->like('kode_satwil',$satwil ,'after');
		}
		$this->db->order_by('nama', 'asc');
		$q = $this->db->get('pos');
		return $q->result_array();
	}
	function jml_Kej_laka($jenis=null,$satwil=null){ 
		$month = date('m');
		if($jenis=='jen_laka'){
			$select = "SELECT b.name, COUNT( a.jenis_kecelakaan ) AS jumlah
						FROM apk_laka a
						JOIN kategori_kecelakaan b ON b.id = a.jenis_kecelakaan
						 where a.kode_satwil like'$satwil%' and month(`timestamp`) = '$month'
						 GROUP BY a.jenis_kecelakaan;";
		} elseif ($jenis=='korban') {
			$select = "select sum(a.md) as jumlah_md, sum(a.lr) as jumlah_lr, sum(a.lb) as jumlah_lb
						 from apk_laka a  where a.kode_satwil like'$satwil%' and month(`timestamp`) = '$month' ";
			# code...
		}elseif($jenis=='waktu_laka'){
			$select = "SELECT b.kategori, COUNT( a.waktu_kejadian ) AS jumlah
						FROM apk_laka a
						JOIN kategori_waktu b ON b.id = a.waktu_kejadian
						 where a.kode_satwil like'$satwil%' and month(`timestamp`) = '$month'
						 GROUP BY a.waktu_kejadian;";
		}
		elseif($jenis=='faktor_laka'){
			$select = "SELECT b.name, COUNT( a.faktor ) AS jumlah
						FROM apk_laka a
						JOIN kategori_faktor_kecelakaan b ON b.id = a.faktor
						 where a.kode_satwil like'$satwil%' and month(`timestamp`) = '$month'
						 GROUP BY a.faktor;";
		}
		$result = $this->db->query($select);
		return $result;
	}
		function get_jumlah_laka_satwil_periode($bulan=null,$tahun=null, $kd_satwil=null){
		if($kd_satwil == null || $kd_satwil == ''){
			$kd_satwil = $this->config->item('kode_polda');
		}
		 
		$select = "select laplaka.* 
					from (
						(select a.kode,a.parent_id,a.nama,
							(select count(b.id) from apk_laka b where b.kode_satwil=a.kode and month(b.timestamp) = '".$bulan."' AND   year(b.timestamp) = '".$tahun."' ) as jml_laka,
							(select COALESCE(sum(b.lr),0) from apk_laka b where b.kode_satwil=a.kode and month(b.timestamp) = '".$bulan."' AND   year(b.timestamp) = '".$tahun."' ) as jml_lr,
							(select  COALESCE(sum(b.lb),0) from apk_laka b where b.kode_satwil=a.kode and month(b.timestamp) = '".$bulan."' AND   year(b.timestamp) = '".$tahun."') as jml_lb ,
							(select  COALESCE(sum(b.md),0) from apk_laka b where b.kode_satwil=a.kode and  month(b.timestamp) = '".$bulan."' AND   year(b.timestamp) = '".$tahun."'  ) as jml_md ,
							(select  COALESCE(sum(b.kerugian_materil),0) from apk_laka b where b.kode_satwil=a.kode and month(b.timestamp) = '".$bulan."' AND   year(b.timestamp) = '".$tahun."'   ) as jml_rugi 
						from kewilayahan a 
						where a.kode = '".$kd_satwil."')
						UNION
						(select a.kode,a.parent_id,a.nama,
							(select count(b.id) from apk_laka b where b.kode_satwil=a.kode and month(b.timestamp) = '".$bulan."' AND   year(b.timestamp) = '".$tahun."' ) as jml_laka,
							(select COALESCE(sum(b.lr),0) from apk_laka b where b.kode_satwil=a.kode and   month(b.timestamp) = '".$bulan."' AND   year(b.timestamp) = '".$tahun."'    ) as jml_lr,
							(select  COALESCE(sum(b.lb),0) from apk_laka b where b.kode_satwil=a.kode and   month(b.timestamp) = '".$bulan."' AND   year(b.timestamp) = '".$tahun."'  ) as jml_lb ,
							(select  COALESCE(sum(b.md),0) from apk_laka b where b.kode_satwil=a.kode and   month(b.timestamp) = '".$bulan."' AND   year(b.timestamp) = '".$tahun."'   ) as jml_md,
							(select  COALESCE(sum(b.kerugian_materil),0) from apk_laka b where b.kode_satwil=a.kode and month(b.timestamp) = '".$bulan."' AND   year(b.timestamp) = '".$tahun."'   ) as jml_rugi 
						from kewilayahan a 
						where a.parent_id ='".$kd_satwil."')
					) as laplaka" ; 
		
		$query=$this->db->query($select);
		return $query;
	}
	
}