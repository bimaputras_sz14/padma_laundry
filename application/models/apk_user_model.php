<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apk_user_model extends CI_Model {
	
	// The following method prevents an error occurring when $this->data is modified.
	// Error Message: 'Indirect modification of overloaded property Demo_cart_admin_model::$data has no effect'.
	public function &__get($key)
	{
		$CI =& get_instance();
		return $CI->$key;
	}
	function get_data_apk($data_login){
		$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
		$kd_satwil = $arr_usr['kode_satwil'];
		
		$this->db->select("a.*,b.nama as wilayah, c.jabatan,d.nama as kesatuan,e.nama as unitkerja");
		$this->db->like('a.kode_wilayah',$kd_satwil,'after');
		$this->db->where_in("a.status",array(0,1));
		$this->db->join("kewilayahan b","b.kode=a.kode_wilayah","left");
		$this->db->join("jabatan c","c.id=a.jabatan_id","left");
		$this->db->join("kesatuan d","a.id_kesatuan=d.id","left");
		$this->db->join("unitkerja e","a.unitkerja_id=e.id","left");
		$this->db->where('a.group', 'USER');
		$data = $this->db->get('apk_users a');
		return $data;
	}
	
	function get_anggota_by_kdWil($kode_wilayah=null)
	{
		$this->db->select("nrp, nama");
		$this->db->like('kode_wilayah', $kode_wilayah, 'after');
		$this->db->where('status', '1');
		$this->db->where('group', 'USER');
		$this->db->order_by('last_activity', 'desc');
		$q = $this->db->get('apk_users');
		if($q->num_rows() > 0)
		{
			return $q->result_array();
		}
		else
		{
			return false;
		}
	}
	
	function do_add_apk_user($data=null)
	{
		$data = $this->db->insert('apk_users', $data);
		return $data;
	}
	function do_edit_apk_user($id=null,$data=null)
	{
		$this->db->where('id', $id);
		$data = $this->db->update('apk_users', $data);
		return $data;
	}
	
	/* */
	
	function get_apkUsers_detail($id=null)
	{
		$this->db->select("a.* "); 
		$this->db->where('a.id', $id);
		$q = $this->db->get('apk_users a');
		if($q->num_rows() > 0)
		{
			return $q->row();
		}
		else
		{
			return false;
		}
	}
	
	/* */
	
	/* */
	
	function do_delete_anggota($id=null)
	{
		$data=array("status"=>3,
					"iccid"=>null,
					"imei"=>null,
					"msisdn"=>null,
					"carrier"=>null,
					"model"=>null
					);
		// if($this->db->delete('apk_users'))
		$this->db->where('id', $id);
		if($this->db->update('apk_users',$data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function get_data(){

	}

	function do_reset_password($id=null,$nrp=null){
		$data=array(
				"password"=>md5('pass'.substr($nrp, 4)),
				"iccid"=>null,
				"imei"=>null,
				"msisdn"=>null,
				"carrier"=>null,
				"model"=>null
			);
		// if($this->db->delete('apk_users'))
		$this->db->where('id', $id);
		if($this->db->update('apk_users',$data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>