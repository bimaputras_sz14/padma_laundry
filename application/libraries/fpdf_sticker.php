<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Name:  FPDF
* 
* Author: Jd Fiscus
* 	 	  jdfiscus@gmail.com
*         @iamfiscus
*          
*
* Origin API Class: http://www.fpdf.org/
* 
* Location: http://github.com/iamfiscus/Codeigniter-FPDF/
*          
* Created:  06.22.2010 
* 
* Description:  This is a Codeigniter library which allows you to generate a PDF with the FPDF library
* 
*/

class Fpdf_sticker {
		
	public function __construct() {
		
		require_once APPPATH.'third_party/fpdf18/fpdf.php';
		
		$pdf = new FPDF();
		$pdf->SetMargins(0,0,0);	
		// $pdf->SetAutoPageBreak(true, 5);
		$pdf->SetAutoPageBreak(false);
		$pdf->AddPage('P',array(102,155));
		
		$CI =& get_instance();
		$CI->fpdf = $pdf;
		
	}
	
}