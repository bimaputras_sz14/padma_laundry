<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

load_class('My_pdf', 'libraries', FALSE);

class Fpdf_poltrait extends My_pdf {
	
	public function __construct() {
		$pdf = new My_pdf('P','mm',array(217,330));
		$pdf->AddFont('ArialNarrow','','ARIALN.php');
		$pdf->AddFont('ArialNarrow','I','ARIALNI.php');
		$pdf->AddFont('ArialNarrow','B','ARIALNB.php');
		$pdf->AddPage();
		
		$CI =& get_instance();
		$CI->fpdf = $pdf;
	}
	
	
}