<?php
$method = $this->uri->segment(3);
$id = $this->uri->segment(4);
switch ($method) {
	case 'tambah':
	$data = array(
		'name'			=>'',
		'harga'			=>'',
		'status'			=>'');
	$hidden = array();
	break;
	case 'ubah':
	$data = array(
		'name'			=>$record['name'],
		'harga'			=>$record['harga'],
		'status'			=>$record['status']);
	$hidden = array('id'	=>$record['id']);
	break;
}
?>

<div class="row">
	<div class="col-lg-6">
		<?php
		if($this->session->flashdata('msg_success')) {?>
		<div class="alert alert-success alert-dismissible fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $this->session->flashdata('msg_success');?>
		</div>
		<?php }?>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Formulir Tambah Teknik Cucian</h5>
			</div>
			<div class="ibox-content">
				<?php echo form_open('master_data/teknik/'.$method,'id=form_guru class=form-horizontal',$hidden); ?>
		  	     	<div class="form-group">
		  	     		<div class="row" style="margin-right: 0px;">
			  	     		<label class="col-md-2 control-label">Nama Teknik Cucian</label>
							<div class="col-md-3">
								<input type="text" name="name" class="form-control" placeholder="Nama Teknik Cucian" value="<?php echo $data['name'];?>">
							</div>
							<label class="control-label col-md-1">Harga</label>
							<div class="col-md-3">
								<input type="number" name="harga" id="harga" min="0" class="form-control tgl" placeholder="Rp." value="<?php echo $data['harga'] ?>">
							</div>
						</div>
					</div>
					<div class="form-group">
		  	     		<div class="row" style="margin-right: 0px;">
							<label class="control-label col-md-2">Status</label>
				  	   		<div class="col-md-7">
								<?php
									$options = array(	'1'		=> 'Active',
														'0' 	=> 'Inactive');
									echo form_dropdown('status',$options,$data['status'],'class="form-control" id="status" required');
								?>
							</div>
						</div>
					</div>
					<hr>
					<div class="form-group">
						<div class="pull-left">
							<a class="btn btn-danger" href="<?php echo base_url();?>master_data/teknik">Kembali</a>
						</div>
						<div class="pull-left" style="margin-left: 5px;">
							<button type="submit" name="submit" class="btn btn-block btn-success">Simpan</button>
						</div>
					</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
</div>