<?php
if($this->session->flashdata('msg_success')) {?>
    <div class="alert alert-success alert-dismissible fade in" role="alert">
        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
        <?php echo $this->session->flashdata('msg_success');?>
    </div>
<?php }?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Data Teknik Cucian</h5>
            </div>
            <div class="ibox-content">
                <?php echo anchor('master_data/teknik/tambah','+ Tambah','class="btn btn-primary" style="font-weight:bold;"'); ?>
                <hr>
                <table id="table-teknik" class="table table-striped table-bordered table-hover dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th width="30" class="text-center">No</th>
                            <th>Teknik Cucian</th>
                            <th>Harga</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <td colspan="6" class="dataTables_empty text-center">Loading data from server</td>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Datatables -->
<script type="text/javascript">
    var save_method; //for save method string
    var table;
    $(document).ready(function() {
        //datatables
        table = $('#table-teknik').DataTable({
            "responsive": true,
            "processing": false, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [0,'asc'], //Initial no order.
            dom: 'lTfgitp',
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": '<?php echo site_url('teknik/json');?>',
                "type": "POST",
                "dataType": "json"
            },
            "columnDefs": [{
                "searchable": false,
                "orderable": false,
                "targets": 4
            }],
            "fnRowCallback": function(nRow,aData,iDisplayIndex,iDisplayIndexFull) {
                var index = iDisplayIndex + 1;
                $('td:eq(0)',nRow).html(index);
                return nRow;
            },
            //Set column definition initialisation properties.
            "aoColumns": [
                {"mData": "id",class:'text-center',width:10},
                {"mData": "name"},
                {"mData": "harga"},
                {"mData": "status", class:'text-center',
                 "mRender": function(data, type, full) {
                            if(data == 1){
                                return '<span class="label label-primary">Aktif</span>';
                            }else{
                                return '<span class="label label-default">Tidak Aktif</span>';
                            }
                          }
                },
                {"mData": "action",class:'text-center',width:120}
            ],
            "language": {
                "zeroRecords": "Data tidak ditemukan",
                "infoEmpty": "Tidak ada data"
            }
        });
    });
</script>
<!-- /Datatables -->