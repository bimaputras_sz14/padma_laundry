<?php
$method = $this->uri->segment(3);
$id = $this->uri->segment(4);
switch ($method) {
	case 'tambah':
		$data = array('nama' => '');
		$hidden = array();
	break;
	case 'ubah':
		$data = array('nama' => $record['name']);
		$hidden = array('id'		=> $record['id']);
	break;
}
?>

<div class="row">
	<div class="col-lg-6">
		<?php
		if($this->session->flashdata('msg_success')) {?>
			<div class="alert alert-success alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				<?php echo $this->session->flashdata('msg_success');?>
			</div>
		<?php }?>
<!-- UNTUK ERROR UPLOAD -->
<?php 
echo (empty($error) ? "" :"
	<div class='alert alert-danger alert-dismissible fade in' role='alert'>
		<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
		</button>$error
	</div>
");?>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data Universitas</h5>
			</div>
			<div class="ibox-content">
				<?php echo form_open_multipart('master_data/universitas/'.$method,'id=produk class=form-horizontal',$hidden); ?>
						<div class="form-group">
			  	     		<div class="row" style="margin-right: 0px;">
				  	   			<label class="control-label col-md-2 col-md-offset-1">Nama Universitas</label>
								<div class="col-md-3">
									<input type="text" name="universitas" class="form-control" placeholder="Universitas" value="<?php echo $data['nama'];?>" required>
								</div>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="pull-left">
								<a class="btn btn-danger" href="<?php echo base_url();?>master_data/universitas">Kembali</a>
							</div>
							<div class="pull-left" style="margin-left: 5px;">
								<button type="submit" name="submit" class="btn btn-block btn-success">Simpan</button>
							</div>
						</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
</div>