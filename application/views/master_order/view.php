<?php
if($this->session->flashdata('msg_success')) {?>
	<div class="alert alert-success alert-dismissible fade in" role="alert">
		<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
		<?php echo $this->session->flashdata('msg_success');?>
	</div>
<?php }?>
<?php
if($this->session->flashdata('msg_failed')) {?>
	<div class="alert alert-danger alert-dismissible fade in" role="alert">
		<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
		<?php echo $this->session->flashdata('msg_failed');?>
	</div>
<?php }?>
<style type="text/css">
	.table>tbody>tr>td{
		vertical-align: middle;
	}
</style>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data Order</h5>
			</div>
			<div class="ibox-content">
				<?php echo form_open('data/order','method=get id=form_pegawai class=form-horizontal'); ?>
                    <div class="form-group">
                        <label class="control-label col-md-2">Tanggal</label>
                        <div class="col-md-7">
                            <input type="text" name="tanggal" class="form-control date-range" placeholder="Tanggal" value="" id="tanggal">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" name="submit" class="btn btn-block btn-success"><i class="fa fa-search"></i> Filter</button>
                        </div>
                    </div>
                    <div class="form-group">
                        
                    </div>
                <?php echo form_close();?>
				<!--
				<div class="btn-group pull-right">
                	<button type="button" class="btn btn-md btn-primary btn-labeled dropdown-toggle" data-toggle="dropdown"><b><i class="fa fa-bars"></i></b> Export <span class="caret"></span></button>
                	<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<a href="">
								<i class="fa fa-file-excel-o"></i> Excel
							</a>
						</li>
						<li>
							<a href="">
								<i class="fa fa-file-pdf-o"></i> PDF
							</a>
						</li>
					</ul>
				</div> 
				<br>
				<br> 
				-->
				<div class="table-responsive">
					<table id="table-order" class="table table-striped table-bordered table-hover dt-responsive">
			 			<thead>
							<tr>
								<th class="text-center">No.</th>
								<th>Nomor Order</th> 
								<th>Tanggal Order</th>
								<th>Total Pembayaran</th>
								<th>Nama Pelanggan</th>
								<th>Total Penjualan</th>
								<th>Total Diskon</th>
								<th>Keterangan</th>
								<th>Berat</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<td colspan="12" class="dataTables_empty text-center">Loading data from server</td>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Datatables -->
<script type="text/javascript">
	var save_method; //for save method string
	var table;
	$(document).ready(function() {
		$('#tanggal').daterangepicker({
			<?php $tgl = (isset($_GET['tanggal']))?explode('-', $_GET['tanggal']):""; ?>
		    "startDate": "<?php echo (isset($_GET['tanggal']))?$tgl[0]:date('m/d/Y'); ?>",
		    "endDate": "<?php echo (isset($_GET['tanggal']))?$tgl[1]:date('m/d/Y'); ?>"
		}, function(start, end, label) {
		  console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
		});
		//datatables
		table = $('#table-order').DataTable({
			"sDom": "<'row'<'col-md-6'l><'col-md-3 col-md-offset-3'f>r>t<'row'<'col-md-6'i><'col-md-3 col-md-offset-3'p>>"+
					"<'row'<'col-sm-12'>>" +
					"<'row'<'col-sm-12'>>" +
					"<'row'<'col-sm-12'>>" +
					"<'row'<'col-sm-12'>>" +
					"<'row'<'col-sm-12'>>" +
					"<'row'<'col-sm-12'>>" +
					"<'row'<'col-sm-12'>>" +
					"<'row'<'col-sm-12'>>" +
					"<'row'<'col-sm-12'>>" +
					"<'row'<'col-sm-12'>>" + 
    				"<'row'<'col-sm-4'B><'col-sm-4 text-center'><'col-sm-4'>>",
    		"buttons": {
		       "dom": {
		          "button": {
		            "tag": "button",
		            "className": "btn btn-primary"
		          }
		       },
		       "buttons": [ 
		      		{
		                extend: 'excelHtml5',
		                text: 'Export to Excel',
		                title: 'Report Data Order',
		            	exportOptions:{
			       			columns: [0,1,2,3,4,5,6,7,8,9,10]
			       		}
		       		},
			        {
		                extend: 'pdfHtml5',
		                text: 'Export to PDF',
		                title: 'Report Data Order',
		                orientation: 'landscape',
		                pageSize: 'A4',
		            	exportOptions:{
			       			columns: [0,1,2,3,4,5,6,7,8,9,10]
			       		}
		       		},
            	]   
		    },
			"responsive": true,
			"processing": false, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [2,'desc'], //Initial no order.
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('order_admin/json');?>',
				"type": "POST",
				"dataType": "json",
				"data":{
					'tanggal':'<?php echo (isset($_GET['tanggal']))?$_GET['tanggal']:''; ?>'
				}
			},
			"columnDefs": [
				{
					"searchable": false,
					"orderable": false,
					"targets": 10},
				{
					"visible": false,
					"targets": 3},
				{
					"visible": false,
					"targets": 6},
				{
					"visible": false,
					"targets": 7},
				{
					"visible": false,
					"targets": 8},
			],
			"fnRowCallback": function(nRow,aData,iDisplayIndex,iDisplayIndexFull) {
				var index = iDisplayIndex + 1;
				$('td:eq(0)',nRow).html(index);
				return nRow;
			},
			//Set column definition initialisation properties.
			"aoColumns": [
				{"mData": 'id',class:'text-center'},
				{"mData": "no_penjualan"},
				{"mData": "tanggal_penjualan"}, 
				{"mData": "total_pembayaran"},
				{"mData": "nama_pelanggan"},
				{"mData": "total_penjualan"},
				{"mData": "total_diskon"},
				{"mData": "keterangan"},
				{"mData": "berat_timbangan"},
				{"mData": "status",class:'text-center',
				 "mRender": function(data, type, full) {
                            if(data != null && data != 0){
                                return '<span class="label label-success">'+data+'</span>';
                            }else{
                            	return '<span class="label label-danger">NULL</span>';
                            }
                          }
				},
				/*{"mData": "action",class:'text-center',width:150},*/
				{"mData": function(data, type, full) {
                    /*if(data != null && data != 0){
                        return '<span class="label label-success">'+data.orderid+' '+data.lastid+'</span>';
                    }else{
                    	return '<span class="label label-danger">NULL</span>';
                    }*/
                    if (data.orderid < data.lastid) {
                    	var confirm = "return confirm('Apakah Anda yakin akan melanjutkan ke status selanjutnya untuk No. Penjualan :"+data.no_penjualan+" ?');"
                    	return '<div class="btn-group">'+
                			'<button type="button" class="btn btn-md btn-primary btn-labeled dropdown-toggle" data-toggle="dropdown"> Actions <span class="caret"></span></button>'+
                				'<ul class="dropdown-menu" style="min-width:90px;">'+
									'<li>'+
										'<a href="<?php echo base_url()."data/order/lihat/"; ?>'+data.id+'">'+
											'<i class="fa fa-eye"></i> Lihat'+
										'</a>'+
									'</li>'+
									'<li>'+
										'<a href="<?php echo base_url()."data/order/ubah/"; ?>'+data.id+'">'+
											'<i class="fa fa-edit"></i> Ubah'+
										'</a>'+
									'</li>'+
									'<li>'+
										'<a href="<?php echo base_url()."data/order/updatestats/"; ?>'+data.id+'">'+
											'<i class="fa fa-check"></i> Next'+
										'</a>'+
									'</li>'+
								'</ul>'+
						'</div>';
                    }else{
                    	return '<div class="btn-group">'+
                			'<button type="button" class="btn btn-md btn-primary btn-labeled dropdown-toggle" data-toggle="dropdown"> Actions <span class="caret"></span></button>'+
                				'<ul class="dropdown-menu" style="min-width:90px;">'+
									'<li>'+
										'<a href="<?php echo base_url()."data/order/lihat/"; ?>'+data.id+'">'+
											'<i class="fa fa-eye"></i> Lihat'+
										'</a>'+
									'</li>'+
									'<li>'+
										'<a href="<?php echo base_url()."data/order/ubah/"; ?>'+data.id+'">'+
											'<i class="fa fa-edit"></i> Ubah'+
										'</a>'+
									'</li>'+
									'<li>'+
										'<a href="#">'+
											'<i class="fa fa-flag-checkered"></i> Selesai'+
										'</a>'+
									'</li>'+
								'</ul>'+
						'</div>';
                    }
                  }, class:'text-center'
				}
			],
			"language": {
				"zeroRecords": "Data tidak ditemukan",
				"infoEmpty": "Tidak ada data"
			}
		});
	});
</script>
<!-- /Datatables -->