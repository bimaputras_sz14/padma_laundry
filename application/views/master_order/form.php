<?php
$method = $this->uri->segment(3);
$id = $this->uri->segment(4);
$this->load->helper('my_helper');
switch ($method) {
    case 'ubah':
        $data = array(
            // 'tanggal_pickup' => tanggal(date('Y-m-d', strtotime($record['tanggal_pickup'])))." ".date('H:i:s', strtotime($record['tanggal_pickup'])),
            'no_penjualan' => $record['no_penjualan'],
            'tanggal_penjualan' => tanggal(date('Y-m-d', strtotime($record['tanggal_penjualan'])))." ".date('H:i:s', strtotime($record['tanggal_penjualan'])),
            'pelanggan' => $record['pelanggan_id'],
            'total_pembayaran' => $record['total_pembayaran'],
            'total_penjualan' => $record['total_penjualan'],
            'total_diskon' => $record['total_diskon'],
            'keterangan' => $record['keterangan'],
            'personil' => $record['personil_id'],
            'berat' => $record['berat_timbangan'],
            'status' => $record['status'],
            'bag_no' => $record['bag_no'],
            'keterangan' => $record['keterangan'],
            'foto' => $record['foto'],
            'tanggal_pickup' => $record['tanggal_pickup']);
        $hidden = array('id'=>$record['id']);
    break;
}

$data_header = header_order_view($record['id']);
?>
<style type="text/css">
    .control-label{
        padding-top: 7px;
        margin-bottom: 0;
        text-align: right;
    }
</style>
<div class="row">
    <div class="col-lg-6">
        <?php
        if($this->session->flashdata('msg_success')) {?>
        <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?php echo $this->session->flashdata('msg_success');?>
        </div>
        <?php }?>
    </div>
    <!-- UNTUK ERROR UPLOAD -->
    <?php 
    echo (empty($error) ? "" :"
        <div class='alert alert-danger alert-dismissible fade in' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
        </button>$error
        </div>
        ");?>
</div>

<?php echo form_open_multipart('data/order/'.$method,'id=data_order_form',$hidden); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-1"> Rincian</a></li>
                <li class=""><a data-toggle="tab" href="#tab-2">Gambar</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="wrapper wrapper-content animated fadeInRight">
                                    <div class="row">
                                        <div class="col-sm-6 text-left">
                                            <h5>
                                                <div class="row">
                                                    <div class="col-md-4">Nomor Order</div>
                                                    <div class="col-md-8">: <?php echo $record['no_penjualan'];?></div>
                                                </div>
                                            </h5>
                                            <h5>
                                                <div class="row">
                                                    <div class="col-md-4">Nama Pelanggan</div>
                                                    <div class="col-md-8">: <?php echo $data_header['nama_pelanggan'];?></div>
                                                </div>
                                            </h5>
                                            <h5>
                                                <div class="row">
                                                    <div class="col-md-4">Jenis Layanan</div>
                                                    <div class="col-md-8">: <?php echo $data_header['layanan'];?></div>
                                                </div>
                                            </h5>
                                            <h5>
                                                <div class="row">
                                                    <div class="col-md-4">Jam Jemput</div>
                                                    <div class="col-md-8">: <?php echo $data_header['jam_pickup'];?></div>
                                                </div>
                                            </h5>
<?php
if ($data_header['gedung_id'] != '') {
    echo '
        <h5>
        <div class="row">
        <div class="col-md-4">Gedung</div>
        <div class="col-md-8">: '.get_data_gedung('gedung',$data_header["gedung_id"]).'</div>
        </div>
        </h5>';}
else {
    echo '
    <h5>
    <div class="row">
    <div class="col-md-4">Alamat</div>
    <div class="col-md-8">: '.get_data_gedung('member',$data_header["id"]).'</div>
    </div>
    </h5>';}
?>
                                                
                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <h5>
                                                <div class="row">
                                                    <div class="col-md-7">Tanggal Order :</div>
                                                    <div class="col-md-5 text-left"><?php echo $record['tanggal_penjualan'];?></div>
                                                </div>
                                            </h5>
                                            <h5>
                                                <div class="row">
                                                    <div class="col-md-7 control-label" style="padding-top:11px;">Tanggal Jemput :</div>
                                                    <div class="input-group col-md-5" style="padding-left: 15px;padding-right: 15px;">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input type="text" name="tanggal_pickup" class="form-control" id="tanggal_pickup" value="<?php echo $data['tanggal_pickup'];?>">
                                                    </div>
                                                </div>
                                            </h5>
                                            <h5>
                                                <div class="row">
                                                    <div class="col-md-7">Jenis Cucian :</div>
                                                    <div class="col-md-5 text-left"><?php echo $data_header['jenis'];?></div>
                                                </div>
                                            </h5>
                                            <h5>
                                                <div class="row">
                                                    <div class="col-md-7 control-label" style="padding-top:11px;">Nomor Kantung :</div>
                                                    <div class="col-md-5 text-left">
                                                        <input type="text" name="bag_no" class="form-control" value="<?php echo $data['bag_no'];?>">
                                                    </div>
                                                </div>
                                            </h5>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="table-responsive m-t">
                                        <table id="table-order" class="table table-striped table-responsive datatable-basic">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">No.</th>
                                                    <th>Nama Barang</th>
                                                    <th>Quantity</th>
                                                    <th>Harga</th>
                                                    <th>Catatan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
<?php
    $no = 1;
    $total = 0;
    foreach(penjualan_item($record['id'])->result() as $r){
    echo "
        <tr>
            <td style='width:50px;'class='text-center'>$no</td>
            <td style='width:150px;'>$r->nama_produk</td>
            <td style='width:150px;'>$r->qty</td>
            <td style='width:200px;'>
                <input type='hidden' name='id_total[]' value='$r->id'>
                <input type='number' name='total[]' class='form-control' value='$r->total'>
            </td>
            <td>$r->note</td>
        </tr>";
    $no += 1;
    $total += $r->total;}

    $semua = $data_header['harga'] * $data['berat'];
    $semua = $semua + $total;
?>
                                            </tbody>
                                        </table>
                                    </div><!-- /table-responsive -->
                                    <div class="row">
                                        <table class="table invoice-total">
                                            <tbody>
                                                <tr>
                                                    <td><strong>TOTAL : </strong></td>
                                                    <td>Rp .<?php echo $semua; ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="control-label col-md-2">Catatan Petugas :</label>
                                            <div class="col-md-10">
                                                 <textarea name="keterangan" class="autosize form-control" style="resize: none;" required><?php echo $data['keterangan'] ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="control-label col-md-2">Berat :</label>
                                            <div class="col-md-2">
                                                <div class="input-group">
                                                    <input type="number" name="berat" class="form-control" placeholder="Berat Timbangan" value="<?php echo $data['berat'];?>">
                                                    <div class="input-group-addon">
                                                        Kg
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="control-label col-md-2" style="margin-top:22px;">Foto :</label>
                                            <div class="col-md-7">
                                                <?php
                                                if ($method == 'ubah') {
                                                    echo '
                                                        <input type="file" name="foto" class="form-control file" placeholder="Foto" data-show-upload="false" accept=".jpg">';
                                                }
                                                ?>
                                                <small>File harus dibawah 1 MB dengan tipe *.JPG</small>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <div class="pull-left">
                                            <a class="btn btn-danger" href="<?php echo base_url();?>data/order">Kembali</a>
                                        </div>
                                        <div class="pull-left" style="margin-left: 5px;">
                                            <button type="submit" name="submit" class="btn btn-block btn-success">Simpan</button>
                                        </div>
                                    </div>
                                <?php echo form_close();?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab-2" class="tab-pane">
                    <div class="panel-body">
<?php
    echo ' <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">';
    foreach(penjualan_photo($record['id'])->result() as $r){
        echo ' 
        <div class="col-md-3">
        <div class="ibox">
            <div class="ibox-content product-box"> 
                <div class="product-imitation">
                    <img alt="image" width="200" src="'.base_url().'/uploads/cucian/'.$r->path.'">
                </div> 
                <div class="product-desc"> 
                    <a href="#" class="product-name"> '.$r->nama_produk.'</a>    
                </div>
            </div>
        </div></div>'; 
    }
    echo ' 
        <div class="col-md-3">
        <div class="ibox">
            <div class="ibox-content product-box"> 
                <div class="product-imitation">
                    <img alt="image" width="200" src="'.base_url().'uploads/'.$data["foto"].'">
                </div> 
                <div class="product-desc"> 
                    <a href="#" class="product-name"> Foto Timbangan</a>    
                </div>
            </div>
        </div></div>'; 
    echo '</div></div>';
?>
                    </div>
                </div>
            </div>
            <br>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#tanggal_pickup').datetimepicker({
            format:'YYYY-MM-DD HH:mm:ss'
        });
    });
</script>