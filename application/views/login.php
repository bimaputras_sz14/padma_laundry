<div class="container">

  <form class="login-form" action="<?php echo current_url()?>" method="post">        
    <div class="login-wrap">
        <p class="login-img"><img src="<?php echo base_url().'assets/img/logo.png' ?>"></p>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-user"></i></span>
          <input name="login_identity" type="text" class="form-control" placeholder="Username" autofocus>
        </div>
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            <input type="password" name="login_password"  class="form-control" placeholder="Password">
        </div> 
        <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button> 
    </div>
  </form>
</div>