<?PHP
	date_default_timezone_set('Asia/Jakarta');
	$today = date('Y-m-d');
?>

			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id='container1'>
                            <div class="dashboard-stat red-intense">
                                <div class="visual">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="details">
                                    <div class="number" id="jml_distribusi">
                                         0
                                    </div>
                                    <div class="desc">
                                        Distribusi KTMDU
                                    </div>
                                </div>
                                <a class="more" href="#">
                                View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id='container2'>
                            <div class="dashboard-stat blue-madison">
                                <div class="visual">
                                    <i class="fa fa-file-text"></i>
                                </div>
                                <div class="details">
                                    <div class="number" id="jml_ktmdu_telusur">
                                         0
                                    </div>
                                    <div class="desc">
                                         KTMDU Telah Ditelusuri
                                    </div>
                                </div>
                                <a class="more" href="#">
                                View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id='container3'>
                            <div class="dashboard-stat green-haze">
                                <div class="visual">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="details">
                                    <div class="number" id="ktmdu">
                                         0
                                    </div>
                                    <div class="desc">
                                         KTMDU
                                    </div>
                                </div>
                                <a class="more" href="#">
                                View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" id='container4'>
                            <div class="dashboard-stat purple-plum">
                                <div class="visual">
                                    <i class="fa fa-file-text-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number" id="nonktmdu">
                                         0
                                    </div>
                                    <div class="desc">
                                        NONKTDMU
                                    </div>
                                </div>
                                <a class="more" href="#">
                                View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>
                            </div>
                        </div>
                    </div>
			        <!-- END DASHBOARD STATS -->
					
                    <div class="row" id='container5'>
						<div class="col-md-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet box green-seagreen">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bar-chart"></i>
                                        <span class="caption-subject">Data Distribusi Wilayah </span>
                                        
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="initChart1" class="chart" style="height: 450px;"></div>
                                </div>
                            </div> 
                        </div>
                    </div>
					<div class="row" id='container6'>
						<div class="col-md-12">
                            <!-- BEGIN PORTLET-->
                            <div class="portlet box red-sunglo">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bar-chart"></i>
                                        <span class="caption-subject">Data Penulusuran Wilayah</span>
										
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="initChart2" class="chart" style="height: 450px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
						<div class="col-md-6" id='container7'>
                            <!-- BEGIN PORTLET-->
                            <div class="portlet box green-seagreen">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bar-chart"></i>
                                        <span class="caption-subject">Data Pertumbuhan Penelusuran </span>
                                        <span class="caption-helper font-green-turquoise"><?PHP echo date('M Y');?></span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="initChart3" class="chart" style="height: 450px;"></div>
                                </div>
                            </div>
                        </div>
						<div class="col-md-6" id='container8'>
                            <!-- BEGIN PORTLET-->
                            <div class="portlet box red-sunglo">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-bar-chart"></i>
                                        <span class="caption-subject">KTMDU</span>
                                        <span class="caption-helper font-green-turquoise"><?PHP echo date('M Y');?></span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="initChart4" class="chart" style="height: 450px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
					
					<div class="row" id='container9'>
				        <div class="col-md-12">                                       
                            <!-- BEGIN MARKERS PORTLET-->
                            <div id="map-container">
                                <div class="portlet box blue">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="icon-pointer"></i>Lokasi KTMDU yang Sudah Ditelusuri Hari Ini
                                        </div>
                                        <div class="tools">
                                            <a class="fullscreen"></a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div id="map-canvas">
                                            <div id="map" class="gmaps" style="height: 600px;"></div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                        <!-- END MARKERS PORTLET-->
                    </div>
					
				</div>
			</div>
			<!-- END PAGE CONTENT-->