
<div class="logo">
	<a href="">
	<img src="<?php echo $this->config->item('assets_dir');?>img/logo-bigs.png" alt=""/>
	</a>
</div>


<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<!--<form class="login-form" action="index.html" method="post">-->
	<?php echo form_open(current_url(), 'class="parallel"');?> 
		<h3 class="form-title">Sign In</h3>
		
		<?php if (! empty($message)) { ?>
			<div class="alert alert-danger">
				<button class="close" data-close="alert"></button>
				<span><?php echo $message; ?></span>
			</div>
		<?php } ?>
		
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Username</label>
			<!--<input class="form-control form-control-solid placeholder-no-fix" type="text" id="identity" name="login_identity" value="<?php ///echo set_value('login_identity', '999999');?>" autocomplete="off" placeholder="NRP"/>-->
			<input class="form-control form-control-solid placeholder-no-fix" type="text" id="identity" name="login_identity" value="<?php echo set_value('login_identity', '');?>" autocomplete="off" placeholder="Username"/>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<!--<input class="form-control form-control-solid placeholder-no-fix" type="password" id="password" name="login_password" value="<?php //echo set_value('login_password', 'password123');?>" autocomplete="off" placeholder="Password"/>-->
			<input class="form-control form-control-solid placeholder-no-fix" type="password" id="password" name="login_password" value="<?php echo set_value('login_password', '');?>" autocomplete="off" placeholder="Password"/>
		</div>
		 
		<!-- <div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Tipe Login</label>
			<select class="form-control select2me" id='divisi' name="divisi">
				<option value="">Login Polda</option> 
				<option value="1">Polda</option>
				<option value="0">Provinsi</option> 
			</select>
		</div> -->
		 
		<div class="form-actions">
			<input type="submit" name="login_user" id="submit" value="Sign In" class="btn btn-success uppercase"/>
			<label class="rememberme check">
			<!--<input type="checkbox" id="remember_me" name="remember_me" value="1" <?php //echo set_checkbox('remember_me', 1); ?>/>Remember </label>-->
		</div>
	<?php echo form_close();?>
	<!--</form>-->
	<!-- END LOGIN FORM -->
</div>

<div class="copyright">
	 2018 © Fleet Management.
</div>

<div class="logo-sertifikat-center"></div>
