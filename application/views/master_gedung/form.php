<?php
$method = $this->uri->segment(3);
$id = $this->uri->segment(4);
switch ($method) {
	case 'tambah':
		$data = array(
					'gedung'			=>'',
					'kuota'			=>'',
					'pic'			=>'',
					'nohp'			=>'',
					'universitas_id'			=>'',
					'status'		=>'');
		$hidden = array();
	break;
	case 'ubah':
		$data = array(
					'gedung' => $record['name'],
					'kuota' => $record['quota'],
					'pic' => $record['nama_pic'],
					'nohp' => $record['phone_pic'],
					'universitas_id' =>$record['univesitas_id'],
					'status'		=>$record['status']);
		$hidden = array('id'	=>$record['id']);
	break;
}
?>

<div class="row">
	<div class="col-lg-6">
		<?php
		if($this->session->flashdata('msg_success')) {?>
			<div class="alert alert-success alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				<?php echo $this->session->flashdata('msg_success');?>
			</div>
		<?php }?>
<!-- UNTUK ERROR UPLOAD -->
<?php 
echo (empty($error) ? "" :"
	<div class='alert alert-danger alert-dismissible fade in' role='alert'>
		<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
		</button>$error
	</div>
");?>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data Gedung</h5>
			</div>
			<div class="ibox-content">
				<?php echo form_open_multipart('gedung/'.$method,'id=gedung class=form-horizontal',$hidden); ?>
		  	   		 	<div class="form-group">
                        	<div class="row" style="margin-right: 0px;">
				  	   			<label class="control-label col-md-2 col-md-offset-1">Nama Gedung</label>
								<div class="col-md-3">
									<input type="text" name="gedung" class="form-control" placeholder="Nama Gedung" value="<?php echo $data['gedung'];?>" required>
								</div>
								<label class="control-label col-md-1">Kuota</label>
				  	   			<div class="col-md-3">
									<input type="number" name="kuota" class="form-control" placeholder="Kuota" value="<?php echo $data['kuota'];?>" required>
								</div>
							</div>
						</div>
						<div class="form-group">
                        	<div class="row" style="margin-right: 0px;">
								<label class="control-label col-md-2 col-md-offset-1">Nama PIC</label>
								<div class="col-md-3">
									<input type="text" name="pic" class="form-control" placeholder="Nama PIC" value="<?php echo $data['pic'] ?>" required>
								</div>
								<label class="control-label col-md-1">Telepon PIC</label>
								<div class="col-md-3">
									<input type="number" name="nohp" class="form-control" placeholder="08xxxxxxxxxx" value="<?php echo $data['nohp'] ?>" required">
								</div>
							</div>
						</div>
						<div class="form-group">
                        	<div class="row" style="margin-right: 0px;">
				  	   			<label class="control-label col-md-3">Universitas</label>
				  	   			<div class="col-md-7">
									<?php
										unset($options);
										$options = array('' => 'Pilih Universitas');
										$selected = '';
										foreach ($tmp_options = auto_get_options('universitas','id,name','ORDER BY name ASC') as $info){
											$options[$info['id']] = $info['name'];
										}
										echo form_dropdown('universitas',$options,$data['universitas_id'],'class="form-control" id="universitas" required');
									?>
								</div>
							</div>
						</div>
						<?php
							if ($method == 'ubah') {
								echo '	<div class="form-group">
										<div class="row" style="margin-right: 0px;">
											<label class="control-label col-md-3">Status</label>
											<div class="col-md-7">';
												$options = array(	''	=> 'Status',
																	'0'	=> 'Tidak Aktif',
																	'1'	=> 'Aktif',
																	'2'	=> 'Blokir'
																);
												echo form_dropdown('status',$options,$data['status'],'class="form-control" id="status" required');
								echo '		</div>
										</div>
										</div>';
							}
						?>
						<hr>
						<div class="form-group">
							<div class="pull-left">
								<a class="btn btn-danger" href="<?php echo base_url();?>master_data/gedung">Kembali</a>
							</div>
							<div class="pull-left" style="margin-left: 5px;">
								<button type="submit" name="submit" class="btn btn-block btn-success">Simpan</button>
							</div>
						</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
</div>