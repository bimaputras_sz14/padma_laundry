<?php
$method = $this->uri->segment(3);
$id = $this->uri->segment(4);
switch ($method) {
	case 'tambah':
		$data = array(
					'nama'			=>'',
					'harga'			=>'',
					'foto'			=>'');
		$hidden = array();
	break;
	case 'ubah':
		$data = array(
					'nama'			=>$record['nama_produk'],
					'harga'			=>$record['harga'],
					'foto'			=>'');
		$hidden = array(
					'id'		=> $record['id'],
					'img'		=> $record['foto'],
					'tmpnama'	=> $record['nama_produk']);
	break;
}
?>

<div class="row">
	<div class="col-lg-6">
		<?php
		if($this->session->flashdata('msg_success')) {?>
			<div class="alert alert-success alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				<?php echo $this->session->flashdata('msg_success');?>
			</div>
		<?php }?>
<!-- UNTUK ERROR UPLOAD -->
<?php 
echo (empty($error) ? "" :"
	<div class='alert alert-danger alert-dismissible fade in' role='alert'>
		<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
		</button>$error
	</div>
");?>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data Produk</h5>
			</div>
			<div class="ibox-content">
				<?php echo form_open_multipart('master_data/produk/'.$method,'id=produk class=form-horizontal',$hidden); ?>
		  	   		 	<div class="form-group">
		  	   		 		<div class="row" style="margin-right: 0px;">
				  	   			<label class="control-label col-md-1">Nama</label>
								<div class="col-md-3">
									<input type="text" name="nama" class="form-control" placeholder="Nama" value="<?php echo $data['nama'];?>" required>
								</div>
								<label class="control-label col-md-1">Harga</label>
								<div class="col-md-3">
									<input type="number" name="harga" id="harga" class="form-control tgl" placeholder="Rp." value="<?php echo $data['harga'] ?>" required>
								</div>
							</div>
						</div>
						<div class="form-group">
		  	   		 		<div class="row">
								<label class="control-label col-md-1" style="margin-top:22px;">Foto</label>
								<div class="col-md-7">
									<?php
									if ($method == "ubah") {
										echo "<input type='file' name='foto' class='file' data-show-upload='false' accept='.jpg'>";
									}
									else {
										echo "<input type='file' name='foto' class='file' data-show-upload='false' required accept='.jpg'>";
									}
									?>
									<small>File harus dibawah 1 MB dengan tipe *.JPG</small>
							</div>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="pull-left">
								<a class="btn btn-danger" href="<?php echo base_url();?>master_data/produk">Kembali</a>
							</div>
							<div class="pull-left" style="margin-left: 5px;">
								<button type="submit" name="submit" class="btn btn-block btn-success">Simpan</button>
							</div>
						</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
</div>