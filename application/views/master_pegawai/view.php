<?php
if($this->session->flashdata('msg_success')) {?>
	<div class="alert alert-success alert-dismissible fade in" role="alert">
		<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
		<?php echo $this->session->flashdata('msg_success');?>
	</div>
<?php }?>
<?php
if($this->session->flashdata('msg_failed')) {?>
	<div class="alert alert-danger alert-dismissible fade in" role="alert">
		<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
		<?php echo $this->session->flashdata('msg_failed');?>
	</div>
<?php }?>
<style type="text/css">
	.table>tbody>tr>td{
		vertical-align: middle;
	}
</style>	
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data Pegawai</h5>
			</div>
			<div class="ibox-content">
				<?php echo anchor('master_data/pegawai/tambah','+ Tambah','class="btn btn-primary" style="font-weight:bold;"'); ?>
				<hr>
				<div class="table-responsive">
					<table id="table-pegawai" class="table table-striped table-bordered table-hover dt-responsive nowrap">
			 			<thead>
							<tr>
								<th class="text-center">No.</th>
								<th>NIK</th>
								<th>Nama</th>
								<th>Email</th>
								<th>Foto</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<td colspan="6" class="dataTables_empty text-center">Loading data from server</td>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Datatables -->
<script type="text/javascript">
	var save_method; //for save method string
	var table;
	$(document).ready(function() {
		//datatables
		table = $('#table-pegawai').DataTable({
            dom: 'lTfgitp',
			"responsive": true,
			"processing": false, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [2,'asc'], //Initial no order.
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('pegawai/json');?>',
				"type": "POST",
				"dataType": "json"
			},
			"columnDefs": [
				{
					"searchable": false,
					"orderable": false,
					"targets": 5
				},
				{
					"searchable": false,
					"orderable": false,
					"targets": 6
				},
				{
					"class": "middle-text",
					"targets": 4,
					"data": 16,
					"orderable": false,
					"sortable": false,
					"render": function (data, type, row, meta){
					// If data will be displayed
						if(type === "display"){
							if (data == 'null') {
								return '<img src="<?php echo base_url(); ?>uploads/no-image.png" style="max-width: 150px; height: auto;">';
							}
							else{
								return '<img src="<?php echo base_url(); ?>uploads/' + data + '" style="max-width: 150px; height: auto;">';
							}
							// Otherwise, if search/filtering is performed
						} else {
							return data;  
						}
					}
				}
			],
			"fnRowCallback": function(nRow,aData,iDisplayIndex,iDisplayIndexFull) {
				var index = iDisplayIndex + 1;
				$('td:eq(0)',nRow).html(index);
				return nRow;
			},
			//Set column definition initialisation properties.
			"aoColumns": [
				{"mData": 'id',class:'text-center',width:10},
				{"mData": "nik"},
				{"mData": "nama"},
				{"mData": "email"},
				{"mData": "foto",class:'text-center',width:100},
				{"mData": "status", class:'text-center',
                 "mRender": function(data, type, full) {
                            if(data == 1){
                                return '<span class="label label-primary">Aktif</span>';
                            }else if(data == 2){
                                return '<span class="label label-danger">Blokir</span>';
                            }else{
                                return '<span class="label label-default">Tidak Aktif</span>';
                            }
                          }
                },
				{"mData": "action",class:'text-center',width:120}
			],
			"language": {
				"zeroRecords": "Data tidak ditemukan",
				"infoEmpty": "Tidak ada data"
			}
		});
	});
</script>
<!-- /Datatables -->