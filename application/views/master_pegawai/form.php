<?php
$method = $this->uri->segment(3);
$id = $this->uri->segment(4);
switch ($method) {
    case 'tambah':
    $data = array(
        'nik'          =>'',
        'nama'         =>'',
        'password'     =>'',
        'tempat'       =>'',
        'tanggal'      =>'',
        'jabatan'      =>'',
        'telepon'      =>'',
        'email'        =>'',
        'foto'         =>'',
        'status'       =>'',
        'info'         =>'',
        'user_level'   =>'');
    $hidden = array();
    break;
    case 'ubah':
    $data = array(
        'nik'          =>$record['nik'],
        'nama'         =>$record['nama'],
        'password'     =>$record['password'],
        'tempat'       =>$record['ttl'],
        'tanggal'      =>$record['dob'],
        'jabatan'      =>$record['jabatan_id'],
        'telepon'      =>$record['telepon'],
        'email'        =>$record['email'],
        'foto'         =>$record['foto'],
        'status'       =>$record['status'],
        'info'         =>$record['info'],
        'user_level'   =>$record['user_level_id']);
    $hidden = array(
        'id'=>$record['id'],
        'tnama'=>$record['nama'],
        'foto'=>$record['foto']);
    break;
}
?>

<div class="row">
    <div class="col-lg-6">
        <?php
        if($this->session->flashdata('msg_success')) {?>
        <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?php echo $this->session->flashdata('msg_success');?>
        </div>
        <?php }?>
    </div>
<!-- UNTUK ERROR UPLOAD -->
<?php 
echo (empty($error) ? "" :"
    <div class='alert alert-danger alert-dismissible fade in' role='alert'>
        <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
        </button>$error
    </div>
");?>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Formulir Tambah Pegawai</h5>
            </div>
            <div class="ibox-content">
                <?php echo form_open_multipart('master_data/pegawai/'.$method,'id=form_pegawai class=form-horizontal',$hidden); ?>
                    <div class="form-group">
                        <div class="row" style="margin-right: 0px;">
                            <label class="col-md-2 control-label">NIK</label>
                            <div class="col-md-3">
                                <input type="text" name="nik" class="form-control" placeholder="NIK" value="<?php echo $data['nik'];?>" required>
                            </div>
                            <label class="col-md-1 control-label">Nama</label>
                            <div class="col-md-3">
                                <input type="text" name="nama" class="form-control" placeholder="Nama" value="<?php echo $data['nama'];?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        
                    </div>
<?php
if ($method == 'tambah') {
    echo '
        <div class="form-group">
            <div class="row" style="margin-right: 0px;">
                <label class="control-label col-md-2 ">Password</label>
                <div class="col-md-3">
                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                </div>
                <label class="col-md-1" style="text-align:right;">Confirm Password</label>
                <div class="col-md-3">
                        <input type="password" name="cpassword" class="form-control" placeholder="Confirm Password" required>
                </div>
            </div>
        </div>
    ';
}
?>
                    <div class="form-group" id="data_1">
                        <div class="row" style="margin-right: 0px;">
                            <label class="control-label col-md-2">Tempat Lahir</label>
                            <div class="col-md-3">
                                <input type="text" name="tempat" class="form-control" placeholder="Tempat Lahir" value="<?php echo $data['tempat'];?>" required>
                            </div>
                            <label class="col-md-1" style="text-align:right;">Tanggal Lahir</label>
                            <div class="input-group date col-md-3" style="padding-left: 15px;padding-right: 15px;">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="tanggal" class="form-control" placeholder="Tanggal Lahir" value="<?php echo $data['tanggal'];?>">
                            </div>
                            <!--<label class="col-md-1" style="text-align:right;">Tanggal Lahir</label>
                            <div class="col-md-3">
                                <input type="text" name="tanggal" class="form-control" placeholder="Tanggal Lahir" value="<?php echo $data['tanggal'];?>" required>
                            </div>-->
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row" style="margin-right: 0px;">
                            <label class="control-label col-md-2">Jabatan</label>
                            <div class="col-md-3">
                                <?php
                                    unset($options);
                                    foreach ($tmp_options = auto_get_options('jabatan','id,jabatan','ORDER BY jabatan ASC') as $info){
                                        $options[$info['id']] = $info['jabatan'];
                                    }
                                    echo form_dropdown('jabatan',$options,$data['jabatan'],'class="form-control" id=opsi-jabatan');
                                 ?>
                            </div>
                            <label class="control-label col-md-1">Telepon</label>
                            <div class="col-md-3">
                                <input type="number" name="telepon" class="form-control" placeholder="Telepon" value="<?php echo $data['telepon'];?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row" style="margin-right: 0px;">
                            <label class="control-label col-md-2" style="margin-top:22px;">Foto</label>
                            <div class="col-md-7">
                                <?php
                                if ($method == 'tambah') {
                                    echo '
                                        <input type="file" name="foto" class="form-control file" placeholder="Foto" required data-show-upload="false" accept=".jpg">
                                    ';
                                }
                                else {
                                    echo '
                                        <input type="file" name="foto" class="form-control file" placeholder="Foto" data-show-upload="false" accept=".jpg">
                                    ';
                                }
                                ?>
                                <small>File harus dibawah 1 MB dengan tipe *.JPG</small>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row" style="margin-right: 0px;">
                            <label class="control-label col-md-2">Email</label>
                            <div class="col-md-3">
                                <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo $data['email'];?>" required>
                            </div>
                            <label class="control-label col-md-1">Status</label>
                            <div class="col-md-3">
                                <select name="status" class="form-control" required>
                                    <option value="">Status</option>
                                    <option value="0" <?php echo (!empty($data['status']) && $data['status'] == 0)?"selected":""; ?>>Tidak Aktif</option>
                                    <option value="1" <?php echo (!empty($data['status']) && $data['status'] == 1)?"selected":""; ?>>Aktif</option>
                                    <option value="2" <?php echo (!empty($data['status']) && $data['status'] == 2)?"selected":""; ?>>Blok</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row" style="margin-right: 0px;">
                            <label class="control-label col-md-2 ">Info</label>
                            <div class="col-md-3">
                                <input type="text" name="info" class="form-control" placeholder="Info" value="<?php echo $data['info'];?>" required>
                            </div>
                            <label class="col-md-1" style="text-align: right;">User Level</label>
                            <div class="col-md-3">
                                <?php
                                    unset($options);
                                    $options = array('' => 'User Level');
                                    foreach ($tmp_options = auto_get_options('user_level','lvl_id,nama','ORDER BY nama ASC') as $info){
                                        $options[$info['lvl_id']] = $info['nama'];
                                    }
                                    echo form_dropdown('user_level',$options,$data['user_level'],'class="form-control" id=user_level required');
                                 ?>
                            </div>
                            <!--<label class="col-md-1" style="text-align: right;">User Level</label>
                            <div class="col-md-3">
                                <input type="text" name="user_level" class="form-control" placeholder="User Level" value="<?php echo $data['user_level'];?>" required>
                            </div>-->
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="pull-left">
                            <a class="btn btn-danger" href="<?php echo base_url();?>master_data/pegawai">Kembali</a>
                        </div>
                        <div class="pull-left" style="margin-left: 5px;">
                            <button type="submit" name="submit" class="btn btn-block btn-success">Simpan</button>
                        </div>
                    </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: 'yyyy-mm-dd'
    });
</script>