<div class="row m-b-xl">
    <?php if($produk!=''){ ?>
    <!-- <div class="col-md-3">
        <div class="ibox float-e-margins no-paddings"> 
            <div> 
                <div class="ibox-content profile-content no-paddings">
                    <?php  
                    if($produk->foto){
                        echo '<img alt="image" class="img-responsive" src="'.base_url().'uploads/produk/'.$produk->foto.'">';
                    } 
                    echo '<h4><strong>'.$produk->nama_produk.'</strong></h4> 
                      ';
                    ?>
                    <hr> 
                </div>
            </div>
        </div>
       
    </div> -->
    <?php } ?>
    <div class="col-md-9"> 
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-1"> Data Produk</a></li>
                 
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="panel-body">
                        <form method='post' id="form_insert" class="form-horizontal" enctype="multipart/form-data" >
                              <div class="form-group  col-md-6">
                                <label class="control-label col-md-5 font-green-haze">Kode Produk </label>
                                <div class="col-md-7">
                                    <input  type='text' id='kode_produk' name='kode_produk' class='form-control' value="<?php echo isset($produk->kode_produk)?$produk->kode_produk:''?>">
                                </div>
                            </div>  
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-5 font-green-haze">Nama Produk<span class="required  ">
                                    * </span>
                                </label>
                                <div class="col-md-7">
                                    <input  type='text' id='nama_produk' name='nama_produk' class='form-control' value="<?php echo isset($produk->nama_produk)?$produk->nama_produk:''?>">
                                </div>
                            </div>
                            <div class="form-group  col-md-6">
                                <label class="control-label col-md-5 font-green-haze">Kategori Produk </label>
                                <div class="col-md-7">  
                                 <?php echo form_dropdown('kategori_produk_id', $kategori_produk, isset($produk->kategori_produk_id)?$produk->kategori_produk_id:'','class="form-control" id="kategori_produk_id"'); ?> 
                                </div>
                            </div> 
                          <!--   <div class="form-group  col-md-6">
                                <label class="control-label col-md-5 font-green-haze">Kelas </label>
                                <div class="col-md-7">  
                                 <?php echo form_dropdown('kelas_id', $kelas, isset($produk->kelas_id)?$produk->kelas_id:'','class="form-control" id="kelas_id"'); ?> 
                                </div>
                            </div>  -->
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-5 font-green-haze">Harga Produk<span class="required  ">
                                    * </span>
                                </label>
                                <div class="col-md-7">
                                    <input  type='text' id='harga' name='harga' class='form-control' value="<?php echo isset($produk->harga)?$produk->harga:''?>">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-5 font-green-haze">Jumlah Token<span class="required  ">
                                    * </span>
                                </label>
                                <div class="col-md-7">
                                    <input  type='text' id='total_token' name='total_token' class='form-control' value="<?php echo isset($produk->total_token)?$produk->total_token:''?>">
                                </div>
                            </div> 
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-5 font-green-haze">Awal Promo<span class="required  ">
                                    * </span>
                                </label>
                                <div class="col-md-7" id="data_1">
                                   <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="promo_mulai" class="form-control" value="<?php echo isset($produk->promo_mulai)?date('d/m/Y',  strtotime($produk->promo_mulai)):''?>">
                                    </div>
                                </div>
                            </div> 
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-5 font-green-haze">Akhir Promo<span class="required  ">
                                    * </span>
                                </label>
                                <div class="col-md-7" id="data_1">
                                   <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="promo_akhir" class="form-control" value="<?php echo isset($produk->promo_akhir)?date('d/m/Y',  strtotime($produk->promo_akhir)):''?>">
                                    </div>
                                </div>
                            </div> 
                            <div class="form-group col-md-6">
                                <label class="control-label col-md-5 font-green-haze">Harga Promo<span class="required  ">
                                    * </span>
                                </label>
                                <div class="col-md-7">
                                    <input type='text' id='harga_promo' name='harga_promo' class='form-control' value="<?php echo isset($produk->harga_promo)?$produk->harga_promo:''?>">
                                </div>
                            </div>  
                            <!-- <div class="form-group col-md-6">
                                <label class="control-label col-md-5 font-green-haze">Upload Image</label>
                                <div class="input-group col-md-7">
                                    <span class="input-group-btn">
                                        <span class="btn btn-default btn-file">
                                            Browse… <input type="file" id="imgInp" name="foto">
                                        </span>
                                    </span>
                                    <input type="text" class="form-control" readonly>
                                </div> 
                            </div>  -->
                            <div class="clearfix"></div> 
                            <hr>
                            <div class="form-group col-md-6">
                                <a href="<?php echo site_url('produk')?>"  class="btn btn-danger" data-dismiss="modal">Kembali</a> 
                                <?php if($produk!=''){ ?>
                                 <input type="hidden" name="produk_id" value="<?php echo $produk->id?>">
                                <button type="button" onclick='do_update();' class="btn btn-primary">Update</button>
                                <?php } else {?> 
                                <button type="button" onclick='do_insert();' class="btn btn-primary">Simpan</button>
                                  <?php } ?>
                            </div> 
                        </form>
                    </div>
                </div>
               
            </div> 
        </div> 
    </div> 
    <div class="clearfix"></div>
</div>  

 