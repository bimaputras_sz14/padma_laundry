 <!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title"> 
                <div class="ibox-tools">   
                    <a href="<?php echo site_url('produk/manage')?>" class="btn btn-primary">
                    	<i class="fa fa-plus"></i> Tambah
                    </a>
                </div>
            </div> 
            <div class="ibox-content"> 
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-produk" >
                        <thead>
							<tr>
								<th style="max-width:10%">
									No
								</th>
								<th style="max-width:15%">
									Kode Produk
								</th>
								<th style="max-width:15%">
									Nama
								</th>
								<th style="max-width:25%">
									Kategori
								</th>
								<th style="max-width:10%">
									Harga
								</th> 
								<th style="max-width:10%">
									Tindakan
								</th>
							</tr>
						</thead>
                        <tbody> 
                        </tbody> 
                    </table>
                </div> 
            </div>
        </div>
    </div>
</div>