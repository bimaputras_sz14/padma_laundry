<?php $this->load->view('includes/scripts_standart'); ?> 
<script src="<?php echo $this->config->item('assets_dir');?>js/plugins/dataTables/datatables.min.js"></script>
<script>
    function do_insert(){ 
        var form_data = new FormData($("#form_insert")[0]);
        $.ajax({
            url:"<?php echo base_url(); ?>produk/do_insert",
            type:"POST",
            // data:$("#form_insert").serialize(),
            enctype: 'multipart/form-data',
            data: form_data,
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success:function(data){
                if(data.status!="error"){
                    toastr.success('Data Berhasil Di Simpan', 'Data Member');
                    window.location.href = '<?php echo site_url("produk/manage"); ?>/'+data.id_pel; 
                }else{
                    toastr.error(data.error, 'Data Member');
                }
            }
        });
    } 

    function do_update(){ 
         var form_data = new FormData($("#form_insert")[0]);
        $.ajax({
            url:"<?php echo base_url(); ?>produk/do_update",
            type:"POST", 
            enctype: 'multipart/form-data',
            data: form_data,
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success:function(data){
                if(data.status!="error"){
                    toastr.success('Data Berhasil Di Simpan', 'Data Produk');
                    window.location.href = '<?php echo site_url("produk/manage"); ?>/'+data.id_pel; 
                }else{
                    toastr.error(data.error, 'Data Member');
                }
            }
        });
    } 

    function do_delete(id){  
        if(confirm("Apakah data produk ini akan dihapus?")){ 
            if((id != "") && (id != null)){  
                $.ajax({
                    url: "<?php echo site_url('produk/do_delete'); ?>",
                    type: 'POST',
                    data: {
                        id: id
                    },
                      dataType:"json",
                    success: function(msg) { 
                        if(msg.result == "success"){ 
                            toastr['success']( 'Data telah berhasil dihapus','Data Member');  
                            datatables();
                        }else{
                            toastr['error'](  'Data gagal dihapus','Data Member'); 
                        }
                    }
                });
            }else{
                alert("data tidak ditemukan"); 
            }
        }
    }

    
    function datatables(){
        var table = $('.table-produk');
        var oTable = table.dataTable();
        oTable.fnClearTable();
        oTable.fnDestroy();

        $('.table-produk').DataTable({
            pageLength: 25,
            responsive: true,
            ajax: {
                url: "<?php echo site_url('produk/data_produk')?>",
                type: "POST"
            },
            dom: '<"html5buttons"B>lTfgitp',
            columns: [
                { "data": "no" },
                { "data": "kode_produk" },
                { "data": "nama_produk" },
                { "data": "nama_kategori" },
                { "data": "harga" }, 
                { "data": "btn" }
            ],
            buttons: [] 
        });
    }

    $(document).ready(function(){
        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
        datatables(); 
    });

    </script>