<div class="row">
    <?php 
    if ($member) {
     foreach($member as $row){  ?>

     <div class="col-lg-4 m-b-xs">
        <div class="contact-box">
            <a href="<?php echo site_url('member/manage/').'/'.$row->id ?>">
                <div class="col-sm-4">
                    <div class="text-center">
                        <img alt="image" class="  m-t-xs img-responsive" src="<?php echo base_url(); ?>/uploads/member/<?php echo $row->foto?>"> 
                    </div>
                </div>
                <div class="col-sm-8">
                    <h3><strong><?php echo strtoupper( $row->nama_pelanggan )?></strong></h3> 
                    <address>
                        <i class="fa fa-tags"></i> <?php echo $row->no_kartu?><br>
                        <abbr title="Phone"><i class="fa fa-phone"></i> </abbr> <?php echo $row->hp?>
                    </address>
                </div>
               <div class="clearfix"></div>
                
              <!--   <div class="col-sm-12 col-md-4" >
                    <button onclick='do_checkin(<?php echo $row->id ?>);' class="btn btn-xs btn-primary">Masuk <i class="fa fa-arrow-circle-right"></i></button>  
                </div>
                <div class="col-sm-12 col-md-4" >   
                    <button onclick='do_checkin(<?php echo $row->id ?>);' class="btn btn-xs btn-danger">Keluar <i class="fa fa-arrow-circle-left"></i></button> 
                </div> -->
                <div class="col-sm-12 col-md-4" >
                    <a href="<?php echo site_url('penjualan/penjualan_token').'/'.$row->id ?>" class="btn btn-xs btn-white">Pembelian <i class="fa fa-shopping-cart"></i></a>  
                </div>
                <div class="clearfix"></div>      
                
            </a>
        </div>
    </div>

     
    <?php }  
    }else{ ?>
    <div class="alert alert-danger">
        Data Tidak Ada.
    </div>
    <?php  } ?>  
</div>