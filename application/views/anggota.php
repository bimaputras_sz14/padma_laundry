<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anggota extends MY_Controller {
 
	function index()
    {
		$this->data_anggota();
	}
 

 
 
 
 
	//SHOW MENU INTERFACE
    function data_anggota()
    {
		$this->load->model('sipinter_anggota_model');
		$this->load->model('sipinter_kesatuan_model');
		$this->load->model('sipinter_pangkat_model');
		
		$this->data['title'] = "Data Anggota";
		$this->data['body'] = "anggota/anggota_view";
		$this->data['styles'] = "includes/styles_data_table_full";
		$this->data['scripts'] = "anggota/scripts_anggota";
		$this->data['page_bar'] = "includes/template_site_bar_blank";
		$this->data['desc'] = "daftar anggota kesatuan";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['message'] = $this->session->flashdata('message');
		
		$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
		$nrp = $arr_usr['uacc_nrp'];
		$level = $arr_usr['uacc_group_fk'];
		
		if($level != 1){
			$usr = $this->sipinter_anggota_model->get_anggota_by_nrp($nrp);
			$kelas = $usr->kelas;
			$kode_kesatuan = $usr->kode_kesatuan;		
			$relasi = $this->sipinter_kesatuan_model->get_relasi_kesatuan($kelas, $kode_kesatuan);
			
			if($kelas == "KORAMIL"){
				$this->data['select_koramil'] = $this->set_select_kesatuan($relasi['KORAMIL']);
				$this->data['select_kodim'] = $this->set_select_kesatuan($relasi['KODIM']);
				$this->data['select_korem'] = $this->set_select_kesatuan($relasi['KOREM']);
				$this->data['select_kodam'] = $this->set_select_kesatuan($relasi['KODAM']);
				
				//input data anggota
				$this->data['ida_select_kesatuan'] = $this->set_select_kesatuan($relasi['KORAMIL']);
				$this->data['ida_select_induk_kesatuan'] = $this->set_select_kesatuan($relasi['KODIM']);
			}else if($kelas == "KODIM"){
				$this->data['select_koramil'] = $this->get_kesatuan_by_parent($relasi['KODIM']);
				$this->data['select_kodim'] = $this->set_select_kesatuan($relasi['KODIM']);
				$this->data['select_korem'] = $this->set_select_kesatuan($relasi['KOREM']);
				$this->data['select_kodam'] = $this->set_select_kesatuan($relasi['KODAM']);
				
				//input data anggota
				$this->data['ida_select_kesatuan'] = $this->get_kesatuan_by_parent($relasi['KODIM']);
				$this->data['ida_select_induk_kesatuan'] = $this->set_select_kesatuan($relasi['KODIM']);
			}else if($kelas == "KOREM"){
				$this->data['select_koramil'] = $this->set_select_kesatuan('');
				$this->data['select_kodim'] = $this->get_kesatuan_by_parent($relasi['KOREM']);
				$this->data['select_korem'] = $this->set_select_kesatuan($relasi['KOREM']);
				$this->data['select_kodam'] = $this->set_select_kesatuan($relasi['KODAM']);
				
				//input data anggota
				$this->data['ida_select_kesatuan'] = $this->get_kesatuan_by_parent($relasi['KOREM']);
				$this->data['ida_select_induk_kesatuan'] = $this->set_select_kesatuan($relasi['KOREM']);
			}else if($kelas == "KODAM"){
				$this->data['select_koramil'] = $this->set_select_kesatuan('');
				$this->data['select_kodim'] = $this->set_select_kesatuan('');
				$this->data['select_korem'] = $this->get_kesatuan_by_parent($relasi['KODAM']);
				$this->data['select_kodam'] = $this->set_select_kesatuan($relasi['KODAM']);
				
				//input data anggota
				$this->data['ida_select_kesatuan'] = $this->get_kesatuan_by_parent($relasi['KODAM']);
				$this->data['ida_select_induk_kesatuan'] = $this->set_select_kesatuan($relasi['KODAM']);
				
				if($level == 2){
					$this->data['select_koramil'] = $this->set_select_kesatuan('');
					$this->data['select_kodim'] = $this->set_select_kesatuan('');
					$this->data['select_korem'] = $this->get_kesatuan_by_parent($relasi['KODAM']);
					$this->data['select_kodam'] = $this->set_select_kesatuan($relasi['KODAM']);
			
					//input data anggota
					$this->data['ida_select_kesatuan'] = $this->set_select_kesatuan('');
					$this->data['ida_select_induk_kesatuan'] = $this->set_select_list_induk_kesatuan();
				}
			}else{
				$this->data['select_koramil'] = $this->set_select_kesatuan('');
				$this->data['select_kodim'] = $this->set_select_kesatuan('');
				$this->data['select_korem'] = $this->set_select_kesatuan('');
				$this->data['select_kodam'] = $this->set_select_kesatuan_by_kelas("KODAM");
				
				//input data anggota
				$this->data['ida_select_kesatuan'] = $this->set_select_kesatuan('');
				$this->data['ida_select_induk_kesatuan'] = $this->set_select_list_induk_kesatuan();
			}
			
			
		}else{
			$this->data['select_koramil'] = $this->set_select_kesatuan('');
			$this->data['select_kodim'] = $this->set_select_kesatuan('');
			$this->data['select_korem'] = $this->set_select_kesatuan('');
			$this->data['select_kodam'] = $this->set_select_kesatuan_by_kelas("KODAM");
			
			//input data anggota
			$this->data['ida_select_kesatuan'] = $this->set_select_kesatuan('');
			$this->data['ida_select_induk_kesatuan'] = $this->set_select_list_induk_kesatuan();
		}
		
		//input data anggota
		$this->data['ida_select_pangkat'] = $this->set_select_pangkat();
		$this->data['ida_select_jabatan'] = $this->set_select_jabatan();
		
		
		$this->load->view('includes/template_site', $this->data);
	}
	
	
	
	
	function web_user()
    {
		$this->load->model('sipinter_anggota_model');
		$this->load->model('sipinter_kesatuan_model');
		$this->load->model('sipinter_pangkat_model');
		
		$this->data['title'] = "User WEB";
		$this->data['page_bar'] = "includes/template_site_bar_blank";
		$this->data['desc'] = "daftar user WEB-APP";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['message'] = $this->session->flashdata('message');
		
		
		if ($this->flexi_auth->is_privileged('View Users')) {
			$this->data['body'] = "anggota/user_web_view";
			$this->data['styles'] = "includes/styles_data_table";
			$this->data['scripts'] = "anggota/scripts_user_web";
		
		
			$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
			$nrp = $arr_usr['uacc_nrp'];
			$level = $arr_usr['uacc_group_fk'];
			
			if($level != 1){
				$current_level = $level;
				$usr = $this->sipinter_anggota_model->get_anggota_by_nrp($nrp);
				$kelas = $usr->kelas;
				$kode_kesatuan = $usr->kode_kesatuan;		
				$relasi = $this->sipinter_kesatuan_model->get_relasi_kesatuan($kelas, $kode_kesatuan);
				
				if($kelas == "KORAMIL"){
					$this->data['select_koramil'] = $this->set_select_kesatuan($relasi['KORAMIL']);
					$this->data['select_kodim'] = $this->set_select_kesatuan($relasi['KODIM']);
					$this->data['select_korem'] = $this->set_select_kesatuan($relasi['KOREM']);
					$this->data['select_kodam'] = $this->set_select_kesatuan($relasi['KODAM']);
				}else if($kelas == "KODIM"){
					$this->data['select_koramil'] = $this->get_kesatuan_by_parent($relasi['KODIM']);
					$this->data['select_kodim'] = $this->set_select_kesatuan($relasi['KODIM']);
					$this->data['select_korem'] = $this->set_select_kesatuan($relasi['KOREM']);
					$this->data['select_kodam'] = $this->set_select_kesatuan($relasi['KODAM']);
				}else if($kelas == "KOREM"){
					$this->data['select_koramil'] = $this->set_select_kesatuan('');
					$this->data['select_kodim'] = $this->get_kesatuan_by_parent($relasi['KOREM']);
					$this->data['select_korem'] = $this->set_select_kesatuan($relasi['KOREM']);
					$this->data['select_kodam'] = $this->set_select_kesatuan($relasi['KODAM']);
				}else if($kelas == "KODAM"){
					$this->data['select_koramil'] = $this->set_select_kesatuan('');
					$this->data['select_kodim'] = $this->set_select_kesatuan('');
					$this->data['select_korem'] = $this->get_kesatuan_by_parent($relasi['KODAM']);
					$this->data['select_kodam'] = $this->set_select_kesatuan($relasi['KODAM']);
				}else{
					$this->data['select_koramil'] = $this->set_select_kesatuan('');
					$this->data['select_kodim'] = $this->set_select_kesatuan('');
					$this->data['select_korem'] = $this->set_select_kesatuan('');
					$this->data['select_kodam'] = $this->set_select_kesatuan('');
				}
			}else{
				$current_level = 0;
				
				$this->data['select_koramil'] = $this->set_select_kesatuan('');
				$this->data['select_kodim'] = $this->set_select_kesatuan('');
				$this->data['select_korem'] = $this->set_select_kesatuan('');
				$this->data['select_kodam'] = $this->set_select_kesatuan_by_kelas("KODAM");
			}
			
			
			

			if (!$this->flexi_auth->is_privileged('Update Users')) {
				//$crud->unset_add();
				//$crud->unset_edit();
			}
			if (!$this->flexi_auth->is_privileged('Delete Users')) {
				//$crud->unset_delete();
			}
			if ((!$this->flexi_auth->is_privileged('Update Users')) && (!$this->flexi_auth->is_privileged('Delete Users'))) {
				//$crud->unset_operations();
			}
			
			
			//INPUT FORM
			$this->data['iuw_select_level'] = $this->set_select_user_level($current_level);
			$this->data['iuw_select_level_selected'] = array('7');
			
			
			$this->load->view('includes/template_site', $this->data);
		}else{
			$this->data['body'] = "error/403";
			$this->data['styles'] = "includes/styles_standart";
			$this->data['scripts'] = "includes/scripts_standart";
			$this->load->view('includes/template_error', $this->data);
		}
		
	}
	
	
	
	
	function app_user()
    {
		$this->load->model('sipinter_anggota_model');
		$this->load->model('sipinter_kesatuan_model');
		$this->load->model('sipinter_pangkat_model');
		
		$this->data['title'] = "User APP Android";
		$this->data['page_bar'] = "includes/template_site_bar_blank";
		$this->data['desc'] = "daftar user mobile APP";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['message'] = $this->session->flashdata('message');
		
		
		if ($this->flexi_auth->is_privileged('View Users')) {
			$this->data['body'] = "anggota/user_apk_view";
			$this->data['styles'] = "includes/styles_data_table";
			$this->data['scripts'] = "anggota/scripts_user_apk";
		
		
			$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
			$nrp = $arr_usr['uacc_nrp'];
			$level = $arr_usr['uacc_group_fk'];
			
			if($level != 1){
				$current_level = $level;
				$usr = $this->sipinter_anggota_model->get_anggota_by_nrp($nrp);
				$kelas = $usr->kelas;
				$kode_kesatuan = $usr->kode_kesatuan;		
				$relasi = $this->sipinter_kesatuan_model->get_relasi_kesatuan($kelas, $kode_kesatuan);
				
				if($kelas == "KORAMIL"){
					$this->data['select_koramil'] = $this->set_select_kesatuan($relasi['KORAMIL']);
					$this->data['select_kodim'] = $this->set_select_kesatuan($relasi['KODIM']);
					$this->data['select_korem'] = $this->set_select_kesatuan($relasi['KOREM']);
					$this->data['select_kodam'] = $this->set_select_kesatuan($relasi['KODAM']);
				}else if($kelas == "KODIM"){
					$this->data['select_koramil'] = $this->get_kesatuan_by_parent($relasi['KODIM']);
					$this->data['select_kodim'] = $this->set_select_kesatuan($relasi['KODIM']);
					$this->data['select_korem'] = $this->set_select_kesatuan($relasi['KOREM']);
					$this->data['select_kodam'] = $this->set_select_kesatuan($relasi['KODAM']);
				}else if($kelas == "KOREM"){
					$this->data['select_koramil'] = $this->set_select_kesatuan('');
					$this->data['select_kodim'] = $this->get_kesatuan_by_parent($relasi['KOREM']);
					$this->data['select_korem'] = $this->set_select_kesatuan($relasi['KOREM']);
					$this->data['select_kodam'] = $this->set_select_kesatuan($relasi['KODAM']);
				}else if($kelas == "KODAM"){
					$this->data['select_koramil'] = $this->set_select_kesatuan('');
					$this->data['select_kodim'] = $this->set_select_kesatuan('');
					$this->data['select_korem'] = $this->get_kesatuan_by_parent($relasi['KODAM']);
					$this->data['select_kodam'] = $this->set_select_kesatuan($relasi['KODAM']);
				
					if($level == 2){
						$this->data['select_koramil'] = $this->set_select_kesatuan('');
						$this->data['select_kodim'] = $this->set_select_kesatuan('');
						$this->data['select_korem'] = $this->set_select_kesatuan('');
						$this->data['select_kodam'] = $this->set_select_kesatuan_by_kelas("KODAM");
					}
				}else{
					$this->data['select_koramil'] = $this->set_select_kesatuan('');
					$this->data['select_kodim'] = $this->set_select_kesatuan('');
					$this->data['select_korem'] = $this->set_select_kesatuan('');
					$this->data['select_kodam'] = $this->set_select_kesatuan('');
				}
				
				
				
				//IMPORT FILE
				$this->data['display_import_form']  = " display: none; ";
			}else{
				$current_level = 0;
				
				$this->data['select_koramil'] = $this->set_select_kesatuan('');
				$this->data['select_kodim'] = $this->set_select_kesatuan('');
				$this->data['select_korem'] = $this->set_select_kesatuan('');
				$this->data['select_kodam'] = $this->set_select_kesatuan_by_kelas("KODAM");
				
				
				
				//IMPORT FILE
				$this->data['display_import_form']  = "";
			}
		

			if (!$this->flexi_auth->is_privileged('Update Users')) {
				//$crud->unset_add();
				//$crud->unset_edit();
			}
			if (!$this->flexi_auth->is_privileged('Delete Users')) {
				//$crud->unset_delete();
			}
			if ((!$this->flexi_auth->is_privileged('Update Users')) && (!$this->flexi_auth->is_privileged('Delete Users'))) {
				//$crud->unset_operations();
			}
			
			
			//INPUT FORM
			$this->data['iua_select_level'] = $this->set_select_user_level($current_level);
			$this->data['iua_select_level_selected'] = array('7');
			
			
			$this->load->view('includes/template_site', $this->data);
		}else{
			$this->data['body'] = "error/403";
			$this->data['styles'] = "includes/styles_standart";
			$this->data['scripts'] = "includes/scripts_standart";
			$this->load->view('includes/template_error', $this->data);
		}
		
	}
	//END OF SHOW MENU INTERFACE

	

	
	
	
	
	
	
	
	//DATA ANGGOTA
	//SHOW & GET DATA TABLE
	function get_tabel_anggota(){
        if('IS_AJAX') {
			$this->load->model('sipinter_anggota_model');
			$kodam = $this->input->post('select_kodam');
			$korem = $this->input->post('select_korem');
			$kodim = $this->input->post('select_kodim');
			$koramil = $this->input->post('select_koramil');
			$aktif = $this->input->post('aktif');
			
			if($koramil != ""){
				$kode_kesatuan = $koramil;
			}else if($kodim != ""){
				$kode_kesatuan = $kodim;
			}else if($korem != ""){
				$kode_kesatuan = $korem;
			}else if($kodam != ""){
				$kode_kesatuan = $kodam;
			}else{
				$kode_kesatuan = null;
			}
			
			// $anggota = $this->sipinter_anggota_model->get_list_anggota_by_kesatuan($kodam, $korem, $kodim, $koramil, $aktif);
			$anggota = $this->sipinter_anggota_model->get_list_anggota($kode_kesatuan, $aktif);
			$json['data'] = array();
			
			if ($anggota->num_rows() > 0){
				$i = 1;
				foreach ($anggota->result() as $row){
					$btn = "";
					if (($this->flexi_auth->is_privileged('Update Users')) && ($this->flexi_auth->is_privileged('Delete Users')) ){
						$btn = "<button title='detail anggota' type='button' class='btn btn-primary pull-right' onclick='showDetail(\"".$row->nrp."\")'><i class='fa fa-search'></i></button>";
					}
					$json['data'][] = array(
												"no"=>$i,
												"nrp"=>$row->nrp,
												"nama_anggota"=>$row->nama_anggota,
												"id_pangkat"=>$row->pangkat,
												"jabatan"=>$row->jabatan,
												"nama_kesatuan"=>$row->nama_kesatuan,
												"no_hp"=>$row->no_hp,
												"btn"=>$btn
											);
					$i++;
				}
			}
			print(json_encode($json));
        }
	}
	//END OF SHOW & GET DATA TABLE
	
	
	
	
	
	
	
	//DATA USER WEB
	//SHOW & GET DATA TABLE
	function get_tabel_user_web(){
        if('IS_AJAX') {
			$this->load->model('sipinter_anggota_model');
			$kodam = $this->input->post('select_kodam');
			$korem = $this->input->post('select_korem');
			$kodim = $this->input->post('select_kodim');
			$koramil = $this->input->post('select_koramil');
			$aktif = $this->input->post('aktif');
			
			$user = $this->sipinter_anggota_model->get_list_user_web_by_kesatuan($kodam, $korem, $kodim, $koramil, $aktif);
			$json['data'] = array();
			
			if ($user->num_rows() > 0){
				$i = 1;
				foreach ($user->result() as $row){
					$json['data'][] = array(
												"no"=>$i,
												"ulvl_name"=>$row->ulvl_name,
												"uacc_nrp"=>$row->uacc_nrp,
												"nama_anggota"=>$row->nama_anggota,
												"nama_kesatuan"=>$row->nama_kesatuan,
												"uacc_date_last_login"=>$row->uacc_date_last_login,
												"uacc_date_added"=>$row->uacc_date_added
											);
					$i++;
				}
			}
			print(json_encode($json));
        }
	}
	//END OF SHOW & GET DATA TABLE
	
	
	
	
	
	
	
	
	
	//DATA USER APK
	//SHOW & GET DATA TABLE
	function get_tabel_user_apk(){
        if('IS_AJAX') {
			$this->load->model('sipinter_anggota_model');
			$kodam = $this->input->post('select_kodam');
			$korem = $this->input->post('select_korem');
			$kodim = $this->input->post('select_kodim');
			$koramil = $this->input->post('select_koramil');
			$aktif = $this->input->post('aktif');
			
			$user = $this->sipinter_anggota_model->get_list_user_apk_by_kesatuan($kodam, $korem, $kodim, $koramil, $aktif);
			$json['data'] = array();
			
			if ($user->num_rows() > 0){
				$i = 1;
				foreach ($user->result() as $row){
					$json['data'][] = array(
												"no"=>$i,
												"ulvl_name"=>$row->ulvl_name,
												"uapk_nrp"=>$row->uapk_nrp,
												"nama_anggota"=>$row->nama_anggota,
												"uapk_mdn"=>$row->uapk_mdn,
												"nama_kesatuan"=>$row->nama_kesatuan
											);
					$i++;
				}
			}
			print(json_encode($json));
        }
	}
	//END OF SHOW & GET DATA TABLE
	
	
	
	
	
	
	
	
	
	
	
	//DATA ANGGOTA
	//SET SELECT OPTION
	function set_select_list_induk_kesatuan(){
		$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
		$nrp = $arr_usr['uacc_nrp'];
		$level = $arr_usr['uacc_group_fk'];
		
		if($level != 1){
			$this->load->model('sipinter_anggota_model');
			$usr = $this->sipinter_anggota_model->get_anggota_by_nrp($nrp);
			$kode_kesatuan = $usr->kode_kesatuan;
			$sats = explode("-",$kode_kesatuan);
			$kodam = $sats[0];
			$sql_filter = " AND sat.kode_kesatuan LIKE '$kodam%' ";
		}else{
			$sql_filter = " ";
		}
		
		$sql = "SELECT sat.*, kls.level
					FROM sipinter_kesatuan AS sat, sipinter_kelas_kesatuan AS kls
					WHERE sat.kelas=kls.kelas
					AND kls.level > 1
					AND kls.level < 5
					$sql_filter
					ORDER BY kls.level, sat.nama; ";					
		$query = $this->db->query($sql);
        
		foreach ($query->result() as $row)
        {
            $arr_kesatuan['']= '';
            $arr_kesatuan[$row->kode_kesatuan]= $row->nama;
        } 
        return $arr_kesatuan;
	}
	
	
	
	
	
	function set_select_kesatuan_by_kelas($kelas=""){
		$kesatuan = $this->sipinter_kesatuan_model->get_wilayah_kesatuan($kelas);
		foreach ($kesatuan->result() as $row)
        {
            $arr_kesatuan['']= '';
            $arr_kesatuan[$row->kode_kesatuan]= $row->nama;
        } 
        return $arr_kesatuan;
	}
	
	
	
	
	
	function get_kesatuan_by_parent($parent=""){
		$kesatuan = $this->sipinter_kesatuan_model->get_kesatuan_by_parent($parent);
		foreach ($kesatuan->result() as $row)
        {
            $arr_kesatuan['']= '';
            $arr_kesatuan[$row->kode_kesatuan]= $row->nama;
        } 
        return $arr_kesatuan;
	}
	
	
	
	
	
	function set_select_kesatuan($kode_kesatuan){
		if($kode_kesatuan != ""){
			$kesatuan = $this->sipinter_kesatuan_model->get_nama_kesatuan($kode_kesatuan);
			if(count($kesatuan) > 0){
				$arr_kesatuan[$kesatuan->kode_kesatuan]= $kesatuan->nama;
			}else{
				$arr_kesatuan['']= '';
			}
		}else{
			$arr_kesatuan['']= '';
		}
        return $arr_kesatuan;
	}
	
	
	
	
	
	function select_kesatuan_by_parent(){
        if('IS_AJAX') {
			$this->load->model('sipinter_kesatuan_model');
			$parent = $this->input->post('parent');
			$data['option_kesatuan'] = $this->sipinter_kesatuan_model->get_kesatuan_by_parent($parent);		
			$this->load->view('anggota/input_select_kesatuan',$data);
        }
	}
	
	
	
	
	
	function select_all_kesatuan_by_parent(){
        if('IS_AJAX') {
			$this->load->model('sipinter_kesatuan_model');
			$parent = $this->input->post('parent');
			$data = $this->sipinter_kesatuan_model->get_all_kesatuan_by_parent($parent);
			$option = "";
			foreach ($data->result() as $row)
			{
				$option .= "<option value='".$row->kode_kesatuan."'>".$row->nama."</option>";
			} 
			echo $option;
        }
	}
	
	function select_all_kesatuan_by_parent2(){
        if('IS_AJAX') {
			$this->load->model('sipinter_kesatuan_model');
			$parent = $this->input->post('parent');
			$sel = $this->input->post('sel');
			$data = $this->sipinter_kesatuan_model->get_all_kesatuan_by_parent($parent);
			$option = "";
			foreach ($data->result() as $row)
			{
				$selected = "";
				if($row->kode_kesatuan==$sel){
					$selected = "SELECTED";
				}
				$option .= "<option value='".$row->kode_kesatuan."' ".$selected.">".$row->nama."</option>";
			} 
			echo $option;
        }
	}
	
	
	
	
	
	function set_select_pangkat(){
		$pangkat = $this->sipinter_pangkat_model->get_pangkat_list();
		foreach ($pangkat->result() as $row)
        {
            $arr_pangkat['']= '';
            $arr_pangkat[$row->id_pangkat]= $row->pangkat;
        } 
        return $arr_pangkat;
	}
	
	
	
	
	
	function set_select_jabatan(){
		$jabatan = $this->sipinter_pangkat_model->get_jabatan_list();
		foreach ($jabatan->result() as $row)
        {
            $arr_jabatan['']= '';
            $arr_jabatan[$row->kode_jabatan]= $row->jabatan;
        } 
        return $arr_jabatan;
	}
	
	
	
	
	
	function select_wilayah_kesatuan(){
        if('IS_AJAX') {
			$this->load->model('sipinter_kesatuan_model');
			$kesatuan = $this->input->post('kesatuan');
			$query = $this->sipinter_kesatuan_model->get_wilayah_teritorial_kesatuan($kesatuan);		
			$data = $query->row();
			$wilayah = $data->wilayah;
			
			$pos = strpos($data->id_wilayah, ",");
			if ($pos !== false) {
				$id_wilayah = explode(",", $data->id_wilayah);
			}else{
				$id_wilayah = $data->id_wilayah;
			}
			
			if($wilayah == 'PROV'){
				$option = $this->set_select_wilayah_prov($id_wilayah);
			}else if($wilayah == 'KABKOT'){
				$option = $this->set_select_wilayah_kabkot($id_wilayah);
			}else{
				$option = $this->set_select_wilayah_kec($id_wilayah);
				$wilayah = 'DESA';
			}
			
			$arr['wilayah'] = $wilayah;
			$arr['option'] = $option;
			
			//echo $arr;
			echo json_encode($arr);
        }
	}
	
	function set_select_wilayah_prov($id_wilayah){
		$this->db->select('*');
		$this->db->from('sipinter_provinsi');
		$this->db->where_in('id_prov', $id_wilayah); 
		$query = $this->db->get();
		
		$option = "";
		foreach ($query->result() as $row)
		{
			$option .= "<option value='".$row->id_prov."'>".$row->nama_prov."</option>";
		} 
		
		return $option;
	}
	
	function set_select_wilayah_kabkot($id_wilayah){
		$this->db->select('*');
		$this->db->from('sipinter_kota_kabupaten');
		$this->db->where_in('id_kabkot', $id_wilayah); 
		$query = $this->db->get();
		
		$option = "";
		foreach ($query->result() as $row)
		{
			$option .= "<option value='".$row->id_kabkot."'>".$row->nama_kabkot."</option>";
		} 
		
		return $option;
	}
	
	function set_select_wilayah_kec($id_wilayah){
		$this->db->select('sipinter_kelurahan.*, sipinter_kecamatan.nama_kec');
		$this->db->from('sipinter_kecamatan');
		$this->db->join('sipinter_kelurahan', 'sipinter_kecamatan.id_kec = sipinter_kelurahan.id_kec');
		$this->db->where_in('sipinter_kelurahan.id_kec', $id_wilayah); 
		$this->db->order_by("sipinter_kecamatan.id_kec", "acs"); 
		$this->db->order_by("sipinter_kelurahan.nama_kel", "acs"); 
		$query = $this->db->get();
		
		foreach ($query->result() as $row)
		{
			$arr_data[$row->nama_kec][$row->id_kel] = $row->nama_kel;
		} 
		
		$option = "";
		foreach ($arr_data as $key => $val)
		{
			$option .= "<optgroup label='Kec. $key'>";
			foreach ($val as $key2 => $row){
				$option .= "<option value='$key2'>$row</option>";
			}
			$option .= "</optgroup>";
			
		} 
		
		return $option;
	}
	//END OF SET SELECT OPTION
	
	
	
	
	
	
	
	
	
	
	
	//USER WEB
	//SET SELECT OPTION
	function set_select_user_level($current_level=0){
		$level = $this->sipinter_anggota_model->get_list_user_level($current_level);
		foreach ($level->result() as $row)
        {
            $arr_level['']= '';
            $arr_level[$row->ulvl_id]= $row->ulvl_name;
        } 
        return $arr_level;
	}
	//END OF SET SELECT OPTION
	
	
	
	
	
	
	
	
	
	
	
	
	//DATA ANGGOTA
	//INPUT VALIDATION
	function cek_nrp(){
        if('IS_AJAX') {
			$this->load->model('sipinter_anggota_model');
			$nrp = $this->input->post('nrp');
			echo $this->sipinter_anggota_model->cek_nrp($nrp);
        }
	}
	
	function cek_user_web_nrp($nrp){
		$this->load->model('sipinter_anggota_model');
		$result = $this->sipinter_anggota_model->cek_user_web_nrp($nrp);
		if ($result->num_rows() > 0){
			$usr_check = $result->row();
			if($usr_check->uacc_nrp == $usr_check->nrp){
				echo "already";
			}else{
				echo "valid";
			}
		}else{
			echo "invalid";
		}
		
	}
	
	function cek_user_apk_nrp($nrp){
		$this->load->model('sipinter_anggota_model');
		$result = $this->sipinter_anggota_model->cek_user_apk_nrp($nrp);
		if ($result->num_rows() > 0){
			$usr_check = $result->row();
			if($usr_check->uapk_nrp == $usr_check->nrp){
				echo "already";
			}else{
				echo "valid";
			}
		}else{
			echo "invalid";
		}
		
	}
	//END OF INPUT VALIDATION
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//DATA ANGGOTA
	//INSERT FUNCTION
	function insert_anggota(){
		$this->load->model('sipinter_anggota_model');
		$this->load->model('sipinter_wilayah_model');

		$data1 = array(
			'nrp' => $this->input->post('nrp'),
			'nama_anggota' => $this->input->post('nama_anggota'),
			'id_pangkat' => $this->input->post('id_pangkat'),
			'kode_jabatan' => $this->input->post('id_jabatan'),
			'kode_kesatuan' => $this->input->post('kode_kesatuan'),
			'komandan' => $this->input->post('komandan'),
			'perintah' => $this->input->post('perintah'),
			'no_hp' => $this->input->post('no_hp')
		);

		$data2 = array(
			'nrp' => $this->input->post('nrp'),
			'id_wilayah' => $this->input->post('id_wilayah'),
			'wilayah' => $this->input->post('wilayah')
		);

		if(($this->sipinter_anggota_model->insert_anggota($data1)) && ($this->sipinter_wilayah_model->insert_data_wilayah_teritorial_user($data2))){
			echo "success";
		}
		
    }
	
	function update_anggota(){
		$this->load->model('sipinter_anggota_model');
		$this->load->model('sipinter_wilayah_model');

		$nrp = $this->input->post('nrp');
		$data1 = array(
			'nama_anggota' => $this->input->post('nama_anggota'),
			'id_pangkat' => $this->input->post('id_pangkat'),
			'kode_jabatan' => $this->input->post('id_jabatan'),
			'komandan' => $this->input->post('komandan'),
			'perintah' => $this->input->post('perintah'),
			'kode_kesatuan' => $this->input->post('kode_kesatuan'),
			'no_hp' => $this->input->post('no_hp')
		);

		$data2 = array(
			'nrp' => $this->input->post('nrp'),
			'id_wilayah' => $this->input->post('id_wilayah'),
			'wilayah' => $this->input->post('wilayah')
		);

		$data3 = array(
			'id_wilayah' => $this->input->post('id_wilayah'),
			'wilayah' => $this->input->post('wilayah')
		);

		if(($this->sipinter_anggota_model->update_anggota($nrp, $data1)) && ($this->sipinter_wilayah_model->update_data_wilayah_teritorial_user($nrp, $data3, $data2))){
			echo "success";
		}
		
    }
	
	function set_status_aktif_anggota(){
		$this->load->model('sipinter_anggota_model');

		$data = array(
			'aktif' => $this->input->post('aktif')
		);

		if($this->sipinter_anggota_model->update_anggota($this->input->post('nrp'), $data)){
			echo "success";
		}
		
    }
	//END OF INSERT FUNCTION
	
	
	
	
	
	
	
	
	
	
	
	//DATA USER WEB
	//INSERT FUNCTION
	function insert_user_web(){
		$this->load->model('sipinter_anggota_model');
		date_default_timezone_set('Asia/Jakarta');		
		$now = date('Y-m-d H:i:s');
		
		$password = $this->input->post('pass');
		
		$store_database_salt = $this->auth->auth_security['store_database_salt'];
	    $database_salt = $store_database_salt ? $this->flexi_auth_model->generate_token($this->auth->auth_security['database_salt_length']) : FALSE;
		$hash_password = $this->flexi_auth_model->generate_hash_token($password, $database_salt, TRUE);

		$data = array(
			'uacc_group_fk' => $this->input->post('level_user'),
			'uacc_nrp' => $this->input->post('nrp'),
			'uacc_email' => $this->input->post('email'),
			'uacc_password' => $hash_password,
			'uacc_salt' => $database_salt,
			'uacc_date_added' => $now
		);

		if($this->sipinter_anggota_model->insert_user_web($data)){
			echo "success";
		}
		
    }
	//END OF INSERT FUNCTION
	
	
	
	
	
	
	
	
	
	
	
	//DATA USER APK
	//INSERT FUNCTION
	function insert_user_apk(){
		$this->load->model('sipinter_anggota_model');
		
		$password = $this->input->post('pass');
		
		$escapedPW = mysql_real_escape_string($password);
		$salt = bin2hex(mcrypt_create_iv(8, MCRYPT_DEV_URANDOM));
		$saltedPW =  $escapedPW . $salt;
		$hashedPW = hash('sha256', $saltedPW);

		$data = array(
			'uapk_level_id' => $this->input->post('level_user'),
			'uapk_nrp' => $this->input->post('nrp'),
			'uapk_email' => $this->input->post('email'),
			'uapk_password' => $hashedPW,
			'uapk_salt' => $salt
		);

		if($this->sipinter_anggota_model->insert_user_apk($data)){
			echo "success";
		}
		
    }
	//END OF INSERT FUNCTION
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//EDiT ANGGOTA
	function getDetailAnggota(){
		$this->load->model('sipinter_anggota_model');
		$nrp = $this->input->post('nrp');
		$result = $this->sipinter_anggota_model->get_anggota_by_nrp2($nrp);
        echo json_encode($result);
	}
	//EDiT ANGGOTA
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//IMPORT FORM
	function import_data_excel($filename){
		$excel_file = 'assets/uploads/files/'.$filename;

		
		$this->load->library('excel');
		$objPHPExcel = PHPExcel_IOFactory::load($excel_file);
		$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
		foreach ($cell_collection as $cell) {
			$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
			$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
			$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
			//header will/should be in row 1 only. of course this can be modified to suit your need.
			if ($row == 1) {
				$header[$row][$column] = $data_value;
			} else {
				$arr_data[$row][$column] = $data_value;
			}
		}
		//send the data in an array format
		//$data['header'] = $header;
		//$data['values'] = $arr_data;
		
		$i = 0;
		$error = 0;
		$inserted = 0;
		$updated = 0;
		
		foreach ($arr_data as $row) {
			$nrp = $this->cekArrIndex('A', $row);
			$data_query = array(
				'uapk_nrp' => $nrp,
				'uapk_level_id' => '7', //set default user level ke Peninjau/babinsa (level terendah)
				'uapk_email' => $this->cekArrIndex('B', $row),
				'uapk_mdn' => $this->cekArrIndex('C', $row),
				'uapk_imei' => $this->cekArrIndex('D', $row),
				'uapk_iccid' => $this->cekArrIndex('E', $row)
			);
			$this->db->where('uapk_nrp',$nrp);
			$q = $this->db->get('sipinter_user_apk');

			if ( $q->num_rows() > 0 ) {
				$this->db->where('uapk_nrp',$nrp);
				if($this->db->update('sipinter_user_apk', $data_query)){
					$updated++;
				}else{
					$error++;
				}
			} else {
				$this->db->set('uapk_nrp', $nrp);
				if($this->db->insert('sipinter_user_apk', $data_query)){
					$inserted++;
				}else{
					$error++;
				}
			}
			
			$i++;
		}
		
		echo $i." data telah diproses<br>
				inserted = ".$inserted."<br>
				updated = ".$updated."<br>
				error = ".$error;
	}
	
	
	function cekArrIndex($key, $arr){
		if (array_key_exists($key, $arr)) {
			return $arr[$key];
		}else{
			return "0";
		}
	}
	
	
	public function process_ajax(){		
		if(isset($_FILES['userImage']))
		{
			$config['upload_path'] = './assets/uploads/files';
			$config['allowed_types'] = 'xls|xlsx';
			$config['max_size'] = 300000;
			$config['encrypt_name'] = FALSE;
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload('userImage')){
				echo "file gagal diupload";
			}else{
				$uploaded_data = $this->upload->data();
				$this->import_data_excel($uploaded_data['file_name']);
			}
		}else{
			echo "tidak ada file";
		}
	}
	

	
	


}

/* End of file auth_admin.php */
/* Location: ./application/controllers/auth_admin.php */