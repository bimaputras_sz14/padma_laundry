<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo $this->config->item('plugins_dir');?>jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<!--<script type="text/javascript" src="<?php echo $this->config->item('plugins_dir');?>jquery-inputmask/jquery.inputmask.bundle.min.js"></script>-->
<script type="text/javascript" src="<?php echo $this->config->item('plugins_dir');?>select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('plugins_dir');?>datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('plugins_dir');?>datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('plugins_dir');?>datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('plugins_dir');?>datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('plugins_dir');?>datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script type="text/javascript" src="<?php echo $this->config->item('plugins_dir');?>bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('plugins_dir');?>jquery-validation/js/jquery.validate.min.js"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>bootstrap-toastr/toastr.min.js"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>fancybox/source/jquery.fancybox.pack.js"></script>

<!--<script src="<?php echo $this->config->item('plugins_dir');?>bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>-->
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo $this->config->item('assets_dir');?>global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('assets_dir');?>scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('assets_dir');?>scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>tinymce/js/tinymce/tinymce.min.js"></script>
<style type="text/css">
	#mceu_14   {display: none!important;}
</style>
<!-- END PAGE LEVEL SCRIPTS -->








<script>
tinymce.init({
	  selector: 'textarea',
	  height: 300,
	  menubar:false,
	  automatic_uploads: false,
	  plugins: [
		'advlist autolink lists link image charmap print preview anchor',
		'searchreplace visualblocks code fullscreen',
		'insertdatetime media table contextmenu paste code'
	  ],
	  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
	  content_css: [
		'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
		'//www.tinymce.com/css/codepen.min.css'
	  ]
	});
//SET TABEL
function setTabelBerita(){
	Metronic.blockUI({
		boxed: true,
		message: 'Proses menampilkan tabel...',
		textOnly: true
	});
	
	var table = $('#table_berita');
	var oTable = table.dataTable();
	oTable.fnClearTable();
	oTable.fnDestroy();
	
    $('#table_berita').dataTable( {
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "Data tidak tersedia pada database",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "Show _MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },
		
        "processing": true,
        "ajax": "<?php echo site_url('berita/get_list_berita')?>",
        "columns": [
            { "data": "no" },
            { "data": "tgl" },
            { "data": "foto" },
            { "data": "judul" },
            { "data": "isi" },
            // { "data": "kepada" },
            // { "data": "oleh" },
            { "data": "btn" }
        ],
		"order": [
            [0, 'asc']
        ],
        "lengthMenu": [
            [5, 10, 15, 20, -1],
            [5, 10, 15, 20, "All"] // change per page values here
        ],
        "pageLength": 10,
		"initComplete": function(settings, json) {    
			Metronic.unblockUI(); 
		}
    } );
	
    var tableWrapper = $('#table_berita_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
    tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
	
}
//END OF SET TABEL


//FORM VALIDATION
var handleValidation = function() {
		tinymce.triggerSave();
        $('#form_insert').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            rules: {
                judul: {
                    required: true
                },
                // uraian: {
                    // required: true
                // }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit 
				toastr.error("Data yang diinputkan tidak valid. Judul dan uraian tidak boleh kosong.");
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var icon = $(element).parent('.input-icon').children('i');
                icon.removeClass('fa-check').addClass("fa-warning");  
                icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
				
                var icon_sel = $(element).parent('.input-group').children('span').children('i');
                icon_sel.removeClass('fa-check').addClass("fa-warning");
            },

            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group   
            },

            unhighlight: function (element) { // revert the change done by hightlight
             
            },

            success: function (label, element) {
                var icon = $(element).parent('.input-icon').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon.removeClass("fa-warning").addClass("fa-check");
			
                var icon_sel = $(element).parent('.input-group').children('span').children('i');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                icon_sel.removeClass("fa-warning");
            },
 
            submitHandler: function (form) {
				var mode_form = $("#mode").val();
				if(tinyMCE.get('uraian').getContent() !=""){
					if(mode_form == "EDIT"){
						updateBerita();
					}else{
						insertBerita();
					}
				}else{
					toastr.error("Data uraian tidak boleh kosong.");
				}
				// alert("oke");
            }
        });
}
//FORM VALIDATION

	

function clearField(){
	// $('#satwil').html("");
	$('#satwil').select2('val', '');
	$('#arr_satwil').val("");
	$('#foto').val("");
	$('#judul').val("");
	tinyMCE.activeEditor.setContent('');
	// $('#uraian').val("");
	$('#mode').val("");
	$('#id').val("");
	$('#foto_url').val("");
	$('#foto_url_new').val("");
	$('.fileinput').removeClass('fileinput-exists').addClass('fileinput-new');
	
	$("#bt_reset_img").click();
}

function showFormAdd(){
	$('#bt_del').hide();
	$('#fInput').modal('show');
}
	
	
	
var setMultiselect = function () {
    $('#satwil').select2({
        placeholder: "Semua Wilayah"//,
        //allowClear: true
    });
}



function setSelSatwil(){
	$('#satwil').html("");
	$('#satwil').select2('val', '');
	
	$.ajax({
		type: "POST",
		url: "<?php echo site_url('berita/set_sel_satwil'); ?>",
		success: function(result){
			$('#satwil').html(result);
			// var arr = multiselval.split(',');
			// $('#satwil').select2('val', arr);
		}
	});
}


function setArrSatwil(){
	var satwil = $('#satwil').val() || [];
	var list_satwil = satwil.map(function(id_satwil){
			return id_satwil;
		}).join(","); 
		
	$('#arr_satwil').val(list_satwil);
}



	
function showFormUpdate(id){
	if(id != ""){
		Metronic.blockUI({
			boxed: true,
			message: 'Sedang diproses...',
			textOnly: true
		});
		
		clearField();
		
		$.ajax({
			url: "<?php echo site_url('berita/prepare_edit_berita'); ?>",
			type: 'POST',
			data: {
				id: id
			},
			dataType: 'json',
			success: function(msg) {
				Metronic.unblockUI();
				if(msg.status == 'success'){
					var dt = msg.data;
					
					$('#id').val(id);
					$('#mode').val('EDIT');
					$('#judul').val(dt.judul);
					// $('#uraian').val(dt.isi);
					$(tinymce.get('uraian').getBody()).html(dt.isi);
					$('#foto_url').val(dt.foto);
					$('#foto_url_new').val(dt.foto);
					$('#arr_satwil').val(dt.satwil_tujuan);
					
					if((dt.satwil_tujuan !== "") && (dt.satwil_tujuan !== null)){
						var arr = dt.satwil_tujuan.split(',');
						$('#satwil').select2('val', arr);
					}
					
					if((dt.foto !== "") && (dt.foto !== null)){
						$('.fileinput').addClass('fileinput-exists').removeClass('fileinput-new');
						var imgprev = "<img src='"+dt.foto+"' />";
						$('.fileinput-preview').html(imgprev);
					}
					
					if(dt.del == 1){
						$('#bt_del').show();
					}else{
						$('#bt_del').hide();
					}
					
					$('#fInput').modal('show');
				}else{
					alert(msg.msg);
				}
			}
		});
	}else{
		alert("data tidak ditemukan");
	}
}




function insertBerita(){
	Metronic.blockUI({
		target: '#fInput',
		boxed: true,
		message: 'Sedang diproses...',
		textOnly: true
	});
	
	var form_data = new FormData($("#form_insert")[0]);
	form_data.append("uraian",tinyMCE.get('uraian').getContent());
    $.ajax({
        url: "<?php echo site_url('berita/insert_berita'); ?>",
        type: 'POST',
		enctype: 'multipart/form-data',
		processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        data: form_data,
        success: function(msg) {
			Metronic.unblockUI('#fInput');
			if(msg == "success"){
				toastr.success('Data telah berhasil disimpan', 'Input Berita/Pengumuman');
				$("#fInput").modal("hide");
				
				prepareGetData();
			}
        }
    });
}







function updateBerita(){
	Metronic.blockUI({
		target: '#fInput',
		boxed: true,
		message: 'Sedang diproses...',
		textOnly: true
	});
	
	var form_data = new FormData($("#form_insert")[0]);
	form_data.append("uraian",tinyMCE.get('uraian').getContent());
    $.ajax({
        url: "<?php echo site_url('berita/update_berita'); ?>",
        type: 'POST',
		enctype: 'multipart/form-data',
		processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        data: form_data,
        success: function(msg) {
			Metronic.unblockUI('#fInput');
			if(msg == "success"){
				toastr.success('Data telah berhasil disimpan', 'Update Berita/Pengumuman');
				$("#fInput").modal("hide");
				
				prepareGetData();
			}
        }
    });
}



function delBerita(){
	if(confirm("Apakah berita/pengumuman ini akan dihapus?")){
		var id = $('#id').val();
		
		if((id != "") && (id != null)){
			$("#fInput").modal("hide");
			Metronic.blockUI({
				boxed: true,
				message: 'Sedang diproses...',
				textOnly: true
			});
			
			$.ajax({
				url: "<?php echo site_url('berita/delete_berita'); ?>",
				type: 'POST',
				data: {
					id: id
				},
				success: function(msg) {
					Metronic.unblockUI();
					if(msg == "success"){
						toastr.success('Data telah berhasil dihapus', 'Hapus Berita/Pengumuman');
						
						prepareGetData();
					}else{
						toastr.error('Data gagal dihapus', 'Hapus Berita/Pengumuman');
					}
				}
			});
		}else{
			alert("data tidak ditemukan");
		}
	}
}



function listSatwil(id){
	Metronic.blockUI({
		boxed: true,
		message: 'Sedang diproses...',
		textOnly: true
	});
	
	var form_data = {
        id: id
    };
	
    $.ajax({
        url: "<?php echo site_url('berita/get_list_penerima'); ?>",
        type: 'POST',
        data: form_data,
        success: function(msg) {
			Metronic.unblockUI();
			//alert(msg.id);
			if(msg == "kosong"){
				alert('data tidak ditemukan');
			}else{
				$("#list_container").html('');
				$("#list_container").html(msg);
				$('#fDetail').modal('show');
			}
        }
    });
}



//CEK LOGIN
function prepareGetData(){
    $.ajax({
        url: "<?php echo site_url('user/cek_session_login'); ?>",
        type: 'POST',
        success: function(msg) {
			if(msg=='true'){
				setTabelBerita();
			}else{
				window.location.replace("<?php echo site_url(); ?>");
			}
        }
    });
}



function reposition() {
	var modal = $(this),
		dialog = modal.find('.modal-dialog');
	modal.css('display', 'block');

	// Dividing by two centers the modal exactly, but dividing by three 
	// or four works better for larger screens.
	dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
}

jQuery(document).ready(function() {    
	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	QuickSidebar.init(); // init quick sidebar
	
	$('.modal').on('show.bs.modal', reposition);
    // Reposition when the window is resized
	$(window).on('resize', function() {
		$('.modal:visible').each(reposition);
	});
	
	$(".fancybox").fancybox({
		helpers : {
			title: {
				type	: 'inside',
				position: 'bottom'
				}
		},
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	
	prepareGetData();
	setSelSatwil();
	setMultiselect();	
	
	$("#bt_add").click(function(){
		$("#mode").val("INPUT");
		clearField();
		showFormAdd();
	});
	
	$("#bt_reset_img").click(function(){
		$('#foto_url_new').val("");
	});
	
	$("#bt_del").click(function(){
		delBerita();
	});
	
	$("#satwil").change(function(){
		setArrSatwil();
	});
	
	$("#foto").change(function(){
		var foto = $("#foto").val();
		$("#foto_url_new").val(foto);
	});
	
	
	
	
	
	//FORM INPUT
	handleValidation();
	
	
	

	
	$('#bt_cancel').click(function() {
		$("#fInput").modal("hide");
	});
	
	$('#bt_close').click(function() {
		$("#fDetail").modal("hide");
	});
	
	
	
	
});
</script>
<!-- END JAVASCRIPTS -->
