
<div class="modal fade" id="fInput" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<!-- BEGIN FORM-->
			<form method='post' id="form_insert" enctype="multipart/form-data" class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title bold"> <span class="font-bold">Berita / Pengumuman</span> </h4>
				</div>
				<div class="modal-body form">
					 <div class="col-md-12">
							<!-- BEGIN VALIDATION STATES-->
						<div class="portlet-body form">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-2 font-green-haze">Judul<span class="required  ">
											* </span>
										</label>
										<div class="col-md-10">
											<input  type='text' id='judul' name='judul' placeholder="judul berita/pengumuman" class='form-control'>
										</div>
									</div> 
									<input type="hidden" class="form-control" id="mode" name="mode"/>
									<input type="hidden" class="form-control" id="id" name="id"/>
									<input type="hidden" class="form-control" id="foto_url" name="foto_url"/>
									<input type="hidden" class="form-control" id="foto_url_new" name="foto_url_new"/>
									<!-- <div class="form-group">
										<label class="control-label col-md-2 font-green-haze">Untuk
										</label>
										<div class="col-md-10">
											<select class="form-control select2me" id="satwil" name="satwil" multiple placeholder="Semua Wilayah">
											</select>
											<input type="hidden" id="arr_satwil" name="arr_satwil" class="form-control"/>
											<input type="hidden" class="form-control" id="mode" name="mode"/>
											<input type="hidden" class="form-control" id="id" name="id"/>
											<input type="hidden" class="form-control" id="foto_url" name="foto_url"/>
											<input type="hidden" class="form-control" id="foto_url_new" name="foto_url_new"/>
											<div class="clearfix margin-top-10">
												kosongkan pilihan wilayah ini untuk memilih seluruh wilayah
											</div>
										</div>
									</div> -->
									<div class="form-group">
										<label class="control-label col-md-2 font-green-haze">Foto
										</label>
										<div class="col-md-10">
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
														<img src="<?php echo $this->config->item('assets_dir');?>img/AAAAAA&amp;text=no+image.png" alt=""/>
													</div>
													<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
													</div>
													<div>
														<span class="btn default btn-file">
														<span class="fileinput-new">
														Select image </span>
														<span class="fileinput-exists">
														Change </span>
														<input type="file" name="foto" id="foto">
														</span>
														<a href="#" id="bt_reset_img" class="btn red fileinput-exists" data-dismiss="fileinput">
														Remove </a>
													</div>
												</div>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-md-2 font-green-haze">Uraian<span class="required  ">
											* </span>
										</label>
										<div class="col-md-10">
											<textarea id="uraian" name="uraian" class="form-control" maxlength="1000" rows="10" placeholder="uraian berita/pengumuman (600 karakter)" ></textarea>
										</div>
									</div>
								</div>
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
				<div class="modal-footer">
					<button id="bt_cancel" type="button" class="btn  btn-sm default" ><i class="icon-action-undo"></i> Batal </button>
					<button id="bt_del" type="button" class="btn  btn-sm red-sunglo"><i class="icon-trash"></i> Hapus </button>
					<button id="bt_save" type="submit" class="btn  btn-sm blue"><i class="fa fa-send"></i> Simpan </button>
				</div>
			</form>
			<!-- END FORM-->
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>



										   <div id="fDetail" class="modal fade" tabindex="-1" aria-hidden="true">
												<div class="modal-dialog" style="width:800px;">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
															<h4 class="modal-title" id="det_judul">Satuan Wilayah Penerima Berita/Pengumuman</h4>
														</div>
														<div class="modal-body">
															<!--<div class="scroller" style="height:260px" data-always-visible="1" data-rail-visible1="1">-->
																<div class="row">
																	<div class="col col-md-12" id="list_container">
																		
																	</div>
																</div>
															<!--</div>-->
														</div>
														<div class="modal-footer">
																<button type="button" class="btn  btn-sm default" id="bt_close"><i class="icon-action-undo"></i> Tutup </button>
														</div>
													</div>
												</div>
											</div>
											

<!-- BEGIN PAGE CONTENT-->
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet">
			<div class="portlet-body">
				<div class="tabbable-custom ">
					<ul class="nav nav-tabs ">
						<li class="active">
							<a href="#tab_5_1" data-toggle="tab">
							<i class="icon-speech font-red-intense"></i> Data Berita & Pengumuman</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="tab_5_1">
							<div class="row">
								<div class="col-md-12">
									<!-- BEGIN EXAMPLE TABLE PORTLET-->
									<div class="portlet box">
										<div class="portlet-body">
                                            <div class="table-toolbar">
												<?php if($add_new == "1"){ ?>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="btn-group">
                                                                <button id="bt_add" role="button" data-toggle="modal" class="btn green-jungle">
                                                                <i class="icon-plus"></i> Tambah Berita
                                                                </button>
                                                            </div>
                                                        </div>
														<div class="col-md-6">
														</div>
                                                    </div>
												<?php } ?>
                                            </div>

											<table class="table table-striped table-bordered table-hover" id="table_berita">
												 <thead>
													<tr>
														<th>
															 No
														</th>
														<th style="min-width: 70px;">
															 Tanggal
														</th>
														<th style="min-width: 90px;">
															 Foto
														</th>
														<th style="min-width: 150px;">
															 Judul
														</th>
														<th style="min-width: 400px;">
															 Isi Berita/Pengumuman
														</th>
														<!-- <th style="min-width: 80px;">
															 Kepada
														</th> -->
														<!-- <th style="min-width: 150px;">
															 Penulis
														</th> -->
														<th style="min-width: 50px;">
															 Action
														</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div>
									</div>
									<!-- END EXAMPLE TABLE PORTLET-->
								</div>
							</div>             
							</div>
						</div>
					<!-- PESAN KELUAR -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>