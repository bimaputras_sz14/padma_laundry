  
<div class="row">
    <div class="col-lg-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-success pull-right">Hari Ini</span>
                <h5>Member Baru</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><?php echo $member_baru?></h1>
                <!-- <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div> -->
                <small>Total Member</small>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-info pull-right">Hari Ini</span>
                <h5>Penjualan</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><?php echo number_format($penjualan)?></h1>
               <!--  <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div> -->
                <small>Total Penjualan</small>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <span class="label label-primary pull-right">Hari Ini</span>
                <h5>Member Visits</h5>
            </div>
            <div class="ibox-content">
                <h1 class="no-margins"><?php echo $member_visit?></h1>
                <!-- <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div> -->
                <small>Total Visit</small>
            </div>
        </div>
    </div> 
</div> 
<div class="row">
    <?php 
    if ($member) {
    foreach($member as $row){  
    ?> 
    <div class="col-lg-4 m-b-xs">
        <div class="contact-box">
            <a href="<?php echo site_url('member/manage/').'/'.$row->id ?>">
                <div class="col-sm-4">
                    <div class="text-center">
                        <img alt="image" class="  m-t-xs img-responsive" src="<?php echo base_url(); ?>/uploads/member/<?php echo $row->foto?>"> 
                    </div>
                </div>
                <div class="col-sm-8">
                    <h3><strong><?php echo strtoupper( $row->nama_pelanggan )?></strong></h3> 
                    <address>
                        <i class="fa fa-tags"></i> <?php echo $row->no_kartu?><br>
                        <abbr title="Phone"><i class="fa fa-phone"></i> </abbr> <?php echo $row->hp?>
                    </address>
                </div>
               <div class="clearfix"></div>
                
                <div class="col-sm-12 col-md-4" >
                    <button onclick='do_checkin(<?php echo $row->id ?>);' class="btn btn-xs btn-primary">Masuk <i class="fa fa-arrow-circle-right"></i></button>  
                </div>
                <div class="col-sm-12 col-md-4" >   
                    <button onclick='do_checkin(<?php echo $row->id ?>);' class="btn btn-xs btn-danger">Keluar <i class="fa fa-arrow-circle-left"></i></button> 
                </div>
                <div class="col-sm-12 col-md-4" >
                    <a href="<?php echo site_url('penjualan/penjualan_token').'/'.$row->id ?>" class="btn btn-xs btn-white">Pembelian <i class="fa fa-shopping-cart"></i></a>  
                </div>
                <div class="clearfix"></div>      
                
            </a>
        </div>
    </div>

     
    <?php }  
    }else{ ?>
    <div class="alert alert-danger">
        Data Tidak Ada.
    </div>
    <?php  } ?>  
</div>
             
