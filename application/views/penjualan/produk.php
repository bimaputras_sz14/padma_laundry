<div class="row ">
    <div class="col-lg-8 text-center">   
        <div class="row ">
           <!--  <div class="col-md-12 m-b-md">
                <div class="input-group">
                    <input type="text" placeholder="Cari Produk " class="input form-control typeahead_1">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn btn-white"> <i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div> -->
            <div class="col-md-12">
                <div class="row">
                    <?php 
                    foreach ( $produk->result() as $row ) { 
                        echo '  <div class="col-md-4">
                                    <div class="ibox">
                                        <div class="ibox-content product-box"> 
                                            <div class="product-imitation">
                                               <img alt="image" class="img-responsive" src="'.base_url().'uploads/prduct/'.$row->foto.'">
                                            </div>
                                            <div class="product-desc">
                                                <span class="product-price">
                                                    RP '.number_format( $row->harga,0,'','.').'
                                                </span> 
                                                <a href="#" class="product-name" data-row="'.$row->kategori_produk_id.'"> '.$row->nama_produk.'</a> 
                                                 
                                                <div class="m-t text-righ"> 
                                                    <a href="#" class="btn btn-xs btn-outline btn-primary" onclick="save_cart('.$row->id.');">Beli <i class="fa fa-long-arrow-right"></i> </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>';
                    } 
                    ?>
                   
                    
                </div>
            </div> 
        </div> 
    </div>
    <div class="col-md-4">   
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Data Transaksi</h5>

            </div>
            <div> 
                <div class="ibox-content profile-content"> 
                    <table class="table">
                        <thead>
                            <tr> 
                                <th width="30%">Product</th>
                                <th width="20%">QTY</th>
                                <th width="30%">Total</th>
                                <th width="10%">&nbsp;</th>

                            </tr>
                        </thead>
                        <tbody id="list_item">
                            <!-- <tr> 
                                <td>Kartu Member</td>
                                <td>1</td>
                                <td>Rp 2.000.000</td>
                                <td><i class="fa fa-times-circle" aria-hidden="true"></i></td>
                            </tr>  -->
                        </tbody>
                        <tfoot id="recap_transaction">
                            <tr> 
                                <th width="30%">Total</th> 
                                <th width="10%">&nbsp;</th>
                                <th width="40%" id="total_payment" colspan="2">Rp 0 </th>
                            </tr>
                            
                        </tfoot>
                    </table>
                </div>
                <div class="ibox-content profile-content">  
                    <div class="user-button">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-primary btn-sm btn-block" data-toggle="modal" data-target="#myModal4"><i class="fa fa-money"></i> Bayar</button> 
                            </div>
                            <div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content animated fadeIn">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> 
                                            <h4 class="modal-title">Pembayaran</h4>  
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                               <div class="panel-body">
                                                    <div class="col-md-12"> 
                                                    <form id="payment_checkout" >
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="form-group">
                                                                    <label>Jenis Pembayaran</label>
                                                                    <div class="input-group">
                                                                         <select name="tipe_payment" class="form-control">
                                                                             <option value="1"> Tunai </option>
                                                                             <option value="2"> Kartu Debit </option>
                                                                             <option value="3"> Kartu Kredit </option>
                                                                         </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12">
                                                                <div class="form-group"> 
                                                                    <div class="input-group">
                                                                        <input type="number" class="form-control" name="total_bayar"    placeholder="Total Pembayaran">
                                                                        <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                         <!--    <div class="col-xs-12">
                                                                <label class="col-md-4">Total Penjualan</label>
                                                                <div class="form-group col-md-8"> 
                                                                    <div class="input-group">
                                                                        Rp. <?php echo number_format($this->cart->total(), 0, ',', '.') ?>
                                                                    </div>
                                                                </div>
                                                            </div> -->

                                                            <!-- <div class="col-xs-12">
                                                                <label class="col-md-4">Total Kembalian</label>
                                                                <div class="form-group col-md-8"> 
                                                                    <div class="input-group">
                                                                         Rp. 0 
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                        </div> 
                                                        <input type="hidden" name="member_id" id="member_id"  value="<?php echo $member_id?>">
                                                    </form> 
                                                </div>
                                               </div>  
                                            </div>
                                        </div>
                                        <div class="modal-footer"> 
                                            <button type="button" class="btn btn-primary" onclick='make_payment(<?php echo $member_id?>);'>Bayar</button>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                           
                            <div class="col-md-12">
                                <button type="button" class="btn btn-danger btn-sm btn-block"><i class="fa fa-reply"></i> Batal</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Profile Detail</h5>
            </div>
            <div>
                <div class="ibox-content border-left-right">
                    <img alt="image" class="img-responsive" src="<?php echo base_url().'uploads/member/'.$member->foto?>">
                </div>
                <div class="ibox-content profile-content">
                    <h4><strong><?php echo strtoupper( $member->nama_pelanggan )?></strong></h4> 
                </div>
                
            </div>
        </div>
         
      
    </div>
</div> 
