<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="ibox-content p-xl">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>From:</h5>
                            <address>
                                <strong>Rai GYM</strong><br>
                                 
                                <abbr title="Phone">P:</abbr> (123) 601-4590
                            </address>
                            <p>
                                <span><strong>Invoice Date:</strong> 09 Agustus 2017</span> 
                            </p>
                        </div> 
                        <div class="col-sm-6 text-right">
                            <h4>Invoice No.</h4>
                            <h4 class="text-navy">INV-000567F7-00</h4>
                            <span>To:</span>
                            <address>
                                <strong>Dany Darmawan</strong><br>
                                Bandung no 34<br>
                                <abbr title="Phone">P:</abbr> 0813123333
                            </address>
                            
                        </div>
                    </div>

                    <div class="table-responsive m-t">
                        <table class="table invoice-table">
                            <thead>
                            <tr>
                                <th>Item List</th>
                                <th>Quantity</th>
                                <th>Unit Price</th> 
                                <th>Total Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><div><strong>Token Reguler</strong></div>
                                    <small>Token Reguler @ 26.000</small></td>
                                <td>1</td>
                                <td>Rp 26.000</td> 
                                <td>Rp 1.250.000</td>
                            </tr>
                          

                            </tbody>
                        </table>
                    </div><!-- /table-responsive -->

                    <table class="table invoice-total">
                        <tbody>
                        <tr>
                            <td><strong>Sub Total :</strong></td>
                            <td>Rp 1.250.000</td>
                        </tr>
                        <tr>
                            <td><strong>TAX :</strong></td>
                            <td>Rp 100.00</td>
                        </tr>
                        <tr>
                            <td><strong>TOTAL :</strong></td>
                            <td>Rp 1.350.00</td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- <div class="text-right">
                        <button class="btn btn-primary"><i class="fa fa-dollar"></i> Make A Payment</button>
                    </div>  -->
                </div>
        </div>
    </div>
</div>