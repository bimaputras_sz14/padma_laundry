<?php $this->load->view('includes/scripts_standart'); ?> 
<script src="<?php echo $this->config->item('assets_dir');?>js/plugins/typehead/bootstrap3-typeahead.min.js"></script> 
<script>
    $(document).ready(function(){ 
        $('.typeahead_1').typeahead({
            source: ["item 1","item 2","item 3"]
        }); 
        $.get('js/api/typehead_collection.json', function(data){
            $(".typeahead_2").typeahead({ source:data.countries });
        },'json'); 
        $('.typeahead_3').typeahead({
            source: [
                {"name": "Afghanistan", "code": "AF", "ccn0": "040"},
                {"name": "Land Islands", "code": "AX", "ccn0": "050"},
                {"name": "Albania", "code": "AL","ccn0": "060"},
                {"name": "Algeria", "code": "DZ","ccn0": "070"}
            ]
        });  
    });
</script>
<script>
    function save_cart(id){ 
        $.ajax({
            url: "<?php echo site_url('penjualan/store_cart'); ?>",
            type: 'POST',
            data: {
                id_product: id
            },
            dataType:"json",
            success: function(msg){  
                if(msg.status == "success"){ 
                    load_cart();
                    load_total();
                    toastr['success']('Data telah berhasil masuk','Data Penjualan');   
                }else{
                    toastr['error']('Data gagal dihapus','Data Penjualan'); 
                }
            }
        });
    }

    function load_cart(){
         $("#list_item").load('<?php echo site_url('penjualan/load_cart')?>'); 
    }
     function load_total(){
         $("#total_payment").load('<?php echo site_url('penjualan/load_total')?>'); 
    }
    function do_insert(){ 
        var form_data = new FormData($("#form_insert")[0]);
        $.ajax({
            url:"<?php echo base_url(); ?>penjualan/cart_ajax",
            type:"POST",
            data:$("#form_insert").serialize(),
            dataType:"json",
            success:function(data){ 
                if(data.status!="error"){
                    toastr.success('Data Berhasil Di Tambahkan', 'Data Pembelian'); 
                }else{
                    toastr.error(data.error, 'Data Pembelian');
                }
            }
        });
    } 
 
    function delete_cart(id){  
        if(confirm("Apakah data pembelian ini akan dihapus?")){ 
            if((id != "") && (id != null)){  
                $.ajax({
                    url: "<?php echo site_url('penjualan/delete_cart'); ?>",
                    type: 'POST',
                    data: {
                        id_product: id
                    },
                      dataType:"json",
                    success: function(msg) { 
                        if(msg.status == "success"){ 
                            load_cart();
                            load_total();
                            toastr['success']( 'Data telah berhasil dihapus','Data Pembelian');   
                        }else{
                            load_cart();
                            load_total();
                            toastr['error'](  'Data gagal dihapus','Data Pembelian'); 
                        }
                    }
                });
            }else{
                alert("data tidak ditemukan"); 
            }
        }
    }

    function do_checkin(id){  
        if(confirm("Apakah data member ini akan latihan?")){ 
            if((id != "") && (id != null)){  
                $.ajax({
                    url: "<?php echo site_url('member/do_checkin'); ?>",
                    type: 'POST',
                    data: {
                        member_id: id
                    },
                      dataType:"json",
                    success: function(msg) { 
                        if(msg.result == "success"){ 
                            toastr['success']( 'Data telah berhasil ','Data Member');  
                            datatables();
                        }else{
                            toastr['error'](  'Data gagal ','Data Member'); 
                        }
                    }
                });
            }else{
                alert("data tidak ditemukan"); 
            }
        }
    }

    function do_checkout(id){  
        if(confirm("Apakah data member ini akan latihan?")){ 
            if((id != "") && (id != null)){  
                $.ajax({
                    url: "<?php echo site_url('member/do_checkout'); ?>",
                    type: 'POST',
                    data: {
                        member_id: id
                    },
                      dataType:"json",
                    success: function(msg) { 
                        if(msg.result == "success"){ 
                            toastr['success']( 'Data telah berhasil ','Data Member');  
                            datatables();
                        }else{
                            toastr['error'](  'Data gagal ','Data Member'); 
                        }
                    }
                });
            }else{
                alert("data tidak ditemukan"); 
            }
        }
    }


    function make_payment(id_pel){   
         var form_data = new FormData($("#payment_checkout")[0]);
            $.ajax({
                url:"<?php echo base_url(); ?>penjualan/checkout",
                type:"POST",
                data:$("#payment_checkout").serialize(),
                dataType:"json",
                success:function(data){
                    if(data.status!="error"){
                        toastr.success('Data Berhasil Di Simpan', 'Data Penjualan');
                        // window.location.href = '<?php echo site_url("penjualan/generate_struk"); ?>/'+data.id_sales; 
                        window.open('<?php echo site_url("penjualan/generate_struk"); ?>/'+data.id_sales,'_blank');
                        // window.location.href = '<?php echo site_url("penjualan/penjualan_token"); ?>/'+id_pel;
                    }else{
                        toastr.error(data.error, 'Data Penjualan');
                    }
                }
            });
         
    }

    function datatables(){
        var table = $('.table-member');
        var oTable = table.dataTable();
        oTable.fnClearTable();
        oTable.fnDestroy();

        $('.table-member').DataTable({
            pageLength: 25,
            responsive: true,
            ajax: {
                url: "<?php echo site_url('member/data_pelanggan')?>",
                type: "POST"
            },
            dom: '<"html5buttons"B>lTfgitp',
            columns: [
                { "data": "no" },
                { "data": "id_no" },
                { "data": "nama_pelanggan" },
                { "data": "alamat" },
                { "data": "hp" },
                { "data": "total_token" },
                { "data": "btn" }
            ],
            buttons: [] 
        });
    }

    $(document).ready(function(){
        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
       load_cart();
       load_total();
    });

    </script>