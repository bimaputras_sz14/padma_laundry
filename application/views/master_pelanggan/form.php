<?php
$method = $this->uri->segment(3);
$id = $this->uri->segment(4);
switch ($method) {
	case 'tambah':
		$data = array(
					'nama'			=>'',
					'jenis'			=>'',
					'email'			=>'',
					'nohp'			=>'',
					'prov'			=>'',
					'kota'			=>'',
					'kec'			=>'',
					'kelurahan'		=>'',
					'alamat'		=>'',
					'status'		=>'');
		$hidden = array();
	break;
	case 'ubah':
		$data = array(
					'nama'			=>$record['nama_pelanggan'],
					'jenis'			=>$record['membership_type'],
					'email'			=>$record['email'],
					'nohp'			=>$record['hp'],
					'prov'			=>$record['id_prov'],
					'kota'			=>$record['id_kabkot'],
					'kec'			=>$record['id_kec'],
					'kelurahan'		=>$record['id_kel'],
					'alamat'		=>$record['alamat'],
					'gedung'		=>$record['gedung_id'],
					'member'		=>$record['membership_type'],
					'kuota'			=>$record['stock'],
					'status'		=>$record['status']);
		$hidden = array('id'	=>$record['id']);
	break;
}
?>

<div class="row">
	<div class="col-lg-6">
		<?php
		if($this->session->flashdata('msg_success')) {?>
			<div class="alert alert-success alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				<?php echo $this->session->flashdata('msg_success');?>
			</div>
		<?php }?>
<!-- UNTUK ERROR UPLOAD -->
<?php 
echo (empty($error) ? "" :"
	<div class='alert alert-danger alert-dismissible fade in' role='alert'>
		<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>×</span>
		</button>$error
	</div>
");?>
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data Pelanggan</h5>
			</div>
			<div class="ibox-content">
				<?php echo form_open_multipart('pelanggan/'.$method,'id=pelanggan class=form-horizontal',$hidden); ?>
		  	   		 	<div class="form-group">
                        	<div class="row" style="margin-right: 0px;">
				  	   			<label class="control-label col-md-2 ">Nama</label>
								<div class="col-md-3">
									<input type="text" name="nama" class="form-control" placeholder="Nama" value="<?php echo $data['nama'];?>" required>
								</div>
								<label class="control-label col-md-1">Jenis</label>
				  	   			<div class="col-md-3">
									<?php
										$options = array(	''			=> 'Jenis',
															'gedung' 	=> 'Gedung',
															'member'	=> 'Member');
										echo form_dropdown('jenis',$options,'','class="form-control" id="jenis" required');
									?>
								</div>
							</div>
						</div>
						<div id="gedung-input" class="form-group" style="display: none;">
                        	<div class="row" style="margin-right: 0px;">
				  	   			<label class="control-label col-md-3">Gedung</label>
				  	   			<div class="col-md-7">
									<?php
										unset($options);
										$options = array('' => 'Gedung');
										foreach ($tmp_options = auto_get_options('gedung','id,name','ORDER BY name ASC') as $info){
											$options[$info['id']] = $info['name'];
										}
										echo form_dropdown('gedung',$options,'','class="form-control" id="gedung"');
									?>
								</div>
							</div>
						</div>
						<div id="member-input" class="form-group" style="display: none;">
                        	<div class="row" style="margin-right: 0px;">
				  	   			<label class="control-label col-md-3">Membership</label>
				  	   			<div class="col-md-7">
									<?php
										unset($options);
										$options = array('' => 'Membership');
										foreach ($tmp_options = auto_get_options('membership_type','id,name','ORDER BY name ASC') as $info){
											$options[$info['id']] = $info['name'];
										}
										echo form_dropdown('member',$options,'','class="form-control" id="member"');
									?>
								</div>
							</div>
						</div>
						<div id="kuota-input" class="form-group" style="display: none;">
                        	<div class="row" style="margin-right: 0px;">
								<label class="control-label col-md-3">Kuota</label>
								<div class="col-md-7">
									<input type="number" name="kuota" class="form-control" id="kuota" required>
								</div>
							</div>
						</div>
						<div class="form-group">
                        	<div class="row" style="margin-right: 0px;">
								<label class="control-label col-md-2">Email</label>
								<div class="col-md-3">
									<input type="email" name="email" class="form-control" placeholder="padma@padma.ga" value="<?php echo $data['email'] ?>" required>
								</div>
								<label class="control-label col-md-1">No. HP</label>
								<div class="col-md-3">
									<input type="number" name="nohp" class="form-control" placeholder="08xxxxxxxxxx" value="<?php echo $data['nohp'] ?>" required>
								</div>
							</div>
						</div>
						<div class="form-group">
                        	<div class="row" style="margin-right: 0px;">
				  	   			<label class="control-label col-md-2">Provinsi</label>
				  	   			<div class="col-md-3">
									<?php
										unset($options);
										$options = array('' => 'Provinsi');
										foreach ($tmp_options = auto_get_options('all_provinsi','id_prov,nama_prov','ORDER BY nama_prov ASC') as $info){
											$options[$info['id_prov']] = $info['nama_prov'];
										}
										echo form_dropdown('provinsi',$options,$data['prov'],'class="form-control" id="provinsi" required');
									?>
								</div>
								<label class="col-md-1" style="text-align: right;">Kota / Kabupaten</label>
				  	   			<div class="col-md-3">
									<?php
										$options = array(	''			=> 'Kota / Kabupaten');
										echo form_dropdown('kota',$options,$data['kota'],'class="form-control" id=kota required');
									?>
								</div>
							</div>
						</div>
						<div class="form-group">
                        	<div class="row" style="margin-right: 0px;">
				  	   			<label class="control-label col-md-2">Kecamatan</label>
				  	   			<div class="col-md-3">
									<?php
										$options = array(	''			=> 'Kecamatan');
										echo form_dropdown('kecamatan',$options,'','class="form-control" id="kecamatan" required');
									?>
								</div>
								<label class="col-md-1" style="text-align: right;">Kelurahan / Desa</label>
				  	   			<div class="col-md-3">
									<?php
										$options = array(	''			=> 'Kelurahan');
										echo form_dropdown('kelurahan',$options,'','class="form-control" id="kelurahan" required');
									?>
								</div>
							</div>
						</div>
<?php
if ($method == 'ubah') {
	echo '	<div class="form-group">
				<label class="control-label col-md-3">Status</label>
				<div class="col-md-7">';
					$options = array(	''	=> 'Status',
										'0'	=> 'Tidak Aktif',
										'1'	=> 'Aktif');
					echo form_dropdown('status',$options,$data['status'],'class="form-control" id="status" required');
	echo '		</div>
			</div>';
}
?>
						<div class="form-group">
                        	<div class="row" style="margin-right: 0px;">
				  	   			<label class="control-label col-md-2">Alamat</label>
				  	   			<div class="col-md-7">
									<textarea name="alamat" class="autosize form-control" style="resize: none;" required><?php echo $data['alamat'] ?></textarea>
								</div>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="pull-left">
								<a class="btn btn-danger" href="<?php echo base_url();?>master_data/pelanggan">Kembali</a>
							</div>
							<div class="pull-left" style="margin-left: 5px;">
								<button type="submit" name="submit" class="btn btn-block btn-success">Simpan</button>
							</div>
						</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
</div>

<!-- JSon 2 Options -->
<script type="text/javascript">
	var data = 0;

	function opt_kota(def){
		var data_1 		= $('#provinsi').val();
		var loda_kota	= "<?php echo $data['kota'];?>";
		$.ajax({
			type 	: 'get',
			dataType: "json",
			url 	: '<?php echo base_url()."pelanggan/option_kota";?>',
			data 	: 'data_1='+data_1,
			success : function(data) {
				if (data == '0') {
					$('#kota option').remove();
					$('#kota').append("<option value=''>Kota / Kabupaten </option>");
					$('#kecamatan option').remove();
					$('#kecamatan').append("<option value=''>Kecamatan</option>");
					$('#kelurahan option').remove();
					$('#kelurahan').append("<option value=''>Kelurahan</option>");
				}
				else {
					$('#kota option').remove();
					$('#kota').append("<option value=''>Kota / Kabupaten</option>");
					$('#kecamatan option').remove();
					$('#kecamatan').append("<option value=''>Kecamatan</option>");
					$('#kelurahan option').remove();
					$('#kelurahan').append("<option value=''>Kelurahan</option>");
					for (var i=0;i<data.id_kabkot.length;i++){
						if (loda_kota == data.id_kabkot[i] && def == 1) {
							$('#kota').append("<option value='"+data.id_kabkot[i]+"' selected>"+data.nama_kabkot[i]+"</option>");
						}
						else{
							$('#kota').append("<option value='"+data.id_kabkot[i]+"'>"+data.nama_kabkot[i]+"</option>");
						}
					}
					console.log('cek_kota');
				}
				if (def == 1) {opt_kec(1);}
			}
		})
	}
	function opt_kec(def){
		if (def == 1) {
			var data_1	= "<?php echo $data['kota'] ?>";
		}
		else {
			var data_1	= $('#kota').val();
		}
		var load_kec	= "<?php echo $data['kec'];?>";
		$.ajax({
			type 	: 'get',
			dataType: "json",
			url 	: '<?php echo base_url()."pelanggan/option_kecamatan";?>',
			data 	: 'data_1='+data_1,
			success : function(data) {
				if (data == '0') {
					$('#kecamatan option').remove();
					$('#kecamatan').append("<option value=''>Kecamatan</option>");
					$('#kelurahan option').remove();
					$('#kelurahan').append("<option value=''>Kelurahan</option>");
				}
				else {
					$('#kecamatan option').remove();
					$('#kecamatan').append("<option value=''>Kecamatan</option>");
					$('#kelurahan option').remove();
					$('#kelurahan').append("<option value=''>Kelurahan</option>");
					for (var i=0;i<data.id_kec.length;i++){
						if (load_kec == data.id_kec[i] && def == 1) {
							$('#kecamatan').append("<option value='"+data.id_kec[i]+"' selected>"+data.nama_kec[i]+"</option>");
						}
						else{
							$('#kecamatan').append("<option value='"+data.id_kec[i]+"'>"+data.nama_kec[i]+"</option>");
						}
					}
					console.log('cek_kec');
				}
				if (def == 1) {opt_kel(1);}
			}
		})
	}
	function opt_kel(def){
		if (def == 1) {
			var data_1	= "<?php echo $data['kec'] ?>";
		}
		else {
			var data_1 	= $('#kecamatan').val();
		}
		var load_kab	= "<?php echo $data['kelurahan'];?>";
		$.ajax({
			type 	: 'get',
			dataType: "json",
			url 	: '<?php echo base_url()."pelanggan/option_kelurahan";?>',
			data 	: 'data_1='+data_1,
			success : function(data) {
				if (data == '0') {
					$('#kelurahan option').remove();
					$('#kelurahan').append("<option value=''>Kelurahan</option>");
				}
				else {
					$('#kelurahan option').remove();
					$('#kelurahan').append("<option value=''>Kelurahan</option>");
					for (var i=0;i<data.id_kel.length;i++){
						if (load_kab == data.id_kel[i] && def == 1) {
							$('#kelurahan').append("<option value='"+data.id_kel[i]+"' selected>"+data.nama_kel[i]+"</option>");
						}
						else{
							$('#kelurahan').append("<option value='"+data.id_kel[i]+"'>"+data.nama_kel[i]+"</option>");
						}
					}
					console.log('cek_kelurahan');
				}
			}
		})
	}
	$(document).ready(function() {

		opt_kota(1);

		// Tambah
		$('#provinsi').change(function() {
			opt_kota(0);
		});
		$('#kota').change(function() {
			opt_kec(0);
		});
		$('#kecamatan').change(function() {
			opt_kel(0);
		});
	});
</script>
<!-- /JSon 2 Options -->

<!-- Get Kuota -->
<script type="text/javascript">
	function get_kuota_gedung(){
		var data_1 		= $('#gedung').val();
		$.ajax({
			type 	: 'get',
			dataType: "json",
			url 	: '<?php echo base_url()."pelanggan/kuota_gedung";?>',
			data 	: 'data_1='+data_1,
			success : function(data) {
				$('#kuota').val('');
				$('#kuota').val(data.quota);
			}
		})
	}
	function get_kuota_member(){
		var data_1 		= $('#member').val();
		$.ajax({
			type 	: 'get',
			dataType: "json",
			url 	: '<?php echo base_url()."pelanggan/kuota_member";?>',
			data 	: 'data_1='+data_1,
			success : function(data) {
				$('#kuota').val('');
				$('#kuota').val(data.quota);
			}
		})
	}
	$(document).ready(function() {
		$('#gedung').change(function() {
			get_kuota_gedung();
		});
		$('#member').change(function() {
			get_kuota_member();
		});
	});
</script>

<!-- Show/Hide element -->
<script type="text/javascript">
	$(document).ready(function() {
		$('#jenis').change(function() {
			var value = $('#jenis').val();
			if (value == 'gedung'){
				$("#gedung-input").show("slow");
				$("#member-input").hide("slow");
				$("#kuota-input").show("slow");
			}
			else if (value == 'member'){
				$("#member-input").show("slow");
				$("#gedung-input").hide("slow");
				$("#kuota-input").show("slow");
			}
			else {
				$("#member-input").hide("slow");
				$("#gedung-input").hide("slow");
				$("#kuota-input").hide("slow");
				$('#kuota').val('');
			}
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
		var jenis 	= "<?php echo $data['jenis']; ?>";
		var method	= "<?php echo $method; ?>";
		var gedung 	= "<?php echo $data['gedung']; ?>";
		var member 	= "<?php echo $data['member']; ?>";
		var kuota 	= "<?php echo $data['kuota']; ?>";
		if (method == 'ubah') {
			if (jenis == '') {
				$("#jenis").val("gedung");
				$("#gedung-input").show("slow");
				$("#gedung").val(gedung);
				$("#kuota-input").show("slow");
				$('#kuota').val('');
				$('#kuota').val(kuota);
			}
			else {
				$("#jenis").val("member");
				$("#member-input").show("slow");
				$("#member").val(member);
				$("#kuota-input").show("slow");
				$('#kuota').val('');
				$('#kuota').val(kuota);
			}
		}
	});
</script>