<?php
if($this->session->flashdata('msg_success')) {?>
	<div class="alert alert-success alert-dismissible fade in" role="alert">
		<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
		<?php echo $this->session->flashdata('msg_success');?>
	</div>
<?php }?>
<?php
if($this->session->flashdata('msg_failed')) {?>
	<div class="alert alert-danger alert-dismissible fade in" role="alert">
		<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
		<?php echo $this->session->flashdata('msg_failed');?>
	</div>
<?php }?>
<style type="text/css">
	.table>tbody>tr>td{
		vertical-align: middle;
	}
</style>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>Data Membership</h5>
			</div>
			<div class="ibox-content">
				<?php echo anchor('master_data/membership/tambah','+ Tambah','class="btn btn-primary" style="font-weight:bold;"'); ?>
				<hr>
				<div class="table-responsive">
					<table id="table-membership" class="table table-striped table-bordered table-hover dt-responsive nowrap">
			 			<thead>
							<tr>
								<th width="30" class="text-center">No</th>
								<th>Membership Type</th>
								<th>Quota</th>
								<th>Harga</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<td colspan="6" class="dataTables_empty text-center">Loading data from server</td>
						</tbody>
					</table>
				<div class="table-responsive">
			</div>
		</div>
	</div>
</div>

<!-- Datatables -->
<script type="text/javascript">
	var save_method; //for save method string
	var table;
	$(document).ready(function() {
		//datatables
		table = $('#table-membership').DataTable({
			"lengthMenu": [[5, 10, -1], [5, 10, "All"]],
        	"pageLength": 5,
            dom: 'lTfgitp',
			"responsive": true,
			"processing": false, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [0,'desc'], //Initial no order.
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": '<?php echo site_url('membership/json');?>',
				"type": "POST",
				"dataType": "json"
			},
			"columnDefs": [{
                "searchable": false,
                "orderable": false,
                "targets": 4
            }],
			"fnRowCallback": function(nRow,aData,iDisplayIndex,iDisplayIndexFull) {
				var index = iDisplayIndex + 1;
				$('td:eq(0)',nRow).html(index);
				return nRow;
			},
			//Set column definition initialisation properties.
			"aoColumns": [
				{"mData": "id",width:10},
				{"mData": "name",width:300},
				{"mData": "quota",width:200},
				{"mData": "harga",width:200},
				{"mData": "action",class:'text-center',width:120}
			],
			"language": {
				"zeroRecords": "Data tidak ditemukan",
				"infoEmpty": "Tidak ada data"
			}
		});
	});
</script>
<!-- /Datatables -->