<link href="<?php echo $this->config->item('assets_dir');?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $this->config->item('assets_dir');?>css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="<?php echo $this->config->item('assets_dir');?>css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="<?php echo $this->config->item('assets_dir');?>font-awesome/css/font-awesome.css" rel="stylesheet"> 
<link href="<?php echo $this->config->item('assets_dir');?>css/animate.css" rel="stylesheet">
<link href="<?php echo $this->config->item('assets_dir');?>css/style.css" rel="stylesheet">
<script src="<?php echo $this->config->item('assets_dir');?>js/jquery-2.1.1.js"></script>
<script src="<?php echo $this->config->item('assets_dir');?>js/plugins/bootstrap-fileinput/css/fileinput.css"></script> 
<link href="<?php echo $this->config->item('assets_dir');?>js/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap Datetimepicker CSS-->
<link href="<?php echo base_url();?>assets/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">