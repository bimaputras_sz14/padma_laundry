
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo site_url(); ?>">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<?PHP if($parent !== null) {?>
						<li>
							<a href="#"><?php echo $parent; ?></a>
							<i class="fa fa-angle-right"></i>
						</li>
					<?PHP }?>
					<li>
						<a href="#"><?php echo $title; ?></a>
					</li>
				</ul>
			</div>