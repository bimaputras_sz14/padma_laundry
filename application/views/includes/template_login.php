<!DOCTYPE html>
<html> 
<head> 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <title><?php echo $this->config->item('site_title');?></title> 
    <link href="<?php echo $this->config->item('assets_dir');?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $this->config->item('assets_dir');?>font-awesome/css/font-awesome.css" rel="stylesheet"> 
    <link href="<?php echo $this->config->item('assets_dir');?>css/animate.css" rel="stylesheet">
    <link href="<?php echo $this->config->item('assets_dir');?>css/style.css" rel="stylesheet"> 
</head> 
	<body class="login-img3-body"> 
		<?php $this->load->view($body); ?>   
	</body> 
</html>
