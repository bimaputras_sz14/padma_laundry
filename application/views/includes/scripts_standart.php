<!-- Mainly scripts -->
<script src="<?php echo $this->config->item('assets_dir');?>js/jquery-2.1.1.js"></script>
<script src="<?php echo $this->config->item('assets_dir');?>js/bootstrap.min.js"></script>
<script src="<?php echo $this->config->item('assets_dir');?>js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo $this->config->item('assets_dir');?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script> 
<script src="<?php echo $this->config->item('assets_dir');?>js/plugins/datapicker/bootstrap-datepicker.js"></script> 
<script src="<?php echo $this->config->item('assets_dir');?>js/plugins/toastr/toastr.min.js"></script> 
<script src="<?php echo $this->config->item('assets_dir');?>js/plugins/select2/select2.full.min.js"></script> 
<!-- Custom and plugin javascript -->
<script src="<?php echo $this->config->item('assets_dir');?>js/djavacode.js"></script>
<script src="<?php echo $this->config->item('assets_dir');?>js/plugins/pace/pace.min.js"></script>    
