<div class="row border-bottom">
    <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-danger " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" method="get"  class="navbar-form-custom" action="<?php echo site_url('member/find')?>">
                <div class="form-group">
                    <input type="text" placeholder="Cari No/Nama Member " class="form-control" name="keyword" id="top-search">
                </div>
            </form>
        </div>
        <ul class="nav navbar-top-links navbar-right">  
            <li>
                <a href="<?php echo base_url(); ?>otorisasi/logout">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>

    </nav>
</div>