<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--><html lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8"/>
<title><?php echo $this->config->item('site_title');?> - <?php echo $title; ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="Copyright 2015 � Nagabendu Teknologi Utama" name="copyright"/>
<meta content="" name="description"/>
<meta content="Nagabendu Teknologi Utama" name="author"/>

<?php $this->load->view($styles); ?> 

<link rel="shortcut icon" href="<?php echo $this->config->item('assets_dir');?>img/favicon.png"/>
</head>

<body style='background-color:#fff'>

<!-- BEGIN HEADER -->
<?php //$this->load->view('includes/template_site_header'); ?> 
<!-- END HEADER -->



<!-- BEGIN CONTAINER -->

	<!-- BEGIN SIDEBAR -->
	<?php //$this->load->view('includes/menu'); ?> 
	<!-- END SIDEBAR -->
	
	<!-- BEGIN CONTENT -->
	
			<!-- BEGIN PAGE HEADER-->
			<h3 style="color:#000">
			<?php echo $title; ?> <small><?php echo $desc; ?></small>
			</h3>
			<?PHP //$this->load->view($page_bar); ?>
			<!--<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php //echo site_url(); ?>">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#"><?php //echo $title; ?></a>
					</li>
				</ul>
			</div>-->
			<!-- END PAGE HEADER-->
			
			<!-- BEGIN DASHBOARD STATS -->
			<?php $this->load->view($body); ?> 
			<!-- END DASHBOARD STATS -->
		
	<!-- END CONTENT -->

<!-- END CONTAINER -->



<!-- Footer -->  
<?php //$this->load->view('includes/footer'); ?> 

<!-- Scripts -->  
<?php $this->load->view($scripts); ?> 

</body>
</html>