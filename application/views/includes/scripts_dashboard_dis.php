<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo $this->config->item('plugins_dir');?>jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo $this->config->item('plugins_dir');?>amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>amcharts/amcharts/themes/light.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo $this->config->item('base_url');?>common/js/jquery.countTo.js"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>bootstrap-toastr/toastr.min.js"></script>

<script src='https://api.mapbox.com/mapbox.js/v2.2.3/mapbox.js'></script>
<script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js'></script>
<!-- END PAGE LEVEL PLUGINS -->


<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo $this->config->item('assets_dir');?>global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('assets_dir');?>scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('assets_dir');?>scripts/quick-sidebar.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->


<script>

function getJmlDashboard(){
	Metronic.blockUI({
		target: '#container1',
		boxed: true,
		message: 'Proses...',
		textOnly: true
	});
	Metronic.blockUI({
		target: '#container2',
		boxed: true,
		message: 'Proses...',
		textOnly: true
	});
	Metronic.blockUI({
		target: '#container3',
		boxed: true,
		message: 'Proses...',
		textOnly: true
	});
	Metronic.blockUI({
		target: '#container4',
		boxed: true,
		message: 'Proses...',
		textOnly: true
	});
	var form_data = {
		tgl: '<?PHP echo date('Y-m-d'); ?>',
		thn: '<?PHP echo date('Y'); ?>'
	};
	
	$.ajax({
		url: "<?php echo site_url('home/get_jml_dashboard'); ?>",
		type: 'POST',
		dataType: 'json',
		data: form_data,
		success: function(result) {
			$("#jml_distribusi").html(result.jml_distribusi);
			$('#jml_distribusi').countTo({from: 0, to: result.jml_distribusi, speed: 2000});
			$("#jml_ktmdu_telusur").html(result.jml_ktmdu_telusur);
			$('#jml_ktmdu_telusur').countTo({from: 0, to: result.jml_ktmdu_telusur, speed: 2000});
			$("#ktmdu").html(result.jml_ktmdu);
			$('#ktmdu').countTo({from: 0, to: result.jml_ktmdu, speed: 2000});
			$("#nonktmdu").html(result.jml_nonKtmdu);
			$('#nonktmdu').countTo({from: 0, to: result.jml_nonKtmdu, speed: 2000});
			Metronic.unblockUI('#container1');
			Metronic.unblockUI('#container2');
			Metronic.unblockUI('#container3');
			Metronic.unblockUI('#container4');
		}
	});
	
}

var arrLokasiMarker = [];
var mapWidthStandart = 0;

function showMap(mapcanvas, arrLokasi, mapMode){
	var portlet = $('#'+mapcanvas).closest(".portlet");
	if (mapMode !== 'full') {
		var height = 600;
		var width = mapWidthStandart;
	}else{
		var height = Metronic.getViewPort().height - 75;
		var width = Metronic.getViewPort().width -
						parseInt(portlet.children('.portlet-body').css('padding-left')) -
						parseInt(portlet.children('.portlet-body').css('padding-right'));
	}
	
	document.getElementById('map-canvas').innerHTML = '<div id="'+mapcanvas+'" class="gmaps"></div>';
	$('#'+mapcanvas).height(height);
	$('#'+mapcanvas).width(width);

	if((arrLokasi != "") && (arrLokasi != null)){
		
		L.mapbox.accessToken = '<?php echo $this->config->item('access_token');?>';
		var map = L.mapbox.map(mapcanvas, 'mapbox.streets')
			.setView([<?php echo '"'. $this->lat_gmap .'"' ?>, <?php echo '"'. $this->lon_gmap .'"' ?>], <?php echo $this->config->item('default_zoom');?>);

		var markers = new L.MarkerClusterGroup();
		var arrLatLon = [];
		
		$.each(arrLokasi, function(i, val) {
			var title = setMarkerPopup(val.foto, val.waktu, val.petugas, val.acc,val.pemilik,val.yg_ttd,val.no_pol);
			var myIcon = {
					"iconUrl": val.marker,
					"iconSize": [48, 48], 
					"iconAnchor": [24, 48], 
					"popupAnchor": [0, -50],
					"className": "dot",
					"shadowUrl": '<?php echo $this->config->item('marker_dir');?>shadow.png',
					"shadowSize": [48, 48],
					"shadowAnchor": [24, 24]
			};
			var marker = L.marker(new L.LatLng(val.lat, val.lon), {
				icon: L.icon(myIcon),
				title: title
			});
			marker.bindPopup(title);
			markers.addLayer(marker);
			
			arrLatLon.push([val.lat, val.lon]);
		});

		map.addLayer(markers);
		map.fitBounds(arrLatLon);

		
	}else{
		toastr.error('Data tidak ditemukan', 'Laporan Penulusuran');
		L.mapbox.accessToken = '<?php echo $this->config->item('access_token');?>';
		var map = L.mapbox.map(mapcanvas, 'mapbox.streets')
			.setView([<?php echo '"'. $this->lat_gmap .'"' ?>, <?php echo '"'. $this->lon_gmap .'"' ?>], <?php echo $this->config->item('default_zoom');?>);
	}
}

function setMarkerPopup(foto,  waktu, petugas, akurasi,pemilik,yg_ttd,no_pol){	
	var text = foto;
	// text += "<b>"+info+"</b><br>"; 
	text += "Pemilik Ranmor :"+pemilik+"<br>";
	text += "No Pol         :"+no_pol+"<br>";
	text += "Yg TTD         :"+yg_ttd+"<br>";
	text += waktu+" WIB<br><b>Petugas</b><br>";
	text += petugas+"<br><br>";
	text += "<i>akurat sampai dengan "+akurasi+" m</i>";
	
	return "<div style='color:#000;'>"+text+"</div>";
	// return text;
}


//GET LOKASI
function getLapLantas(){
	var form_data = {
		// tgl: '<?PHP echo date('Y-m-d'); ?>'
		tgl: '2016-03-10'
	};
	
	$.ajax({
		url: "<?php echo site_url('monitoring/get_lapinfo'); ?>",
		type: 'POST',
		dataType: 'json',
		data: form_data,
		success: function(result) {
			showMap("map", result, 'standart');
			arrLokasiMarker = result;
		}
	});
	
}

function getLapSambang(){
	Metronic.blockUI({
		target: '#container9',
		boxed: true,
		message: 'Proses...',
		textOnly: true
	});
	var form_data = {
		tgl: '<?PHP echo date('Y-m-d'); ?>'
		// tgl: '2016-03-10'
	};
	
	$.ajax({
		url: "<?php echo site_url('home/get_LapSambang'); ?>",
		type: 'POST',
		dataType: 'json',
		data: form_data,
		success: function(result) {
			Metronic.unblockUI('#container9');
			showMap("map", result, 'standart');
			arrLokasiMarker = result;
		}
	});
	
}


function initChart1(arrDataChart) {
    var chart = AmCharts.makeChart("initChart1", {
            "type": "serial",
            "theme": "light",
            "pathToImages": Metronic.getGlobalPluginsPath() + "amcharts/amcharts/images/",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,
			"legend": {
            "equalWidths": true,
            "useGraphSettings": true,
            "valueAlign": "left"
			},
            "fontFamily": 'Open Sans',            
            "color":    '#888',
            
            "dataProvider": [arrDataChart['data_chart']],
            
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left"
            }],
            "startDuration": 1,
			"graphs": arrDataChart['chart_graphs'],
            
            "categoryField": "kab",
            "categoryAxis": {
                "labelRotation": 60,
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            }
    });

    $('#initChart1').closest('.portlet').find('.fullscreen').click(function() {
        chart.invalidateSize();
    });
}
function initChart2(arrDataChart) {
	var chart = AmCharts.makeChart("initChart2", {
            "type": "serial",
            "theme": "light",
            "pathToImages": Metronic.getGlobalPluginsPath() + "amcharts/amcharts/images/",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,
			"legend": {
            "equalWidths": true,
            "useGraphSettings": true,
            "valueAlign": "left"
			},
            "fontFamily": 'Open Sans',            
            "color":    '#888',
            
            "dataProvider": [arrDataChart['data_chart']],
            
            "valueAxes": [{
                "axisAlpha": 0,
                "position": "left"
            }],
            "startDuration": 1,
			"graphs": arrDataChart['chart_graphs'],
            
            "categoryField": "kab",
            "categoryAxis": {
                "labelRotation": 60,
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            }
    });

    $('#initChart2').closest('.portlet').find('.fullscreen').click(function() {
        chart.invalidateSize();
    });

	// $('#judul_chart_trend').html(arrDataChart['chart_title']);
}

function initChart3(arrDataChart) {
	var chart = AmCharts.makeChart("initChart3",{
			"type": "serial",
			"categoryField": "tgl",
			"startDuration": 1,
			"export": {
				"enabled": true,
				"libs": {
					"path": "http://cdn.amcharts.com/lib/3/plugins/export/libs/"
				}
			},
			"pathToImages": "http://cdn.amcharts.com/lib/3/images/",
			"theme": "dark",
			"categoryAxis": {
				"gridPosition": "start"
			},
			"trendLines": [],
			"graphs": [
				{
					"balloonText": "[[title]] Tanggal [[tgl]]<br>Jumlah : [[jml]]",
					"bullet": "round",
					"id": "AmGraph-1",
					"title": "Penulusuran",
					"valueField": "jml"
				}
			],
			"guides": [],
			"valueAxes": [
				{
					"id": "ValueAxis-1",
					"title": "Jumlah"
				}
			],
			"allLabels": [],
			"balloon": {},
			"legend": {
				"enabled": true,
				"useGraphSettings": true
			},
			"titles": [
				{
					"id": "Title-1",
					"size": 15,
					"text": "Pertumbuhan Penulusuran KTMDU"
				}
			],
			"dataProvider":arrDataChart['data_chart'] 
		});

    $('#initChart3').closest('.portlet').find('.fullscreen').click(function() {
        chart.invalidateSize();
    });

	
}

function initChart4(arrDataChart) {
	 var chart = AmCharts.makeChart("initChart4", {
            "type": "pie",
            "theme": "light",
            "fontFamily": 'Open Sans',
            "color":    '#888',
			"legend": {
                "align": "center",
                "position": "bottom",
                "marginRight": 21,
                "markerType": "circle",
                "right": -4,
                "labelText": "[[title]]",
                "valueText": " [[value]]",
                "valueWidth": 80,
                "textClickEnabled": true
            },
            "dataProvider": arrDataChart['data_chart'],
            "valueField": "jumlah",
            "titleField": "kriteria"
        });
    $('#initChart4').closest('.portlet').find('.fullscreen').click(function() {
        chart.invalidateSize();
    });

}

function getChartDistribusi(){
	Metronic.blockUI({
		target: '#container5',
		boxed: true,
		message: 'Proses...',
		textOnly: true
	});
	$.ajax({
		url: "<?php echo site_url('home/getChartDistribusi'); ?>",
		dataType: 'json',
		asycn:false,
		success: function(result) {
			Metronic.unblockUI('#container5');
			initChart1(result);
		}
	});
	
}

function getChartDistribusiPenulusuran(){
	Metronic.blockUI({
		target: '#container6',
		boxed: true,
		message: 'Proses...',
		textOnly: true
	});
	$.ajax({
		url: "<?php echo site_url('home/getChartDistribusiPenulusuran'); ?>",
		dataType: 'json',
		asycn:false,
		success: function(result) {
			Metronic.unblockUI('#container6');
			initChart2(result);
		}
	});
	
}

function getChartPertumbuhanPenulusuran(){
	Metronic.blockUI({
		target: '#container7',
		boxed: true,
		message: 'Proses...',
		textOnly: true
	});
	$.ajax({
		url: "<?php echo site_url('home/ChartJumlahPenelusuran'); ?>",
		dataType: 'json',
		asycn:false,
		success: function(result) {
			Metronic.unblockUI('#container7');
			initChart3(result);
		}
	});
	
}

function getChartJumlahKTMDU(){
	Metronic.blockUI({
		target: '#container8',
		boxed: true,
		message: 'Proses...',
		textOnly: true
	});
	$.ajax({
		url: "<?php echo site_url('home/getChartJumlahKTMDU'); ?>",
		dataType: 'json',
		asycn:false,
		success: function(result) {
			Metronic.unblockUI('#container8');
			initChart4(result);
		}
	});
	
}
	
function load_samdong(level){
	// var level =level_user;
	if(typeof  notif_samdong !='undefined'){
		// console.log($(notif_samdong).css("display"));
		toastr.clear(notif_samdong);
		
	}
	// if(typeof  notif_samdong =='undefined'){
		if(level==10 || level==11){
			$.ajax({
				url: "<?php echo site_url('samdong/get_new_request'); ?>",
				dataType: 'json',
				success: function(result) {
					
					// else{
						jumlah_request = result.jumlah;
						if(jumlah_request > 0 ){
							toastr.options.fadeOut = 0;
							toastr.options.showDuration = 0;
							toastr.options.timeOut = 0;
							toastr.options.onclick = function () { go_toSamdong(); };
							toastr.options.hideDuration = 0;
							toastr.options.extendedTimeOut = 0;
							toastr.options.closeButton = true;
							notif_samdong = toastr.info("Terdapat "+result.jumlah+" request Samdong Belum di Respon", 'Request Samdong');
						}
					// }
						
				}
			});
		}
	// }
	
	
}
function load_samdong2(level){
	
		if(level==10 || level==11){
			$.ajax({
				url: "<?php echo site_url('samdong/get_notif_request'); ?>",
				dataType: 'json',
				success: function(result) {
					jumlah_request = result.jumlah;
						if(jumlah_request > 0 ){
							// $("#jumlahNotif1").css('display','block');
							// $("#jumlahNotif2").css('display','block');
							$("#jumlahNotif3").css('display','block');
							$("#jumlahNotif1").html(jumlah_request);
							$(".jumlahNotif2").html(jumlah_request);
						}else{
							$("#jumlahNotif1").html(0);
							$(".jumlahNotif2").html(0);
							// $("#jumlahNotif2").css('display','none');
							$("#jumlahNotif3").css('display','none');
						}
						
				}
			});
		}
}
//CEK LOGIN
function prepareGetData(){
    $.ajax({
        url: "<?php echo site_url('user/cek_session_login'); ?>",
        type: 'POST',
        success: function(msg) {
			if(msg=='true'){
				/* jalankan fungsi */
				getJmlDashboard();
				getChartDistribusi(); 
				getChartDistribusiPenulusuran(); 
				getLapSambang();
				getChartPertumbuhanPenulusuran(); 
				getChartJumlahKTMDU(); 
				// getLapLantas();
				// getChartLapinfo();
				// getChartLakalantas();
			}else{
				window.location.replace("<?php echo site_url(); ?>");
			}
        }
    });
}


jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   load_samdong2(level_user);
   setInterval( function() { load_samdong(level_user); }, 60000 );
   setInterval( function() { load_samdong2(level_user); }, 60000 );
   
   prepareGetData();
   
   // MAP FULLSCREEN
   mapWidthStandart = $('#map').width();
   $('#map').closest('.portlet').find('.fullscreen').click(function() {
		var portlet = $('#map').closest(".portlet");
		if (portlet.hasClass('portlet-fullscreen')) {
			showMap("map", arrLokasiMarker, 'standart');
		}else{
			showMap("map", arrLokasiMarker, 'full');
		}
   });
   
});
</script>
<!-- END JAVASCRIPTS -->
