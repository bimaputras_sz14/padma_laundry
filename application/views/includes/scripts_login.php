<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo $this->config->item('plugins_dir');?>jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('plugins_dir');?>uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo $this->config->item('plugins_dir');?>jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo $this->config->item('assets_dir');?>global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('assets_dir');?>scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('assets_dir');?>scripts/pages/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {     
	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	Login.init();
});
</script>
<!-- END JAVASCRIPTS -->