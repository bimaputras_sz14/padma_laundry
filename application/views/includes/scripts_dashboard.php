 <!-- Mainly scripts -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/jquery-2.1.1.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/bootstrap.min.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/inspinia.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/pace/pace.min.js"></script>

	 <!-- Flot -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/flot/jquery.flot.time.js"></script> 
     <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/peity/jquery.peity.min.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/demo/peity-demo.js"></script> 
    <!-- Custom and plugin javascript -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/djavacode.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/pace/pace.min.js"></script> 
    <!-- jQuery UI -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <!-- Jvectormap -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> 
    <!-- EayPIE -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/easypiechart/jquery.easypiechart.js"></script> 
    <!-- Sparkline -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/sparkline/jquery.sparkline.min.js"></script> 
    <!-- Sparkline demo data  -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/demo/sparkline-demo.js"></script> 
    <script>
        $(document).ready(function() {
            $('.chart').easyPieChart({
                barColor: '#f8ac59',
                //scaleColor: false,
                scaleLength: 5,
                lineWidth: 4,
                size: 80
            });

            $('.chart2').easyPieChart({
                barColor: '#1c84c6',
                //scaleColor: false,
                scaleLength: 5,
                lineWidth: 4,
                size: 80
            });

            var data2 = [
            <?php 
                foreach ($visit_graph as $row) {
                    // yyyy-mm-dd
                     $tgl = $row->date_start;
                     $y = substr( $tgl, 0,4);
                     $m = substr( $tgl, 5,2);
                     $d = substr( $tgl, 8,2);
                    
                    echo '[gd('.$y.', '.$m.', '.$d.'), '.$row->total_visit.'],';
                }

            ?>
                  
                 
            ]; 
            var data3 = [
                <?php 
                foreach ($sales_graph as $row) {
                    // yyyy-mm-dd
                     $tgl = $row->tanggal_penjualan;
                     $y = substr( $tgl, 0,4);
                     $m = substr( $tgl, 5,2);
                     $d = substr( $tgl, 8,2);
                    
                    echo '[gd('.$y.', '.$m.', '.$d.'), '.$row->total_penjualan.'],';
                }?>
             
            ]; 
            var dataset = [
                {
                    label: "Visit",
                    data: data3,
                    color: "#1ab394",
                    bars: {
                        show: true,
                        align: "center",
                        barWidth: 24 * 60 * 60 * 600,
                        lineWidth:0
                    }

                }, {
                    label: "Penjualan",
                    data: data2,
                    yaxis: 2,
                    color: "#1C84C6",
                    lines: {
                        lineWidth:1,
                            show: true,
                            fill: true,
                        fillColor: {
                            colors: [{
                                opacity: 0.2
                            }, {
                                opacity: 0.4
                            }]
                        }
                    },
                    splines: {
                        show: false,
                        tension: 0.6,
                        lineWidth: 1,
                        fill: 0.1
                    },
                }
            ]; 
            var options = {
                xaxis: {
                    mode: "time",
                    tickSize: [3, "day"],
                    tickLength: 0,
                    axisLabel: "Date",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Arial',
                    axisLabelPadding: 10,
                    color: "#d5d5d5"
                },
                yaxes: [{
                    position: "left",
                    max: 1070,
                    color: "#d5d5d5",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Arial',
                    axisLabelPadding: 3
                }, {
                    position: "right",
                    clolor: "#d5d5d5",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: ' Arial',
                    axisLabelPadding: 67
                }
                ],
                legend: {
                    noColumns: 1,
                    labelBoxBorderColor: "#000000",
                    position: "nw"
                },
                grid: {
                    hoverable: false,
                    borderWidth: 0
                }
            }; 
            function gd(year, month, day) {
                return new Date(year, month - 1, day).getTime();
            } 
            var previousPoint = null, previousLabel = null; 
            $.plot($("#flot-dashboard-chart"), dataset, options); 
            var mapData = {
                "US": 298,
                "SA": 200,
                "DE": 220,
                "FR": 540,
                "CN": 120,
                "AU": 760,
                "BR": 550,
                "IN": 200,
                "GB": 120,
            }; 
            $('#world-map').vectorMap({
                map: 'world_mill_en',
                backgroundColor: "transparent",
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 0.9,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 0
                    }
                }, 
                series: {
                    regions: [{
                        values: mapData,
                        scale: ["#1ab394", "#22d6b1"],
                        normalizeFunction: 'polynomial'
                    }]
                },
            });
        });
    </script>
<!-- END JAVASCRIPTS -->
