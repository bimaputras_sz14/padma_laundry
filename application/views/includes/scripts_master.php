 <!-- Mainly scripts -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/jquery-2.1.1.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/bootstrap.min.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/pace/pace.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?php echo base_url();?>vendor/datatables/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/datatables/datatables/media/js/dataTables.bootstrap.min.js"></script>

    <!-- export datatable-->
    <script src="<?php echo base_url();?>assets/datatables-excel/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>assets/datatables-excel/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>assets/datatables-excel/jszip.min.js"></script>
    <script src="<?php echo base_url();?>assets/datatables-excel/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>assets/datatables-excel/vfs_fonts.js"></script>

    <!-- Moment -->
    <script src="<?php echo base_url();?>assets/moment/min/moment.min.js"></script>
    <!-- Bootstrap Datetimepicker JavaScript -->
    <script src="<?php echo base_url();?>assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

	 <!-- Flot -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/flot/jquery.flot.time.js"></script> 
     <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/peity/jquery.peity.min.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/demo/peity-demo.js"></script> 
    <!-- Custom and plugin javascript -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/djavacode.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/pace/pace.min.js"></script> 
    <!-- jQuery UI -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/jquery-ui/jquery-ui.min.js"></script> 
    <!-- Jvectormap -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> 
    <!-- EayPIE -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/easypiechart/jquery.easypiechart.js"></script> 
    <!-- Sparkline -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/sparkline/jquery.sparkline.min.js"></script> 
    <!-- Sparkline demo data  -->
    <script src="<?php echo $this->config->item('assets_dir');?>js/demo/sparkline-demo.js"></script> 
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/bootstrap-fileinput/js/fileinput.js"></script> 
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/moment/min/moment.min.js"></script>
    <script src="<?php echo $this->config->item('assets_dir');?>js/plugins/daterangepicker/daterangepicker.js"></script> 
