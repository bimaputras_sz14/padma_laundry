<!DOCTYPE html>
<html> 
<head> 
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <title><?php echo $this->config->item('site_title');?> - <?php echo $title; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="Copyright 2017 � B4studio" name="copyright"/>
    <meta content="" name="description"/>
    <meta content="B4studio" name="author"/>   
    <?php $this->load->view($styles); ?> 
</head> 
<body class="">  
    <div id="wrapper">  
        <?php $this->load->view('includes/menu'); ?> 
        <div id="page-wrapper" class="gray-bg">
            <?php $this->load->view('includes/template_site_header'); ?> 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>Master</h2> 
                    <ol class="breadcrumb">
                        <li>
                            <a href="#"><?php echo $title; ?></a>
                        </li> 
                        <li class="active">
                            <strong><?php echo $desc; ?></strong>
                        </li>
                    </ol>
                </div>
               <!--  <div class="col-sm-8">
                    <div class="title-action">
                        <a href="" class="btn btn-primary">This is action area</a>
                    </div>
                </div> -->
            </div> 
            <div class="wrapper wrapper-content "> 
                <?php $this->load->view($body); ?> 
                       
            </div>
           <?php $this->load->view('includes/footer'); ?>  
        </div>
    </div> 
<?php $this->load->view($scripts); ?> 
</body>

</html>
