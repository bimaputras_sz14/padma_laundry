<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">  
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> 
                            <span class="block m-t-xs"> 
                                <strong class="font-bold">Padma Laundry</strong>
                            </span>  
                        </span> 
                    </a> 
                </div>
                <div class="logo-element">
                Padma Laundry
                </div>
            </li> 
            <li>
                 <?PHP
					$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
					$level = $arr_usr['lvl_id'];
					// var_dump($level);
					if(($level != '') && ($level != null)){ 
						if($level == 1){
							$mMenuAdmin = $this->config->item('menu_superadmin');
						}else if($level == 2){
							$mMenuAdmin = $this->config->item('menu_admin');
						}else if($level == 3){
							$mMenuAdmin = $this->config->item('menu_pemilik');
						}else if($level == 4){
							$mMenuAdmin = $this->config->item('menu_kasir');
						}else if($level == 5){
							$mMenuAdmin = $this->config->item('menu_finance');
						}else{
							$mMenuAdmin = $this->config->item('menu_sales');
						} 
						foreach ($mMenuAdmin as $parent => $parent_params) 
						{
							$name = $parent_params['name'];
							$url = $parent_params['url'];
							$icon = $parent_params['icon'];
							
							if ( empty($parent_params['children']) )
							{
								$active = ( current_url()==$url ) ? "class='active ' " : '';
								$selected = ( current_url()==$url ) ? "<span class='selected'></span>" : "";
								echo "<li $active>
											<a href='$url'>
											<i class='$icon'></i>
											<span class='title'>$name</span>
											$selected
											</a>
										</li>";
							}
							else
							{
								// $ctrler = $this->router->fetch_class();
								$ctrler = $this->uri->segment(1);
								if( $ctrler==$parent ){
									$parent_active = "class='active'";
									$parent_open = "open";
									$parent_selected = "<span class='selected'></span>";
								}else{
									$parent_active = "";
									$parent_open = "";
									$parent_selected = "";
								}
								
								$parent_name = $parent_params['name'];
								$parent_url = $parent_params['url'];
								$parent_icon = $parent_params['icon'];
								echo "<li $parent_active>
											<a href=''>
											<i class='$parent_icon'></i>
											<span class='title'>$parent_name</span>
											$parent_selected
											<span class='fa arrow'></span>
											</a>";
											
								echo "<ul class='nav nav-second-level collapse'>";
								foreach ($parent_params['children'] as $child => $child_params)
								{
									// $action = $this->router->fetch_method();
									$action = $this->uri->segment(2);
									$child_active = ( $action==$child ) ? " class='active'" : '';
									$child_name = $child_params['child_name'];
									$child_url = $child_params['child_url'];
									
									echo "<li $child_active>
												<a href='$child_url'>$child_name</a>
											</li>";
								}
								echo "</ul>";
								
								echo "</li>";
							}
						}
					}
				?>
            </li>  
        </ul>

    </div>
</nav>
