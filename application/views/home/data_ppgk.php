<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-body">
                <div class="tabbable-custom ">
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_5_1" data-toggle="tab">
                            <i class="icon-users font-red-intense"></i> Data PPGK</a>
                        </li>
                        
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_5_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet box">
                                        <div class="portlet-body" style="overflow-y: scroll;">
                                            <div class="table-toolbar">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="btn-group">
                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <table class="table table-striped table-bordered table-hover" id="table_tahanan">
                                                <thead>
                                                    <tr>
                                                        <th style="max-width:3%">
                                                            No
                                                        </th>
                                                        <th style="max-width:5%">
                                                            Jenis
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLRESTABES<br>SURABAYA
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK<br>BUBUTAN
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK<br>TAMBAKSARI
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK<br>SIMOKERTO
                                                        </th> 
                                                        <th style="max-width:18%">
                                                            POLSEK<br> SAWAHAN
                                                        </th> 
                                                        <th style="max-width:18%">
                                                           POLSEK <br>GENTENG
                                                        </th> 
                                                         <th style="max-width:18%">
                                                           POLSEK <br>TEGALSARI
                                                        </th>
                                                         <th style="max-width:18%">
                                                           POLSEK <br>WONOKROMO
                                                        </th>
                                                         <th style="max-width:18%">
                                                           POLSEK <br>DUKUH PAKIS
                                                        </th>
                                                         <th style="max-width:18%">
                                                           POLSEK<br> GUBENG
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK <br>MULYOREJO
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK <br>SUKOLILO
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK <br>WONOCOLO
                                                        </th>
                                                        <th style="max-width:18%">
                                                           POLSEK <br>TENGGILIS MJY
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK <br>RUNGKUT
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK<br> KARANGPILANG
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK <br>GAYUNGAN 
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK <br>JAMBANGAN
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK <br>SK. MANUNGGAL
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK<br> BENOWO
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK<br> PAKAL
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK <br>TANDES
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK <br>WIYUNG
                                                        </th>
                                                        <th style="max-width:18%">
                                                            POLSEK <br>LAKARSANTRI
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            1
                                                        </td>
                                                        <td style="max-width:5%">
                                                            THD KEAMANAN NEGARA
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            2
                                                        </td>
                                                        <td style="max-width:5%">
                                                            THD PRESIDEN / WAKIL
                                                        </td>
                                                        <td style="max-width:18%">
                                                            3
                                                        </td>
                                                        <td style="max-width:18%">
                                                            2
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            4
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            3
                                                        </td>
                                                        <td style="max-width:5%">
                                                           THD KETERTIBAN UMUM
                                                        </td>
                                                        <td style="max-width:18%">
                                                            2
                                                        </td>
                                                        <td style="max-width:18%">
                                                            3
                                                        </td>
                                                        <td style="max-width:18%">
                                                            4
                                                        </td>
                                                        <td style="max-width:18%">
                                                            2
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            4
                                                        </td>
                                                        <td style="max-width:5%">
                                                            THD KETERTIBAN UMUM
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            5
                                                        </td>
                                                        <td style="max-width:5%">
                                                            KEBAKARAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            6
                                                        </td>
                                                        <td style="max-width:5%">
                                                            U P A L
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            7
                                                        </td>
                                                        <td style="max-width:5%">
                                                            PERKOSAAN/ PENCABULAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            8
                                                        </td>
                                                        <td style="max-width:5%">
                                                            PERJUDIAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            9
                                                        </td>
                                                        <td style="max-width:5%">
                                                            PENCULIKAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            10
                                                        </td>
                                                        <td style="max-width:5%">
                                                            PEMBUNUHAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            11
                                                        </td>
                                                        <td style="max-width:5%">
                                                            PENGANIAYAAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            12
                                                        </td>
                                                        <td style="max-width:5%">
                                                            MENGAKIBATKAN ORG M D
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            13
                                                        </td>
                                                        <td style="max-width:5%">
                                                            PENCURIAN BIASA
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            14
                                                        </td>
                                                        <td style="max-width:5%">
                                                            C U R A T
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            15
                                                        </td>
                                                        <td style="max-width:5%">
                                                            C U R A S
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            16
                                                        </td>
                                                        <td style="max-width:5%">
                                                            PEMERASAN DAN ANCAMAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            17
                                                        </td>
                                                        <td style="max-width:5%">
                                                            PENGGELAPAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            18
                                                        </td>
                                                        <td style="max-width:5%">
                                                            PENIPUAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            19
                                                        </td>
                                                        <td style="max-width:5%">
                                                            PENGERUSAKAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            20
                                                        </td>
                                                        <td style="max-width:5%">
                                                            BAHAN BERBAHAYA
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           21
                                                        </td>
                                                        <td style="max-width:5%">
                                                            PENYELUNDUPAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            22
                                                        </td>
                                                        <td style="max-width:5%">
                                                            NARKOTIKA
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            23
                                                        </td>
                                                        <td style="max-width:5%">
                                                            OBAT KERAS
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            24
                                                        </td>
                                                        <td style="max-width:5%">
                                                            PERBANKAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            25
                                                        </td>
                                                        <td style="max-width:5%">
                                                           P   O   A
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            26
                                                        </td>
                                                        <td style="max-width:5%">
                                                            PRODUKSI & PERDAGANGAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            27
                                                        </td>
                                                        <td style="max-width:5%">
                                                            SENPI / HANDAK
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            28
                                                        </td>
                                                        <td style="max-width:5%">
                                                            GANGGUAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            29
                                                        </td>
                                                        <td style="max-width:5%">
                                                            LAIN – LAIN KEJADIAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                         <!--  -->
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                        <td style="max-width:18%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>
                            </div>             
                            </div>
                        </div>

                    <!-- PESAN KELUAR -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>