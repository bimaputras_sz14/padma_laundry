<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet">
            <div class="portlet-body">
                <div class="tabbable-custom ">
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#tab_5_1" data-toggle="tab">
                            <i class="icon-users font-red-intense"></i> Data Tahanan</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_5_1">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet box">
                                        <div class="portlet-body">
                                            <div class="table-toolbar">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="btn-group">
                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <table class="table table-striped table-bordered table-hover" id="table_tahanan">
                                                <thead>
                                                    <tr>
                                                        <th style="max-width:3%">
                                                            No
                                                        </th>
                                                        <th style="max-width:5%">
                                                            Kesatuan
                                                        </th>
                                                        <th style="max-width:18%">
                                                            Jumlah Data Tahanan
                                                        </th>
                                                        <th style="max-width:10%">
                                                            Laki-Laki
                                                        </th>
                                                        <th style="max-width:10%">
                                                            Perempuan
                                                        </th>
                                                        <th style="max-width:10%">
                                                            Anak Anak
                                                        </th> 
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                            1
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLRESTABES SURABAYA
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                     <tr>
                                                        <td style="max-width:3%">
                                                            2
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK BUBUTAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            4
                                                        </td>
                                                        <td style="max-width:10%">
                                                            2
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td> 
                                                    </tr>
                                                     <tr>
                                                        <td style="max-width:3%">
                                                            3
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK TAMBAKSARI
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           4
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK SIMOKERTO
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           4
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK SIMOKERTO
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           5
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK SAWAHAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           6
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK GENTENG
                                                        </td>
                                                        <td style="max-width:18%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           7
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK TEGALSARI
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           8
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK WONOKROMO
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           9
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK DUKUH PAKIS
                                                        </td>
                                                        <td style="max-width:18%">
                                                            3
                                                        </td>
                                                        <td style="max-width:10%">
                                                            3
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           10
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK GUBENG
                                                        </td>
                                                        <td style="max-width:18%">
                                                           0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           11
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK MULYOREJO
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           12
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK SUKOLILO
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           13
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK WONOCOLO
                                                        </td>
                                                        <td style="max-width:18%">
                                                            4
                                                        </td>
                                                        <td style="max-width:10%">
                                                            3
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           14
                                                        </td>
                                                        <td style="max-width:5%">
                                                           POLSEK TENGGILIS MJY
                                                        </td>
                                                        <td style="max-width:18%">
                                                            5
                                                        </td>
                                                        <td style="max-width:10%">
                                                            5
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           15
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK RUNGKUT
                                                        </td>
                                                        <td style="max-width:18%">
                                                            3
                                                        </td>
                                                        <td style="max-width:10%">
                                                            3
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           16
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK KARANGPILANG
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           17
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK GAYUNGAN 
                                                        </td>
                                                        <td style="max-width:18%">
                                                            3
                                                        </td>
                                                        <td style="max-width:10%">
                                                           2
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           18
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK JAMBANGAN
                                                        </td>
                                                        <td style="max-width:18%">
                                                            3
                                                        </td>
                                                        <td style="max-width:10%">
                                                            3
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           19
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK SK. MANUNGGAL
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           20
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK BENOWO
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           21
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK PAKAL
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           22
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK TANDES
                                                        </td>
                                                        <td style="max-width:18%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            1
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           23
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK WIYUNG
                                                        </td>
                                                        <td style="max-width:18%">
                                                           5
                                                        </td>
                                                        <td style="max-width:10%">
                                                            5
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                    <tr>
                                                        <td style="max-width:3%">
                                                           24
                                                        </td>
                                                        <td style="max-width:5%">
                                                            POLSEK LAKARSANTRI
                                                        </td>
                                                        <td style="max-width:18%">
                                                            2
                                                        </td>
                                                        <td style="max-width:10%">
                                                           2
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td>
                                                        <td style="max-width:10%">
                                                            0
                                                        </td> 
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>
                            </div>             
                            </div>
                        </div>
                    <!-- PESAN KELUAR -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>