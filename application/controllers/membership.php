<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Membership extends MY_Controller {

	var $folder = "master_membership";
	var $table 	= "membership_type";

	public function __construct(){
		parent::__construct();
		$this->load->model('model_crud');
		$this->load->library('upload');
		$this->load->helper('my_helper');
	}

	function index(){
		// redirect('master_data/produk');
		$this->data['title']		= "Membership";
		$this->data['body']			= $this->folder."/view";
		$this->data['styles']		= "includes/styles_master";
		$this->data['scripts']		= "includes/scripts_master";
		$this->data['page_bar'] 	= "includes/template_site_bar";
		$this->data['desc']			= "Data Membership";
		$this->data['mMenuUtama']	= $this->mMenuUtama;
		$this->data['message'] 		= $this->session->flashdata('message'); 
		$this->load->view('includes/template_site', $this->data);
	}


	public function tambah(){
		if(isset($_POST['submit'])) {
			$nama 			= $this->input->post('nama');
			$kuota			= $this->input->post('kuota');
			$harga			= $this->input->post('harga');
			$data	= array('name'	=>	$nama,
							'quota'	=>	$kuota,
							'harga'	=>	$harga);
			$record = $this->db->select('name')
									->from($this->table)
									->where('name = ',$nama)
									->get();
			foreach($record->result() as $r) {
				if($r->name == $nama) {
					$this->session->set_flashdata('msg_failed','Nama Membership " '.$nama.' " telah digunakan.');
					redirect('membership');
				}
			}
			$this->model_crud->create_data($this->table,$data);
			$this->session->set_flashdata('msg_success',' Tambah Data Membership " '.$nama.' " berhasil.');
			redirect('master_data/membership');
		}
		else {
			$this->data['title']		= "Tambah Produk";
			$this->data['body']			= $this->folder."/form";
			$this->data['styles']		= "includes/styles_master";
			$this->data['scripts']		= "includes/scripts_master";
			$this->data['page_bar'] 	= "includes/template_site_bar";
			$this->data['desc']			= "tambah data produk";
			$this->data['mMenuUtama']	= $this->mMenuUtama; 
			$this->data['message'] 		= $this->session->flashdata('message'); 
			$this->load->view('includes/template_site', $this->data);
		}
	}

	public function ubah(){
		if(isset($_POST['submit'])) {
			$id 			= $this->input->post('id');
			$nama 			= $this->input->post('nama');
			$kuota			= $this->input->post('kuota');
			$harga			= $this->input->post('harga');
			$data	= array('name'	=>	$nama,
							'quota'	=>	$kuota,
							'harga'	=>	$harga);
			$this->model_crud->update_data($this->table,$data,'id',$id);
			$this->session->set_flashdata('msg_success',' Ubah data membership " '.$nama.' " berhasil.');
			redirect('master_data/membership');
		}
		else {
			$id					= $this->uri->segment(4);
			$data['record']		= $this->model_crud->get_one($this->table,'id',$id)->row_array();
			$data['title']		= "Ubah Produk";
			$data['body']		= $this->folder."/form";
			$data['styles']		= "includes/styles_master";
			$data['scripts']	= "includes/scripts_master";
			$data['page_bar'] 	= "includes/template_site_bar";
			$data['desc']		= "ubah data produk";
			$data['mMenuUtama']	= $this->mMenuUtama; 
			$data['message'] 	= $this->session->flashdata('message'); 
			$this->load->view('includes/template_site', $data);
		}
	}

	public function hapus(){
		$id = $this->uri->segment(4);
		delete_image($id);
		$this->model_crud->delete_data($this->table,'id',$id);
		$this->session->set_flashdata('msg_success','Data Membership berhasil dihapus.');
		redirect('master_data/membership');
	}

	public function json(){
		$this->datatables->select('id,name,harga,quota');
		$this->datatables->add_column('action',anchor('master_data/membership/ubah/$1','Edit',array('class'=>'btn btn-warning btn-xs')).'&nbsp'.anchor('master_data/membership/hapus/$1','Delete',array('class'=>'btn btn-danger btn-xs',"onclick"=>"return confirm('Anda yakin ingin menghapus?')")),'id');
		$this->datatables->from($this->table);
		return print_r($this->datatables->generate());
	}
}

/* End of file produk.php */
/* Location: ./application/controllers/produk.php */