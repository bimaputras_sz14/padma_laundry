<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berita extends MY_Controller {
 	
 	function __construct()
	{
		parent::__construct();
		$this->load->helper('image'); 
	}

	function index()
    {
		$this->data_berita();
	}
 
	
    function data_berita()
    {
		$this->data['title'] = "Berita & Pengumuman";
		$this->data['body'] = "berita/berita_view";
		$this->data['styles'] = "includes/styles_data_table";
		$this->data['scripts'] = "berita/scripts_berita";
		$this->data['page_bar'] = "includes/template_site_bar";
		$this->data['desc'] = "";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['message'] = $this->session->flashdata('message');
		
		$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
		$nrp = $arr_usr['nrp'];
		$level = $arr_usr['lvl_id'];
		
		if(($level <= 3)){ //administrator //admin mlantas // admin simobo
			$this->data['add_new'] = 1;
		}else{
			$this->data['add_new'] = 0;
		}
		
		$this->load->view('includes/template_site', $this->data);
	}
	
	
	
	
	
	
	
	function set_sel_satwil(){
        if('IS_AJAX') {
			$this->load->model('lantas_m');
		
			$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
			$nrp = $arr_usr['nrp'];
			$kode_satwil = $arr_usr['kode_satwil'];
			$sel = "";
			
			$query = $this->lantas_m->get_satwil($kode_satwil);
			
			if ($query->num_rows() > 0){
				foreach ($query->result() as $row){
					$sel .= "<option value='".$row->kode."'>".$row->nama."</option>";
				}
			}
			echo $sel;
        }
	}
	
	
function get_list_berita(){
		$this->load->model('master_m');
		
		$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
		$nrp = $arr_usr['nrp'];
		$lvl = $arr_usr['lvl_id'];
		$kode_satwil = $arr_usr['kode_satwil'];
		
		$berita = $this->master_m->get_list_berita($kode_satwil);
		$json['data'] = array();
		
		if ($berita->num_rows() > 0){
			$i = 1;
			foreach ($berita->result() as $row){
				$foto=$row->foto !=='' && $row->foto !==null?$row->foto:"default.png";
				$fancytitle = "<b>".$row->judul."</b><br><i>".substr($row->upd_time,0,16)." WIB</i><br><br>".$row->isi."<br><br>oleh:<br>".$row->pangkat." ".$row->nama_agt." ";
				$fancyimg = "<a class='fancybox' rel='gallery1' href='".$this->config->item('upload_dir')."berita/".$foto."' title='".$fancytitle."' data-rel='fancybox-button'><img width='80px' height='80px' src=".$this->config->item('upload_dir')."berita/".$foto."></a>";
				
				if($lvl == 1){
					$btn = "<button title='Edit' role='button' class='btn btn-sm blue' onClick='showFormUpdate(".$row->id.")'><i class=' icon-note'></i></button>";
				}else if( $lvl <= 3 ){
					$btn = "<button title='Edit' role='button' class='btn btn-sm blue' onClick='showFormUpdate(".$row->id.")'><i class=' icon-note'></i></button>";
				}else{
					$btn = "";
				}
				if(strlen($row->isi)  > 200)  {
					$info = substr($this->master_m->show_text(($row->isi)), 0, 200)." ...";
				}else{
					$info = $this->master_m->show_text($row->isi);
				}
				 
				$json['data'][] = array(
								"no"=>$i,
								"tgl"=>substr($row->upd_time,0,10)."<br>".substr($row->upd_time,11,5)." WIB",
								"foto"=>$fancyimg,
								"judul"=>$row->judul,
								"isi"=>$info, 
								"oleh"=>$row->pangkat." ".$row->nama_agt."",
								"btn"=>$btn
							);
				$i++;
			}
		}
		print(json_encode($json));
	}
	
	
	function get_list_penerima(){
        if('IS_AJAX') {
			$this->load->model('lantas_m');
			$id = $this->input->post('id');
			
			$satwil = array();
			
			$q0 = $this->lantas_m->get_satwil();
			if ($q0->num_rows() > 0){
				foreach ($q0->result() as $row){
					$satwil[$row->kode] = $row->nama;
				}
			}
			
			$q1 = $this->lantas_m->get_detil_berita($id);
			
			if ($q1->num_rows() > 0){
				$row = $q1->row();
				$arr_satwil = explode(',', $row->satwil_tujuan);
				
				$i = 1;
				$list = '<ul class="list-inline">';
				foreach ($arr_satwil as $val) {
					$list .= '<li>'.$i.'. '.$satwil[$val].'</li>';
					$i++;
				}
				$list .= '</ul>';
				
				echo $list;
			}else{
				echo "kosong";
			}
        }
	}
	
	
	
	function prepare_edit_berita(){
        if('IS_AJAX') {
			$this->load->model('lantas_m');
			$id = $this->input->post('id');
			// $id = 1;
		
			$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
			$nrp = $arr_usr['nrp'];
			$lvl = $arr_usr['lvl_id'];
			
			if($lvl == 1){
				$del = 1;
			}else{
				$del = 0;
			}
			
			$query = $this->lantas_m->get_detil_berita($id);
			
			if ($query->num_rows() > 0){
				$row = $query->row();
				
				if($row->all_satwil == '1'){
					$arr_satwil = "";
				}else{
					$arr_satwil = $row->satwil_tujuan;
				}
				
				// if($row->foto !=''){
				// 	$foto=$row->foto !=='' && $row->foto !==null?$row->foto:"default.png";
				// 	$gambar = get_foto_source('berita',$foto);
				// 	$foto_url = base_url()."uploads/berita/".$row->foto;
				// }else{
				// 	$foto_url = "";
				// }

				$foto=$row->foto !=='' && $row->foto !==null?$row->foto:"default.png";

				// $foto_url = get_foto_source('berita',$foto);
				
				$json = array(
								"id"=>$id,
								"judul"=>$row->judul,
								"isi"=>$row->isi,
								"foto"=>$this->config->item('upload_dir')."berita/".$foto,
								"satwil_tujuan"=>$arr_satwil,
								"del"=>$del
							);
				$res = array("status" => "success", "data" => $json);
			}else{
				$res = array("status" => "error", "msg" => "tidak ada data");
			}
			print(json_encode($res));
        }
	}
	
	
	
	function insert_beritax(){
		$this->load->model('lantas_m');
		$this->load->model('mobile_svc_m2');
		//
		$this->load->library('gcm');
		$this->load->helper('mobile_svc');
		//
		
		date_default_timezone_set('Asia/Jakarta');		
		$now = date('Y-m-d H:i:s');
		$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
		$nrp = $arr_usr['nrp'];
		$kode_satwil = $arr_usr['kode_satwil'];
		
		$nama_file="";
		$dir = $this->config->item('abs_upload_dir').'berita/';
		$res_upload = uploadPhoto($dir, $_FILES);
		if($res_upload['status']){
			$nama_file = $res_upload['filename'];
		}else{
			$data["status"]="error";
		}
		 
		
		$arr_satwil = $this->input->post('arr_satwil');
		$all_satwil = 0;
		
		if(($arr_satwil == "") || ($arr_satwil == null)){
			$query = $this->lantas_m->get_satwil($kode_satwil);
			$sat = array();
			if ($query->num_rows() > 0){
				foreach ($query->result() as $row){
					$sat[] = $row->kode;
				}
			}
			$arr_satwil = implode(",", $sat);
			$all_satwil = 1;
		}
		
		$judul = $this->input->post('judul');
		
		$data = array(
			'satwil_tujuan' => $arr_satwil,
			'judul' => $judul,
			'isi' => $this->input->post('uraian'),
			'foto' => $nama_file,
			'all_satwil' => $all_satwil,
			'upd_satwil' => $kode_satwil,
			'upd_time' => $now,
			'upd_nrp' => $nrp
		);

		if(!$id_berita = $this->lantas_m->insert_berita($data)){
			echo "error";
		}else{
			echo "success";
			/* =========================================================== GCM ============ */
			// Send to GCM 
			// Get GCM RegIDs of wilayah and Parent 
		 
			/* ================================================ end of GCM ================= */
		}
		
    }
	
	function insert_berita(){
		$this->load->model('master_m');  

		date_default_timezone_set('Asia/Jakarta');		
		$now = date('Y-m-d H:i:s');
		$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
		$nrp = $arr_usr['nrp'];
		$kode_satwil = $arr_usr['kode_satwil'];
		
		$nama_file="";
		$config['upload_path'] = './uploads/berita';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '2000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		
		$this->load->library('upload', $config);
		
		if(isset($_FILES['foto'])){
			if($_FILES['foto']['size'] > 0){
				$_FILE['foto']=str_replace('', '_', $_FILES['foto']['name'])."".time();
				if (!$this->upload->do_upload('foto')){
					$data["status"]="error";
					$data["error"]=$this->upload->display_errors();
				}else{
					$upload_data = $this->upload->data(); 
					$nama_file = $upload_data['file_name'];
				}
			}
		} 
		 
		$judul = $this->input->post('judul');
		
		$data = array( 
			'judul' => $judul,
			'isi' => $this->input->post('uraian'),
			'foto' => $nama_file, 
			'upd_satwil' => $kode_satwil,
			'upd_time' => $now,
			'upd_nrp' => $nrp
		);

		if(!$id_berita = $this->master_m->insert_berita($data)){
			echo "error";
		}else{
			echo "success";
		 
		}
		
    }
	
	 function update_berita(){
		$this->load->model('master_m');
	 
		
		date_default_timezone_set('Asia/Jakarta');		
		$now = date('Y-m-d H:i:s');
		$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
		$nrp = $arr_usr['nrp'];
		$kode_satwil = $arr_usr['kode_satwil'];
		
		$id_berita = $this->input->post('id');
		$foto_url = $this->input->post('foto_url');
		$foto_url_new = $this->input->post('foto_url_new');
		$judul = $this->input->post('judul');
		 
		$uraian = $this->input->post('uraian');
		$nama_file="";
		
		  
		
		$data = array(
			'judul' => $judul,
			'isi' => $uraian,
			'upd_satwil' => $kode_satwil,
			'upd_time' => $now,
			'upd_nrp' => $nrp
		);
		
		 
				$config['upload_path'] = './uploads/berita';
				$config['allowed_types'] = 'gif|jpg|png';
				// $config['max_size']	= '2000';
				// $config['max_width']  = '1024';
				// $config['max_height']  = '768';
				
				$this->load->library('upload', $config);
				
				if(isset($_FILES['foto'])){

					if($_FILES['foto']['size'] > 0){

						$_FILE['foto']=str_replace('', '_', $_FILES['foto']['name'])."".time();
						if (!$this->upload->do_upload('foto')){

							// $data["status"]="error";
							// $data["error"]=$this->upload->display_errors();
						}else{

							$upload_data = $this->upload->data(); 
							$nama_file = $upload_data['file_name'];
							$data['foto'] = $nama_file;
						}
					}
				}
			 
			 
		 else{
			$nama_file="";
		}
		

		if($this->master_m->update_berita($data, $id_berita)){
			echo "success";
			 
			/* ================================================ end of GCM ================= */
		}else{
			echo "error";
		}
		
    }
	
	
	
	function delete_berita(){
		$this->load->model('lantas_m');
		
		$id_berita = $this->input->post('id');
		
		$data = array(
			'status' => 0
		);
		
		if($this->lantas_m->update_berita($data, $id_berita)){
			echo "success";
		}else{
			echo "error";
		}
		
    }
	
	
	
	
	
	
	
	
	
	
	

}

/* End of file auth_admin.php */
/* Location: ./application/controllers/auth_admin.php */