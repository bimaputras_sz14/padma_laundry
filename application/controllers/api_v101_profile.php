<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_v101_profile extends CI_Controller {
 
	function __construct()
	{
		parent::__construct();
		//
		$this->load->model('Rikkes_v101_profile_m');
		$this->load->model('Rikkes_v101_svc_m');
		$this->load->helper('mobile_svc');
		$this->load->helper('words');
		$this->load->helper('image');
		
	}

	function index()
	{
	
	}
	
	function reset_password()
	{
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kode_akses = trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));
		$lat = trim($this->input->post('lat')) == '' ? null : trim($this->input->post('lat'));
		$lon = trim($this->input->post('lon')) == '' ? null : trim($this->input->post('lon'));
		$acc = trim($this->input->post('acc')) == '' ? null : trim($this->input->post('acc'));
		$passwordlama = trim($this->input->post('passwordlama')) == '' ? null : trim($this->input->post('passwordlama'));
		$passwordbaru = trim($this->input->post('passwordbaru')) == '' ? null : trim($this->input->post('passwordbaru'));
		//
		if($email == null || $kode_akses == null || $passwordlama == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akses tidak dikenali');
		}
		else
		{
			if(!$data = $this->Rikkes_v101_svc_m->do_login($email))
			{
				$res = array('result' => 'error', 'msg' => 'Email anda tidak terdaftar.', 'showmsg' => 'true');
			}
			else if($data->password != md5($passwordlama))
			{
				$res = array('result' => 'error', 'msg' => 'Password anda tidak tepat.', 'showmsg' => 'true');
			}
			else if($data->kode_akses != $kode_akses)
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else
			{
				switch($data->status)
				{
					case 1:
					
						$upd = array(
							'password' => md5($passwordbaru)
						);
						
						if(!$this->Rikkes_v101_profile_m->update_profile($data->email,$upd))
						{
							$res = array('result' => 'error', 'msg' => 'Tidak berhasil mereset password');
						}
						else
						{
							// update position
							if($lat != null && $lon != null)
							{
								$activity = 'RESET PASSWORD';
								$note = '<b>'.$data->nama.'</b> mengganti password';
								
								$pos = array(
									'email' => $data->email,
									'kode_wilayah' => $data->kode_wilayah,
									'waktu' => date('Y-m-d H:i:s'),
									'lat' => $lat,
									'lng' => $lon,
									'acc' => $acc,
									'aktivitas' => $activity,
									'pesan' => $note,
									'timestamp' => date('Y-m-d H:i:s')
								);
								$this->Rikkes_v101_svc_m->record_position($pos,$data->email);
							}
						}
						
						$res = array('result' => 'success', 'msg' => 'Password berhasil diubah');
					
					break;
					case 2:
						$res = array('result' => 'error', 'msg' => 'Akun Anda diblokir', 'showmsg' => 'true');
					break;
					case 3:
						$res = array('result' => 'error', 'msg' => 'Akun anda telah dihapus', 'showmsg' => 'true');
					break;
					case 0:
						$res = array('result' => 'error', 'msg' => 'Akun Anda belum di Aktivasi', 'showmsg' => 'true');
					break;
					default:
						$res = array('result' => 'error', 'msg' => 'Akun anda tidak dapat digunakan', 'showmsg' => 'true');
					break;
				}
				
			}
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	function edit_profile(){
		$lat = trim($this->input->post('lat')) == '' ? null : trim($this->input->post('lat'));
		$lon = trim($this->input->post('lon')) == '' ? null : trim($this->input->post('lon'));
		$acc = trim($this->input->post('acc')) == '' ? null : trim($this->input->post('acc'));
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kode_akses = trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));
		$activity = trim($this->input->post('activity')) == '' ? null : trim($this->input->post('activity'));
		$note = trim($this->input->post('note')) == '' ? null : trim($this->input->post('note'));
		$my_field = trim($this->input->post('my_field')) == '' ? null : trim($this->input->post('my_field'));
		$my_value = trim($this->input->post('my_value')) == '' ? null : trim($this->input->post('my_value'));
		
		//
		if($email == null || $kode_akses == null || $my_field == null || $my_value == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akses tidak dikenali');
		}
		else
		{
			if(!$data = $this->Rikkes_v101_profile_m->get_profile($email, $kode_akses))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else 
			{
				$upd = array(
					$my_field => $my_value
				);
				if(!$this->Rikkes_v101_profile_m->update_profile($data->id,$upd))
				{
					$res = array('result' => 'error', 'msg' => 'Data tidak berhasil disimpan');
				}
				else
				{
					// update position
					if($lat != null && $lon != null)
					{
						$pos = array(
							'email' => $data->email,
							'kode_wilayah' => $data->kode_wilayah,
							'waktu' => date('Y-m-d H:i:s'),
							'lat' => $lat,
							'lng' => $lon,
							'acc' => $acc,
							'aktivitas' => $activity,
							'pesan' => $note,
							'timestamp' => date('Y-m-d H:i:s')
						);
						$this->Rikkes_v101_svc_m->record_position($pos,$data->id);
					}
					
					$res = array('result' => 'success', 'msg' => 'Data berhasil disimpan');
				}
				// $res = array('result' => 'success', 'msg' => 'Akun dikenali', 'data' => $data);
			}
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	/* =========================== */
	
	function update_avatar()
	{
		$lat = trim($this->input->post('lat')) == '' ? null : trim($this->input->post('lat'));
		$lon = trim($this->input->post('lon')) == '' ? null : trim($this->input->post('lon'));
		$acc = trim($this->input->post('acc')) == '' ? null : trim($this->input->post('acc'));
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kode_akses = trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));
		$gambar = trim($this->input->post('gambar')) == '' ? null : trim($this->input->post('gambar'));
		
		if($email == null || $kode_akses == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
		}
		else
		{
			if(!$data = $this->Rikkes_v101_profile_m->get_profile($email, $kode_akses))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else
			{
				if($gambar == null)
				{
					$res = array('result' => 'error', 'msg' => 'Silahkan pilih gambar terlebih dahulu');
				}
				else
				{
					$dir = $this->config->item('abs_upload_dir').'profile/dokter/';
					$thumb_dir = $this->config->item('abs_upload_dir').'profile/dokter/thumb/';
					$fname = $data->id.'-'.time().'.jpg';
					// $fname = $data->id.'.jpg';
					//
					$gambar = str_replace("data:image/jpeg;base64,", "", $gambar);
					$gambar = str_replace("data:image/png;base64,", "", $gambar);
					$gambar = str_replace("[removed]", "", $gambar);
					$gambar	= trim($gambar);
					//$gambar = str_replace(' ', '+', $gambar);
					$gbrdt = base64_decode($gambar);						
					//
					if(file_put_contents($dir.$fname, $gbrdt))
					{
						$upd = array(
							'foto' => $fname
						);
						if(!$this->Rikkes_v101_profile_m->update_profile($data->email,$upd))
						{
							$res = array('result' => 'error', 'msg' => 'Tidak berhasil mengupload gambar');
						}
						else
						{
							$this->load->library('image_lib');
							$config['image_library'] = 'gd2';
							$config['source_image'] = $dir.$fname;       
							$config['create_thumb'] = TRUE;
							$config['maintain_ratio'] = FALSE;
							$config['thumb_marker'] = '';
							$config['width'] = 120;
							$config['height'] = 120;
							$config['overwrite'] = TRUE;
							$config['new_image'] = $thumb_dir.$fname;               
							$this->image_lib->initialize($config);
							//
							if($this->image_lib->resize())
							{ 
								//
								$upd = array(
									'avatar' => $fname
								);
								//
								$this->Rikkes_v101_profile_m->update_profile($data->email,$upd);
							}					
							$this->image_lib->clear();
							//
							
							// delete old file
							$abs_dir_photo = $this->config->item('abs_upload_dir').'profile/dokter/';
							if(file_exists($abs_dir_photo.$data->foto)){
								unlink($abs_dir_photo.$data->foto);
							}
							$abs_dir_thumb = $this->config->item('abs_upload_dir').'profile/dokter/thumb/';
							if(file_exists($abs_dir_thumb.$data->avatar)){
								unlink($abs_dir_thumb.$data->avatar);
							}
							
							// update position
							if($lat != null && $lon != null)
							{
								$activity = 'RESET AVATAR';
								$note = '<b>'.$data->nama.'</b> mengganti foto profil';
								
								$pos = array(
									'email' => $data->email,
									'kode_wilayah' => $data->kode_wilayah,
									'waktu' => date('Y-m-d H:i:s'),
									'lat' => $lat,
									'lng' => $lon,
									'acc' => $acc,
									'aktivitas' => $activity,
									'pesan' => $note,
									'timestamp' => date('Y-m-d H:i:s')
								);
								$this->Rikkes_v101_svc_m->record_position($pos,$data->email);
							}
						
							$ava = $this->Rikkes_v101_svc_m->get_my_avatar($data->email, true);
							$res = array('result' => 'success', 'msg' => 'Avatar berhasil diubah', 'ava' => $ava);
						}
						
					}
					else
					{
						$res = array('result' => 'error', 'msg' => 'Tidak berhasil mengupload gambar');
					}
					//
				}
			}
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	/* ================================ */
	
	function save_ttd()
	{
		$lat = trim($this->input->post('lat')) == '' ? null : trim($this->input->post('lat'));
		$lon = trim($this->input->post('lon')) == '' ? null : trim($this->input->post('lon'));
		$acc = trim($this->input->post('acc')) == '' ? null : trim($this->input->post('acc'));
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kode_akses = trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));
		$ttd = trim($this->input->post('ttd')) == '' ? null : trim($this->input->post('ttd'));
		
		if($email == null || $kode_akses == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
		}
		else
		{
			if(!$data = $this->Rikkes_v101_profile_m->get_profile($email, $kode_akses))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else
			{
				if($ttd == null)
				{
					$res = array('result' => 'error', 'msg' => 'Silahkan isi tanda tangan terlebih dahulu');
				}
				else
				{
					//===============UPLOAD TTD=================
					$dir = $this->config->item('abs_upload_dir').'profile/dokter/ttd/';
					$fnameJPG = uniqid().'.jpg';
					$fnamePNG = uniqid().'.png';
					
					if (strpos($ttd, 'data:image/png;base64') !== false) {
						$isPNG = true;
					}else{
						$isPNG = false;
					}
					
					$ttd = str_replace("data:image/jpeg;base64,", "", $ttd);
					$ttd = str_replace("data:image/png;base64,", "", $ttd);
					$ttd = str_replace("[removed]", "", $ttd);
					$ttd	= trim($ttd);
					
					$gbrdt = base64_decode($ttd);
					
					if($isPNG){
						$fname = $fnamePNG;
					}else{
						$fname = $fnameJPG;
					}
					
					if(file_put_contents($dir.$fname, $gbrdt))
					{
						$upd = array(
							'ttd' => $fname
						);
						if(!$this->Rikkes_v101_profile_m->update_dokter($data->email,$upd))
						{
							$res = array('result' => 'error', 'msg' => 'Tidak berhasil mengupload tanda tangan');
						}
						else
						{
							if($isPNG){
								$this->png2jpg($dir.$fnamePNG, $dir.$fnameJPG, 100);
							}
							
							// delete old file
							if($data->ttd != null && $data->ttd != ''){
								$abs_dir_photo = $this->config->item('abs_upload_dir').'profile/dokter/ttd/';
								if(file_exists($abs_dir_photo.$data->ttd)){
									unlink($abs_dir_photo.$data->ttd);
								}
							}
							
							// update position
							if($lat != null && $lon != null)
							{
								$activity = 'SET TANDATANGAN';
								$note = '<b>'.$data->nama.'</b> mengganti tanda tangan';
								
								$pos = array(
									'email' => $data->email,
									'kode_wilayah' => $data->kode_wilayah,
									'waktu' => date('Y-m-d H:i:s'),
									'lat' => $lat,
									'lng' => $lon,
									'acc' => $acc,
									'aktivitas' => $activity,
									'pesan' => $note,
									'timestamp' => date('Y-m-d H:i:s')
								);
								$this->Rikkes_v101_svc_m->record_position($pos,$data->email);
							}
						
							$res = array('result' => 'success', 'msg' => 'Tanda tangan berhasil disimpan');
						}
					}
					else
					{
						$res = array('result' => 'error', 'msg' => 'Tidak berhasil mengupload tanda tangan');
					}
					
				}
			}
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	/* =========================== */
	
	function get_myttd(){
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kode_akses = trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));
		
		// $email = 'bandung.wijaya@gmail.com';
		// $kode_akses = '7bb705add10ea0ef5d2bc4fdef6c9f9a';
		
		if($email == null || $kode_akses == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akses tidak dikenali');
		}
		else
		{
			if(!$data = $this->Rikkes_v101_profile_m->get_profile($email, $kode_akses))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else 
			{
				if($data->status == 1){
				
					if(!$qry = $this->Rikkes_v101_profile_m->get_myttd($email, $kode_akses)){
						$res = array('result' => 'error', 'msg' => 'Data tidak ditemukan');
					}else{
						$res = array('result' => 'success', 'msg' => 'data ditemukan', 'qry' => $qry);
					}
					
				}else{
					$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
				}
				
			}
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
}
