<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master extends MY_Controller {
 
	function __construct()
	{
		parent::__construct();
		$this->load->helper('wilayah'); 
		$this->load->model('master_model');
	} 
	
	function personil()
    {
		$this->load->helper('personil');
		$this->load->model("jabatan_model");
		$wilayah = null;
		$this->data['title']		= "Personil";
		$this->data['body']			= "personil/index";
		$this->data['styles']		= "includes/styles_data_table";
		$this->data['scripts']		= "personil/scripts_personil";
		$this->data['page_bar'] 	= "includes/template_site_bar";
		$this->data['desc']			= "data personil";
		$this->data['mMenuUtama']	= $this->mMenuUtama;
		$this->data['level'] 		= $this->jabatan_model->get_level();  
		$this->data['jabatan'] 		= $this->jabatan_model->get_jabatan_list();
		$this->data['message'] 		= $this->session->flashdata('message');
		$this->load->view('includes/template_site', $this->data);
	}
	
	

	 
}

?>