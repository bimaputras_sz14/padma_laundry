<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends MY_Controller {
 
	function __construct()
	{
		parent::__construct();
		 
		$this->load->model('penjualan_model'); 
		// $this->load->model('produk_model'); 
		$this->load->model('pelanggan_model');  
		// $this->load->model('master_model');
		// $this->load->library('cart');
	}

	// menu untuk tampilan jika login sebagai kasir
	function index()
    { 
		$this->data['title']		= "Order";
		$this->data['body']			= "support/penjualan";
		$this->data['styles']		= "includes/styles_data_table";
		$this->data['scripts']		= "support/scripts_penjualan";
		$this->data['page_bar'] 	= "includes/template_site_bar";
		$this->data['desc']			= "data order";
		$this->data['mMenuUtama']	= $this->mMenuUtama; 
		$this->data['message'] 		= $this->session->flashdata('message'); 
		$this->load->view('includes/template_site', $this->data);
	} 

	function store_cart(){
		$id_product = $this->input->post('id_product');
		$row 		= $this->produk_model->get_produk_detail($id_product);
		$qty 		= 1; 
		$data 		= 	array(
		               		'id'    => $id_product,
		               		'qty'   => $qty , 
		               		'price' => $row->harga,
            				'name'  => $row->nama_produk,
		            	);
		if($this->cart->contents()){
			
			$this->cart->update($data); 
			foreach ($this->cart->contents() as $item) { 
		        if ( $item['id'] == $id_product ) {  
		            $data = array('rowid'=>$item['rowid'],'qty'=>++$item['qty']);
		            $this->cart->update($data);  
		        } else{
		        	$this->cart->insert($data);
		        }
		    }
		}else{ 
			$this->cart->insert($data);
		}
		
		
		$alert['status']="success";
		$alert['result']="Berhasil"; 
		echo json_encode($alert);
	}

	function delete_cart(){
		$id_product = $this->input->post('id_product'); 
		$qty 		= 1; 
		$data 		= 	array(
		               		'rowid'    => $id_product,
		               		'qty'   =>0 
		            	);
		
		$this->cart->update($data);
		
		$alert['status']="success";
		$alert['result']="Berhasil"; 
		echo json_encode($alert);
	}

	function load_cart(){  
		foreach ($this->cart->contents() as $row) {
			echo '<tr><td>'.$row['name'].'</td>
	                <td>'.$row['qty'].'</td>
	                <td>Rp '.number_format( $row['price'] ,0,'','.').'</td>
	                <td><i class="fa fa-times-circle" aria-hidden="true" onclick="delete_cart(\''.$row['rowid'].'\')"></i></td>
	            </tr> ';
		}
		
	}

	function load_total(){  
		$total = 0 ; 
		foreach ($this->cart->contents() as $row) {
			$total = $total + ( $row['price'] * $row['qty']); 
		}

		echo 'Rp '.number_format( $total,0,'','.');
		
	}

	function destroy_cart(){
		$this->cart->destroy(); 
		$data['status']="success";
		$data['result']="Berhasil"; 
		echo json_encode($data); 
	}

	function checkout(){
		 
		// 
		$data['status']="sukses";
		$data['error']="";
		$validation_rules = array(
			array('field' => 'total_bayar', 'label' => 'Total Pembayaran','rules' => 'required'),
			array('field' => 'tipe_payment', 'label' => 'Jenis Pembayaran','rules' => 'required') 
		);
		$this->form_validation->set_rules($validation_rules);
		
		if(!$this->form_validation->run()){
				$data["status"]="error";
				$data["error"]=validation_errors();				
		}
 
		if($data["status"]=="sukses"){ 
			$no_penjualan 	    = date('YmdHi');
			$tanggal_penjualan 	= date('Y-m-d H:i:s');	
			$pelanggan_id 	    = $this->input->post('member_id');		
			$tipe_pembayaran    = $this->input->post('tipe_payment');
			$total_pembayaran   = $this->input->post('total_bayar');
			$personil_id        = 1;
			$data = array(
				'no_penjualan' 			=>$no_penjualan,
				'tanggal_penjualan' 	=>$tanggal_penjualan,	
				'pelanggan_id' 			=>$pelanggan_id,
				'tipe_pembayaran' 		=>$tipe_pembayaran,
				'total_penjualan' 		=>$this->cart->total(),
				'total_pembayaran' 		=>$total_pembayaran,
				'personil_id' 			=>$personil_id, 
			);
  
			$id_data = $this->penjualan_model->add_penjualan_barang($data);
	 
			if(!$id_data)
			{	 
				$data["status"]="error";
				$data["error"]="<p>error!</p>";	
			}else{ 
				foreach ($this->cart->contents() as $row) {  
					$data = array(
						'penjualan_id' 	=>$id_data, 
						'produk_id' 	=>$row['id'],
						'harga' 		=>$row['price'],
						'qty' 			=>$row['qty'],
						'total' 		=>$row['price'] * $row['qty'],
						 
					); 
					$id_data_det = $this->penjualan_model->add_penjualan_barang_det($data);
					$produk_det = $this->produk_model->get_produk_detail($row['id']);
					if($produk_det->kategori_produk_id!=4){
						$data = array(
							'pelanggan_id' 	=>$pelanggan_id,  
							'kelas_id' 		=>$produk_det->kelas_id,
							'total' 		=>$produk_det->total_token, 
							'tipe_transaksi'=>'in'  
						); 
						$id_data_pel = $this->pelanggan_model->add_pelanggan_transaksi_token($data);
					}
				}
				$data['id_sales'] = $id_data ;
				$this->cart->destroy();
			}
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($data); 
	}
	
	// module penjualan start
	function get_penjualan(){
		$json=array(); 
		$json['data'] = array();
		$data = $this->penjualan_model->get_penjualan();
		if ($data->num_rows() > 0){
			$i = 1;
				foreach ($data->result() as $row){
					$btn='<button title="Edit Data" onclick="action(\'edit\',\''. $row->id .'\')" class="btn btn-edit btn-sm blue"><i class="icon-note"></i></button>';
					$btn.='<button onclick="hapus(\''. $row->id .'\');"  title="Hapus Data" role="button" class="btn btn-sm red"><i class="icon-trash"></i></button>'; 

					$btn = "<div class='btn-group'>
                                <a href='".site_url('order/manage/').'/'.$row->id."' class='btn btn-white btn-xs'>Ubah</a>
                                <button class='btn btn-danger btn-xs' onclick='do_delete(".$row->id.");'>Hapus</button>
                            </div>"; 

					$json['data'][] = array(
										"no"=>'<center>'. $i .'</center>',
										"no_penjualan"=>$row->no_penjualan, 
										"tanggal_penjualan"=>$row->tanggal_penjualan, 
										"berat_timbangan"=>$row->berat_timbangan, 
										"pelanggan"=>$row->nama_pelanggan, 
										"total_penjualan"=>"<div class='text-right'>".  number_format($row->total_penjualan)."</div>", 
										"btn"=>$btn
									);
					$i++;
				}
		}
		echo json_encode($json);
	}

	function form_penjualan($module=null,$id=null){   
		$this->data['user_login']   = $this->get_user_session();  
		$this->data['pelanggan'] 	= $this->pelanggan_model->get_pelanggan()->result_array();
		$this->data['lokasi'] 		= $this->lokasi_model->get_lokasi()->result_array();
		$this->data['tipe'] 		= $this->penjualan_model->get_tipe_sales();
		if($module=='edit'){
			$this->data['info'] 	= $this->penjualan_model->get_penjualan($id);  
		}  
		$this->data['module']		= $module;
		$this->load->view('support/include/detail_penjualan', $this->data);
	}
	
	// function insert_penjualan(){ 
	// 	$data['status']="sukses";
	// 	$data['result']=""; 
	// 	$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
	// 	$nik = $arr_usr['nik']; 
	// 	$validation_rules = array( 
	// 		array('field' => 'date_sales', 'label' => 'Tanggal Penjualan','rules' => 'required'),
	// 		array('field' => 'id_pel', 'label' => 'Tenant','rules' => 'required'),
	// 		array('field' => 'start_date', 'label' => 'Tgl Mulai Kontrak','rules' => 'required'),
	// 		array('field' => 'end_date', 'label' => 'Tgl Selesai Kontrak','rules' => 'required'),
	// 		array('field' => 'id_item', 'label' => 'Item','rules' => 'required'),
	// 		array('field' => 'tipe_sales', 'label' => 'Jenis Penjualan','rules' => 'required') 
	// 	);

	// 	$this->form_validation->set_rules($validation_rules);
		
	// 	if(!$this->form_validation->run()){
	// 			$data["status"]="error";
	// 			$data["error"]=validation_errors();				
	// 	}
		
	// 	if($data["status"]=="sukses"){

	// 		$total = $this->input->post('price') - $this->input->post('diskon')  ;
			 
	// 		$pelanggan_id  		= $this->input->post('id_pel');
	// 		$no_nota 			= $this->input->post('nota');
	// 		$tanggal 			= $this->input->post('date_sales');
	// 		$tanggal_mulai 		= $this->input->post('start_date');
	// 		$tanggal_akhir	 	= $this->input->post('end_date');
	// 		$tipe_penjualan_id 	= $this->input->post('tipe_sales');
	// 		$id_lokasi 			= $this->input->post('id_item');
	// 		$harga 				= $this->input->post('price'); 
	// 		$total_diskon 		= $this->input->post('diskon'); 
	// 		$total_bayar 		= $this->input->post('tot_bayar');
	// 		$catatan 			= $this->input->post('description'); 
	// 		$data = array(
	// 			'pelanggan_id'		=>$pelanggan_id,
	// 			'no_nota'			=>$no_nota,
	// 			'tanggal'			=>$tanggal,
	// 			'tanggal_mulai'		=>$tanggal_mulai,
	// 			'tanggal_akhir'		=>$tanggal_akhir,
	// 			'tipe_penjualan_id'	=>$tipe_penjualan_id,
	// 			'id_lokasi'			=>$id_lokasi,
	// 			'harga'				=>$harga,
	// 			'total_penjualan'	=>$total,
	// 			'total_diskon'		=>$total_diskon,
	// 			'total_bayar'		=>$total_bayar,
	// 			'catatan'			=>$catatan,
	// 			'timestamp' 		=> date('Y-m-d H:i:s'),
	// 			'upd' 				=> $nik 
	// 		);
			
	// 		$id_penjualan = $this->penjualan_model->add_penjualan_barang($data);
	// 		if(!$id_penjualan)
	// 		{
	// 			$data["status"]="error";
	// 			$data["error"] ="<p>Kode barang sudah ada!</p>";	
	// 		}else{ 
				 
	// 		}
	// 	}

	// 	header('Cache-Control: no-cache, must-revalidate');
	// 	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	// 	header('Content-type: application/json');
	// 	echo json_encode($data); 
	// }

	// function update_penjualan(){
	// 	$data['status']	="sukses";
	// 	$data['error']	=""; 
		
	// 	$arr_usr 	= $this->flexi_auth->get_user_by_identity_row_array();
	// 	$nik 		= $arr_usr['nik'];
		 
	// 	$validation_rules = array(
	// 		array('field' => 'date_sales', 'label' => 'Tanggal Penjualan','rules' => 'required'),
	// 		array('field' => 'id_pel', 'label' => 'Tenant','rules' => 'required'),
	// 		array('field' => 'start_date', 'label' => 'Tgl Mulai Kontrak','rules' => 'required'),
	// 		array('field' => 'end_date', 'label' => 'Tgl Selesai Kontrak','rules' => 'required'),
	// 		array('field' => 'id_item', 'label' => 'Item','rules' => 'required'),
	// 		array('field' => 'tipe_sales', 'label' => 'Jenis Penjualan','rules' => 'required') 
	// 	);
	// 	$this->form_validation->set_rules($validation_rules);

	// 	if(!$this->form_validation->run()){
	// 			$data["status"]="error";
	// 			$data["error"]=validation_errors();				
	// 	}
		   
	// 	if($data["status"]=="sukses"){
	// 		$total = $this->input->post('price') - $this->input->post('diskon')  ;
	// 		$id 				= $this->input->post('id');
	// 		$pelanggan_id  		= $this->input->post('id_pel');
	// 		$no_nota 			= $this->input->post('nota');
	// 		$tanggal 			= $this->input->post('date_sales');
	// 		$tanggal_mulai 		= $this->input->post('start_date');
	// 		$tanggal_akhir	 	= $this->input->post('end_date');
	// 		$tipe_penjualan_id 	= $this->input->post('tipe_sales');
	// 		$id_lokasi 			= $this->input->post('id_item');
	// 		$harga 				= $this->input->post('price'); 
	// 		$total_diskon 		= $this->input->post('diskon'); 
	// 		$total_bayar 		= $this->input->post('tot_bayar');
	// 		$catatan 			= $this->input->post('description');
		 
	// 		$data = array(
	// 			'pelanggan_id'		=>$pelanggan_id,
	// 			'no_nota'			=>$no_nota,
	// 			'tanggal'			=>$tanggal,
	// 			'tanggal_mulai'		=>$tanggal_mulai,
	// 			'tanggal_akhir'		=>$tanggal_akhir,
	// 			'tipe_penjualan_id'	=>$tipe_penjualan_id,
	// 			'id_lokasi'			=>$id_lokasi,
	// 			'harga'				=>$harga,
	// 			'total_penjualan'	=>$total,
	// 			'total_diskon'		=>$total_diskon,
	// 			'total_bayar'		=>$total_bayar,
	// 			'catatan'			=>$catatan,
	// 			'timestamp' 		=> date('Y-m-d H:i:s'),
	// 			'upd' 				=> $nik 
	// 		);
			 

	// 		$id_pembelian = $this->penjualan_model->update_penjualan($id,$data);

	// 		if(!$id_pembelian)
	// 		{
	// 			$data["status"]="error";
	// 			$data["error"]="<p>Kode barang sudah ada!</p>";	
	// 		}else{
			   
	// 		}
	// 	}

	// 	header('Cache-Control: no-cache, must-revalidate');
	// 	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
	// 	header('Content-type: application/json');
	// 	echo json_encode($data);
	// }
	
	function delete_penjualan($id){
		$res = array(
					'result' => 'success',
					'msg' => '<h4>Data berhasil dihapus</h4>'
				);
		if($id!=null){
			if(!$this->penjualan_model->delete_penjualan($id))
			{
				$res = array(
					'result' => 'error',
					'msg' => '<h4>Tidak berhasil menghapus data</h4>'
				);
			}
			else
			{	 
				$res = array(
					'result' => 'success',
					'msg' => '<h4>Data berhasil dihapus</h4>'
				);
			}
		}
	}
	//module penjualan end

	function penjualan_token(){ 	
		$this->load->model('pelanggan_model'); 
		$this->load->model('produk_model');  
		$this->data['title']		= "Pembelian Token";
		$this->data['body']			= "penjualan/produk";
		$this->data['styles']		= "includes/styles_data_table";
		$this->data['scripts']		= "penjualan/scripts_penjualan";
		$this->data['page_bar'] 	= "includes/template_site_bar";
		$this->data['desc']			= "Pembelian Token";
		$this->data['mMenuUtama']	= $this->mMenuUtama; 
		$this->data['membership'] 	= $this->master_model->membership_type();
		$this->data['message'] 		= $this->session->flashdata('message');  
		$this->data['produk'] 		= $this->produk_model->get_produk();
		$this->data['member'] 		= $this->pelanggan_model->get_pelanggan_detail($this->uri->segment(3));
		$this->data['member_id']    = $this->uri->segment(3) ; 

		$this->load->view('includes/template_site', $this->data);
	}

	function invoice(){ 	 
		$this->data['title']		= "Pembelian Token";
		$this->data['body']			= "penjualan/invoice";
		$this->data['styles']		= "includes/styles_data_table";
		$this->data['scripts']		= "pelanggan/scripts_pelanggan";
		$this->data['page_bar'] 	= "includes/template_site_bar";
		$this->data['desc']			= "Invoice";
		$this->data['mMenuUtama']	= $this->mMenuUtama; 
		$this->data['membership'] 	= $this->master_model->membership_type();
		$this->data['message'] 		= $this->session->flashdata('message'); 
		$this->load->view('includes/template_site', $this->data);
	}

	function SetDash($black=null, $white=null)
    {
        if($black!==null)
            $s=sprintf('[%.3F %.3F] 0 d',$black*$this->k,$white*$this->k);
        else
            $s='[] 0 d';
        $this->_out($s);
    }

	function generate_struk()
    {
    	$this->load->helper('words');
        $id 	= 1;        
        $bayar 	= $this->input->post('bayar');        

        // $result = $this->trx_model->get_trx_by_id($id);
        
        	$filenamepdf = 'Invoice.pdf'; 
            $this->load->library('fpdf_struk');
            $this->fpdf->SetMargins(0,0,0);

            // Header
            $this->fpdf->SetFont('Courier','B',10);
            $this->fpdf->Ln(5);
            $this->fpdf->Cell(18);
            $this->fpdf->Cell(24,1,"RAI GYM",0,1,'C');
            $this->fpdf->Ln(3);
            $this->fpdf->Cell(18);
            $this->fpdf->SetFont('Courier','',9);
            $this->fpdf->Cell(24,1,"Jalan Cihapit No. 33",0,1,'C');
            // $this->fpdf->Ln(2);
            // $this->fpdf->Cell(18);
            // $this->fpdf->SetFont('Courier','',9);
            // $this->fpdf->Cell(24,1,"NPWP : ",0,1,'C');
            // $this->fpdf->Ln(2);
            $this->fpdf->SetFont('Arial','',8);
            $this->fpdf->Cell(18);
            // $this->fpdf->Cell(23,5,"Alamat Rai",0,1,'C');
            $this->fpdf->Ln(3); 

            $this->fpdf->SetFont('Courier','',8);
            $this->fpdf->Cell(2);
            $this->fpdf->Cell(16,4, 'Kasir',0,0);
            $this->fpdf->Cell(3,4,':',0,0);
            $this->fpdf->SetFont('Courier','',8);
            $this->fpdf->Cell(10,4,''. 'Kasir',0,0, 'L');
            $this->fpdf->Ln();
 
            $sales = $this->penjualan_model->get_penjualan_detail($this->uri->segment(3));
           
            
            $member = $this->pelanggan_model->get_pelanggan_detail($sales->pelanggan_id);
            $this->fpdf->SetFont('Courier','',8);
            $this->fpdf->Cell(2);
            $this->fpdf->Cell(16,4, 'Member',0,0);
            $this->fpdf->Cell(3,4,':',0,0);
            $this->fpdf->SetFont('Courier','',8);
            $this->fpdf->Cell(20,4,$member->nama_pelanggan,0,0,'L');
            $this->fpdf->Ln();
           
            $this->fpdf->SetFont('Courier','',8);
            $this->fpdf->Cell(2);
            $this->fpdf->Cell(16,4, 'No Member',0,0);
            $this->fpdf->Cell(3,4,':',0,0);
            $this->fpdf->SetFont('Courier','',8);
            $this->fpdf->Cell(20,4,$member->no_kartu,0,0,'L');
            $this->fpdf->Ln();

            $this->fpdf->SetFont('Courier','',8);
            $this->fpdf->Cell(2);
            $this->fpdf->Cell(16,4, 'No Nota',0,0);
            $this->fpdf->Cell(3,4,':',0,0);
            $this->fpdf->SetFont('Courier','',8);
            $this->fpdf->Cell(20,4,$sales->no_penjualan.$sales->id,0,0,'L');
            $this->fpdf->Ln();  
          $header = array("Item", "Qty",   "Total");
 		  $w = array(30, 5, 18 );
 		   $this->fpdf->Cell(3);
		  for($i = 0; $i < count($header); $i++) {
		   // $this->fpdf->Cell($w[$i], 7, $header[$i], 0, 0, 'C');
		  }
		  $this->fpdf->Ln();
		  // Mark start coords
		  $x = $this->fpdf->GetX();
		  $y = $this->fpdf->GetY();
		  $i = 0;
		  $this->fpdf->Ln(2);
		  $this->fpdf->SetLineWidth(0.1);
		  $this->fpdf->SetDash(3,2); //5mm on, 5mm off
		  $this->fpdf->Line($x+2,$y+2,55,$y+2);
		   $this->fpdf->Ln(2);
		  $sales_item = $this->penjualan_model->get_penjualan_item_detail($this->uri->segment(3));
		 
          foreach ($sales_item as $row) { 
          		$produk = $this->produk_model->get_produk_detail($row->produk_id);
		        $y1 = $this->fpdf->GetY();
	           	$this->fpdf->Cell(3);
			   	$this->fpdf->MultiCell($w[0], 4, $produk->nama_produk, ''); 
			   	$y2 = $this->fpdf->GetY();
			   	$yH = $y2 - $y1 ; 
			   	$this->fpdf->SetXY(($x+3) + $w[0], $this->fpdf->GetY() - $yH); 
			   	$this->fpdf->Cell($w[1], $yH, $row->qty, '', 'R');
			   	$this->fpdf->Cell($w[2], $yH, number_format( $row->harga ,0,'','.'), '', 0, 'R'); 
			      
			   $this->fpdf->Ln();

			}
			$this->fpdf->Ln(1);
			 $x = $this->fpdf->GetX();
		  	$y = $this->fpdf->GetY();
			$this->fpdf->Line($x+2,$y+2,55,$y+2);
            // repeat 
            // AMOUNT
            $this->fpdf->Ln(3);

            $this->fpdf->SetFont('Courier','',8);
            $this->fpdf->Cell(2);
            $this->fpdf->Cell(13,4, 'Total Sales',0,0); 
            $this->fpdf->SetFont('Courier','',9);
            $this->fpdf->Cell(41,4,  number_format($sales->total_penjualan, 0, ',', '.'),0,0, 'R');
            $this->fpdf->Ln();

             // AMOUNT
            $this->fpdf->SetFont('Courier','',8);
            $this->fpdf->Cell(2);
            $this->fpdf->Cell(13,4, 'Total Bayar',0,0); 
            $this->fpdf->SetFont('Courier','',9);
            $this->fpdf->Cell(41,4,  number_format( $sales->total_pembayaran, 0, ',', '.'),0,0, 'R');
            $this->fpdf->Ln();
            $this->fpdf->SetFont('Courier','',8);

            $this->fpdf->Cell(2);
            $this->fpdf->Cell(13,4, 'Total Kembali',0,0); 
            $this->fpdf->SetFont('Courier','',9);
            $this->fpdf->Cell(41,4, number_format($sales->total_penjualan - $sales->total_pembayaran , 0, ',', '.'),0,0, 'R');
            $this->fpdf->Ln(10);
 
            
             
            // Footer
            $this->fpdf->SetFont('Courier','B',7);
            $this->fpdf->ln(2);
            $this->fpdf->Cell(14);
            $this->fpdf->Cell(32,2,"Build Body Strong",0,1,'C'); 
            $this->fpdf->ln();
            $this->fpdf->SetFont('Courier','',7);
            $this->fpdf->Cell(14);
            $this->fpdf->Cell(32,2,convertTanggal(date('Y-m-d'), true). ' ' . date('H:i:s' ),0,1,'C');
            $this->fpdf->SetFont('Courier','',7);
           
            // $this->fpdf->Cell(14);
            // $this->fpdf->Cell(32,2,"*** Terima Kasih ***",0,1,'C'); 
            $this->fpdf->Output(); 

         
    }
  
}

?>