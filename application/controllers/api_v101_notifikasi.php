<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_v101_notifikasi extends CI_Controller {
 
	function __construct()
	{
		parent::__construct();
		//
		$this->load->model('Api_v101_profile_m');
		$this->load->model('Api_v101_notifikasi_m');
		$this->load->model('Api_v101_svc_m');
		$this->load->helper('mobile_svc');
		$this->load->helper('words');
		
	}

	function index()
	{
	
	}
	
	/* =========================== */
	
	function get_list_notifikasi(){
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kode_akses = trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));
		$timestamp = trim($this->input->post('timestamp')) == '' ? null : trim($this->input->post('timestamp'));
		$direction = trim($this->input->post('direction')) == '' ? 'newer' : trim($this->input->post('direction'));
		$limit = trim($this->input->post('limit')) == '' ? 5 : trim($this->input->post('limit'));
		
		// $email = 'bandung.wijaya@gmail.com';
		// $kode_akses = 'cb470246fb965cad4e349b61114ae1f6';
		
		if($email == null || $kode_akses == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
		}
		else
		{
			if(!$data = $this->Api_v101_profile_m->get_profile($email, $kode_akses))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else 
			{
				if($data->status == 1){
				
					if(!$notif = $this->Api_v101_notifikasi_m->get_list_notifikasi($email,$timestamp,$limit,$direction)){
						$res = array('result' => 'error', 'msg' => 'Belum ada notifikasi');
					}else{
						$res = array('result' => 'success', 'info' => $notif);
					}
					
				}else{
					$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
				}
				
			}
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	/* =========================== */
	
	function get_jml_unread_notif(){
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kode_akses = trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));
		
		// $email = 'bandung.wijaya@gmail.com';
		// $kode_akses = 'cb470246fb965cad4e349b61114ae1f6';
		
		if($email == null || $kode_akses == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
		}
		else
		{
			if(!$data = $this->Api_v101_profile_m->get_profile($email, $kode_akses))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else 
			{
				//
				if($data->status == 1){
					if(!$notif = $this->Api_v101_notifikasi_m->get_jml_unread_notif($email)){
						$notif = '';
					}else{
						$notif = '<span class="badge bg-red">'.$notif.'</span>';
					}
					$res = array('result' => 'success', 'notif' => $notif);
				}else{
					$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
				}
				
			}
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	/* ================================= */
	
	function get_notifikasi_detail()
	{
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kode_akses = trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));
		$id = trim($this->input->post('id')) == '' ? null : trim($this->input->post('id'));
		
		// $email = 'bandung.wijaya@gmail.com';
		// $kode_akses = 'cb470246fb965cad4e349b61114ae1f6';
		// $id = '1';
		
		if($email == null || $kode_akses == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
		}
		else
		{
			if(!$data = $this->Api_v101_profile_m->get_profile($email, $kode_akses))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak terdaftar dalam data penelusur', 'forcelogout' => 'true');
			}
			else 
			{
				//
				if($data->status == 1){
				
					if(!$data = $this->Api_v101_notifikasi_m->get_notifikasi_detail($id))
					{
						$res = array('result' => 'error', 'msg' => 'Informasi tidak tersedia');
					}
					else
					{
						$res = array('result' => 'success', 'data' => $data);
					}
					
				}else{
					$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
				}
				
			}
			
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
}
