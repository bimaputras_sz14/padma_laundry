<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_v101_svc extends CI_Controller {
 
	function __construct()
	{
		parent::__construct();
		//
		// $this->load->library('gcm');
		$this->load->model('api_v101_svc_m'); 
		$this->load->model('api_v101_profile_m'); 
		
	}
	
	/* */
	
	function index()
    {
		
	} 


	function do_checkout()
	{

		$product 	 = trim($this->input->post('product')) == '' ? null : trim($this->input->post('product'));  
		$arr_product = json_decode( $product) ;   
		 
		$photo 		= trim($this->input->post('photo')) == '' ? null : trim($this->input->post('photo'));
		$photo 		=  str_replace("[removed]", '"', $photo);
		$arr_photo 	= json_decode(  $photo ) ;  

		$special_treatmeant 	= trim($this->input->post('special_treatmeant')) == '' ? null : trim($this->input->post('special_treatmeant'));  
		$arr_special_treatmeant = json_decode( $special_treatmeant) ;  


		$jenis_layanan			= trim($this->input->post('jenis_layanan')) == '' ? null : trim($this->input->post('jenis_layanan'));  
		$jenis_cucian			= trim($this->input->post('jenis_cucian')) == '' ? null : trim($this->input->post('jenis_cucian'));  
		$time_pickup			= trim($this->input->post('time_pickup')) == '' ? null : trim($this->input->post('time_pickup'));   
		$kode_akses				= trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));  
		$pelanggan_id			= trim($this->input->post('pelanggan_id')) == '' ? null : trim($this->input->post('pelanggan_id'));  
		
 
		if($pelanggan_id == null || $kode_akses == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
		}  
		else
		{
			$res = array('result' => 'success', 'msg' => 'Data berhasil disimpan.' );
			if(!$data = $this->api_v101_profile_m->get_profile2($pelanggan_id))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else 
			{
				switch($data->status)
				{
					case 1: 
						$no_penjualan = $this->api_v101_svc_m->queue_exec();
						 // $sequence_no = $this->api_v101_svc_m->get_seq_numer();
						 	
						$ins_order = array(
							'no_penjualan'		=> $no_penjualan, 
							'tanggal_penjualan' => date('Y-m-d H:i:s'),
							'pelanggan_id'   	=> $pelanggan_id,   
							'jenis_layanan'	 	=> $jenis_layanan, 
							'jenis_cucian' 	 	=> $jenis_cucian, 
							'time_pickup' 	 	=> $time_pickup, 
							'status' 	 	=>1, 
						);
						
						if($penjualan_id = $this->api_v101_svc_m->insert_penjualan($ins_order)){ 
							foreach ($arr_product as $key) {
								$ins_order = array(
									'penjualan_id'	  => $penjualan_id, 
									'produk_id' 	  => $key->id, 
									'qty' 	  			=> $key->total, 
								); 
								$penjualan_item_id = $this->api_v101_svc_m->insert_penjualan_item($ins_order);
							}

							foreach ($arr_special_treatmeant as $key) {
								$ins_order = array(
									'penjualan_id'	  => $penjualan_id, 
									'produk_id' 	  => $key->id, 
									'note' 	 		  => $key->value, 
								); 
								$penjualan_item_id = $this->api_v101_svc_m->insert_penjualan_note($ins_order);
							}

							foreach ($arr_photo as $key) {
								$fname = uniqid().date('Y-m-d_H:i:s').'.jpg';
								$dir_foto = './uploads/cucian/';
								if($key->src){
									$key->src = str_replace("data:image/jpeg;base64,", "",  $key->src);
									$key->src = str_replace("data:image/png;base64,", "", $key->src);
									$key->src = str_replace("[removed]", "", $key->src);
									$key->src	= trim($key->src);
									$foto_dt = base64_decode($key->src);	
									
									if(file_put_contents($dir_foto.$fname, $foto_dt))
									{ 
										$ins_order_photo = array(
											'penjualan_id'	  => $penjualan_id, 
											'produk_id' 	  => $key->id,
											'path' 	  		  => $fname,
											 
										);
										$penjualan_photo_id = $this->api_v101_svc_m->insert_penjualan_photo($ins_order_photo);
										 
									}
								}


							}


							


							$res = array('result' => 'success', 'msg' => 'Data berhasil disimpan.' );
							 	
						}else{
							$res = array('result' => 'error', 'msg' => 'Tidak berhasil menyimpan data pemohon');
						}
						
					break;
					case 2:
						$res = array('result' => 'error', 'msg' => 'Akun Anda diblokir', 'showmsg' => 'true');
					break;
					case 3:
						$res = array('result' => 'error', 'msg' => 'Akun anda telah dihapus', 'showmsg' => 'true');
					break;
					case 0:
						$res = array('result' => 'error', 'msg' => 'Akun Anda belum di Aktivasi', 'showmsg' => 'true');
					break;
					default:
						$res = array('result' => 'error', 'msg' => 'Akun anda tidak dapat digunakan', 'showmsg' => 'true');
					break;
				}
				
			}
		}
		
		
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	function do_register()
	{

		$email 		= trim($this->input->post('email')) == '' ? null : trim($this->input->post('email')); 
		$name 		= trim($this->input->post('name')) == '' ? null : trim($this->input->post('name')); 
		$phone 		= trim($this->input->post('phone')) == '' ? null : trim($this->input->post('phone')); 
		$password 	= trim($this->input->post('password')) == '' ? null : trim($this->input->post('password')); 
		  
		if($email == null || $password == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
		}  
		else
		{
			// if(!$data = $this->api_v101_profile_m->get_profile2($pelanggan_id))
			// {
			// 	$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			// }
			// else 
			// { 
				$ins_pelanggan = array(
					'nama_pelanggan' 	=> $name, 
					'email' 			=> $email, 
					'hp' 				=> $phone, 
					'password' 			=> md5($password) ,
					'ganti_password' 	=> 1 ,
					'status' 	=> 1 ,

				);
				
				if($pelanggan_id = $this->api_v101_svc_m->insert_pelanggan($ins_pelanggan)){ 
					 
						// $res = array('result' => 'success', 'msg' => 'Hasil pemeriksaan berhasil disimpan.', 'my_html' => $my_html);
						$res = array('result' => 'success', 'msg' => 'Data berhasil disimpan.' );
					 	
				}else{
					$res = array('result' => 'error', 'msg' => 'Tidak berhasil menyimpan data pemohon');
				} 
			// }
		}
		
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	function get_input_order()
	{
		$opt_layanan 		= $this->api_v101_svc_m->get_opt_layanan(); 
		$opt_jam_pickup 	= $this->api_v101_svc_m->get_opt_jam_pickup(); 
		$opt_teknik_cucian 	= $this->api_v101_svc_m->get_opt_teknik_cucian(); 
		
		$res = array('result' => 'success',   'opt_jam_pickup' => $opt_jam_pickup, 'opt_layanan' => $opt_layanan, 'opt_teknik_cucian' => $opt_teknik_cucian );
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	function get_product_list()
	{
		$list_product 		= $this->api_v101_svc_m->get_product();  
		
		$res = array('result' => 'success',   'list_product' => $list_product  );
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	function get_history()
	{

		
		$kode_akses				= trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));  
		$pelanggan_id			= trim($this->input->post('pelanggan_id')) == '' ? null : trim($this->input->post('pelanggan_id'));  
		$status					= trim($this->input->post('status')) == '' ? null : trim($this->input->post('status'));  
		
 
		if($pelanggan_id == null || $kode_akses == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
		}  
		else
		{
			$list_histori		= $this->api_v101_svc_m->get_hist_order($status	,$pelanggan_id);  
			$res = array('result' => 'success',   'list_histori' => $list_histori  );
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	
	 
}	