<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_v101_pemohon extends CI_Controller {
 
	function __construct()
	{
		parent::__construct();
		//
		$this->load->model('Api_v101_svc_m');
		$this->load->model('Api_v101_profile_m');
		$this->load->model('Api_v101_pemohon_m');
		$this->load->helper('mobile_svc');
		$this->load->helper('words');
		$this->load->helper('image');
		
	}
	
	/* */
	
	function index()
    {
		
	}
	
	/* =========================== */
	
	function get_pemohon()
	{
		$ktp = trim($this->input->post('ktp')) == '' ? null : trim($this->input->post('ktp'));
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kode_akses = trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));
		
		if($email == null || $kode_akses == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akses tidak dikenali');
		}
		else if($ktp == null)
		{
			$res = array('result' => 'error', 'msg' => 'Nomor KTP harus diisi');
		}
		else
		{
			if(!$data = $this->Api_v101_profile_m->get_profile($email, $kode_akses))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else 
			{
				//
				switch($data->status)
				{
					case 1:
					
						if(!$data = $this->Api_v101_pemohon_m->get_pemohon($ktp))
						{
							$data = array();
							$card = '';
							$res = array('result' => 'success', 'msg' => 'Pemohon tidak ditemukan', 'data' => $data, 'card' => $card);
						}
						else
						{
							$arr_foto = $this->Api_v101_pemohon_m->get_pemohon_foto($data->id);
							$photo_foto = '{url: \''.$arr_foto["foto"].'\', caption: \''.$data->nama.'\'}';
							$photo_ktp = '{url: \''.$arr_foto["ktp"].'\', caption: \'No.KTP: '.$data->no_ktp.'\'}';
							$photo_pop = '['.$photo_foto.','.$photo_ktp.']';
							
							if($data->tgl_lahir != null && $data->tgl_lahir != '' && $data->tgl_lahir != '0000-00-00'){
								$arr_tgl = explode('-', $data->tgl_lahir);
								$th = $arr_tgl[0];
								$this_year = date('Y');
								$umur = intval($this_year) - intval($th);
								$umur = $umur.' th';
							}else{
								$umur = '0 th';
							}
							
							$card = '<div class="card">
										  <div class="card-content"> 
											<div class="list-block media-list">
											  <ul>
												<li class="item-content bg-green">
												  <div class="item-media" onClick="showMyPhotoBrowserArr('.$photo_pop.')">
													<div>
														<img src="'.$arr_foto["foto"].'" style="height:44px; width:44px; border: 4px solid white;"/>
														<div style="height:20px; width:100%; font-size:10pt; text-align:center; color:#fff; margin-top: 10px;"><b>'.$umur.'</b></div>
													</div>
												  </div>
												  <div class="item-inner" style="background: #fff; padding-left:15px;">
													<div class="item-title-row" style="color:#000">
													  <div class="item-title"><b>'.$data->nama.'</b></div>
													</div>
													<hr size="5" style="border-color:#fff; background:#00B086;">
													<div class="item-subtitle" style="color:#7D7975; font-size:10pt;">
														'.$data->no_ktp.'<br>
														'.$data->pekerjaan.'
													</div>
												  </div>
												</li>
											  </ul>
											  </ul>
											</div>
										  </div>
										</div>';
							
							$res = array('result' => 'success', 'msg' => 'Pemohon ditemukan', 'data' => $data, 'card' => $card);
						}
					
					break;
					case 2:
						$res = array('result' => 'error', 'msg' => 'Akun Anda diblokir', 'showmsg' => 'true');
					break;
					case 3:
						$res = array('result' => 'error', 'msg' => 'Akun anda telah dihapus', 'showmsg' => 'true');
					break;
					case 0:
						$res = array('result' => 'error', 'msg' => 'Akun Anda belum di Aktivasi', 'showmsg' => 'true');
					break;
					default:
						$res = array('result' => 'error', 'msg' => 'Akun anda tidak dapat digunakan', 'showmsg' => 'true');
					break;
				}
				
				
			}
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	/* =========================== */
	
	function get_pemohon_list(){
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kode_akses = trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));
		$offset = trim($this->input->post('offset')) == '' ? 10 : trim($this->input->post('offset'));
		$direction = trim($this->input->post('direction')) == '' ? 'newer' : trim($this->input->post('direction'));
		
		// $email = 'bandung.wijaya@gmail.com';
		// $kode_akses = '3b6afb715501685696145494b2526f8b';
		
		if($email == null || $kode_akses == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akses tidak dikenali');
		}
		else
		{
			if(!$data = $this->Api_v101_profile_m->get_profile($email, $kode_akses))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else 
			{
				if($data->status == 1){
				
					if(!$qry = $this->Api_v101_pemohon_m->get_pemohon_list($data->id,$offset,$direction)){
						$res = array('result' => 'error', 'msg' => 'Belum ada pemohon SIM / pasien');
					}else{
						$res = array('result' => 'success', 'msg' => 'data ditemukan', 'qry' => $qry);
					}
					
				}else{
					$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
				}
				
			}
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	/* =========================== */
	
	function get_pemohon_detail(){
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kode_akses = trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));
		$id = trim($this->input->post('id')) == '' ? null : trim($this->input->post('id'));
		
		// $email = 'bandung.wijaya@gmail.com';
		// $kode_akses = '3b6afb715501685696145494b2526f8b';
		// $id = '8';
		
		if($email == null || $kode_akses == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akses tidak dikenali');
		}
		else if($id == null)
		{
			$res = array('result' => 'error', 'msg' => 'Data tidak dikenali');
		}
		else
		{
			if(!$data = $this->Api_v101_profile_m->get_profile($email, $kode_akses))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else 
			{
				if($data->status == 1){
				
					if(!$qry = $this->Api_v101_pemohon_m->get_pemohon_detail($id)){
						$res = array('result' => 'error', 'msg' => 'Data tidak ditemukan');
					}else{
						$res = array('result' => 'success', 'msg' => 'data ditemukan', 'qry' => $qry);
					}
					
				}else{
					$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
				}
				
			}
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
}	