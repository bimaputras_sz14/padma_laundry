<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gambar extends CI_Controller {
 
	function __construct()
	{
		parent::__construct();
		//
	}
	
	/* */
	
	function index()
    {
	
	}
	
	/* */
	
	function get($path1=null,$path2=null,$path3=null,$path4=null,$path5=null,$path6=null,$path7=null)
    {
		$img = $path1;
		
		if($path2 != null && $path2 != ''){
			$img .= '/'.$path2;
		}
		if($path3 != null && $path3 != ''){
			$img .= '/'.$path3;
		}
		if($path4 != null && $path4 != ''){
			$img .= '/'.$path4;
		}
		if($path5 != null && $path5 != ''){
			$img .= '/'.$path5;
		}
		if($path6 != null && $path6 != ''){
			$img .= '/'.$path6;
		}
		if($path7 != null && $path7 != ''){
			$img .= '/'.$path7;
		}
		
		if($img != null && $img != ''){
			$arr_img = explode('.', $img);
			if($arr_img[(count($arr_img) - 1)] == 'png'){
				header('Content-Type: image/png');
			}else{
				header('Content-Type: image/jpeg');
			}
			
			// readfile('file:///'.$this->config->item('abs_upload_dir').$img);
			
			if($arr_img[(count($arr_img) - 1)] == 'png' || $arr_img[(count($arr_img) - 1)] == 'jpg' || $arr_img[(count($arr_img) - 1)] == 'jpeg'){
				$abs_dir = $this->config->item('abs_upload_dir');
				$abs_dir_old = $this->config->item('abs_upload_dir_old');
				
				if(file_exists($abs_dir.$img)){
					readfile('file:///'.$abs_dir.$img);
				}else if(file_exists($abs_dir_old.$img)){
					readfile('file:///'.$abs_dir_old.$img);
				}else{
					readfile('file:///'.$this->config->item('abs_upload_dir').'default.png');
				}
			}else{
				readfile('file:///'.$this->config->item('abs_upload_dir').'default.png');
			}
		}else{
			header('Content-Type: image/png');
			readfile('file:///'.$this->config->item('abs_upload_dir').'default.png');
		}
	}
	
	/* */
	
	function get0($path1=null,$path2=null,$path3=null,$path4=null,$path5=null,$path6=null,$path7=null)
    {
		$img = $path1;
		
		if($path2 != null && $path2 != ''){
			$img .= '/'.$path2;
		}
		if($path3 != null && $path3 != ''){
			$img .= '/'.$path3;
		}
		if($path4 != null && $path4 != ''){
			$img .= '/'.$path4;
		}
		if($path5 != null && $path5 != ''){
			$img .= '/'.$path5;
		}
		if($path6 != null && $path6 != ''){
			$img .= '/'.$path6;
		}
		if($path7 != null && $path7 != ''){
			$img .= '/'.$path7;
		}
		
		// echo $img;
		if($img != null && $img != ''){
			$arr_img = explode('.', $img);
			if($arr_img[(count($arr_img) - 1)] == 'png'){
				header('Content-Type: image/png');
			}else{
				header('Content-Type: image/jpeg');
			}
			
			// readfile('./uploads/'.$img);
			readfile('file:///'.$this->config->item('abs_upload_dir').$img);
		}else{
			header('Content-Type: image/png');
			readfile('file:///'.$this->config->item('abs_upload_dir').'default.png');
		}
	}
	
	
}	