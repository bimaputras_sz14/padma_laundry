<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gedung extends MY_Controller {

	var $folder = "master_gedung";
	var $table 	= "gedung";

	public function __construct(){
		parent::__construct();
		$this->load->model('model_crud');
		$this->load->library('upload');
		$this->load->helper('my_helper');
	}

	function index(){
		// redirect('master_data/produk');
		$this->data['title']		= "Gedung";
		$this->data['body']			= $this->folder."/view";
		$this->data['styles']		= "includes/styles_master";
		$this->data['scripts']		= "includes/scripts_master";
		$this->data['page_bar'] 	= "includes/template_site_bar";
		$this->data['desc']			= "Data Gedung";
		$this->data['mMenuUtama']	= $this->mMenuUtama;
		$this->data['message'] 		= $this->session->flashdata('message'); 
		$this->load->view('includes/template_site', $this->data);
	}


	public function tambah(){
		if(isset($_POST['submit'])) {
			$nama = $this->input->post('gedung');
			$kuota = $this->input->post('kuota');
			$pic = $this->input->post('pic');
			$nohp = $this->input->post('nohp');
			$univ = $this->input->post('universitas');
			$data	= array(
				'name'	=>	$nama,
				'quota'	=>	$kuota,
				'nama_pic'	=>	$pic,
				'phone_pic'	=>	$nohp,
				'univesitas_id'	=>	$univ
			);
			$record = $this->db->select('name')
									->from($this->table)
									->where('name = ',$nama)
									->get();
			foreach($record->result() as $r) {
				if($r->name == $nama) {
					$this->session->set_flashdata('msg_failed','Nama Gedung " '.$nama.' " telah digunakan.');
					redirect('gedung');
				}
			}
			$this->model_crud->create_data($this->table,$data);
			$this->session->set_flashdata('msg_success',' Tambah Data Gedung " '.$nama.' " berhasil.');
			redirect('master_data/gedung');
		}
		else {
			$this->data['title']		= "Tambah Gedung";
			$this->data['body']			= $this->folder."/form";
			$this->data['styles']		= "includes/styles_master";
			$this->data['scripts']		= "includes/scripts_master";
			$this->data['page_bar'] 	= "includes/template_site_bar";
			$this->data['desc']			= "tambah data gedung";
			$this->data['mMenuUtama']	= $this->mMenuUtama; 
			$this->data['message'] 		= $this->session->flashdata('message'); 
			$this->load->view('includes/template_site', $this->data);
		}
	}

	public function ubah(){
		if(isset($_POST['submit'])) {
			$id 			= $this->input->post('id');
			$nama = $this->input->post('gedung');
			$kuota = $this->input->post('kuota');
			$pic = $this->input->post('pic');
			$nohp = $this->input->post('nohp');
			$univ = $this->input->post('universitas');
			$status = $this->input->post('status');
			$data	= array(
				'name'	=>	$nama,
				'quota'	=>	$kuota,
				'nama_pic'	=>	$pic,
				'phone_pic'	=>	$nohp,
				'status'	=>	$status,
				'univesitas_id'	=>	$univ
			);
			$this->model_crud->update_data($this->table,$data,'id',$id);
			$this->session->set_flashdata('msg_success',' Ubah data Gedung " '.$nama.' " berhasil.');
			redirect('master_data/gedung');
		}
		else {
			$id					= $this->uri->segment(4);
			$data['record']		= $this->model_crud->get_one($this->table,'id',$id)->row_array();
			$data['title']		= "Ubah Gedung";
			$data['body']		= $this->folder."/form";
			$data['styles']		= "includes/styles_master";
			$data['scripts']	= "includes/scripts_master";
			$data['page_bar'] 	= "includes/template_site_bar";
			$data['desc']		= "ubah data gedung";
			$data['mMenuUtama']	= $this->mMenuUtama; 
			$data['message'] 	= $this->session->flashdata('message'); 
			$this->load->view('includes/template_site', $data);
		}
	}

	public function hapus(){
		$id = $this->uri->segment(4);
		$data = array('status' => 3);
		$this->model_crud->update_data($this->table,$data,'id',$id);
		// $this->model_crud->delete_data($this->table,'id',$id);
		$this->session->set_flashdata('msg_success','Data Gedung berhasil dihapus.');
		redirect('master_data/gedung');
	}

	public function json(){
		$this->datatables->select('id,name,quota,nama_pic,status');
		$this->datatables->add_column('action',anchor('master_data/gedung/ubah/$1','Edit',array('class'=>'btn btn-warning btn-xs')).'&nbsp'.anchor('master_data/gedung/hapus/$1','Delete',array('class'=>'btn btn-danger btn-xs',"onclick"=>"return confirm('Anda yakin ingin menghapus?')")),'id');
		$this->datatables->from($this->table);
		$this->datatables->where('status != 3');
		return print_r($this->datatables->generate());
	}
}