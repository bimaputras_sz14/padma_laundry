<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pegawai extends MY_Controller {

	var $folder = "master_pegawai";
	var $table = "personil";

	public function __construct(){
		parent::__construct();
		$this->load->model('model_crud');
        $this->load->library('upload');
        $this->load->helper('my_helper');
	}

	function index(){
		$this->data['title']		= "Pegawai";
		$this->data['body']			= "master_pegawai/view";
		$this->data['styles']		= "includes/styles_master";
		$this->data['page_bar'] 	= "includes/template_site_bar";
		$this->data['scripts']		= "includes/scripts_master";
		$this->data['desc']			= "data pegawai";
		$this->data['mMenuUtama']	= $this->mMenuUtama; 
		$this->data['message'] 		= $this->session->flashdata('message'); 
		$this->load->view('includes/template_site', $this->data);
	}

    public function tambah(){
        if(isset($_POST['submit'])) {
            $nik        = $this->input->post('nik');
            $nama       = $this->input->post('nama');
            $password   = md5($this->input->post('password'));
            $cpassword  = md5($this->input->post('cpassword'));
            $tempat     = $this->input->post('tempat');
            $tanggal    = $this->input->post('tanggal');
            $jabatan    = $this->input->post('jabatan');
            $telepon    = $this->input->post('telepon');
            $email      = $this->input->post('email');
            $status     = $this->input->post('status');
            $info       = $this->input->post('info');
            $user_level = $this->input->post('user_level');
//Cek password = cpassword
            if ($password != $cpassword) {
                $this->session->set_flashdata('msg_failed','Password tidak sama.');
                redirect('pegawai');
            }
//Config file upload
            if(!is_dir('uploads/pegawai/'.$nik.'/')) {
                mkdir('uploads/pegawai/'.$nik.'/',0777,TRUE);
            }
            $path = 'uploads/pegawai/'.$nik.'/';
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'jpg';
            $config['max_size']         = '1024';
            $config['file_ext_tolower'] = 'true';
            $config['file_name']        = strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_FILENAME)).'-foto';
            $this->upload->initialize($config);
            $data   = array('nik'           => $nik,
                            'nama'          => $nama,
                            'password'      => $password,
                            'ttl'           => $tempat,
                            'dob'           => $tanggal,
                            'jabatan_id'    => $jabatan,
                            'telepon'       => $telepon,
                            'email'         => $email,
                            'status'        => $status,
                            'info'          => $info,
                            'user_level_id' => $user_level);
//Cek file upload
            if(!$this->upload->do_upload('foto')) {
                $data['error']  = $this->upload->display_errors();
                // Redirect (idk)
                $data['title']      = "Tambah Pegawai";
                $data['body']       = $this->folder."/form";
                $data['styles']     = "includes/styles_master";
                $data['scripts']    = "includes/scripts_master";
                $data['page_bar']   = "includes/template_site_bar";
                $data['desc']       = "tambah data pegawai";
                $data['mMenuUtama'] = $this->mMenuUtama; 
                $data['message']    = $this->session->flashdata('message'); 
                $this->load->view('includes/template_site', $data);
            }
            else {
//Cek nama produk jika ada yang sama
                $record = $this->db->select('nik')
                                    ->from($this->table)
                                    ->where('nik = ',$nik)
                                    ->get();
                foreach($record->result() as $r) {
                    if($r->nik == $nik) {
                        $this->session->set_flashdata('msg_failed','NIK " '.$nik.' " telah digunakan.');
                        redirect('pegawai');
                    }
                }
                if(!empty($_FILES['foto']['name'])) {
                    $file_name = $this->upload->data('file_name');
                    $data['foto'] = 'pegawai/'.$nik.'/'.$file_name['file_name'];
                }
                $this->model_crud->create_data($this->table,$data);
                $this->session->set_flashdata('msg_success',' Tambah data Pegawai " '.$nama.' " berhasil.');
                redirect('pegawai');
            }
        }
        else {
            $this->data['title']        = "Tambah Pegawai";
            $this->data['body']         = $this->folder."/form";
            $this->data['styles']       = "includes/styles_master";
            $this->data['scripts']      = "includes/scripts_master";
            $this->data['page_bar']     = "includes/template_site_bar";
            $this->data['desc']         = "tambah data Pegawai";
            $this->data['mMenuUtama']   = $this->mMenuUtama; 
            $this->data['message']      = $this->session->flashdata('message'); 
            $this->load->view('includes/template_site', $this->data);
        }
    }

    public function ubah(){
        if(isset($_POST['submit'])) {
            $id         = $this->input->post('id');
            $tnama      = $this->input->post('tnama');
            $img        = $this->input->post('foto');
            $nik        = $this->input->post('nik');
            $nama       = $this->input->post('nama');
            $tempat     = $this->input->post('tempat');
            $tanggal    = $this->input->post('tanggal');
            $jabatan    = $this->input->post('jabatan');
            $telepon    = $this->input->post('telepon');
            $email      = $this->input->post('email');
            $status     = $this->input->post('status');
            $info       = $this->input->post('info');
            $user_level = $this->input->post('user_level');
//Config file upload
            if(!is_dir('uploads/pegawai/'.$nik.'/')) {
                mkdir('uploads/pegawai/'.$nik.'/',0777,TRUE);
            }
            $path = 'uploads/pegawai/'.$nik.'/';
            $config['upload_path']      = $path;
            $config['allowed_types']    = 'jpg';
            $config['max_size']         = '1024';
            $config['file_ext_tolower'] = 'true';
            $config['file_name']        = strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_FILENAME)).'-foto';
            $this->upload->initialize($config);
            $data   = array('nik'           => $nik,
                            'nama'          => $nama,
                            'password'      => $password,
                            'ttl'           => $tempat,
                            'dob'           => $tanggal,
                            'jabatan_id'    => $jabatan,
                            'telepon'       => $telepon,
                            'email'         => $email,
                            'status'        => $status,
                            'info'          => $info,
                            'user_level_id' => $user_level);
//Send file upload
            if(!empty($_FILES['foto']['name'])) {
                if(!$this->upload->do_upload('foto')) {
                    $data['error']      = $this->upload->display_errors();
                    // Redirect (idk)
                    $data['title']      = "Ubah Pegawai";
                    $data['body']       = $this->folder."/form";
                    $data['styles']     = "includes/styles_master";
                    $data['scripts']    = "includes/scripts_master";
                    $data['page_bar']   = "includes/template_site_bar";
                    $data['desc']       = "ubah data pegawai";
                    $data['mMenuUtama'] = $this->mMenuUtama; 
                    $data['message']    = $this->session->flashdata('message'); 
                    $this->load->view('includes/template_site', $data);
                }
                else {
                    $file_name = $this->upload->data('file_name');
                    $data['foto'] = 'pegawai/'.$nik.'/'.$file_name['file_name'];
                    $img = 'uploads/'.$img;
                    if ($img != 'uploads/'.$data['foto'] AND file_exists($img)) {
                        unlink($img);
                    }
                }
            }
            $this->model_crud->update_data($this->table,$data,'id',$id);
            $this->session->set_flashdata('msg_success',' Ubah data Pegawai " '.$nama.' " berhasil.');
            redirect('master_data/pegawai');
        }
        else {
            $id                 = $this->uri->segment(4);
            $data['record']     = $this->model_crud->get_one($this->table,'id',$id)->row_array();
            $data['title']      = "Ubah Pegawai";
            $data['body']       = $this->folder."/form";
            $data['styles']     = "includes/styles_master";
            $data['scripts']    = "includes/scripts_master";
            $data['page_bar']   = "includes/template_site_bar";
            $data['desc']       = "ubah data Pegawai";
            $data['mMenuUtama'] = $this->mMenuUtama; 
            $data['message']    = $this->session->flashdata('message'); 
            $this->load->view('includes/template_site', $data);
        }
    }

    public function hapus(){
        $id = $this->uri->segment(4);
        $data = array('status' => 3);
        $this->model_crud->update_data($this->table,$data,'id',$id);
        // delete_pegawai($id);
        // $this->model_crud->delete_data($this->table,'id',$id);
        $this->session->set_flashdata('msg_success','Data Pegawai berhasil dihapus.');
        redirect('master_data/pegawai');
    }

	public function json(){
        $this->datatables->select('id,nik,nama,email,foto,status');
        $this->datatables->add_column('action',anchor('master_data/pegawai/ubah/$1','Edit',array('class'=>'btn btn-warning btn-xs')).'&nbsp'.anchor('master_data/pegawai/hapus/$1','Delete',array('class'=>'btn btn-danger btn-xs',"onclick"=>"return confirm('Anda yakin ingin menghapus?')")),'id');
        $this->datatables->from($this->table);
        $this->datatables->where('status != 3');
        return print_r($this->datatables->generate());
    }
}

/* End of file pegawai.php */
/* Location: ./application/controllers/pegawai.php */