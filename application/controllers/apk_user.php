<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apk_user extends MY_Controller {
 
	function __construct()
	{
		parent::__construct();
		if (!$this->flexi_auth->is_logged_in_via_password()) 
		{
			redirect(site_url().'otorisasi');
		}
		// if (!$this->ion_auth->logged_in())
		// {
			// redirect(site_url().'auth/login');
		// }
		$this->load->model('apk_user_model');
		// $this->load->model('kewilayahan_model');
		$this->load->model('m_master');
		$this->load->helper('anggota_helper');
		$this->load->helper('kota_helper');
		$this->load->helper('wilayah_helper');
	}	
	function index()
    {
		$this->apk_user();
	}
	
	function apk_user()
    {
		$wilayah = null;
		$this->data['title']		= "Anggota";
		$this->data['body']			= "apk_user/apk_user";
		$this->data['styles']		= "includes/styles_data_table";
		$this->data['scripts']		= "apk_user/scripts_apk_user";
		$this->data['page_bar'] 	= "includes/template_site_bar";
		$this->data['desc']			= "data anggota";
		$this->data['mMenuUtama']	= $this->mMenuUtama;
		$this->data['wilayah'] 		= getAllWilayah($wilayah);
		$this->data['kesatuan'] 	= getAllKesatuan(null);
		$this->data['pangkat']		= getAllPangkat();
		$this->data['fungsi']		= getAllFungsi();
		$this->data['message'] 		= $this->session->flashdata('message');
		$this->load->view('includes/template_site', $this->data);
	}
	
	function get_anggota_by_kdSatwil($kode_wilayah)
	{
		if($data_users = $this->apk_user_model->get_anggota_by_kdWil($kode_wilayah))
		{
			// $data_users = array();
			$res = array(
				'result' => 'success',
				'options' => $data_users
			);
		}
		else
		{
			$res = array(
				'result' => 'error',
				'msg' => 'Data tidak ditemukan'
			);				
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($res);
	}
	
	function data_apk_user(){
		$json=array(); 
		$json['data'] = array();
		$data_login=$this->cek_wilayah();
		$data=$this->apk_user_model->get_data_apk($data_login);
		if ($data->num_rows() > 0){
			$i = 1;
				foreach ($data->result() as $row){
					$btn='<button title="Edit Data" onclick="edit_anggota('.$row->id.')" class="btn btn-edit btn-sm blue"><i class="icon-note"></i></button>';
					if($data_login['lvl_id']==1){
						$btn.='<button onclick="hapus_anggota('.$row->id.');"  title="Hapus Data" role="button" class="btn btn-sm red"><i class="icon-trash"></i></button>';
					}
					
					if($row->status == 1){
						$status="Aktif";
					}elseif($row->status == 0){
						$status="Non-Aktif";
					}elseif($row->status == 2){
						$status="Blocked";
					}else{
						$status="Deleted";
					}
					$json['data'][] = array(
												"no"=>$i,
												"nrp"=>$row->nrp,
												"nama"=>$row->nama,
												"pangkat"=>$row->pangkat,
												"fungsi"=>$row->fungsi,
												"jabatan"=>$row->jabatan,
												"satwil"=>$row->wilayah,
												"satuan"=>$row->kesatuan,
												"unit"=>$row->unitkerja,
												"status"=>$status,
												"verifikasi"=>$row->verified == 1 ? 'Terverifikasi' : 'Belum Verifikasi',
												"btn"=>$btn
											);
					$i++;
				}
		}
		echo json_encode($json);
	}
	
	function get_kesatuan_options()
	{
		$wilayaTeritorial = "";
		$wilayahBinaan = "";
		$statusWilayah = "Tersedia";
		$id = trim($this->input->post('id')) == '' ? 0 : trim($this->input->post('id'));
		$data = getWilayahAndScope($id);
		// var_dump($data);die();
		if(strtolower($data['tipe']) =="polsek"){
			if(!$data['parent_id']){
				$dataWt = $data['wilayah_teritorial'] ==null ? "":explode(',',$data['wilayah_teritorial']);
			}else{
				$statusWilayah = "Tidak Tersedia";
				$dataWt = $data['wilayah_teritorial'];
			}
			$wilayaTeritorial = getWilayahTeritorial($dataWt,strtolower($data['tipe']));
			$wilayahBinaan = getWilayahBinaan($dataWt,strtolower($data['tipe']));
			// var_dump($kecamatan);die();
		}else{
			// $dataWt = $data['wilayah_teritorial'] ==null ? "":explode(',',$data['wilayah_teritorial']);
			$dataWt = $data['wilayah_teritorial'] ==null ? "": $data['wilayah_teritorial'];
			$wilayaTeritorial = getWilayahTeritorial($dataWt,strtolower($data['tipe']));
			$wilayahBinaan = getWilayahBinaan($dataWt,strtolower($data['tipe']));
		}
		$kesatuan = getAllKesatuan($data['tipe']);
		$res = array(
			'result' => 'success',
			'options' => $kesatuan,
			'scope' => strtolower($data['tipe']),
			'statusWilayah' => $statusWilayah,
			'wlt' => $wilayaTeritorial,
			'wlb' => $wilayahBinaan,
		);
		//
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($res);
	}
	
	/* */ 
	
	function get_unit_options()
	{
		$id = trim($this->input->post('id')) == '' ? 0 : trim($this->input->post('id'));
		$unit = getAllUnit($id);
		// $jabatan = getAllJabatan($id);
		if($id!=0){
			$jabatan = getAllJabatan($id);	
		}else{
			$tipe = trim($this->input->post('tipe')) == '' ? 0 : trim($this->input->post('tipe'));
			$jabatan = getJabatanWilayah($tipe);
		}
		$res = array(
			'result' => 'success',
			'unit' => $unit,
			'jabatan' => $jabatan
		);
		//
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($res);
	}
	
	function get_kecamatan()
	{
		$id = trim($this->input->post('id')) == '' ? 0 : trim($this->input->post('id'));
		$kecamatan = getKec($id);
		$res = array(
			'result' => 'success',
			'kecamatan' => $kecamatan
		);
		//
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($res);
	}
	
	function get_kelurahan()
	{
		$id = trim($this->input->post('id')) == '' ? 0 : trim($this->input->post('id'));
		$kelurahan = getKelurahan($id);
		$res = array(
			'result' => 'success',
			'kelurahan' => $kelurahan
		);
		//
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($res);
	}
	
	public function insert_apk_user()
	{
		date_default_timezone_set('Asia/Jakarta');
		$today = date("Y-m-d H:i:s"); 
		$data['status']="sukses";
		$data['error']="";
		$data_login=$this->cek_wilayah();
		$validation_rules = array(
			array('field' => 'wilayah', 'label' => 'Wilayah','rules' => 'required'),
			// array('field' => 'kesatuan', 'label' => 'Kesatuan','rules' => 'required'), 
			array('field' => 'jabatan', 'label' => 'Jabatan','rules' => 'required'),
			array('field' => 'fungsi', 'label' => 'Fungsi','rules' => 'required'),
			// array('field' => 'unit', 'label' => 'Unit Kerja','rules' => 'required'),
			array('field' => 'pangkat', 'label' => 'Pangkat','rules' => 'required'),
			array('field' => 'nrp', 'label' => 'NRP','rules' => 'required|numeric|is_unique[apk_users.nrp]'),
			array('field' => 'nama', 'label' => 'Nama','rules' => 'required'),
			// array('field' => 'tgl_lahir', 'label' => 'Jabatan','rules' => 'required'),
			array('field' => 'nohp', 'label' => 'No HP','rules' => 'numeric'),
			// array('field' => 'email', 'label' => 'email','rules' => 'required'),
			// array('field' => 'smartfren', 'label' => 'No Smartfren','rules' => 'numeric')
		);
		$this->form_validation->set_rules($validation_rules);
		
		if(!$this->form_validation->run()){
				$data["status"]="error";
				$data["error"]=validation_errors();				
		}
		
			if($data["status"]=="sukses"){
				$wilayah = $this->input->post('wilayah') == '' ? null : $this->input->post('wilayah');
				$kesatuan = $this->input->post('kesatuan') == '' ? null : $this->input->post('kesatuan');
				$unit = $this->input->post('unit') == '' ? null : $this->input->post('unit');
				$jabatan = $this->input->post('jabatan') == '' ? null : $this->input->post('jabatan');
				$pangkat = trim($this->input->post('pangkat')) == '' ? null : trim($this->input->post('pangkat'));
				$fungsi = trim($this->input->post('fungsi')) == '' ? null : trim($this->input->post('fungsi'));
				$nrp = trim($this->input->post('nrp')) == '' ? null : trim($this->input->post('nrp'));
				$nama = trim($this->input->post('nama')) == '' ? null : trim($this->input->post('nama'));
				$tgl_lahir = trim($this->input->post('tgl_lahir')) == '' ? null : trim($this->input->post('tgl_lahir'));
				$no_kr = trim($this->input->post('no_kr')) == '' ? null : trim($this->input->post('no_kr'));
				// $smartfren = trim($this->input->post('smartfren')) == '' ? null : trim($this->input->post('smartfren'));
				$telepon = trim($this->input->post('nohp')) == '' ? null : trim($this->input->post('nohp'));
				$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
				$status =   1;
				$verified =  1;
				$perintah = $this->input->post('perintah') == '' ? 0 : 1;
				if(isset($_POST['wilayah_binaan'])){
					$wilayah_binaan=count($_POST['wilayah_binaan']) ==0 ? null:implode($_POST['wilayah_binaan'],',');
				}else{
					$wilayah_binaan="";
				}
				// $kecamatan = $this->input->post('idkec') == '' ? null : $this->input->post('idkec');				
								
					$data = array(
							'nama' => $nama,
							'nrp' => $nrp,
							'dob' => $tgl_lahir,
							'pangkat' => $pangkat,
							'fungsi' => $fungsi,
							'grade' => getGrade($pangkat),
							'password' => md5("pass".substr($nrp, 4)),
							'kode_wilayah' =>$wilayah,
							'id_kesatuan' => $kesatuan,
							'unitkerja_id' => $unit,
							'jabatan_id' => $jabatan, 
							// 'id_kec' => $kecamatan,
							'telepon' => $telepon,
							'email' => $email,
							'perintah' => $perintah,
							'verified' => $verified,
							// 'smartfren' => $smartfren,
							'status' => $status
						);
					if($verified!=0){
						$data['verified_by']=$data_login['nrp'];
						$data['verified_date']=$today;
					}
					if(!$this->apk_user_model->do_add_apk_user($data))
					{
						$data["status"]="error";
						$data["error"]="<p>Data Gagal Disimpan</p>";	
					}
					// $data['error']=$jabatan.' pada kesatuan '.getKodeKesatuan($kesatuan).' berhasil di simpan di database';
				
			}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($data);
	}
	
	public function update_apk_user()
	{
		date_default_timezone_set('Asia/Jakarta');
		$today = date("Y-m-d H:i:s"); 
		$data_login = $this->cek_wilayah();
		$data['status']="sukses";
		$data['error']="";
		$validation_rules = array(
			array('field' => 'wilayah', 'label' => 'Wilayah','rules' => 'required'),
			// array('field' => 'kesatuan', 'label' => 'Kesatuan','rules' => 'required'),
			array('field' => 'jabatan', 'label' => 'Jabatan','rules' => 'required'),
			// array('field' => 'unit', 'label' => 'Unit Kerja','rules' => 'required'),
			array('field' => 'pangkat', 'label' => 'Pangkat','rules' => 'required'),
			array('field' => 'fungsi', 'label' => 'fungsi','rules' => 'required'),
			
			array('field' => 'nama', 'label' => 'Nama','rules' => 'required'),
			// array('field' => 'tgl_lahir', 'label' => 'Jabatan','rules' => 'required'),
			array('field' => 'nohp', 'label' => 'No HP','rules' => 'numeric'),
			// array('field' => 'email', 'label' => 'email','rules' => 'required'),
			// array('field' => 'smartfren', 'label' => 'No Smartfren','rules' => 'numeric')
		);
		if(isDataExists($this->input->post('id'),'nrp',$this->input->post('nrp'))){
			array_push($validation_rules,array('field' => 'nrp', 'label' => 'NRP','rules' => 'required|numeric|is_unique[apk_users.nrp]'));
		}
		$this->form_validation->set_rules($validation_rules);
		
		if(!$this->form_validation->run()){
				$data["status"]="error";
				$data["error"]=validation_errors();				
		}
		
			if($data["status"]=="sukses"){ 
				$id = $this->input->post('id'); 
				$wilayah = $this->input->post('wilayah') == '' ? null : $this->input->post('wilayah');
				$kesatuan = $this->input->post('kesatuan') == '' ? null : $this->input->post('kesatuan');
				$unit = $this->input->post('unit') == '' ? null : $this->input->post('unit');
				$jabatan = $this->input->post('jabatan') == '' ? null : $this->input->post('jabatan');
				$pangkat = trim($this->input->post('pangkat')) == '' ? null : trim($this->input->post('pangkat'));
				$fungsi = trim($this->input->post('fungsi')) == '' ? null : trim($this->input->post('fungsi'));
				$nrp = trim($this->input->post('nrp')) == '' ? null : trim($this->input->post('nrp'));
				$nama = trim($this->input->post('nama')) == '' ? null : trim($this->input->post('nama'));
				$tgl_lahir = trim($this->input->post('tgl_lahir')) == '' ? null : trim($this->input->post('tgl_lahir'));
				$no_kr = trim($this->input->post('no_kr')) == '' ? null : trim($this->input->post('no_kr'));
				// $smartfren = trim($this->input->post('smartfren')) == '' ? null : trim($this->input->post('smartfren'));
				$telepon = trim($this->input->post('nohp')) == '' ? null : trim($this->input->post('nohp'));
				$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
				$status =   1;
				$verified = 1;
				$perintah = $this->input->post('perintah') == '' ? 0 : 1;
				// $kecamatan = $this->input->post('idkec') == '' ? null : $this->input->post('idkec');
				if(isset($_POST['wilayah_binaan'])){
					$wilayah_binaan=count($_POST['wilayah_binaan']) ==0 ? null:implode($_POST['wilayah_binaan'],',');
				}else{
					$wilayah_binaan="";
				}
								
					$data = array(
							'nama' => $nama,
							'nrp' => $nrp,
							'dob' => $tgl_lahir,
							'pangkat' => $pangkat,
							'fungsi' => $fungsi,
							'grade' => getGrade($pangkat),
							'kode_wilayah' =>$wilayah,
							'id_kesatuan' => $kesatuan,
							'unitkerja_id' => $unit,
							'jabatan_id' => $jabatan,
							 
							// 'id_kec' => $kecamatan,
							'telepon' => $telepon,
							'email' => $email,
							'verified' => $verified,
							'perintah' => $perintah,
							// 'verified_by' => $verified,
							// 'smartfren' => $smartfren,
							'status' => $status
						);
					if($verified!=0){
						$data['verified_by']=$data_login['nrp'];
						$data['verified_date']=$today;
					}
					if(!$this->apk_user_model->do_edit_apk_user($id,$data))
					{
						$data["status"]="error";
						$data["error"]="<p>Data Gagal Dirubah</p>";	
					}
					// $data['error']=$jabatan.' pada kesatuan '.getKodeKesatuan($kesatuan).' berhasil di simpan di database';
				
			}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($data);
	}
	function get_anggota_detail($id)
	{
		$id = $id == '' ? null : $id;
		if($info = $this->apk_user_model->get_apkUsers_detail($id))
		{
			$res = array(
				'result' => 'success',
				'data' => $info,
				// 'options' => $opt
			);
		}
		else
		{
			$res = array(
				'result' => 'error',
				'msg' => 'Data tidak ditemukan'
			);				
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($res);
	}
	function do_delete_anggota($id=null)
	{
		$res = array(
					'result' => 'success',
					'msg' => '<h4>Data berhasil dihapus</h4>'
				);
		if($id!=null){
			if(!$this->apk_user_model->do_delete_anggota($id))
			{
				$res = array(
					'result' => 'error',
					'msg' => '<h4>Tidak berhasil menghapus data</h4>'
				);
			}
			else
			{
				$res = array(
					'result' => 'success',
					'msg' => '<h4>Data berhasil dihapus</h4>'
				);
			}
		}
		
		//
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($res);
		
	}
	
	function create_dispenda(){
		$data = array(
							// 'nama' => $nama,
							// 'nrp' => $nrp,
							// 'dob' => $tgl_lahir,
							// 'pangkat' => $pangkat,
							// 'fungsi' => $fungsi,
							// 'grade' => getGrade($pangkat),
							'password' => md5("pass".substr('22000022', 4)),
							// 'kode_wilayah' =>$wilayah,
							// 'id_kesatuan' => $kesatuan,
							// 'unitkerja_id' => $unit,
							// 'jabatan_id' => $jabatan,
							// 'wilayah_binaan' => $wilayah_binaan,
							// 'id_kec' => $kecamatan,
							// 'telepon' => $telepon,
							// 'email' => $email,
							// 'perintah' => $perintah,
							'verified' => 1,
							// 'smartfren' => $smartfren,
							'status' => 1
						);
			var_dump($data);die();
			// $this->apk_user_model->do_edit_apk_user(5214,$data)
	}
	
	function reset_password(){
		$res = array(
					'result' => 'error',
					'msg' => '<h4>Tidak berhasil reset data</h4>'
				);
		$id = trim($this->input->post('id')) == '' ? null : trim($this->input->post('id'));
		$nrp = trim($this->input->post('nrp')) == '' ? null : trim($this->input->post('nrp'));
		if($id!=null && $nrp != null){
			if(!$this->apk_user_model->do_reset_password($id,$nrp))
			{
				$res = array(
					'result' => 'error',
					'msg' => '<h4>Tidak berhasil reset data</h4>'
				);
			}
			else
			{
				$res = array(
					'result' => 'success',
					'msg' => '<h4>Data berhasil direset</h4>'
				);
			}
		}
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($res);
	}
	
	
}
?>