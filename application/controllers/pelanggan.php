<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pelanggan extends MY_Controller {

	var $folder = "master_pelanggan";
	var $table 	= "pelanggan";

	public function __construct(){
		parent::__construct();
		$this->load->model('model_crud');
		$this->load->library('upload');
		$this->load->helper('my_helper');
	}

	function index(){
		$this->data['title']		= "Pelanggan";
		$this->data['body']			= $this->folder."/view";
		$this->data['styles']		= "includes/styles_master";
		$this->data['scripts']		= "includes/scripts_master";
		$this->data['page_bar'] 	= "includes/template_site_bar";
		$this->data['desc']			= "data pelanggan";
		$this->data['mMenuUtama']	= $this->mMenuUtama;
		$this->data['message'] 		= $this->session->flashdata('message'); 
		$this->load->view('includes/template_site', $this->data);
	}

    public function tambah(){
        if(isset($_POST['submit'])) {
        	$nama 		= $this->input->post('nama');
			$jenis		= $this->input->post('jenis');
			$gedung 	= $this->input->post('gedung');
			$member 	= $this->input->post('member');
			$kuota 		= $this->input->post('kuota');
			$email		= $this->input->post('email');
			$nohp		= $this->input->post('nohp');
			$provinsi 	= $this->input->post('provinsi');
			$kota 		= $this->input->post('kota');
			$kecamatan 	= $this->input->post('kecamatan');
			$kelurahan 	= $this->input->post('kelurahan');
			$alamat		= $this->input->post('alamat');
			if ($jenis == 'gedung') {
				$member = null ;
			}
			else{
				$gedung = null;
			}
			$data	= array('nama_pelanggan'	=> $nama,
							'gedung_id'			=> $gedung,
							'membership_type'	=> $member,
							'stock'				=> $kuota,
							'email'				=> $email,
							'hp'				=> $nohp,
							'id_prov'			=> $provinsi,
							'id_kabkot'			=> $kota,
							'id_kec'			=> $kecamatan,
							'id_kel'			=> $kelurahan,
							'alamat'			=> $alamat,
							'status'			=> 1);
			$this->model_crud->create_data($this->table,$data);
			$this->session->set_flashdata('msg_success',' Tambah data pelanggan " '.$nama_pelanggan.' " berhasil.');
			redirect('pelanggan');
        }
        else {
            $this->data['title']        = "Tambah Pelanggan";
            $this->data['body']         = $this->folder."/form";
            $this->data['styles']       = "includes/styles_master";
            $this->data['scripts']      = "includes/scripts_master";
            $this->data['page_bar']     = "includes/template_site_bar";
            $this->data['desc']         = "tambah data pelanggan";
            $this->data['mMenuUtama']   = $this->mMenuUtama; 
            $this->data['message']      = $this->session->flashdata('message'); 
            $this->load->view('includes/template_site', $this->data);
        }
    }

    public function ubah(){
        if(isset($_POST['submit'])) {
        	$id 		= $this->input->post('id');
        	$nama 		= $this->input->post('nama');
			$jenis		= $this->input->post('jenis');
			$gedung 	= $this->input->post('gedung');
			$member 	= $this->input->post('member');
			$kuota 		= $this->input->post('kuota');
			$email		= $this->input->post('email');
			$nohp		= $this->input->post('nohp');
			$provinsi 	= $this->input->post('provinsi');
			$kota 		= $this->input->post('kota');
			$kecamatan 	= $this->input->post('kecamatan');
			$kelurahan 	= $this->input->post('kelurahan');
			$alamat		= $this->input->post('alamat');
			$status		= $this->input->post('status');
			if ($jenis == 'gedung') {
				$member = null ;
			}else{
				$gedung = null;
			}
			$data	= array('nama_pelanggan'	=> $nama,
							'gedung_id'			=> $gedung,
							'membership_type'	=> $member,
							'stock'				=> $kuota,
							'email'				=> $email,
							'hp'				=> $nohp,
							'id_prov'			=> $provinsi,
							'id_kabkot'			=> $kota,
							'id_kec'			=> $kecamatan,
							'id_kel'			=> $kelurahan,
							'alamat'			=> $alamat,
							'status'			=> $status);
			$this->model_crud->update_data($this->table,$data,'id',$id);
			$this->session->set_flashdata('msg_success',' Ubah data pelanggan " '.$nama_pelanggan.' " berhasil.');
			redirect('pelanggan');
        }
        else {
        	$id					= $this->uri->segment(4);
			$data['record']		= $this->model_crud->get_one($this->table,'id',$id)->row_array();
            $data['title']        = "Ubah Pelanggan";
            $data['body']         = $this->folder."/form";
            $data['styles']       = "includes/styles_master";
            $data['scripts']      = "includes/scripts_master";
            $data['page_bar']     = "includes/template_site_bar";
            $data['desc']         = "ubah data pelanggan";
            $data['mMenuUtama']   = $this->mMenuUtama; 
            $data['message']      = $this->session->flashdata('message'); 
            $this->load->view('includes/template_site', $data);
        }
    }

    public function hapus(){
		$id = $this->uri->segment(4);
		$data = array('status' => 3);
		$this->model_crud->update_data($this->table,$data,'id',$id);
		// $this->model_crud->delete_data($this->table,'id',$id);
		$this->session->set_flashdata('msg_success','Data pelanggan berhasil dihapus.');
		redirect('master_data/pelanggan');
	}

// Generate option kabupaten
	public function option_kota(){
		$data_1 = $this->input->get('data_1');
		$record = $this->model_crud->get_one('all_kabkot','id_prov',$data_1)->result();
		foreach ($record as $info) {
			$data['id_kabkot'][] 	= $info->id_kabkot;
			$data['nama_kabkot'][] 	= $info->nama_kabkot;
		}
		if (!empty($data)) {
			echo json_encode($data);}
		else{
			echo '0';}
	}

// Generate option kabupaten
	public function option_kecamatan(){
		$data_1 = $this->input->get('data_1');
		$record = $this->model_crud->get_one('all_kecamatan','id_kabkot',$data_1)->result();
		foreach ($record as $info) {
			$data['id_kec'][] 	= $info->id_kec;
			$data['nama_kec'][] 	= $info->nama_kec;
		}
		if (!empty($data)) {
			echo json_encode($data);}
		else{
			echo '0';}
	}

// Generate option kecamatan
	public function option_kelurahan(){
		$data_1 = $this->input->get('data_1');
		$record = $this->model_crud->get_one('all_kelurahan','id_kec',$data_1)->result();
		foreach ($record as $info) {
			$data['id_kel'][] 		= $info->id_kel;
			$data['nama_kel'][] 	= $info->nama_kel;
		}
		if (!empty($data)) {
			echo json_encode($data);}
		else{
			echo '0';}
	}

// Get kuota gedung
	public function kuota_gedung(){
		$data_1 = $this->input->get('data_1');
		$record = $this->model_crud->get_one('gedung','id',$data_1)->result();
		foreach ($record as $info) {
			$data['quota'][] 		= $info->quota;
		}
		if (!empty($data)) {
			echo json_encode($data);}
		else{
			echo '0';}	
	}

// Get kuota member
	public function kuota_member(){
		$data_1 = $this->input->get('data_1');
		$record = $this->model_crud->get_one('membership_type','id',$data_1)->result();
		foreach ($record as $info) {
			$data['quota'][] 		= $info->quota;
		}
		if (!empty($data)) {
			echo json_encode($data);}
		else{
			echo '0';}	
	}

	public function json(){
		$this->datatables->select('id, nama_pelanggan, membership_type, email, status, hp');
		$this->datatables->add_column('action',anchor('master_data/pelanggan/ubah/$1','Edit',array('class'=>'btn btn-warning btn-xs')).'&nbsp'.anchor('master_data/pelanggan/hapus/$1','Delete',array('class'=>'btn btn-danger btn-xs',"onclick"=>"return confirm('Anda yakin ingin menghapus?')")),'id');
		$this->datatables->from($this->table);
		$this->datatables->where('status != 3');
		return print_r($this->datatables->generate());
	}
}
/* End of file pelanggan.php */
/* Location: ./application/controllers/pelanggan.php */