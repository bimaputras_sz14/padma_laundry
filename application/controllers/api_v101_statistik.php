<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_v101_statistik extends CI_Controller {
 
	function __construct()
	{
		parent::__construct();
		//
		$this->load->model('Rikkes_v101_statistik_m');
		$this->load->model('Rikkes_v101_profile_m');
		$this->load->helper('mobile_svc');
		$this->load->helper('words');
		
	}

	function index()
	{
	
	}
	
	/* =========================== */
	
	function get_dashboard_chart1(){
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kode_akses = trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));
		
		// $email = 'abuse@nagabendu.co.id';
		// $kode_akses = 'dffb952700883b73e0975cf7ff3c59d2';
		
		if($email == null || $kode_akses == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akses tidak dikenali', 'showmsg' => 'true');
		}
		else
		{
			if(!$data = $this->Rikkes_v101_profile_m->get_profile($email, $kode_akses))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else 
			{
				switch($data->status)
				{
					case 1:
					
						$content_top = $this->Rikkes_v101_statistik_m->get_dashboard_top($data->id);
						$data_chart = $this->Rikkes_v101_statistik_m->get_dashboard_chart1($data->id);
						$judul = '<b>Rikkes Minggu Ini</b>';
						$legend = '<div class="content-block">
												  <div class="chip">
													<div class="chip-media" style="background:#46BFBD; border:#B3B3B3 solid 1px;"></div>
													<div class="chip-label">lulus</div>
												  </div><br>
												  <div class="chip">
													<div class="chip-media" style="background:#F7464A; border:#B3B3B3 solid 1px;"></div>
													<div class="chip-label">tidak lulus</div>
												  </div>
											</div>';
											
						$content_topb = $this->Rikkes_v101_statistik_m->get_dashboard_transaksi_top($data->id);
						$data_chartb = $this->Rikkes_v101_statistik_m->get_dashboard_transaksi_chart1($data->id);
						$judulb = '<b>Transaksi Minggu Ini</b>';
						$legendb = '<div class="content-block">
												  <div class="chip">
													<div class="chip-media" style="background:#46BFBD; border:#B3B3B3 solid 1px;"></div>
													<div class="chip-label">success</div>
												  </div><br>
												  <div class="chip">
													<div class="chip-media" style="background:#F7464A; border:#B3B3B3 solid 1px;"></div>
													<div class="chip-label">pending</div>
												  </div>
											</div>';
											
						$ext = '<div class="content-block">
												<p class="buttons-row" >
													<a href="javascript: showStatistikChart2()" class="button button-fill color-teal">chart selanjutnya</a>
												</p>
										</div>';
						
						$res = array('result' => 'success', 'msg' => 'OK', 'content_top' => $content_top, 'data_chart' => $data_chart, 'judul' => $judul, 'legend' => $legend, 'content_topb' => $content_topb, 'data_chartb' => $data_chartb, 'judulb' => $judulb, 'legendb' => $legendb, 'ext' => $ext);
					
					break;
					case 2:
						$res = array('result' => 'error', 'msg' => 'Akun Anda diblokir', 'showmsg' => 'true');
					break;
					case 3:
						$res = array('result' => 'error', 'msg' => 'Akun anda telah dihapus', 'showmsg' => 'true');
					break;
					case 0:
						$res = array('result' => 'error', 'msg' => 'Akun Anda belum di Aktivasi', 'showmsg' => 'true');
					break;
					default:
						$res = array('result' => 'error', 'msg' => 'Akun anda tidak dapat digunakan', 'showmsg' => 'true');
					break;
				}
			}
		
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	/* =========================== */
	
	function get_dashboard_chart2(){
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kode_akses = trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));
		
		// $email = 'bandung.wijaya@gmail.com';
		// $kode_akses = '3b6afb715501685696145494b2526f8b';
		
		if($email == null || $kode_akses == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akses tidak dikenali', 'showmsg' => 'true');
		}
		else
		{
			if(!$data = $this->Rikkes_v101_profile_m->get_profile($email, $kode_akses))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else 
			{
				switch($data->status)
				{
					case 1:
					
						$bln = date('Y-m');
						
						$data_chart = $this->Rikkes_v101_statistik_m->get_dashboard_chart2($data->id);
						$judul = '<b>Jenis SIM Yang Diajukan Bulan Ini</b>';
						// $legend = '<hr>';
						$ext = '<div class="content-block">
												<p class="buttons-row" >
													<a href="javascript: showStatistikChart3()" class="button button-fill color-teal">chart selanjutnya</a>
												</p>
										</div>';
						
						$res = array('result' => 'success', 'msg' => 'OK', 'data_chart' => $data_chart['chart'], 'judul' => $judul, 'legend' => $data_chart['legend'], 'ext' => $ext);
					
					break;
					case 2:
						$res = array('result' => 'error', 'msg' => 'Akun Anda diblokir', 'showmsg' => 'true');
					break;
					case 3:
						$res = array('result' => 'error', 'msg' => 'Akun anda telah dihapus', 'showmsg' => 'true');
					break;
					case 0:
						$res = array('result' => 'error', 'msg' => 'Akun Anda belum di Aktivasi', 'showmsg' => 'true');
					break;
					default:
						$res = array('result' => 'error', 'msg' => 'Akun anda tidak dapat digunakan', 'showmsg' => 'true');
					break;
				}
			}
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	/* =========================== */
	
	function get_dashboard_chart3(){
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kode_akses = trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));
		
		// $email = 'bandung.wijaya@gmail.com';
		// $kode_akses = '3b6afb715501685696145494b2526f8b';
		
		if($email == null || $kode_akses == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akses tidak dikenali', 'showmsg' => 'true');
		}
		else
		{
			if(!$data = $this->Rikkes_v101_profile_m->get_profile($email, $kode_akses))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else 
			{
				switch($data->status)
				{
					case 1:
					
						$data_chart = $this->Rikkes_v101_statistik_m->get_dashboard_chart3($data->id);
						$judul = '<b>Rikkes 4 Bulan Terakhir</b>';
						$legend = '<div class="content-block">
												  <div class="chip">
													<div class="chip-media" style="background:#46BFBD; border:#B3B3B3 solid 1px;"></div>
													<div class="chip-label">lulus</div>
												  </div><br>
												  <div class="chip">
													<div class="chip-media" style="background:#F7464A; border:#B3B3B3 solid 1px;"></div>
													<div class="chip-label">tidak lulus</div>
												  </div>
											</div>';
											
						$data_chartb = $this->Rikkes_v101_statistik_m->get_transaksi_chart3($data->id);
						$judulb = '<b>Transaksi 4 Bulan Terakhir</b>';
						$legendb = '';
						$ext = '<br>';
						
						$res = array('result' => 'success', 'msg' => 'OK', 'data_chart' => $data_chart, 'judul' => $judul, 'legend' => $legend, 'data_chartb' => $data_chartb, 'judulb' => $judulb, 'legendb' => $legendb, 'ext' => $ext);
			
					break;
					case 2:
						$res = array('result' => 'error', 'msg' => 'Akun Anda diblokir', 'showmsg' => 'true');
					break;
					case 3:
						$res = array('result' => 'error', 'msg' => 'Akun anda telah dihapus', 'showmsg' => 'true');
					break;
					case 0:
						$res = array('result' => 'error', 'msg' => 'Akun Anda belum di Aktivasi', 'showmsg' => 'true');
					break;
					default:
						$res = array('result' => 'error', 'msg' => 'Akun anda tidak dapat digunakan', 'showmsg' => 'true');
					break;
				}
			}
		
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	function test(){
		echo 'testing';
	}
	
}
