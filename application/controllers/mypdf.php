<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mypdf extends MY_Controller  {
 
	function __construct()
	{
		parent::__construct(); 
		$this->load->model('bagops_model');
		$this->load->model('m_master');
	} 

	function cetak_detail_ranmor($id=null) {	
		$this->load->library('fpdf_gen');
		
		$judul = "SURAT KETERANGAN";
		if($data = $this->ranmor_model->get_ranmor_detail($id)){
			// var_dump($data->ttd);die();
			
			// if($data->ttd !=null && $data->ttd !=''){
			// $this->fpdf->Image('./uploads/ktmdu/default.png',0,0,210);
				// $foto = $data->foto;
			// }
			
			if($data->yg_ttd != null && $data->yg_ttd != ''){
				$yg_ttd = $data->yg_ttd;
			}else{
				$yg_ttd = $data->nm_pemilik;
			}
			
			if($data->alamat_ttd != null && $data->alamat_ttd != ''){
				$alamat_ttd = $data->alamat_ttd;
			}else{
				$alamat_ttd = $data->al_pemilik;
			}
			
			$this->fpdf->SetY(25);
			$this->fpdf->SetFont('Arial','B',16);
			$this->fpdf->Cell(0,20, $judul, 0, 0, 'C');
			
			$this->fpdf->Ln(30);
			$this->fpdf->SetX(25);
			$this->fpdf->SetFont('Arial','',11);
			$this->fpdf->Cell(160,5,'Yang bertanda tangan dibawah ini :');
			$this->fpdf->Ln(10);
			$this->fpdf->SetX(25);
			$this->fpdf->SetFont('Arial','',11);
			$this->fpdf->Cell(40,5,'Nama');
			$this->fpdf->Cell(5,5,':');
			$this->fpdf->SetFont('Arial','B',11);
			$this->fpdf->Cell(115,5,$yg_ttd);
			$this->fpdf->Ln(8);
			$this->fpdf->SetX(25);
			$this->fpdf->SetFont('Arial','',11);
			$this->fpdf->Cell(40,5,'Alamat');
			$this->fpdf->Cell(5,5,':');
			$this->fpdf->SetFont('Arial','B',11);
			$this->fpdf->MultiCell(115,5,$alamat_ttd);			
			$this->fpdf->Ln(10);
			
			$this->fpdf->SetX(25);
			$this->fpdf->SetFont('Arial','',11);
			$this->fpdf->Cell(160,5,'Menerangkan bahwa kendaraan dengan data sebagai berikut :');
			$this->fpdf->Ln(10);
			$this->fpdf->SetX(25);
			$this->fpdf->SetFont('Arial','',11);
			$this->fpdf->Cell(40,5,'No. Registrasi');
			$this->fpdf->Cell(5,5,':');
			$this->fpdf->SetFont('Arial','B',11);
			$this->fpdf->Cell(115,5,$data->no_polisi);
			$this->fpdf->Ln(8);
			$this->fpdf->SetX(25);
			$this->fpdf->SetFont('Arial','',11);
			$this->fpdf->Cell(40,5,'Nama Pemilik');
			$this->fpdf->Cell(5,5,':');
			$this->fpdf->SetFont('Arial','B',11);
			$this->fpdf->Cell(115,5,$data->nm_pemilik);
			$this->fpdf->Ln(8);
			$this->fpdf->SetX(25);
			$this->fpdf->SetFont('Arial','',11);
			$this->fpdf->Cell(40,5,'Merk / Type / Jenis');
			$this->fpdf->Cell(5,5,':');
			$this->fpdf->SetFont('Arial','B',11);
			$this->fpdf->Cell(115,5,$data->nm_merek_kb."/".$data->nm_model_kb);
			$this->fpdf->Ln(8);
			$this->fpdf->SetX(25);
			$this->fpdf->SetFont('Arial','',11);
			$this->fpdf->Cell(40,5,'No.Rangka/Mesin');
			$this->fpdf->Cell(5,5,':');
			$this->fpdf->SetFont('Arial','B',11);
			$this->fpdf->Cell(115,5,$data->no_rangka); 
			$this->fpdf->Ln(15);
			
			$this->fpdf->SetX(25);
			$this->fpdf->SetFont('Arial','',11);
			$this->fpdf->MultiCell(160,5,'Sampai saat ini belum melaksanakan pembayaran Pajak Kendaraan Bermotor dengan alasan :');
			$this->fpdf->Ln();
			$this->fpdf->SetX(25);
			$this->fpdf->SetFont('Arial','B',11);
			$this->fpdf->MultiCell(160,5,$data->status);
			$this->fpdf->SetX(25);
			$this->fpdf->MultiCell(160,5,$data->keterangan);
			// $this->fpdf->Ln(20);
			
			$this->fpdf->SetY(-100);
			$this->fpdf->SetX(25);
			$this->fpdf->SetFont('Arial','B',11);
			$this->fpdf->Cell(65,5,'','LTR');
			$this->fpdf->Cell(30,5,'','T');
			$this->fpdf->Cell(65,5,'','LTR');
			$this->fpdf->Ln();
			
			if($data->nm_petugas != null && $data->nm_petugas != ''){
				$nm_petugas = $data->nm_petugas;
			}else{
				$agt = $this->ranmor_model->get_ranmor_petugas($data->nrp_petugas);
				$nm_petugas = $agt->nama;
			}
			
			$this->fpdf->SetX(25);
			$this->fpdf->SetFont('Arial','',11);
			$this->fpdf->Cell(65,5,'','LR',0,'C');
			$this->fpdf->Cell(30,5,'');
			$this->fpdf->Cell(65,5,$data->kota.', '.convertTanggal($data->tgl),'LR',0,'C');
			$this->fpdf->Ln();
			$this->fpdf->SetX(25);
			$this->fpdf->Cell(65,5,'Petugas Bhabinkamtibmas,','LR',0,'C');
			$this->fpdf->Cell(30,5,'');
			$this->fpdf->Cell(65,5,'Yang Menerangkan,','LR',0,'C');
			$this->fpdf->Ln();
			$this->fpdf->SetX(25);
			$this->fpdf->Cell(65,35,'','LR');
			$this->fpdf->Cell(30,35,'');
			$this->fpdf->Cell(65,35,'','LR');
			$this->fpdf->Ln();
			$this->fpdf->SetX(25);
			$this->fpdf->Cell(65,5,'( '.$data->nm_petugas.' )','LR',0,'C');
			$this->fpdf->Cell(30,5,'');
			$this->fpdf->Cell(65,5,'( '.$yg_ttd.' )','LR',0,'C');
			$this->fpdf->Ln();
			$this->fpdf->SetX(25);
			$this->fpdf->Cell(65,5,'','LBR');
			$this->fpdf->Cell(30,5,'','B');
			$this->fpdf->Cell(65,5,'','LBR');
			
			// $this->fpdf->Image('./uploads/ktmdu/default.png',137,215,30,0);
			// $this->fpdf->Image('./uploads/ktmdu/56d6221a6dd81.jpg',137,210,30,0,'PNG');
			if($data->ttd != null && $data->ttd != ''){
				$this->fpdf->Image('./uploads/ktmdu/'.$data->ttd,137,210,30,0,'PNG');
			}
			
			// echo $this->fpdf->Output('hello_world.pdf','D');
			$this->fpdf->Output();
		}else{
			echo "data tidak ditemukan";		
		}
	
	}

	function cetak_detail_ranmor_blank($id=null, $nrp=null) {	
		$this->load->library('fpdf_gen');
		
		$judul = "SURAT KETERANGAN";
		if($data = $this->ranmor_model->get_ranmor_detail_blank($id)){
			if($agt = $this->ranmor_model->get_ranmor_petugas($nrp)){
				
				$this->fpdf->SetY(15);
				$this->fpdf->SetFont('Arial','B',16);
				$this->fpdf->Cell(0,20, $judul, 0, 0, 'C');
				
				$this->fpdf->Ln(25);
				$this->fpdf->SetX(25);
				$this->fpdf->SetFont('Arial','',11);
				$this->fpdf->Cell(160,5,'Yang bertanda tangan dibawah ini :');
				$this->fpdf->Ln(10);
				$this->fpdf->SetX(25);
				$this->fpdf->SetFont('Arial','',11);
				$this->fpdf->Cell(40,5,'Nama');
				$this->fpdf->Cell(5,5,':');
				$this->fpdf->SetFont('Arial','B',11);
				$this->fpdf->Cell(115,5,"");
				$this->fpdf->Ln(8);
				$this->fpdf->SetX(25);
				$this->fpdf->SetFont('Arial','',11);
				$this->fpdf->Cell(40,5,'Alamat');
				$this->fpdf->Cell(5,5,':');
				$this->fpdf->SetFont('Arial','B',11);
				$this->fpdf->MultiCell(115,5,"");			
				$this->fpdf->Ln(5);
				
				$this->fpdf->SetX(25);
				$this->fpdf->SetFont('Arial','',11);
				$this->fpdf->Cell(160,5,'Menerangkan bahwa kendaraan dengan data sebagai berikut :');
				$this->fpdf->Ln(10);
				$this->fpdf->SetX(25);
				$this->fpdf->SetFont('Arial','',11);
				$this->fpdf->Cell(40,5,'No. Registrasi');
				$this->fpdf->Cell(5,5,':');
				$this->fpdf->SetFont('Arial','B',11);
				$this->fpdf->Cell(115,5,$data->no_polisi);
				$this->fpdf->Ln(8);
				$this->fpdf->SetX(25);
				$this->fpdf->SetFont('Arial','',11);
				$this->fpdf->Cell(40,5,'Nama Pemilik');
				$this->fpdf->Cell(5,5,':');
				$this->fpdf->SetFont('Arial','B',11);
				$this->fpdf->Cell(115,5,$data->nm_pemilik);
				$this->fpdf->Ln(8);
				$this->fpdf->SetX(25);
				$this->fpdf->SetFont('Arial','',11);
				$this->fpdf->Cell(40,5,'Alamat Pemilik');
				$this->fpdf->Cell(5,5,':');
				$this->fpdf->SetFont('Arial','B',11);
				$this->fpdf->MultiCell(115,5,$data->al_pemilik);
				$this->fpdf->Ln(3);
				$this->fpdf->SetX(25);
				$this->fpdf->SetFont('Arial','',11);
				$this->fpdf->Cell(40,5,'Merk / Type / Jenis');
				$this->fpdf->Cell(5,5,':');
				$this->fpdf->SetFont('Arial','B',11);
				$this->fpdf->Cell(115,5,$data->nm_merek_kb."/".$data->nm_model_kb);
				$this->fpdf->Ln(8);
				$this->fpdf->SetX(25);
				$this->fpdf->SetFont('Arial','',11);
				$this->fpdf->Cell(40,5,'No.Rangka/Mesin');
				$this->fpdf->Cell(5,5,':');
				$this->fpdf->SetFont('Arial','B',11);
				$this->fpdf->Cell(115,5,$data->no_rangka); 
				$this->fpdf->Ln(10);
				
				$this->fpdf->SetX(25);
				$this->fpdf->SetFont('Arial','',11);
				$this->fpdf->MultiCell(160,5,'Sampai saat ini belum melaksanakan pembayaran Pajak Kendaraan Bermotor dengan alasan : (lingkari salah satu alasan dibawah ini, serta isi keterangannya)');
				$this->fpdf->Ln();
				// $this->fpdf->SetFont('Arial','B',11);
				// $this->fpdf->MultiCell(160,5,"");//$data->status
				// $this->fpdf->SetX(25);
				// $this->fpdf->MultiCell(160,5,"");//$data->keterangan
				
				$this->fpdf->SetFont('Arial','I',10);
				$this->fpdf->SetX(25);
				$this->fpdf->Cell(5,5,'1.');
				$this->fpdf->Cell(150,5,'Kendaraan hilang (Curanmor) dan tidak melapor ke kepolisian');
				$this->fpdf->Ln();
				$this->fpdf->SetX(30);
				$this->fpdf->Cell(150,5,'(sejak tanggal : ....................................................)');
				$this->fpdf->Ln(6);
				$this->fpdf->SetX(25);
				$this->fpdf->Cell(5,5,'2.');
				$this->fpdf->Cell(150,5,'Kendaraan ditarik leasing dan lembaga penjamin lainnya');
				$this->fpdf->Ln();
				$this->fpdf->SetX(30);
				$this->fpdf->Cell(150,5,'(Nama Leasing/Lembaga Penjamin : ....................................................)');
				$this->fpdf->Ln(6);
				$this->fpdf->SetX(25);
				$this->fpdf->Cell(5,5,'3.');
				$this->fpdf->Cell(150,5,'Kendaraan sudah dipindahtangankan');
				$this->fpdf->Ln();
				$this->fpdf->SetX(30);
				$this->fpdf->Cell(150,5,'(sejak tanggal : ....................................................)');
				$this->fpdf->Ln(6);
				$this->fpdf->SetX(25);
				$this->fpdf->Cell(5,5,'4.');
				$this->fpdf->Cell(150,5,'Kendaraan rusak berat');
				$this->fpdf->Ln();
				$this->fpdf->SetX(30);
				$this->fpdf->Cell(150,5,'(sejak tanggal : ....................................................)');
				$this->fpdf->Ln(6);
				$this->fpdf->SetX(25);
				$this->fpdf->Cell(5,5,'5.');
				$this->fpdf->Cell(150,5,'Alamat WP tidak sesuai dengan alamat di SKPD/STNK');
				$this->fpdf->Ln();
				$this->fpdf->SetX(30);
				$this->fpdf->Cell(150,5,'(pindah alamat / tidak dikenal (tidak jelas) / alamat tidak ditemukan)');
				$this->fpdf->Ln(6);
				$this->fpdf->SetX(25);
				$this->fpdf->Cell(5,5,'6.');
				$this->fpdf->Cell(150,5,'Wajib pajak tidak merasa memiliki kendaraan');
				$this->fpdf->Ln(6);
				$this->fpdf->SetX(25);
				$this->fpdf->Cell(5,5,'7.');
				$this->fpdf->Cell(150,5,'Alasan Lainnya');
				$this->fpdf->Ln();
				$this->fpdf->SetX(30);
				$this->fpdf->Cell(150,5,'.........................................................................................');
				$this->fpdf->Ln();
				
				$this->fpdf->SetY(-80);
				$this->fpdf->SetX(25);
				$this->fpdf->SetFont('Arial','B',11);
				$this->fpdf->Cell(65,5,'','LTR');
				$this->fpdf->Cell(30,5,'','T');
				$this->fpdf->Cell(65,5,'','LTR');
				$this->fpdf->Ln();
				
				$this->fpdf->SetX(25);
				$this->fpdf->SetFont('Arial','',11);
				$this->fpdf->Cell(65,5,'','LR',0,'C');
				$this->fpdf->Cell(30,5,'');
				$this->fpdf->Cell(65,5,'....................... , .... ............. ........','LR',0,'C');
				$this->fpdf->Ln();
				$this->fpdf->SetX(25);
				$this->fpdf->Cell(65,5,'Petugas Bhabinkamtibmas,','LR',0,'C');
				$this->fpdf->Cell(30,5,'');
				$this->fpdf->Cell(65,5,'Yang Menerangkan,','LR',0,'C');
				$this->fpdf->Ln();
				$this->fpdf->SetX(25);
				$this->fpdf->Cell(65,25,'','LR');
				$this->fpdf->Cell(30,25,'');
				$this->fpdf->Cell(65,25,'','LR');
				$this->fpdf->Ln();
				$this->fpdf->SetX(25);
				$this->fpdf->Cell(5,5,'(','L',0,'C');  $this->fpdf->Cell(55,5,$agt->nama,'B',0,'C');  $this->fpdf->Cell(5,5,')','R',0,'C');
				$this->fpdf->Cell(30,5,'');
				$this->fpdf->Cell(65,5,'( ..................................................... )','LR',0,'C');
				$this->fpdf->Ln();
				$this->fpdf->SetX(25);
				$this->fpdf->SetFont('Arial','',10);
				$this->fpdf->Cell(65,5,strtoupper($agt->pangkat).' / NRP '.$agt->nrp,'LR',0,'C');
				$this->fpdf->Cell(30,5,'');
				$this->fpdf->Cell(65,5,'','LR',0,'C');
				$this->fpdf->Ln();
				$this->fpdf->SetX(25);
				$this->fpdf->Cell(65,2,'','LBR');
				$this->fpdf->Cell(30,2,'','B');
				$this->fpdf->Cell(65,2,'','LBR');
				
				// $this->fpdf->Image('./uploads/ktmdu/'.$data->ttd,137,210,30,0,'PNG');
				
				echo $this->fpdf->Output('formulir_ktmdu_'.$nrp.'-'.$id.'.pdf','D');
				// $this->fpdf->Output();
			}else{
				echo "data petugas tidak ditemukan";		
			}
		}else{
			echo "data tidak ditemukan";		
		}
	
	}
	
	function cetak_laphar($awal=null,$akhir=null,$kd_wil=null){  
		$this->load->library('fpdf_poltrait');
		$this->load->helper('wilayah');
		$this->load->model('kewilayahan_model');

		$this->load->helper('words');
		$arr_usr 			= $this->flexi_auth->get_user_by_identity_row_array();
		
		$this->fpdf->SetAutoPageBreak(true, 10);
		$this->fpdf->SetMargins(15,30,5);
		// $list =  $this->fitur_model->get_laka_detil($id_sprin,1)  ;
		$nama_wil= getNamaKewilayahan2('00-10-29');
		$this->fpdf->SetFont('ArialNarrow','',12);
		$this->fpdf->Ln();
		$this->fpdf->Cell(77,4,'KEPOLISIAN NEGARA REPUBLIK INDONESIA',0,0,'C');
		$this->fpdf->Ln();
		$this->fpdf->Cell(77,4,'DAERAH JAWA TIMUR',0,0,'C');
		$this->fpdf->Ln();
		$this->fpdf->Cell(77,4,$nama_wil,'B',0,'C');
		$this->fpdf->Ln(); 
		$this->fpdf->Image('./assets/img/logo_print.png',102,16,21);
		 
		  
		$this->fpdf->Ln(17);
		$this->fpdf->SetFont('ArialNarrow','',12); 
	 	$width = 195;
		$top = 'JURNAL SITUASI KAMTIBMAS SELAMA 1 X 24 JAM'; 

		if($awal == $akhir){
			$arr_tgl = explode('-', $awal);
			$bln = (int)$arr_tgl[1];
			$title_periode = convertTanggal($awal); 
			$title_date = $arr_tgl[2].' '.convertBulan($bln,false).' '.$arr_tgl[0]  ;
		}else{
			$arr_tgl_awal = explode('-', $awal);
			$bln_awal = (int)$arr_tgl_awal[1]; 

			$arr_tgl_akhir = explode('-', $akhir);
			$bln_akhir = (int)$arr_tgl_akhir[1];  

			if($bln_awal == $bln_akhir){
				$title_date = $arr_tgl_awal[2].' - '. $arr_tgl_akhir[2].' '.convertBulan($bln_akhir,false).' '.$arr_tgl_akhir[0]  ;
			}else{
				$title_date = $arr_tgl_awal[2].' '.convertBulan($bln_awal,false).' '.$arr_tgl_awal[0] .' - '. $arr_tgl_akhir[2].' '.convertBulan($bln_akhir,false).' '.$arr_tgl_akhir[0]  ;
			}

			
		}


		$bottom = 'Dari Tanggal '.$title_date  ;
		$width_t = round($this->fpdf->GetStringWidth($top));
		$width_b = round($this->fpdf->GetStringWidth($bottom));
		if($width_t > $width_b){
			$txtWidth = $width_t;
		}else{
			$txtWidth = $width_b;
		}
		$xs = $width - $txtWidth;
		// + margin left (15)
		$x = round($xs / 2) + 15;
		if($width_t > $width_b){
			$this->fpdf->SetFont('ArialNarrow','B',12);
			$this->fpdf->SetX($x);
			$this->fpdf->Cell($txtWidth,5,$top,'B',0,'C');
			$this->fpdf->SetFont('ArialNarrow','',12);
			$this->fpdf->Ln();
			$this->fpdf->Cell($width,6,$bottom,0,0,'C');
		}else{
			$this->fpdf->SetFont('ArialNarrow','B',12);
			$this->fpdf->Cell($width,5,$top,0,0,'C');
			$this->fpdf->SetFont('ArialNarrow','',12);
			$this->fpdf->Ln();
			$this->fpdf->SetX($x);
			$this->fpdf->Cell($txtWidth,6,$bottom,'T',0,'C');
		} 
		$this->fpdf->Ln(10);
		$this->fpdf->SetFont('ArialNarrow','',12);
		$this->fpdf->SetXY(15,$this->fpdf->getY());
		$this->fpdf->SetFont('ArialNarrow','',12); 

	    $this->fpdf->SetWidths(array(10,40,40,60,40));
	    $align_item 	= array('L','L','L','L','L');
	    $this->fpdf->SetFont('ArialNarrow','B',12);
	   
	    $arr_usr = $this->flexi_auth->get_user_by_identity_row_array(); 
		$jurnal = $this->bagops_model->get_jurnal($arr_usr,$awal,$akhir);
	    $this->fpdf->Row(array("NO.","KESATUAN","JENIS KEJADIAN","URAIAN  KEJADIAN","KET"),$align_item,9);
	    $i=1;
	      $this->fpdf->SetFont('ArialNarrow','',11);
	    foreach ($jurnal->result() as $row) {
	    	# code...
	    	 $this->fpdf->Row(array($i, $row->satwil,$row->jenis_kejadian,$row->uraian,$row->keterangan),$align_item,0);
	    }

 		
	    $this->fpdf->Ln();

	    $top = 'LAPORAN HARIAN TAHANAN SELAMA 1 X 24 JAM';
		$bottom = 'Dari Tanggal '.$title_date  ;
		$width_t = round($this->fpdf->GetStringWidth($top));
		$width_b = round($this->fpdf->GetStringWidth($bottom));
		if($width_t > $width_b){
			$txtWidth = $width_t;
		}else{
			$txtWidth = $width_b;
		}
		$xs = $width - $txtWidth;
		// + margin left (15)
		$x = round($xs / 2) + 15;
		if($width_t > $width_b){
			$this->fpdf->SetFont('ArialNarrow','B',12);
			$this->fpdf->SetX($x);
			$this->fpdf->Cell($txtWidth,5,$top,'B',0,'C');
			$this->fpdf->SetFont('ArialNarrow','',12);
			$this->fpdf->Ln();
			$this->fpdf->Cell($width,6,$bottom,0,0,'C');
		}else{
			$this->fpdf->SetFont('ArialNarrow','B',12);
			$this->fpdf->Cell($width,5,$top,0,0,'C');
			$this->fpdf->SetFont('ArialNarrow','',12);
			$this->fpdf->Ln();
			$this->fpdf->SetX($x);
			$this->fpdf->Cell($txtWidth,6,$bottom,'T',0,'C');
		} 
		$this->fpdf->Ln(10);
		$this->fpdf->SetFont('ArialNarrow','',12);
		$this->fpdf->SetXY(15,$this->fpdf->getY());
		$this->fpdf->SetFont('ArialNarrow','',12); 

	    // $this->fpdf->SetWidths(array(10,40,45,15,20,20,20,20));
	    // $align_item 	= array('C','C','C','C','C','C','C','C');
	    // $this->fpdf->SetFont('ArialNarrow','B',12);
	    // $this->fpdf->Row(array("NO.","Kesatuan","TAHANAN UMUM","JML","DEWASA","ANAK","POLRI","LAPAS"),$align_item,9);
	    // $this->fpdf->SetWidths(array(10,40,15,15,15,15,10,10,10,10,20,20)); 
	    // $align_item 	= array('C','L','C','C','C','C','C','C','C','C','C','C');
	    // $this->fpdf->SetFont('ArialNarrow','',11);
	    // $this->fpdf->Row(array("","","SISA","MSK","KLR","","LK","PR","LK","PR","",""),$align_item,9); 
	    // $i=1;
	    // $wilayah = $this->kewilayahan_model->get_data_kewilayahan($arr_usr);
	    // $this->fpdf->SetFont('ArialNarrow','',11); 
	    // foreach ($wilayah->result() as $row) {
	    // 	# code...
	    // 	$this->fpdf->Row(array( $i,$row->nama,0,0,0,0,0,0,0,0,0,0),$align_item,0); 
	    // 	$i++;
	    // }

	    
	 //    $this->fpdf->Row(array("","Jumlah","0","0","0","0","0","0","0","0","0","0"),$align_item,0);
	    
	 //    $this->fpdf->Ln(10);

	 //    $top = 'REKAPITULASI  PPGK  KHUSUS';
		// $bottom = 'Dari Tanggal '.$title_date  ;
		// $width_t = round($this->fpdf->GetStringWidth($top));
		// $width_b = round($this->fpdf->GetStringWidth($bottom));
		// if($width_t > $width_b){
		// 	$txtWidth = $width_t;
		// }else{
		// 	$txtWidth = $width_b;
		// }
		// $xs = $width - $txtWidth;
		// // + margin left (15)
		// $x = round($xs / 2) + 15;
		// if($width_t > $width_b){
		// 	$this->fpdf->SetFont('ArialNarrow','B',12);
		// 	$this->fpdf->SetX($x);
		// 	$this->fpdf->Cell($txtWidth,5,$top,'B',0,'C');
		// 	$this->fpdf->SetFont('ArialNarrow','',12);
		// 	$this->fpdf->Ln();
		// 	$this->fpdf->Cell($width,6,$bottom,0,0,'C');
		// }else{
		// 	$this->fpdf->SetFont('ArialNarrow','B',12);
		// 	$this->fpdf->Cell($width,5,$top,0,0,'C');
		// 	$this->fpdf->SetFont('ArialNarrow','',12);
		// 	$this->fpdf->Ln();
		// 	$this->fpdf->SetX($x);
		// 	$this->fpdf->Cell($txtWidth,6,$bottom,'T',0,'C');
		// } 
		// $this->fpdf->Ln(10);
		// $this->fpdf->SetFont('ArialNarrow','',12);
		// $this->fpdf->SetXY(15,$this->fpdf->getY());
		// $this->fpdf->SetFont('ArialNarrow','',12); 

	 //    $this->fpdf->SetWidths(array(10,40,40));
	 //    $align_item 	= array('L','L','L');
	 //    $this->fpdf->SetFont('ArialNarrow','B',12);
	 //    $this->fpdf->Row(array("NO.","JENIS","TOTAL" ),$align_item,9);


	 //    $this->fpdf->Ln(20);

	 //    $top 		= 'LAPORAN HARIAN TINDAK PIDANA BERDASARKAN PENGGOLONGAN KEJAHATAN';
		// $bottom 	= 'Dari Tanggal '.$title_date  ;
		// $width_t 	= round($this->fpdf->GetStringWidth($top));
		// $width_b 	= round($this->fpdf->GetStringWidth($bottom));
		// if($width_t > $width_b){
		// 	$txtWidth = $width_t;
		// }else{
		// 	$txtWidth = $width_b;
		// }
		// $xs = $width - $txtWidth;
		// // + margin left (15)
		// $x = round($xs / 2) + 15;
		// if($width_t > $width_b){
		// 	$this->fpdf->SetFont('ArialNarrow','B',12);
		// 	$this->fpdf->SetX($x);
		// 	$this->fpdf->Cell($txtWidth,5,$top,'B',0,'C');
		// 	$this->fpdf->SetFont('ArialNarrow','',12);
		// 	$this->fpdf->Ln();
		// 	$this->fpdf->Cell($width,6,$bottom,0,0,'C');
		// }else{
		// 	$this->fpdf->SetFont('ArialNarrow','B',12);
		// 	$this->fpdf->Cell($width,5,$top,0,0,'C');
		// 	$this->fpdf->SetFont('ArialNarrow','',12);
		// 	$this->fpdf->Ln();
		// 	$this->fpdf->SetX($x);
		// 	$this->fpdf->Cell($txtWidth,6,$bottom,'T',0,'C');
		// } 
		// $this->fpdf->Ln(20);
		// $this->fpdf->SetFont('ArialNarrow','',12);
		// $this->fpdf->SetXY(15,$this->fpdf->getY());
		// $this->fpdf->SetFont('ArialNarrow','',12); 

	 //    $this->fpdf->SetWidths(array(10,40,40));
	 //    $align_item 	= array('L','L','L');
	 //    $this->fpdf->SetFont('ArialNarrow','B',12);
	 //    $this->fpdf->Row(array("NO.","Jenis","TOTAL" ),$align_item,9);


	 //    $this->fpdf->Ln(20);

	 //    $top 		= 'REKAPITULASI  P P G K';
		// $bottom 	= 'Dari Tanggal '.$title_date  ;
		// $width_t 	= round($this->fpdf->GetStringWidth($top));
		// $width_b 	= round($this->fpdf->GetStringWidth($bottom));
		// if($width_t > $width_b){
		// 	$txtWidth = $width_t;
		// }else{
		// 	$txtWidth = $width_b;
		// }
		// $xs = $width - $txtWidth;
		// // + margin left (15)
		// $x = round($xs / 2) + 15;
		// if($width_t > $width_b){
		// 	$this->fpdf->SetFont('ArialNarrow','B',12);
		// 	$this->fpdf->SetX($x);
		// 	$this->fpdf->Cell($txtWidth,5,$top,'B',0,'C');
		// 	$this->fpdf->SetFont('ArialNarrow','',12);
		// 	$this->fpdf->Ln();
		// 	$this->fpdf->Cell($width,6,$bottom,0,0,'C');
		// }else{
		// 	$this->fpdf->SetFont('ArialNarrow','B',12);
		// 	$this->fpdf->Cell($width,5,$top,0,0,'C');
		// 	$this->fpdf->SetFont('ArialNarrow','',12);
		// 	$this->fpdf->Ln();
		// 	$this->fpdf->SetX($x);
		// 	$this->fpdf->Cell($txtWidth,6,$bottom,'T',0,'C');
		// } 
		// $this->fpdf->Ln(20);
		// $this->fpdf->SetFont('ArialNarrow','',12);
		// $this->fpdf->SetXY(15,$this->fpdf->getY());
		// $this->fpdf->SetFont('ArialNarrow','',12); 

	 //    $this->fpdf->SetWidths(array(10,40,40));
	 //    $align_item 	= array('L','L','L');
	 //    $this->fpdf->SetFont('ArialNarrow','B',12);
	 //    $this->fpdf->Row(array("NO.","JENIS","TOTAL" ),$align_item,9);

	 //    $this->fpdf->Ln(20);

	 //    $top 		= 'REKAPITULASI GANGGUAN KAMTIBMAS 1 X 24 JAM';
		// $bottom 	= 'Dari Tanggal '.$title_date  ;
		// $width_t 	= round($this->fpdf->GetStringWidth($top));
		// $width_b 	= round($this->fpdf->GetStringWidth($bottom));
		// if($width_t > $width_b){
		// 	$txtWidth = $width_t;
		// }else{
		// 	$txtWidth = $width_b;
		// }
		// $xs = $width - $txtWidth;
		// // + margin left (15)
		// $x = round($xs / 2) + 15;
		// if($width_t > $width_b){
		// 	$this->fpdf->SetFont('ArialNarrow','B',12);
		// 	$this->fpdf->SetX($x);
		// 	$this->fpdf->Cell($txtWidth,5,$top,'B',0,'C');
		// 	$this->fpdf->SetFont('ArialNarrow','',12);
		// 	$this->fpdf->Ln();
		// 	$this->fpdf->Cell($width,6,$bottom,0,0,'C');
		// }else{
		// 	$this->fpdf->SetFont('ArialNarrow','B',12);
		// 	$this->fpdf->Cell($width,5,$top,0,0,'C');
		// 	$this->fpdf->SetFont('ArialNarrow','',12);
		// 	$this->fpdf->Ln();
		// 	$this->fpdf->SetX($x);
		// 	$this->fpdf->Cell($txtWidth,6,$bottom,'T',0,'C');
		// } 
		// $this->fpdf->Ln(20);
		// $this->fpdf->SetFont('ArialNarrow','',12);
		// $this->fpdf->SetXY(15,$this->fpdf->getY());
		// $this->fpdf->SetFont('ArialNarrow','',12); 

	 //    $this->fpdf->SetWidths(array(10,40,20,20,20,40,20,20));
	 //    $align_item 	= array('C','C','C','C','C','C','C','C');
	 //    $this->fpdf->SetFont('ArialNarrow','B',12);
	 //    $this->fpdf->Row(array("NO.","KESATUAN","PPGK","PPGK KHUSUS" ,"LAKA LANTAS" ,"JUMLAH TAHANAN" ,"POLRI" ,"LAPAS" ),$align_item,9);

 
	 //    $this->fpdf->Ln();


	 //    $width = 92;
		// $txtWidth = round($this->fpdf->GetStringWidth('RAHMAN FAUZI, SH'));
		// $xs = $width - ($txtWidth + 7);
		// $x = round(($xs / 2) - $width);
		// $wz = $txtWidth - 30;
		// $this->fpdf->Ln(); 
		// $this->fpdf->SetFont('ArialNarrow','B',12);
		 
		// $this->fpdf->Ln();
		// $this->fpdf->SetX($x); 
		// $this->fpdf->Cell($txtWidth,5,"SURABAYA, ".date('d').' '. convertBulan(date('m')).' '.date('Y'),0,0,'C');
		// $this->fpdf->Ln();
		// $this->fpdf->Ln();
		// $this->fpdf->Ln();
		// $this->fpdf->Ln();
		// $this->fpdf->Ln();  
		// $width = 92;
		// $width_n = round($this->fpdf->GetStringWidth("RAHMAN FAUZI, SH"));
		// $width_p = round($this->fpdf->GetStringWidth("IPTU 08101986"));
		// if($width_n > $width_p){
		// 	$txtWidth = $width_n;
		// }else{
		// 	$txtWidth = $width_p;
		// }

		// $xs = $width - ($txtWidth + 7);
		// $x = round(($xs / 2) - $width);
		
		// if($width_n > $width_p){
		// 	$this->fpdf->SetX($x);
		// 	$this->fpdf->Cell($txtWidth,5,"RAHMAN FAUZI, SH",'B',0,'C');
		// 	$this->fpdf->Ln();
		// 	$this->fpdf->SetX(-92);
		// 	$this->fpdf->Cell(85,6,"IPTU".' NRP '."08101986",0,0,'C');
		// }else{
		// 	$this->fpdf->SetX(-92);
		// 	$this->fpdf->Cell(85,5,"RAHMAN FAUZI, SH",0,0,'C');
		// 	$this->fpdf->Ln();
		// 	$this->fpdf->SetX($x);
		// 	$this->fpdf->Cell($txtWidth,6,"IPTU".' NRP '."08101986",'T',0,'C');
		// }
		
		// $this->fpdf->Ln(-30);
		// $this->fpdf->SetFont('ArialNarrow','B',12); 
		 
		 
	 
		$this->fpdf->Output();
		 
	}

	function cetak_orang_asing($id){ 
		$this->load->library('fpdf_poltrait');
		$this->load->helper('wilayah');
		$this->load->model('kewilayahan_model'); 
		$this->load->model('fitur_model'); 
		$this->load->helper('words');
		$arr_usr 			= $this->flexi_auth->get_user_by_identity_row_array(); 
		$this->fpdf->SetAutoPageBreak(true, 10);
		$this->fpdf->SetMargins(15,30,5);
		// $list =  $this->fitur_model->get_laka_detil($id_sprin,1)  ;
		$nama_wil= getNamaKewilayahan2('00-10-29');
		$this->fpdf->SetFont('ArialNarrow','',12);
		$this->fpdf->Ln();
		$this->fpdf->Cell(77,4,'KEPOLISIAN NEGARA REPUBLIK INDONESIA',0,0,'C');
		$this->fpdf->Ln();
		$this->fpdf->Cell(77,4,'DAERAH JAWA TIMUR',0,0,'C');
		$this->fpdf->Ln();
		$this->fpdf->Cell(77,4,$nama_wil,'B',0,'C');
		$this->fpdf->Ln(); 
		$this->fpdf->Image('./assets/img/logo_print.png',102,16,21);


		$this->fpdf->Ln(17);
		$this->fpdf->SetFont('ArialNarrow','',14); 
	 	$width = 195;
		$top = 'SURAT TANDA MELAPOR'; 
		$bottom = 'SERTIFICATE OF POLICE REGESTRATION'   ;
		$width_t = round($this->fpdf->GetStringWidth($top));
		$width_b = round($this->fpdf->GetStringWidth($bottom));
		if($width_t > $width_b){
			$txtWidth = $width_t;
		}else{
			$txtWidth = $width_b;
		}
		$xs = $width - $txtWidth;
		// + margin left (15)
		$x = round($xs / 2) + 15;
		if($width_t > $width_b){
			$this->fpdf->SetFont('ArialNarrow','B',12);
			$this->fpdf->SetX($x);
			$this->fpdf->Cell($txtWidth,5,$top,'B',0,'C');
			$this->fpdf->SetFont('ArialNarrow','',12);
			$this->fpdf->Ln();
			$this->fpdf->Cell($width,6,$bottom,0,0,'C');
		}else{
			$this->fpdf->SetFont('ArialNarrow','B',12);
			$this->fpdf->Cell($width,5,$top,0,0,'C');
			$this->fpdf->SetFont('ArialNarrow','',12);
			$this->fpdf->Ln();
			$this->fpdf->SetX($x);
			$this->fpdf->Cell($txtWidth,6,$bottom,'T',0,'C');
		} 
		$this->fpdf->Ln(10);
		$this->fpdf->SetFont('ArialNarrow','',12);
		$this->fpdf->SetXY(15,$this->fpdf->getY());
		$this->fpdf->SetFont('ArialNarrow','',12); 
		 
		$oa = $this->fitur_model->det_orang_asing($id)->row();
		 
		$this->fpdf->Cell(20,5,'Nama','TL',0,'L',0);
		$this->fpdf->Cell(75,5,': '.$oa->nama_pelapor,'TR',0,'L',0);
		$this->fpdf->Cell(25,5,'Alamat','T',0,'L',0);
		$this->fpdf->Cell(70,5,': '.$oa->alamat_pelapor,'TR',0,'L',0);
 		$this->fpdf->Ln(); 

 		$this->fpdf->Cell(20,5,'Name','L',0,'L',0);
		$this->fpdf->Cell(75,5,' ','R',0,'L',0);
		$this->fpdf->Cell(25,5,'Address','',0,'L',0);
		$this->fpdf->Cell(70,5,' ','R',0,'L',0);
 		$this->fpdf->Ln(); 
 

 		$arr_tgl = explode(' ', $oa->tanggal_laporan);
 		$tanggal = explode('-', $arr_tgl[0]);
			// $bln = (int)$arr_tgl[1];
			// $title_periode = convertTanggal($awal); 
			// $title_date = $arr_tgl[2].' '.convertBulan($bln,false).' '.$arr_tgl[0]  ;


 		$this->fpdf->Cell(20,5,'Pekerjaan','TL',0,'L',0);
		$this->fpdf->Cell(75,5,': '.$oa->pekerjaan_pelapor,'TR',0,'L',0);
		$this->fpdf->Cell(25,5,'Pada hari','T',0,'L',0);
		$this->fpdf->Cell(70,5,': '.$oa->tanggal_laporan,'TR',0,'L',0);
 		$this->fpdf->Ln(); 

 		$this->fpdf->Cell(20,5,'Proffesion','L',0,'L',0);
		$this->fpdf->Cell(75,5,' ','R',0,'L',0);
		$this->fpdf->Cell(25,5,'Day','',0,'L',0);
		$this->fpdf->Cell(70,5,' ','R',0,'L',0);
 		$this->fpdf->Ln(); 

 		$this->fpdf->Cell(20,5,'Tanggal','TL',0,'L',0);
		$this->fpdf->Cell(75,5,': '.$arr_tgl[0],'TR',0,'L',0);
		$this->fpdf->Cell(25,5,'Jam','T',0,'L',0);
		$this->fpdf->Cell(70,5,': '.$arr_tgl[1],'TR',0,'L',0);
 		$this->fpdf->Ln(); 

 		$this->fpdf->Cell(20,5,'Date','BL',0,'L',0);
		$this->fpdf->Cell(75,5,' ','BR',0,'L',0);
		$this->fpdf->Cell(25,5,'Hour','B',0,'L',0);
		$this->fpdf->Cell(70,5,' ','BR',0,'L',0);
 		$this->fpdf->Ln(); 

		// $this->fpdf->Cell(20,5,'Name','BLR',0,'L',0);
		// $this->fpdf->Cell(75,5,'Ohang','T',0,'L',0);
		// $this->fpdf->Cell(25,5,'Address','BLR',0,'L',0);
 	// 	$this->fpdf->Ln();

 	// 	$this->fpdf->Cell(96,5,'Pekerjaan','LR',0,'L',0);
		// $this->fpdf->Cell(96,5,'Pada hari','LR',0,'L',0);
 	// 	$this->fpdf->Ln(); 
		// $this->fpdf->Cell(96,5,'Proffesion','BLR',0,'L',0);
		// $this->fpdf->Cell(96,5,'Day','BLR',0,'L',0);
 	// 	$this->fpdf->Ln();


 	// 	$this->fpdf->Cell(96,5,'Tanggal','LR',0,'L',0);
		// $this->fpdf->Cell(96,5,'Jam','LR',0,'L',0);
 	// 	$this->fpdf->Ln(); 
		// $this->fpdf->Cell(96,5,'Date','BLR',0,'L',0);
		// $this->fpdf->Cell(96,5,'Hour','BLR',0,'L',0);
 	// 	$this->fpdf->Ln();
		 
		  
        $this->fpdf->Ln();
        $this->fpdf->Ln();
        $this->fpdf->Ln();
		$this->fpdf->Output();

	}
	
}
