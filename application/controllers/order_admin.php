<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_admin extends MY_Controller {

	var $folder = "master_order";
	var $table 	= "penjualan";

	public function __construct(){
		parent::__construct();
		$this->load->model('model_crud');
		$this->load->library('upload');
	}

	public function index(){
		$this->data['title']		= "Order Admin";
		$this->data['body']			= $this->folder."/view";
		$this->data['styles']		= "includes/styles_master";
		$this->data['scripts']		= "includes/scripts_master";
		$this->data['page_bar'] 	= "includes/template_site_bar";
		$this->data['desc']			= "data order";
		$this->data['mMenuUtama']	= $this->mMenuUtama; 
		$this->data['message'] 		= $this->session->flashdata('message'); 
		$this->load->view('includes/template_site', $this->data);
	}

	public function ubah(){
		if(isset($_POST['submit'])) {
			$id 					= $this->input->post('id');
			$tanggal_pickup 		= $this->input->post('tanggal_pickup');
			$bag_no 				= $this->input->post('bag_no');
			$id_total 				= $this->input->post('id_total');
			$total 					= $this->input->post('total');
			$keterangan 			= $this->input->post('keterangan');
			$berat 					= $this->input->post('berat');

			if(!is_dir('uploads/timbangan/'.$id.'/')) {
				mkdir('uploads/timbangan/'.$id.'/',0777,TRUE);
			}

			$path = 'uploads/timbangan/'.$id.'/';
			$config['upload_path']		= $path;
			$config['allowed_types']	= 'jpg';
			$config['max_size']			= '1024';
			$config['file_ext_tolower']	= 'true';
			$config['file_name'] 		= strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_FILENAME)).'-'.date('d-m-Y');
			$this->upload->initialize($config);

			if(!empty($_FILES['attachement_hak']['name'])) {
				if(!$this->upload->do_upload('foto')) {
					$data['error'] 	= $this->upload->display_errors();
					// Redirect (idk)
					$data['record']		= $this->model_crud->get_one($this->table,'id',$id)->row_array();
					$data['title']		= "Ubah Order";
					$data['body']		= $this->folder."/form";
					$data['styles']		= "includes/styles_master";
					$data['scripts']	= "includes/scripts_master";
					$data['page_bar'] 	= "includes/template_site_bar";
					$data['desc']		= "ubah data order";
					$data['mMenuUtama']	= $this->mMenuUtama; 
					$data['message'] 	= $this->session->flashdata('message'); 
					$this->load->view('includes/template_site', $data);
				}
				else{
					if(!empty($_FILES['foto']['name'])) {
						$file_name = $this->upload->data('file_name');
						$data['foto'] = 'timbangan/'.$id.'/'.$file_name['file_name'];
					}
				}
			}
				
			$data = array(	'tanggal_pickup'	=> $tanggal_pickup,
							'bag_no'			=> $bag_no,
							'keterangan'		=> $keterangan,
							'berat_timbangan'	=> $berat);

			// Input ke Database
			$this->db->trans_start();
				// Update t_penjualan
				$this->model_crud->update_data($this->table,$data,'id',$id);
				// Update t_penjualan_item
				for($i = 0; $i < count($id_total); $i++) {
					$data2 	= array('total'=>$total[$i]);
					$this->model_crud->update_data('penjualan_item',$data2,'id',$id_total[$i]);
				}
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE){
				echo "Gagal Input Data !";
				die;
			}
			else{
				$this->session->set_flashdata('msg_success',' Ubah data order berhasil.');
				redirect('order_admin');
			}	
		}
		else {
			$id					= $this->uri->segment(4);
			$data['record']		= $this->model_crud->get_one($this->table,'id',$id)->row_array();
			$data['title']		= "Ubah Order";
			$data['body']		= $this->folder."/form";
			$data['styles']		= "includes/styles_master";
			$data['scripts']	= "includes/scripts_master";
			$data['page_bar'] 	= "includes/template_site_bar";
			$data['desc']		= "ubah data order";
			$data['mMenuUtama']	= $this->mMenuUtama; 
			$data['message'] 	= $this->session->flashdata('message'); 
			$this->load->view('includes/template_site', $data);
		}
	}

	public function lihat(){
		$id					= $this->uri->segment(4);
		$data['record']		= $this->model_crud->get_one($this->table,'id',$id)->row_array();
		$data['title']		= "Detail Order";
		$data['body']		= $this->folder."/lihat";
		$data['styles']		= "includes/styles_master";
		$data['scripts']	= "includes/scripts_master";
		$data['page_bar'] 	= "includes/template_site_bar";
		$data['desc']		= "lihat detail order";
		$data['mMenuUtama']	= $this->mMenuUtama; 
		$data['message'] 	= $this->session->flashdata('message'); 
		$this->load->view('includes/template_site', $data);
	}

	public function json(){
		$this->datatables->select('penjualan.id as id, no_penjualan,tanggal_penjualan,nama_pelanggan,tipe_pembayaran,total_pembayaran,total_penjualan,total_diskon,keterangan,berat_timbangan,status_order.nama as status,status_order.id as orderid, (SELECT id FROM status_order ORDER BY id DESC LIMIT 1) as lastid, personil.id as personid');
		$this->datatables->add_column('action',
			anchor('data/order/lihat/$1','Lihat',array('class'=>'btn btn-success btn-xs')).'&nbsp;'.
			anchor('data/order/ubah/$1','Edit',array('class'=>'btn btn-warning btn-xs'))
			,'id');
		$this->datatables->from($this->table);
		$this->datatables->join('pelanggan','pelanggan.id = penjualan.pelanggan_id','left');
		$this->datatables->join('personil','personil.id = penjualan.personil_id','left');
		$this->datatables->join('status_order','status_order.id = penjualan.status','left');
		if (!empty($_POST['tanggal'])) {
			$tanggal = explode('-', $_POST['tanggal']);
			$start = date("Y-m-d", strtotime($tanggal[0]));
			$end = date("Y-m-d", strtotime($tanggal[1]));
			$this->datatables->where("date(tanggal_penjualan) BETWEEN '".$start."' AND '".$end."'");
		}
		return print_r($this->datatables->generate());
	}

	public function updatestats(){
		if(isset($_POST['submit'])) {
			$id 		= $this->input->post('id');
			$keterangan = $this->input->post('keterangan');
			$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
			$nik = $arr_usr['nik'];
			$temp = $this->model_crud->get_one('personil','nik',$nik)->row_array();
			$personil = $temp['id'];
			$record = $this->model_crud->get_one($this->table,'id',$id)->row_array();
			$temp_status = $record['status'];
			$status = $temp_status+1;
			$data	= array('status' =>	$status, 'keterangan' => $keterangan);
			$this->model_crud->update_data($this->table,$data,'id',$id);
			$data	= array('penjualan_id' => $record['no_penjualan'],
							'personil_id' => $personil,
							'status' => $temp_status,
							'tanggal_history' => date('Y-m-d H:i:s'));
			$this->model_crud->create_data('history_penjualan',$data);
			$this->session->set_flashdata('msg_success',' Status telah berhasil di perbarui.');
			redirect('order_admin');
		}
		else{
			$id					= $this->uri->segment(4);
			$data['record']		= $this->model_crud->get_one($this->table,'id',$id)->row_array();
			$data['title']		= "Detail Order";
			$data['body']		= $this->folder."/lihat";
			$data['styles']		= "includes/styles_master";
			$data['scripts']	= "includes/scripts_master";
			$data['page_bar'] 	= "includes/template_site_bar";
			$data['desc']		= "lihat detail order";
			$data['mMenuUtama']	= $this->mMenuUtama; 
			$data['message'] 	= $this->session->flashdata('message'); 
			$this->load->view('includes/template_site', $data);
		}
	}
}
?>