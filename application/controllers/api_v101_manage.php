<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_v101_manage extends CI_Controller {
 
	function __construct()
	{
		parent::__construct();
		$this->load->model('kewilayahan_model');
		// $this->load->library('gcm');
		$this->load->model('Api_v101_manage_m');
		// $this->load->helper('mobile_svc');
		// $this->load->helper('words');
		// $this->load->helper('image');
		
	}
	
	/* */

	function get_list_satwil()
	{
		$data_login=array();
		$data_login['kode_satwil'] = '00-10';
		$result = $this->kewilayahan_model->get_data_kewilayahan($data_login);
		$opt = $result->result_array();
		
		$res = array('options' => $opt);
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	function submit_permohonan(){
		
		date_default_timezone_set('Asia/Jakarta');
		$today = date("Y-m-d H:i:s"); 
		
		$data['status']="sukses";
		$data['error']="";

		$no_ktp = $this->input->post('no_ktp') == '' ? null : $this->input->post('no_ktp');
		$kode_wilayah = trim($this->input->post('kode_wilayah')) == '' ? null : trim($this->input->post('kode_wilayah'));
		$nama = trim($this->input->post('nama')) == '' ? null : trim($this->input->post('nama'));
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kelamin = trim($this->input->post('kelamin')) == '' ? null : trim($this->input->post('kelamin'));
		$agama = trim($this->input->post('agama')) == '' ? null : trim($this->input->post('agama'));
		$golongan_darah = trim($this->input->post('golongan_darah')) == '' ? null : trim($this->input->post('golongan_darah'));
		$pendidikan = trim($this->input->post('pendidikan')) == '' ? null : trim($this->input->post('pendidikan'));
		$pekerjaan = trim($this->input->post('pekerjaan')) == '' ? null : trim($this->input->post('pekerjaan'));

		$list = array(
				'no_ktp' => $no_ktp,
				'kode_wilayah' => $kode_wilayah,
				'nama' => $nama,
				'email' => $email,
				'kelamin' => $kelamin,
				'agama' => $agama,
				'golongan_darah' => $golongan_darah,
				'pendidikan' => $pendidikan,
				'pekerjaan' => $pekerjaan,
				'reg_date' => $today
			);

		$last_id = $this->Api_v101_manage_m->do_add_pemohon($list);
		if(!$last_id){
			$data["status"]="error";
			$data["error"]="<p>Data Gagal Disimpan</p>";
		}else{
			$sim_id = trim($this->input->post('sim_id')) == '' ? null : trim($this->input->post('sim_id'));
			$tinggi = trim($this->input->post('tinggi')) == '' ? null : trim($this->input->post('tinggi'));
			$berat = trim($this->input->post('berat')) == '' ? null : trim($this->input->post('berat'));
			$tekanan_darah = trim($this->input->post('tekanan_darah')) == '' ? null : trim($this->input->post('tekanan_darah'));
			$buta_warna = trim($this->input->post('buta_warna')) == '' ? null : trim($this->input->post('buta_warna'));
			$hasil_analisa = trim($this->input->post('hasil_analisa')) == '' ? null : trim($this->input->post('hasil_analisa'));
			$lulus = trim($this->input->post('lulus')) == '' ? null : trim($this->input->post('lulus'));
			$noApi = $this->queue_exec();

			$push = array(
					'ref_id' => $last_id,
					'dokter_id' => 15,
					'sim_id' => $sim_id,
					'noApi' => $noApi,
					'kode_wilayah' => $kode_wilayah,
					'tinggi' => $tinggi,
					'berat' => $berat,
					'tekanan_darah' => $tekanan_darah,
					'buta_warna' => $buta_warna,
					'hasil_analisa' => $hasil_analisa,
					'lulus' => $lulus,
					'created_time' => $today,
					// 'expired_date' => $today,
					'expired_date' =>  date('Y-m-d H:i:s',strtotime('+30 days',strtotime($today))) . PHP_EOL,
				);

			$result = $this->Api_v101_manage_m->do_add_pemohon_trx($push);			
			if(!$result){
				$data["status"]="error";
				$data["error"]="<p>Transaksi Gagal Disimpan</p>";
			}else{
				$data['tgl_trx'] = $today;
				$data['id_trx'] = 'A001';
				$data['mercent_id'] = '0000001';
				$data['amount'] = '25000';
				$data['noApi'] = $noApi;
			}
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($data, JSON_PRETTY_PRINT);

		// if($showcase!=NULL)
		// {
		// $no_ktp = $this->input->post('no_ktp');
		// 	$result['status'] = "OK";
		// 	$result['message'] = "Success";
		// 	$result['tgl_trx'] = date('Y-m-d H:i:s');
		// 	$result['id_trx'] = '5552525299';
		// 	$result['mercent_id'] = '0000001';
		// 	$result['amount'] = '25000';
		// 	$result['no_ktp'] = $no_ktp;
		// 	$result['noApi'] = 'A2TWG88';
		// // } 
		// // else 
		// // // {
		// // 	$result['status'] = "NULL";
		// 	// $result['message'] = "Data kosong";
		// // }
		
		// header('Cache-Control: no-cache, must-revalidate');
		// header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		// header('Content-type: application/json');
		// header("access-control-allow-origin: *");
		// echo json_encode($result);
	}

	function queue_exec(){
		$char = 'ABCDEFGHJKLMNPQRSTUVWXY2345678923456789';
		$queue_no = '';
		
		for($i=0; $i < 6; $i++){
			$pos = rand(0, strlen($char)-1);
			$queue_no .= $char{$pos};
		}
		
		$q = $this->db->query("SELECT * FROM Api_pemohon_trx WHERE noApi='". $queue_no ."'");
		if($q->num_rows() > 0){
			$this->queue_exec();
		}else{
			return $queue_no;
		}
	}

	function upload_photo()
	{
		//Directory where uploaded images are saved
	 	$dirname = "./uploads/"; 
		
		// If uploading file
		if ($_FILES) {
 	  		// print_r($_FILES);
		  	mkdir ($dirname, 0777, true);
		  	move_uploaded_file($_FILES["file"]["tmp_name"],$dirname."/".$_FILES["file"]["name"]);
		}
	}
	
}	