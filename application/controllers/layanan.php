<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Layanan extends MY_Controller {

	var $folder = "master_layanan";
	var $table 	= "layanan";

	public function __construct(){
		parent::__construct();
		$this->load->model('model_crud');
	}

	function index(){
		$this->data['title']		= "Layanan";
		$this->data['body']			= $this->folder."/view";
		$this->data['styles']		= "includes/styles_master";
		$this->data['scripts']		= "includes/scripts_master";
		$this->data['page_bar'] 	= "includes/template_site_bar";
		$this->data['desc']			= "data layanan";
		$this->data['mMenuUtama']	= $this->mMenuUtama; 
		$this->data['message'] 		= $this->session->flashdata('message'); 
		$this->load->view('includes/template_site', $this->data);
	}

	public function tambah(){
		if(isset($_POST['submit'])) {
			$name 			= $this->input->post('name');
			$price			= $this->input->post('price');
			$status			= $this->input->post('status');
			$data	= array('name'	=>	$name,
				'price'			=>	$price,
				'status'			=> $status);
			$this->model_crud->create_data($this->table,$data);
			$this->session->set_flashdata('msg_success',' Tambah data layanan " '.$name.' " berhasil.');
			redirect('layanan');
		}
		else {
			$this->data['title']		= "Tambah Layanan";
			$this->data['body']			= $this->folder."/form";
			$this->data['styles']		= "includes/styles_master";
			$this->data['scripts']		= "includes/scripts_master";
			$this->data['page_bar'] 	= "includes/template_site_bar";
			$this->data['desc']			= "tambah data layanan";
			$this->data['mMenuUtama']	= $this->mMenuUtama; 
			$this->data['message'] 		= $this->session->flashdata('message'); 
			$this->data['isi'] =  $this->model_crud->get_record('id, nama','status_order','','','','','');

			$this->load->view('includes/template_site', $this->data);
		}
	}

	public function ubah(){
		if(isset($_POST['submit'])) {
			$id 			= $this->input->post('id');
			$name 			= $this->input->post('name');
			$price			= $this->input->post('price');
			$status			= $this->input->post('status');
			$data	= array('name'	=>	$name,
				'price'			=>	$price,
				'status'			=> $status);
			$this->model_crud->update_data($this->table,$data,'id',$id);
			$this->session->set_flashdata('msg_success',' Ubah data layanan " '.$name.' " berhasil.');
			redirect('layanan');
		}
		else {
			$id					= $this->uri->segment(4);
			$data['record']		= $this->model_crud->get_one($this->table,'id',$id)->row_array();
			$data['title']		= "Ubah Layanan";
			$data['body']		= $this->folder."/form";
			$data['styles']		= "includes/styles_master";
			$data['scripts']	= "includes/scripts_master";
			$data['page_bar'] 	= "includes/template_site_bar";
			$data['desc']		= "ubah data layanan";
			$data['mMenuUtama']	= $this->mMenuUtama; 
			$data['message'] 	= $this->session->flashdata('message');
			$data['isi'] =  $this->model_crud->get_record('id, nama','status_order','','','','','');

			$this->load->view('includes/template_site', $data);
		}
	}

	public function hapus(){
		$id = $this->uri->segment(4);
		$data = array('status' => 3);
		$this->model_crud->update_data($this->table,$data,'id',$id);
		// $this->model_crud->delete_data($this->table,'id',$id);
		$this->session->set_flashdata('msg_success','Data layanan berhasil dihapus.');
		redirect('master_data/layanan');
	}

	public function json(){
		$this->datatables->select('id, name, price, status');
		$this->datatables->add_column('action',anchor('master_data/layanan/ubah/$1','Edit',array('class'=>'btn btn-warning btn-xs')).'&nbsp'.anchor('master_data/layanan/hapus/$1','Delete',array('class'=>'btn btn-danger btn-xs',"onclick"=>"return confirm('Anda yakin ingin menghapus?')")),'id');
		$this->datatables->from($this->table);
		$this->datatables->where('status != 3');
		return print_r($this->datatables->generate());
	}


}

/* End of file layanan.php */
/* Location: ./application/controllers/layanan.php */