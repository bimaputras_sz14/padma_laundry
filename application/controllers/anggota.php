<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anggota extends MY_Controller {
 
	function __construct()
	{
		parent::__construct();
		// if (!$this->ion_auth->logged_in())
		// {
			// redirect(site_url().'auth/login');
		// }
		$this->load->model('anggota_model');
		// $this->load->model('kewilayahan_model');
		$this->load->model('m_master');
		$this->load->helper('anggota');
		$this->load->helper('wilayah');
	}
	
	function index()
    {
		$this->anggota();
	}
	
	function Anggota()
    {
		$wilayah = null;
		$this->data['title']		= "anggota";
		$this->data['body']			= "anggota/index";
		$this->data['styles']		= "includes/styles_data_table";
		$this->data['scripts']		= "anggota/scripts_anggota";
		$this->data['page_bar'] 	= "includes/template_site_bar";
		$this->data['desc']			= "data anggota";
		$this->data['mMenuUtama']	= $this->mMenuUtama;
		$this->data['wilayah'] 		= getAllWilayah($wilayah);
		$this->data['kesatuan'] 	= getAllKesatuan(null);
		$this->data['pangkat']		= getAllPangkat();
		$this->data['message'] 		= $this->session->flashdata('message');
		$this->load->view('includes/template_site', $this->data);
	}
	
	function data_anggota(){
		$json=array(); 
		$json['data'] = array();
		$data=$this->m_master->get_table_noresult('anggota');
		if ($data->num_rows() > 0){
			$i = 1;
				foreach ($data->result() as $row){
					$btn='<button title="Edit Data" onclick="edit_anggota('.$row->id.')" class="btn btn-edit btn-sm blue"><i class="icon-note"></i></button><button onclick="hapus(\'direktori_kota\',\'id-'.$row->id.'\');"  title="Hapus Data" role="button" class="btn btn-sm red"><i class="icon-trash"></i></button>';
					$json['data'][] = array(
												"no"=>$i,
												"nrp"=>$row->nrp,
												"nama"=>$row->nama,
												"pangkat"=>$row->pangkat,
												"jabatan"=>getJabatan($row->jabatan_id),
												"satwil"=>$row->wilayah,
												"satuan"=>getKodeKesatuan($row->kesatuan_id),
												"unit"=>$row->unitkerja_id == null ? '' : getKodeUnit($row->unitkerja_id),
												"status"=>$row->status == 1 ? 'Aktif' : 'Non-aktif',
												"btn"=>$btn
											);
					$i++;
				}
		}
		echo json_encode($json);
	}
	
	function get_kesatuan_options()
	{
		$id = trim($this->input->post('id')) == '' ? 0 : trim($this->input->post('id'));
		$scope = getWilayahScope($id);
		$kesatuan = getAllKesatuan($scope);
		$res = array(
			'result' => 'success',
			'options' => $kesatuan
		);
		//
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($res);
	}
	
	/* */
	
	function get_unit_options()
	{
		$id = trim($this->input->post('id')) == '' ? 0 : trim($this->input->post('id'));
		$unit = getAllUnit($id);
		$jabatan = getAllJabatan($id);
		$res = array(
			'result' => 'success',
			'unit' => $unit,
			'jabatan' => $jabatan
		);
		//
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($res);
	}
	
	public function insert_anggota()
	{
		$data['status']="sukses";
		$data['error']="";
		$validation_rules = array(
			array('field' => 'wilayah', 'label' => 'Wilayah','rules' => 'required'),
			array('field' => 'kesatuan', 'label' => 'Kesatuan','rules' => 'required'),
			array('field' => 'jabatan', 'label' => 'Jabatan','rules' => 'required'),
			// array('field' => 'unit', 'label' => 'Unit Kerja','rules' => 'required'),
			array('field' => 'pangkat', 'label' => 'Pangkat','rules' => 'required'),
			array('field' => 'nrp', 'label' => 'NRP','rules' => 'required|numeric|is_unique[anggota.nrp]'),
			array('field' => 'nama', 'label' => 'Nama','rules' => 'required'),
			// array('field' => 'tgl_lahir', 'label' => 'Jabatan','rules' => 'required'),
			array('field' => 'nohp', 'label' => 'No HP','rules' => 'numeric'),
			// array('field' => 'email', 'label' => 'email','rules' => 'required'),
			array('field' => 'smartfren', 'label' => 'No Smartfren','rules' => 'numeric')
		);
		$this->form_validation->set_rules($validation_rules);
		
		if(!$this->form_validation->run()){
				$data["status"]="error";
				$data["error"]=validation_errors();				
		}
		
			if($data["status"]=="sukses"){
				$wilayah = $this->input->post('wilayah') == '' ? null : $this->input->post('wilayah');
				$kesatuan = $this->input->post('kesatuan') == '' ? null : $this->input->post('kesatuan');
				$unit = $this->input->post('unit') == '' ? null : $this->input->post('unit');
				$jabatan = $this->input->post('jabatan') == '' ? null : $this->input->post('jabatan');
				$pangkat = trim($this->input->post('pangkat')) == '' ? null : trim($this->input->post('pangkat'));
				$nrp = trim($this->input->post('nrp')) == '' ? null : trim($this->input->post('nrp'));
				$nama = trim($this->input->post('nama')) == '' ? null : trim($this->input->post('nama'));
				$tgl_lahir = trim($this->input->post('tgl_lahir')) == '' ? null : trim($this->input->post('tgl_lahir'));
				$smartfren = trim($this->input->post('smartfren')) == '' ? null : trim($this->input->post('smartfren'));
				$telepon = trim($this->input->post('nohp')) == '' ? null : trim($this->input->post('nohp'));
				$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
				$status = trim($this->input->post('status')) == '' ? 0 : 1;
				
								
					$data = array(
							'nama' => $nama,
							'nrp' => $nrp,
							'dob' => $tgl_lahir,
							'pangkat' => $pangkat,
							'wilayah_id' => $wilayah,
							'wilayah' => getNamaKewilayahan($wilayah),
							'kesatuan_id' => $kesatuan,
							'unitkerja_id' => $unit,
							'jabatan_id' => $jabatan,
							'telepon' => $telepon,
							'email' => $email,
							'smartfren' => $smartfren,
							'status' => $status
						);
					if(!$this->anggota_model->do_add_anggota($data))
					{
						$data["status"]="error";
						$data["error"]="<p>Data Gagal Disimpan</p>";	
					}
					// $data['error']=$jabatan.' pada kesatuan '.getKodeKesatuan($kesatuan).' berhasil di simpan di database';
				
			}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($data);
	}
}
?>