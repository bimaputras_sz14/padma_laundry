<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk extends MY_Controller {

	var $folder = "master_produk";
	var $table 	= "produk";

	public function __construct(){
		parent::__construct();
		$this->load->model('model_crud');
		$this->load->library('upload');
		$this->load->helper('my_helper');
	}

	function index(){
		// redirect('master_data/produk');
		$this->data['title']		= "Produk";
		$this->data['body']			= $this->folder."/view";
		$this->data['styles']		= "includes/styles_master";
		$this->data['scripts']		= "includes/scripts_master";
		$this->data['page_bar'] 	= "includes/template_site_bar";
		$this->data['desc']			= "data produk";
		$this->data['mMenuUtama']	= $this->mMenuUtama; 
		$this->data['message'] 		= $this->session->flashdata('message'); 
		$this->load->view('includes/template_site', $this->data);
	}


	public function tambah(){
		if(isset($_POST['submit'])) {
			$nama 			= $this->input->post('nama');
			$harga			= $this->input->post('harga');
//Config file upload
			if(!is_dir('uploads/produk/'.$nama.'/')) {
				mkdir('uploads/produk/'.$nama.'/',0777,TRUE);
			}
			$path = 'uploads/produk/'.$nama.'/';
			$config['upload_path']		= $path;
			$config['allowed_types']	= 'gif|jpg|png';
			$config['max_size']			= '1024';
			$config['file_ext_tolower']	= 'true';
			$config['file_name'] 		= strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_FILENAME)).'-foto';
			$this->upload->initialize($config);
			$data	= array('nama_produk'	=>	$nama,
							'harga'			=>	$harga);
//Cek file upload
			if(!$this->upload->do_upload('foto')) {
				$data['error'] 	= $this->upload->display_errors();
				// Redirect (idk)
				$data['title']		= "Tambah Produk";
				$data['body']			= $this->folder."/form";
				$data['styles']		= "includes/styles_master";
				$data['scripts']		= "includes/scripts_master";
				$data['page_bar'] 	= "includes/template_site_bar";
				$data['desc']			= "tambah data produk";
				$data['mMenuUtama']	= $this->mMenuUtama; 
				$data['message'] 		= $this->session->flashdata('message'); 
				$this->load->view('includes/template_site', $data);
			}
			else {
//Cek nama produk jika ada yang sama
				$record = $this->db->select('nama_produk')
									->from($this->table)
									->where('nama_produk = ',$nama)
									->get();
				foreach($record->result() as $r) {
					if($r->nama_produk == $nama) {
						$this->session->set_flashdata('msg_failed','Nama produk " '.$nama.' " telah digunakan.');
						redirect('produk');
					}
				}
				if(!empty($_FILES['foto']['name'])) {
					$file_name = $this->upload->data('file_name');
					$data['foto'] = 'produk/'.$nama.'/'.$file_name['file_name'];
				}
				$this->model_crud->create_data($this->table,$data);
				$this->session->set_flashdata('msg_success',' Tambah data produk " '.$nama.' " berhasil.');
				redirect('produk');
			}
		}
		else {
			$this->data['title']		= "Tambah Produk";
			$this->data['body']			= $this->folder."/form";
			$this->data['styles']		= "includes/styles_master";
			$this->data['scripts']		= "includes/scripts_master";
			$this->data['page_bar'] 	= "includes/template_site_bar";
			$this->data['desc']			= "tambah data produk";
			$this->data['mMenuUtama']	= $this->mMenuUtama; 
			$this->data['message'] 		= $this->session->flashdata('message'); 
			$this->load->view('includes/template_site', $this->data);
		}
	}

	public function ubah(){
		if(isset($_POST['submit'])) {
			$id 			= $this->input->post('id');
			$nama 			= $this->input->post('nama');
			$harga			= $this->input->post('harga');
			$tmpname		= $this->input->post('tmpnama');
			$img 			= $this->input->post('img');
//Config file upload
			if(!is_dir('uploads/produk/'.$nama.'/')) {
				mkdir('uploads/produk/'.$nama.'/',0777,TRUE);
			}
			$path = 'uploads/produk/'.$nama.'/';
			$config['upload_path']		= $path;
			$config['allowed_types']	= 'gif|jpg|png';
			$config['max_size']			= '1024';
			$config['file_ext_tolower']	= 'true';
			$config['file_name'] 		= strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_FILENAME)).'-foto';
			$this->upload->initialize($config);
			$data	= array('nama_produk'	=>	$nama,
							'harga'			=>	$harga);
//Send file upload
			if(!empty($_FILES['foto']['name'])) {
				if(!$this->upload->do_upload('foto')) {
					$data['error'] 		= $this->upload->display_errors();
					// Redirect (idk)
					$data['title']		= "Tambah Produk";
					$data['body']		= $this->folder."/form";
					$data['styles']		= "includes/styles_master";
					$data['scripts']	= "includes/scripts_master";
					$data['page_bar'] 	= "includes/template_site_bar";
					$data['desc']		= "tambah data produk";
					$data['mMenuUtama']	= $this->mMenuUtama; 
					$data['message'] 	= $this->session->flashdata('message'); 
					$this->load->view('includes/template_site', $data);
				}
				else {
					$file_name = $this->upload->data('file_name');
					$data['foto'] = 'produk/'.$nama.'/'.$file_name['file_name'];
					$img = 'uploads/'.$img;
					if ($img != 'uploads/'.$data['foto'] AND file_exists($img)) {
						unlink($img);
					}
				}
			}
			$this->model_crud->update_data($this->table,$data,'id',$id);
			$this->session->set_flashdata('msg_success',' Ubah data produk " '.$nama.' " berhasil.');
			redirect('produk');
		}
		else {
			$id					= $this->uri->segment(4);
			$data['record']		= $this->model_crud->get_one($this->table,'id',$id)->row_array();
			$data['title']		= "Ubah Produk";
			$data['body']		= $this->folder."/form";
			$data['styles']		= "includes/styles_master";
			$data['scripts']	= "includes/scripts_master";
			$data['page_bar'] 	= "includes/template_site_bar";
			$data['desc']		= "ubah data produk";
			$data['mMenuUtama']	= $this->mMenuUtama; 
			$data['message'] 	= $this->session->flashdata('message'); 
			$this->load->view('includes/template_site', $data);
		}
	}

	public function hapus(){
		$id = $this->uri->segment(3);
		delete_image($id);
		$this->model_crud->delete_data($this->table,'id',$id);
		$this->session->set_flashdata('msg_success','Data produk berhasil dihapus.');
		redirect('produk');
	}

	public function json(){
		$this->datatables->select('id,nama_produk,harga,foto');
		$this->datatables->add_column('action',anchor('master_data/produk/ubah/$1','Edit',array('class'=>'btn btn-warning btn-xs')).'&nbsp'.anchor('produk/hapus/$1','Delete',array('class'=>'btn btn-danger btn-xs',"onclick"=>"return confirm('Anda yakin ingin menghapus?')")),'id');
		$this->datatables->from($this->table);
		return print_r($this->datatables->generate());
	}
}

/* End of file produk.php */
/* Location: ./application/controllers/produk.php */