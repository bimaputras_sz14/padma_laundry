<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu extends MY_Controller {
 
	function __construct()
	{
		parent::__construct();
		$this->load->model('administrator_model');

	}

	function index()
    { 
		$this->data['title']		= "Pengaturan Menu";
		$this->data['body']			= "administrator/menu";
		$this->data['styles']		= "includes/styles_data_table";
		$this->data['scripts']		= "administrator/scripts_administrator";
		$this->data['page_bar'] 	= "includes/template_site_bar";
		$this->data['desc']			= "data pengaturan menu";
		$this->data['mMenuUtama']	= $this->mMenuUtama; 
		$this->data['module'] 		= $this->administrator_model->get_menu();
		$this->data['message'] 		= $this->session->flashdata('message'); 
		$this->load->view('includes/template_site', $this->data);
	}

	function get_data(){
		$json=array(); 
		$json['data'] = array();
		// $kat_barang = $this->input->post('kat_barang');
		$data = $this->administrator_model->get_data_menu();
		if ($data->num_rows() > 0){
			$i = 1;
				foreach ($data->result() as $row){
					$btn='<button title="Edit Data" onclick="edit_menu(\''. $row->id .'\')" class="btn btn-edit btn-sm blue"><i class="icon-note"></i></button>';
					$btn.='<button onclick="hapus_menu(\''. $row->id .'\');"  title="Hapus Data" role="button" class="btn btn-sm red"><i class="icon-trash"></i></button>';
					
					$json['data'][] = array(
											"no"=>'<center>'. $i .'</center>',
											"title"=>$row->title,  
											"group"=>isset($row->groupname)?$row->groupname:'Parent',
											"url"=>$row->url,
											"name"=>$row->name,
											"btn"=>$btn
										);
					$i++;
				}
		}
		echo json_encode($json);
	}

	function insert(){
		$data['status']="sukses";
		$data['error']="";
		$validation_rules = array(
			array('field' => 'title', 'label' => 'Nama Menu','rules' => 'required'),
			array('field' => 'url', 'label' => 'URL Menu','rules' => 'required'), 
		);
		$this->form_validation->set_rules($validation_rules);
		
		if(!$this->form_validation->run()){
				$data["status"]="error";
				$data["error"]=validation_errors();				
		}
		
		if($data["status"]=="sukses"){
			$title = $this->input->post('title');
			if($title){
				$name = str_replace(' ', '', strtolower($title));
			}
			$url = $this->input->post('url');
			$status =  $this->input->post('status') ;
			if($status){
				$status = 0;
			}
			$icon = $this->input->post('icon');
			$parent_id	 = $this->input->post('parent_id');
			if($parent_id){
				$sequence = 1; 
			}else{
				$sequence = 1; 
			}
			
			// $harga_satuan = $this->input->post('harga_satuan');
			
			$data = array(
				'name' => $name,
				'url' => $url,
				'icon' => $icon,
				'parent_id' => $parent_id,
				'title' => $title,
				'sequence' => $sequence,
				'hide' => $status
				 
			);
			
			if(!$this->administrator_model->add_menu($data))
			{
				$data["status"]="error";
				$data["error"]="<p>Nama Menu sudah ada!</p>";	
			}
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($data);
	}
	
	function get_menu_detail($id=null){
		$id = $id == '' ? null : $id;
		//
		if($info = $this->administrator_model->get_menu_detail($id))
		{
			$res = array(
				'result' => 'success',
				'list' => $info
			);
		}
		else
		{
			$res = array(
				'result' => 'error',
				'msg' => 'Data tidak ditemukan'
			);				
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($res);
	}
	
	function update(){
		$data['status']="sukses";
		$data['error']="";
		$validation_rules = array(
			array('field' => 'title', 'label' => 'Nama Menu','rules' => 'required'), 
		);
		$this->form_validation->set_rules($validation_rules);
		
		if(!$this->form_validation->run()){
				$data["status"]="error";
				$data["error"]=validation_errors();				
		}
		
		if($data["status"]=="sukses"){
			$id = $this->input->post('id');
			$title = $this->input->post('title');
			if($title){
				$name = str_replace(' ', '', strtolower($title));
			}
			$url = $this->input->post('url');
			$status =  $this->input->post('status') ;
			if($status){
				$status = 0;
			}
			$icon = $this->input->post('icon');
			$parent_id	 = $this->input->post('parent_id');
			if($parent_id){
				$sequence = 1; 
			}else{
				$sequence = 1; 
			}
			
			// $harga_satuan = $this->input->post('harga_satuan');
			
			$data = array(
				'name' => $name,
				'url' => $url,
				'icon' => $icon,
				'parent_id' => $parent_id,
				'title' => $title,
				'sequence' => $sequence,
				'hide' => $status
				 
			);
			
			$this->administrator_model->update_menu($id, $data);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo json_encode($data);
	}
	
	function delete($id){
		$res = array(
					'result' => 'success',
					'msg' => '<h4>Data berhasil dihapus</h4>'
				);
		if($id!=null){
			if(!$this->administrator_model->delete_menu($id))
			{
				$res = array(
					'result' => 'error',
					'msg' => '<h4>Tidak berhasil menghapus data</h4>'
				);
			}
			else
			{
				$res = array(
					'result' => 'success',
					'msg' => '<h4>Data berhasil dihapus</h4>'
				);
			}
		}
	}

	  
}

?>