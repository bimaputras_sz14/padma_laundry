<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administrator extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('grocery_CRUD');
		
		$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
		$level = $arr_usr['lvl_id'];
		
		if($level != 1){
			$this->hak_akses();
		}
	}
	
	
    function hak_akses()
    {
		$this->data['title'] = "ERROR";
		$this->data['body'] = "error/hak_akses";
		$this->data['styles'] = "includes/styles_dashboard";
		$this->data['scripts'] = "includes/scripts_dashboard";
		$this->data['page_bar'] = "includes/template_site_bar_blank";
		$this->data['desc'] = " user tidak mempunyai otorisasi";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->data['err_msg'] = "anda harus login sebagai admin untuk mengakses halaman ini";
		
		$this->load->view('includes/template_site', $this->data);
	}

 

	function user_web()
	{
		$this->data['title'] = "User WEB";
		$this->data['body'] = "administrator/administrator_view";
		$this->data['styles'] = "administrator/styles_admin";
		$this->data['scripts'] = "administrator/scripts_admin";
		$this->data['page_bar'] = "includes/template_site_bar_blank";
		$this->data['desc'] = "user aplikasi web";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['message'] = $this->session->flashdata('message');
		
		$crud = new grocery_CRUD();
		
		$crud->set_table('user_accounts');
		$crud->set_subject('User WEB');
		$crud->unset_columns('email','password','ip_address','salt');
		$crud->unset_edit_fields('password','ip_address','salt');
		$crud->unset_add_fields('password','ip_address','salt','active','suspend','fail_login_attempts','fail_login_ip_address','date_fail_login_ban','date_last_login','date_added');
		//$crud->unset_add();
		$crud->set_relation('lvl_id','user_level','nama');
		$crud->display_as('lvl_id','Level');
		// $crud->set_relation('nrp','apk_users','nama');
		// $crud->display_as('nrp','Nama Anggota');
		$crud->add_action('Reset Password', '', 'administrator/reset_password', 'fa fa-rotate-left fa-lg');
		
		$this->data['output'] = $crud->render();
		
		$this->load->view('includes/template_site', $this->data);
	}
	
	function data_kelurahan()
	{
		$this->data['title'] = "Kelurahan";
		$this->data['body'] = "administrator/kewilayahan";
		$this->data['styles'] = "administrator/styles_admin";
		$this->data['scripts'] = "administrator/scripts_admin";
		$this->data['page_bar'] = "includes/template_site_bar_blank";
		$this->data['desc'] = "daftar list kelurahan";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['message'] = $this->session->flashdata('message');
		
		$crud = new grocery_CRUD();
		
		$crud->set_table('all_kelurahan');
		$crud->set_subject('Kelurahan');
		// $crud->unset_columns('id_kec','id_kel','nama_kel');
		// $crud->unset_edit_fields('id_kec','id_kel','nama_kel');
		// $crud->unset_add_fields('id_kec','id_kel','nama_kel');
		//$crud->unset_add();
		$crud->set_relation('id_kec','all_kecamatan','nama_kec');
		$crud->display_as('id_kec','Nama Kec');
		// $crud->set_relation('id_kabkot','all_kabkot','nama_kabkot');
		// $crud->display_as('nrp','Nama Anggota');
		// $crud->add_action('Reset Password', '', 'administrator/reset_password', 'fa fa-rotate-left fa-lg');
		
		$this->data['output'] = $crud->render();
		
		$this->load->view('includes/template_site', $this->data);
	}
	
	function data_kecamatan()
	{
		$this->data['title'] = "Kecamatan";
		$this->data['body'] = "administrator/kewilayahan";
		$this->data['styles'] = "administrator/styles_admin";
		$this->data['scripts'] = "administrator/scripts_admin";
		$this->data['page_bar'] = "includes/template_site_bar_blank";
		$this->data['desc'] = "daftar list kelurahan";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['message'] = $this->session->flashdata('message');
		
		$crud = new grocery_CRUD();
		
		$crud->set_table('all_kecamatan');
		$crud->set_subject('Kecamatan');
		// $crud->unset_columns('id_kec','id_kel','nama_kel');
		// $crud->unset_edit_fields('id_kec','id_kel','nama_kel');
		// $crud->unset_add_fields('id_kec','id_kel','nama_kel');
		//$crud->unset_add();
		$crud->set_relation('id_kabkot','all_kabkot','nama_kabkot');
		$crud->display_as('id_kabkot','Nama Kabupaten');
		// $crud->set_relation('nrp','apk_users','nama');
		// $crud->display_as('nrp','Nama Anggota');
		// $crud->add_action('Reset Password', '', 'administrator/reset_password', 'fa fa-rotate-left fa-lg');
		
		$this->data['output'] = $crud->render();
		
		$this->load->view('includes/template_site', $this->data);
	}
	
	function data_kabupaten()
	{
		$this->data['title'] = "Kabupaten";
		$this->data['body'] = "administrator/kewilayahan";
		$this->data['styles'] = "administrator/styles_admin";
		$this->data['scripts'] = "administrator/scripts_admin";
		$this->data['page_bar'] = "includes/template_site_bar_blank";
		$this->data['desc'] = "daftar list kelurahan";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['message'] = $this->session->flashdata('message');
		
		$crud = new grocery_CRUD();
		
		$crud->set_table('all_kabkot');
		$crud->set_subject('Kabupaten');
		// $crud->unset_columns('id_kec','id_kel','nama_kel');
		// $crud->unset_edit_fields('id_kec','id_kel','nama_kel');
		// $crud->unset_add_fields('id_kec','id_kel','nama_kel');
		//$crud->unset_add();
		$crud->set_relation('id_prov','all_provinsi','nama_prov');
		$crud->display_as('id_prov','Nama Provinsi');
		// $crud->set_relation('nrp','apk_users','nama');
		// $crud->display_as('nrp','Nama Anggota');
		// $crud->add_action('Reset Password', '', 'administrator/reset_password', 'fa fa-rotate-left fa-lg');
		
		$this->data['output'] = $crud->render();
		
		$this->load->view('includes/template_site', $this->data);
	}

	function user_apk()
	{
		$this->data['title'] = "User Android";
		$this->data['body'] = "administrator/administrator_view";
		$this->data['styles'] = "administrator/styles_admin";
		$this->data['scripts'] = "administrator/scripts_admin";
		$this->data['page_bar'] = "includes/template_site_bar_blank";
		$this->data['desc'] = "data anggota pengguna sispitibmas";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['message'] = $this->session->flashdata('message');
		
		$crud = new grocery_CRUD();
		
		$crud->set_table('apk_users');
		$crud->set_subject('User Android');
		$crud->unset_columns('password','gcm');
		$crud->unset_edit_fields('password','gcm');
		$crud->unset_add_fields('password','gcm');
		
		$this->data['output'] = $crud->render();
		
		$this->load->view('includes/template_site', $this->data);
	}
	
	function user_device()
	{
		$this->data['title'] = "Device User Sispitibmas";
		$this->data['body'] = "administrator/administrator_view";
		$this->data['styles'] = "administrator/styles_admin";
		$this->data['scripts'] = "administrator/scripts_admin";
		$this->data['page_bar'] = "includes/template_site_bar_blank";
		$this->data['desc'] = "data persebaran handphone";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['message'] = $this->session->flashdata('message');
		
		$crud = new grocery_CRUD();

		$crud->set_table('apk_device_list');
		$crud->set_subject('Device List');

		$this->data['output'] = $crud->render();
		
		$this->load->view('includes/template_site', $this->data);
	}
	
	function secura_device()
	{
		$this->data['title'] = "Device User Secura";
		$this->data['body'] = "administrator/administrator_view";
		$this->data['styles'] = "administrator/styles_admin";
		$this->data['scripts'] = "administrator/scripts_admin";
		$this->data['page_bar'] = "includes/template_site_bar_blank";
		$this->data['desc'] = "data secura device";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['message'] = $this->session->flashdata('message');
		
		$crud = new grocery_CRUD();

		$crud->set_table('secura_device');
		$crud->set_subject('Device List');
		$crud->unset_edit_fields('id_owner','lat', 'lon', 'timestamp');
		$crud->set_relation('id_owner','secura_owner','nama');
		$crud->display_as('id_owner','Owner');
		
		$this->data['output'] = $crud->render();
		
		$this->load->view('includes/template_site', $this->data);
	}
	

	/**
	 * Reset password for user_web
	 */
	public function reset_password($user_id)
	{
		$this->data['title'] = "User WEB";
		$this->data['body'] = "administrator/reset_password";
		$this->data['styles'] = "includes/styles_dashboard";
		$this->data['scripts'] = "administrator/scripts_reset";
		$this->data['page_bar'] = "includes/template_site_bar_blank";
		$this->data['desc'] = " reset password";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['message'] = $this->session->flashdata('message');
		
		$this->load->model('admin_model');
		$result = $this->admin_model->get_user_web($user_id);
		$this->data['user'] =  $result->row();
		
		$this->load->view('includes/template_site', $this->data);
	}

	
	function update_password(){
		// var_dump($id."-".$password);die();
		$this->load->model('admin_model');
		
		$id = $this->input->post('id');
		$password = $this->input->post('pass');
		//$id = "2";
		//$password = "merdeka";
		$store_database_salt = $this->auth->auth_security['store_database_salt'];
	    $database_salt = $store_database_salt ? $this->flexi_auth_model->generate_token($this->auth->auth_security['database_salt_length']) : FALSE;
		$hash_password = $this->flexi_auth_model->generate_hash_token($password, $database_salt, TRUE);

		$data = array(
			'password' => $hash_password,
			'salt' => $database_salt
		);
		
		if($this->admin_model->update_user_web($data, $id)){
			echo "success";
		}
		//echo "id: ".$id."; pass: ".$password."; enc: ".$hash_password;
		
    }
}

/* End of file auth_admin.php */
/* Location: ./application/controllers/auth_admin.php */