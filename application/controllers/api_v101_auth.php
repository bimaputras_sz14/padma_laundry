<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_v101_auth extends CI_Controller {
 
	function __construct()
	{
		parent::__construct();
		//
		$this->load->model('Api_v101_profile_m');
		$this->load->model('Api_v101_svc_m');
		// $this->load->model('Api_send_email');
		// $this->load->helper('mobile_svc');
		$this->load->helper('words');
		// $this->load->helper('image');
		
	}

	function index()
	{
	
	}
	
	function check_access(){
		$lat = trim($this->input->post('lat')) == '' ? null : trim($this->input->post('lat'));
		$lon = trim($this->input->post('lon')) == '' ? null : trim($this->input->post('lon'));
		$acc = trim($this->input->post('acc')) == '' ? null : trim($this->input->post('acc'));
		$appver = trim($this->input->post('appver')) == '' ? null : trim($this->input->post('appver'));
		$appbuild = trim($this->input->post('appbuild')) == '' ? null : trim($this->input->post('appbuild'));
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$password = trim($this->input->post('password')) == '' ? null : trim($this->input->post('password'));
		
		// $email = 'bandung.wijaya@gmail.com';
		// $kode_akses = '51bffa4633f908acf337de0c6f28ae3f';
		
		if($email == null || $password == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akses tidak dikenali');
		}
		else
		{
			if(!$data = $this->Api_v101_profile_m->get_profile($email, $password))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else if(($appbuild < $this->config->item('app_build')) && ($appbuild != '0') && ($appbuild != '') && ($appbuild != null))
			{
				$res = array('result' => 'error', 'msg' => 'Versi aplikasi anda telah kadaluarsa. Silahkan perbarui aplikasi e-Api anda dengan versi yang terbaru di Playstore.', 'expired' => 'true', 'url' => $this->config->item('app_uri'));
			}
			else 
			{
				//
				switch($data->status)
				{
					case 1:
					
						// update position
						// if($lat != null && $lon != null)
						// {
						// 	$activity = 'INIT APP';
						// 	$note = '<b>'.$data->nama.'</b> membuka aplikasi';
							
						// 	$pos = array(
						// 		'email' => $data->email,
						// 		'kode_wilayah' => $data->kode_wilayah,
						// 		'waktu' => date('Y-m-d H:i:s'),
						// 		'lat' => $lat,
						// 		'lng' => $lon,
						// 		'acc' => $acc,
						// 		'aktivitas' => $activity,
						// 		'pesan' => $note,
						// 		'timestamp' => date('Y-m-d H:i:s')
						// 	);
						// 	$this->Api_v101_svc_m->record_position($pos,$data->email);
						// }
						
						// if($data->ttd == null || $data->ttd == ''){
						// 	$ttd_prompt = 1;
						// }else{
						// 	$ttd_prompt = 0;
						// }
						
						// $ava = $this->Api_v101_svc_m->get_my_avatar($data->email, true);
						// $res = array('result' => 'success', 'msg' => 'Akun dikenali', 'user' => $data, 'ava' => $ava, 'ttd_prompt' => $ttd_prompt);
						$res = array('result' => 'success', 'msg' => 'Akun dikenali', 'user' => $data );
					
					break;
					case 2:
						$res = array('result' => 'error', 'msg' => 'Akun Anda diblokir', 'showmsg' => 'true');
					break;
					case 3:
						$res = array('result' => 'error', 'msg' => 'Akun anda telah dihapus', 'showmsg' => 'true');
					break;
					case 0:
						$res = array('result' => 'error', 'msg' => 'Akun Anda belum di Aktivasi', 'showmsg' => 'true');
					break;
					default:
						$res = array('result' => 'error', 'msg' => 'Akun anda tidak dapat digunakan', 'showmsg' => 'true');
					break;
				}
				
				
			}
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	
	function do_login(){
		$lat = trim($this->input->post('lat')) == '' ? null : trim($this->input->post('lat'));
		$fcm = trim($this->input->post('fcm')) == '' ? null : trim($this->input->post('fcm'));
		$lon = trim($this->input->post('lon')) == '' ? null : trim($this->input->post('lon'));
		$acc = trim($this->input->post('acc')) == '' ? null : trim($this->input->post('acc'));
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$password = trim($this->input->post('password')) == '' ? null : trim($this->input->post('password'));
		
		// $email = 'bandung.wijaya@gmail.com';
		// $password = 'abcd1234';
		
		if($email == null || $password == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akses tidak dikenali', 'showmsg' => 'true');
		}
		else
		{
			if(!$data = $this->Api_v101_svc_m->do_login($email))
			{
				$res = array('result' => 'error', 'msg' => 'Email anda tidak dikenali.', 'showmsg' => 'true');
			}
			else if($data->password != md5($password))
			{
				$res = array('result' => 'error', 'msg' => 'Password anda tidak tepat.', 'showmsg' => 'true');
			}
			else if($data->ganti_password == '0')
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda masih menggunakan password default dari sistem. Silahkan ubah dengan password anda yang baru.', 'setnewpassword' => 'true');
			}
			else 
			{
				switch($data->status)
				{
					case 1:
					
						$kode_akses = md5($email.$password.uniqid());
						$upd = array(
							'kode_akses' => $kode_akses,
							'fcm' => $fcm,
						);
						
						$this->Api_v101_profile_m->update_profile($data->email,$upd);
						
						// update position
						// if($lat != null && $lon != null)
						// {
						// 	$activity = 'LOGIN';
						// 	$note = '<b>'.$data->nama.'</b> login';
							
						// 	$pos = array(
						// 		'email' => $data->email,
						// 		'kode_wilayah' => $data->kode_wilayah,
						// 		'waktu' => date('Y-m-d H:i:s'),
						// 		'lat' => $lat,
						// 		'lng' => $lon,
						// 		'fcm' => $fcm,
						// 		'acc' => $acc,
						// 		'aktivitas' => $activity,
						// 		'pesan' => $note,
						// 		'timestamp' => date('Y-m-d H:i:s')
						// 	);
						// 	$this->Api_v101_svc_m->record_position($pos,$data->email);
						// }
						
						 
						
						$data->kode_akses = $kode_akses;
						// $ava = $this->Api_v101_svc_m->get_my_avatar($data->email, true);
						// $res = array('result' => 'success', 'msg' => 'Akun dikenali', 'user' => $data, 'ava' => $ava, 'ttd_prompt' => $ttd_prompt);
						$res = array('result' => 'success', 'msg' => 'Akun dikenali', 'user' => $data );
					
					break;
					case 2:
						$res = array('result' => 'error', 'msg' => 'Akun Anda diblokir', 'showmsg' => 'true');
					break;
					case 3:
						$res = array('result' => 'error', 'msg' => 'Akun anda telah dihapus', 'showmsg' => 'true');
					break;
					case 0:
						$res = array('result' => 'error', 'msg' => 'Akun Anda belum di Aktivasi', 'showmsg' => 'true');
					break;
					default:
						$res = array('result' => 'error', 'msg' => 'Akun anda tidak dapat digunakan', 'showmsg' => 'true');
					break;
				}
				
				
			}
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	function do_register(){
		$lat = trim($this->input->post('lat')) == '' ? null : trim($this->input->post('lat'));
		$lon = trim($this->input->post('lon')) == '' ? null : trim($this->input->post('lon'));
		$acc = trim($this->input->post('acc')) == '' ? null : trim($this->input->post('acc'));
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$password = trim($this->input->post('password')) == '' ? null : trim($this->input->post('password'));
		
		// $email = 'bandung.wijaya@gmail.com';
		// $password = 'abcd1234';
		
		if($email == null || $password == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akses tidak dikenali', 'showmsg' => 'true');
		}
		else
		{
			 $pos = array(
						'email' => $data->email,
						'kode_wilayah' => $data->kode_wilayah,
						'waktu' => date('Y-m-d H:i:s'),
						'lat' => $lat,
						'lng' => $lon,
						'acc' => $acc,
						'aktivitas' => $activity,
						'pesan' => $note,
						'timestamp' => date('Y-m-d H:i:s')
					);
					$this->Api_v101_svc_m->save_register($pos,$data->email);
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	
	function do_logout(){
		$lat = trim($this->input->post('lat')) == '' ? null : trim($this->input->post('lat'));
		$lon = trim($this->input->post('lon')) == '' ? null : trim($this->input->post('lon'));
		$acc = trim($this->input->post('acc')) == '' ? null : trim($this->input->post('acc'));
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$kode_akses = trim($this->input->post('kode_akses')) == '' ? null : trim($this->input->post('kode_akses'));
		
		if($email == null || $kode_akses == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akses tidak dikenali', 'showmsg' => 'true');
		}
		else
		{
			if(!$data = $this->Api_v101_profile_m->get_profile($email, $kode_akses))
			{
				$res = array('result' => 'error', 'msg' => 'Akun Anda tidak dalam kondisi aktif!', 'forcelogout' => 'true');
			}
			else 
			{
				switch($data->status)
				{
					case 1:
					
						$upd = array(
							'kode_akses' => null
						);
						
						$this->Api_v101_profile_m->update_profile($data->email,$upd);
						
						// update position
						if($lat != null && $lon != null)
						{
							$activity = 'LOGOUT';
							$note = '<b>'.$data->nama.'</b> logout';
							
							$pos = array(
								'email' => $data->email,
								'kode_wilayah' => $data->kode_wilayah,
								'waktu' => date('Y-m-d H:i:s'),
								'lat' => $lat,
								'lng' => $lon,
								'acc' => $acc,
								'aktivitas' => $activity,
								'pesan' => $note,
								'timestamp' => date('Y-m-d H:i:s')
							);
							$this->Api_v101_svc_m->record_position($pos,$data->email);
						}
						
						$res = array('result' => 'success', 'msg' => 'Berhasil');
					
					break;
					case 2:
						$res = array('result' => 'error', 'msg' => 'Akun Anda diblokir', 'showmsg' => 'true');
					break;
					case 3:
						$res = array('result' => 'error', 'msg' => 'Akun anda telah dihapus', 'showmsg' => 'true');
					break;
					case 0:
						$res = array('result' => 'error', 'msg' => 'Akun Anda belum di Aktivasi', 'showmsg' => 'true');
					break;
					default:
						$res = array('result' => 'error', 'msg' => 'Akun anda tidak dapat digunakan', 'showmsg' => 'true');
					break;
				}
				
				
			}
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	
	function set_new_password(){
		$lat = trim($this->input->post('lat')) == '' ? null : trim($this->input->post('lat'));
		$lon = trim($this->input->post('lon')) == '' ? null : trim($this->input->post('lon'));
		$acc = trim($this->input->post('acc')) == '' ? null : trim($this->input->post('acc'));
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
		$password = trim($this->input->post('password')) == '' ? null : trim($this->input->post('password'));
		$passwordbaru = trim($this->input->post('passwordbaru')) == '' ? null : trim($this->input->post('passwordbaru'));
		
		// $email = 'bandung.wijaya@gmail.com';
		// $password = 'abcd1234';
		
		if($email == null || $password == null)
		{
			$res = array('result' => 'error', 'msg' => 'Akses tidak dikenali', 'showmsg' => 'true');
		}
		else
		{
			if(!$data = $this->Api_v101_svc_m->do_login($email))
			{
				$res = array('result' => 'error', 'msg' => 'Email anda tidak dikenali.', 'showmsg' => 'true');
			}
			else if($data->password != md5($password))
			{
				$res = array('result' => 'error', 'msg' => 'Password anda tidak tepat.', 'showmsg' => 'true');
			}
			else if($password == $passwordbaru)
			{
				$res = array('result' => 'error', 'msg' => 'Password anda tidak boleh sama dengan password default.', 'showmsg' => 'true');
			}
			else 
			{
				switch($data->status)
				{
					case 1:
					
						$kode_akses = md5($email.$password.uniqid());
						$upd = array(
							'password' => md5($passwordbaru),
							'kode_akses' => $kode_akses,
							'ganti_password' => 1
						);
						
						$this->Api_v101_profile_m->update_profile($data->email,$upd);
						
						// update position
						if($lat != null && $lon != null)
						{
							$activity = 'SET NEW PASSWORD';
							$note = '<b>'.$data->nama.'</b> merubah password default';
							
							$pos = array(
								'email' => $data->email,
								'kode_wilayah' => $data->kode_wilayah,
								'waktu' => date('Y-m-d H:i:s'),
								'lat' => $lat,
								'lng' => $lon,
								'acc' => $acc,
								'aktivitas' => $activity,
								'pesan' => $note,
								'timestamp' => date('Y-m-d H:i:s')
							);
							$this->Api_v101_svc_m->record_position($pos,$data->email);
						}
						
						$data->kode_akses = $kode_akses;
						$ava = $this->Api_v101_svc_m->get_my_avatar($data->email, true);
						$res = array('result' => 'success', 'msg' => 'Akun dikenali', 'user' => $data, 'ava' => $ava);
					
					break;
					case 2:
						$res = array('result' => 'error', 'msg' => 'Akun Anda diblokir', 'showmsg' => 'true');
					break;
					case 3:
						$res = array('result' => 'error', 'msg' => 'Akun anda telah dihapus', 'showmsg' => 'true');
					break;
					case 0:
						$res = array('result' => 'error', 'msg' => 'Akun Anda belum di Aktivasi', 'showmsg' => 'true');
					break;
					default:
						$res = array('result' => 'error', 'msg' => 'Akun anda tidak dapat digunakan', 'showmsg' => 'true');
					break;
				}
				
				
			}
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	
	/* ============================ */
	
	
	function send_email_forgot_pass(){
		$lat = trim($this->input->post('lat')) == '' ? null : trim($this->input->post('lat'));
		$lon = trim($this->input->post('lon')) == '' ? null : trim($this->input->post('lon'));
		$acc = trim($this->input->post('acc')) == '' ? null : trim($this->input->post('acc'));
		$email = trim($this->input->post('email')) == '' ? null : trim($this->input->post('email'));
	
		// $email = 'bandung.wijaya@gmail.com';
		
		if($email == null)
		{
			$res = array('result' => 'error', 'msg' => 'Email harus diisi.');
		}
		else
		{
			if(!$data = $this->Api_v101_svc_m->do_login($email))
			{
				$res = array('result' => 'error', 'msg' => 'Email anda tidak dikenali.', 'showmsg' => 'true');
			}
			else 
			{
				switch($data->status)
				{
					case 1:
					
						$today = date('Y-m-d');
						$now = date('H:i:s');
						$newPass = generateRandomString();
						
						// update userdata
						$upd = array(
							'password' => md5($newPass),
							'kode_akses' => null,
							'ganti_password' => 0,
							'last_activity' => date('Y-m-d H:i:s'),
							'info' => 'FORGOT PASSWORD',
							'lat' => $lat,
							'lon' => $lon,
							'acc' => $acc
						);
						
						if($this->Api_v101_profile_m->update_profile($data->email,$upd))
						{
							$pesan = "Hai <b>".$data->nama."</b>,<br>
											Password anda telah berhasil diubah oleh sistem.<br>
											Silahkan login e-Api dengan akun sebagai berikut.<br>
											Email: <b>".$data->email."</b><br>
											Password: <b>".$newPass."</b><br>
											<br>
											<br>
											Terimakasih...<br>".convertTanggal($today).' '.$now;
											
							$subject = "Reset Password";
							
							if($this->Api_send_email->send_email_gmail($subject,$pesan,$email,'email1')){
								$res = array('result' => 'success', 'msg' => 'Email telah berhasil dikirim. Silahkan periksa inbox email anda.');
							}else{
								$res = array('result' => 'error', 'msg' => 'Email gagal dikirim. Silahkan coba kembali.');
							}
						}
						else
						{
							$res = array('result' => 'error', 'code' => 'not-success', 'msg' => 'Password anda gagal diupdate oleh sistem. Silahkan tutup e-Api kemudian coba lagi.');
						}
					
					break;
					case 2:
						$res = array('result' => 'error', 'msg' => 'Akun Anda diblokir', 'showmsg' => 'true');
					break;
					case 3:
						$res = array('result' => 'error', 'msg' => 'Akun anda telah dihapus', 'showmsg' => 'true');
					break;
					case 0:
						$res = array('result' => 'error', 'msg' => 'Akun Anda belum di Aktivasi', 'showmsg' => 'true');
					break;
					default:
						$res = array('result' => 'error', 'msg' => 'Akun anda tidak dapat digunakan', 'showmsg' => 'true');
					break;
				}
				
				
			}
			
		}
		//
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
		echo json_encode($res);
		
	}
	
	
}
