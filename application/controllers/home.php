<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
 
	function index()
    {
		$this->dashboard();
	}
  
    function dashboard()
    {
    	$this->load->model('master_model');
    	// pengaturan dashboard berdasarkan user login
		$this->data['title'] = "Dashboard";

		$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
		$level = $arr_usr['lvl_id'];
		$this->load->model("pelanggan_model");   
		$this->data['member'] 		= $this->pelanggan_model->find_pelanggan('')->result();	
		if($level == 1){
			$this->data['body'] = "dashboard_view";
		}else{
			$this->data['body'] = "dashboard_view_cashier";
		} 
		// $this->data['member_baru'] 	= $this->master_model->get_total_member();
		// $this->data['penjualan'] 	= $this->master_model->get_sales()->total_penjualan;
		// $this->data['member_visit'] = $this->master_model->get_visit()->total_visit; 
		// $this->data['visit_graph'] = $this->master_model->get_visit_graph() ; 
		// $this->data['sales_graph'] = $this->master_model->get_sales_graph() ; 
		$this->data['styles'] = "includes/styles_dashboard";
		$this->data['scripts'] = "includes/scripts_dashboard";
		$this->data['page_bar'] = "includes/template_site_bar";
		$this->data['desc'] = "reports & statistics";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->view('includes/template_site', $this->data);
		
	}
}

/* End of file auth_admin.php */
/* Location: ./application/controllers/auth_admin.php */