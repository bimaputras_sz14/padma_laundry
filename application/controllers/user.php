<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller {
 
	function index()
    {
		$this->profile();
	}
 
    function profile()
    {
		$this->data['title'] = "User Profile";
		$this->data['body'] = "user/profile";
		$this->data['styles'] = "includes/styles_dashboard";
		$this->data['scripts'] = "user/scripts_user";
		$this->data['page_bar'] = "includes/template_site_bar";
		$this->data['desc'] = "";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['message'] = $this->session->flashdata('message');
		
		$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
		$nik = $arr_usr['nik'];
		// var_dump($arr_usr);die();
		
		$sql = "SELECT a.*, j.jabatan,b.id as id_user FROM  personil a, jabatan j, user_accounts b WHERE a.nik='$nik' AND a.jabatan_id=j.id AND a.nik=b.nik";
					
		$query = $this->db->query($sql);
		$this->data['user'] = $query->row();
		
		$this->load->view('includes/template_site', $this->data);
	}
	
	//-------------------------------------------------------------------------- CEK SESSION -------------------------------------------------------------//
	function cek_session_login(){
		if (! $this->flexi_auth->is_logged_in_via_password()) 
		{
			echo "false";
		}else{
			echo "true";
		}
	}
	
	function management_user(){
		$this->load->model('m_master');
		$this->data['title'] = "User Management";
		$this->data['body'] = "user/list_user";
		$this->data['styles'] = "includes/styles_data_table";
		$this->data['scripts'] = "user/scripts_user";
		$this->data['page_bar'] = "includes/template_site_bar";
		$this->data['desc'] = "daftar user ems";
		$this->data['mMenuUtama'] = $this->mMenuUtama;
		$this->data['personil'] = $this->m_master->get_query('Select a.*, b.profesi from ems_personil a, ems_profesi b where a.id_profesi=b.id_profesi and NOT EXISTS (select c.nrp from ems_user_accounts c where c.nrp=a.nrp)');
		$this->data['level']=$this->m_master->get_query('Select *from ems_profesi');
		$this->data['message'] = $this->session->flashdata('message');
		$this->load->view('includes/template_site', $this->data);
	}
	
	function data_user(){
		$this->load->model('m_master');
		$json=array(); 
		$data=$this->m_master->get_data_user();
		if ($data->num_rows() > 0){
			$i = 1;
				foreach ($data->result() as $row){
					$btn='<button id="edit" title="Edit Data" type="button" onclick="edit_profile(\''.$row->nrp.'\')" class="btn btn-sm blue"><i class="icon-note"></i></button>';
					
					
					$json['data'][] = array(
												"no"=>$i,
												"nrp"=>$row->nrp,
												"nama"=>$row->nama,
												"profesi"=>$row->profesi,
												"email"=>$row->email,
												"btn"=>$btn
											);
					$i++;
				}
		}
		echo json_encode($json);
	}
	
	function insert_user(){
		$this->load->model('m_master');
		$this->m_master->insert_user();
	}
	
	function edit_user(){
		$this->load->model('m_master');
		$data['edit']=$this->m_master->get_query("Select *from ems_user_apk");
		$this->load->view('fitur/detail-request',$data);
	}
	function update_password(){
		$this->load->model('admin_model');
		
		$id = $this->input->post('id');
		$password = $this->input->post('pass');
		$store_database_salt = $this->auth->auth_security['store_database_salt'];
	    $database_salt = $store_database_salt ? $this->flexi_auth_model->generate_token($this->auth->auth_security['database_salt_length']) : FALSE;
		$hash_password = $this->flexi_auth_model->generate_hash_token($password, $database_salt, TRUE);

		$data = array(
			'password' => $hash_password,
			'salt' => $database_salt
		);
		if($this->admin_model->update_user_web($data, $id)){
			echo "success";
		}
    }
}

/* End of file auth_admin.php */
/* Location: ./application/controllers/auth_admin.php */