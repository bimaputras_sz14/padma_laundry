<?php

/**
 * Base Controller with functions for CRUD operations
 */
class MY_Controller extends CI_Controller
{
	/**
	 * Constructor with common logic for pages that required login
	 */
	public function __construct()
	{
		parent::__construct();
 		date_default_timezone_set('Asia/Jakarta');
		// To load the CI benchmark and memory usage profiler - set 1==1.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load required CI libraries and helpers.
		$this->load->database();
		$this->load->library('session');
 		$this->load->helper('url');
 		$this->load->helper('form');
		$this->lang->load('form_validation');
  		
		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;

		// Load 'standard' flexi auth library by default.
		$this->load->library('flexi_auth');	

		// Check user is logged in as an admin.
		// For security, admin users should always sign in via Password rather than 'Remember me'.
		//if (! $this->flexi_auth->is_logged_in_via_password() || ! $this->flexi_auth->is_admin()) 
		if (! $this->flexi_auth->is_logged_in_via_password()) 
		{
			// Set a custom error message.
			$this->flexi_auth->set_error_message('You must login as an admin to access this area.', TRUE);
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			redirect('otorisasi');
		}
		
		// var_dump($this->flexi_auth->get_user_group_id());die();
		// Note: This is only included to create base urls for purposes of this demo only and are not necessarily considered as 'Best practice'.
		$this->load->vars('base_url', $this->config->item('main_page'));
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));
		
		// side menu items
		$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
		$this->config->load('menu_admin');
		$this->mMenuUtama = $this->config->item('menu_utama');
		
		// Define a global variable to store data that is then used by the end view page.
		$this->data = null;
		$this->data['parent'] = null;
	}

	public function get_user_session(){
		$arr_usr = $this->flexi_auth->get_user_by_identity_row_array();
		return $arr_usr;
	}

	public function get_division($user){
		$this->db->select('a.*,b.kode as kode_div'); 
		$this->db->join('group b','b.kode=a.group');
		$this->db->join('divisi c','c.kode=a.divisi');
		$this->db->join('jabatan d','d.id=a.jabatan_id');
		$this->db->where('nik',$user);
		$q = $this->db->from('personil a')->get()	;
		if($q->num_rows()>0){
			return $q->row();
		}else{
			return false;
		}
	}

}